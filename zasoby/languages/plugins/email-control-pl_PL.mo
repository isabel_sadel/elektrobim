��    N      �  k   �      �  w   �  
   !     ,     1     A     Q  !   a     �     �     �     �     �     �     �     �  C   Q  9   �  x   �  +   H	     t	  	   �	     �	     �	     �	     �	     �	     �	  	   �	     �	     �	     �	     
  9   
     U
     d
     s
     �
     �
     �
  J   �
          	               .     <     J     S     Z     _     p  �   �  �   {     j     r     w  �   �  �   L  l     I   u  *   �      �  d     V   p     �     �  x   �  9   t  C   �  /   �  "   "  o   E     �     �     �  x     B   �  �  �  Y   Z     �     �     �     �     �  $         %     2     ?     F     [     b  
   q  �   |  L   "  @   o  �   �  1   B  "   t     �     �     �     �     �     �     �     �               -     M  C   `     �     �     �     �     �       O   %     u     z     �     �     �  
   �     �     �     �     �     �  �     �   �     �     �      �  �     �   �  L   �  K   �  (   (   0   Q   ^   �   S   �      5!     L!  �   k!  G   �!  G   @"  <   �"  .   �"  �   �"  %   #  +   �#     �#  �   �#  E   �$     M       +          -      D      K   F   !   C      ,         2   
                 8       H           L       (       5       )           ;   <                                I          7   &   =                 4          J       9   1         .   %   A   @   >       $      G      E      0      3   B       '   ?   *   /   N                 :   	                #                 "       6    An order has been created for you on [ec_site_link]. To pay for this order please use the following link: [ec_pay_link] Appearance Back Billing Address Billing address Cancelled order Click here to reset your password Customer Details Customer details Edit Failed order Footer Footer Text Header Hello, a note has just been added to your order:

[ec_customer_note]

For your reference, your order details are shown below.

 Hi there. Your order on [ec_site_name] has been partially refunded. Hi there. Your order on [ec_site_name] has been refunded. Hi there. Your recent order on [ec_site_name] has been completed. Your order details are shown below for your reference: Invoice for order #[ec_order show="number"] Invoice for order [ec_order] Main Text Never New New customer order New order received! Note Note: Order #%s Order Date: Order Details Order Items Table Order Number: Order [ec_order show='#,number' hide='container'] details Partial Refund Password Reset Password Reset Instructions Pay now Payment Complete Payment Pending Payment for order [ec_order] from [ec_firstname] [ec_lastname] has failed. Price Product Product Image Product Images Product image Purchase Code Quantity Refund Save Shipping Address Shipping address Someone requested that the password be reset for the following account:

Username: [ec_user_login]

If this was a mistake, just ignore this email and nothing will happen.

To reset your password, visit the following address:
[ec_reset_password_link] Someone requested that the password be reset for the following account:
[ec_user_login]

If this was a mistake, just ignore this email and nothing will happen.

To reset your password, visit the following address:
[ec_reset_password_link] Subject Text Thank you for your order Thanks for creating an account on [ec_site_name].
Your username is: [ec_user_login].

You can access your account area to view your orders and change your password here: [ec_account_link] Thanks for creating an account on [ec_site_name]. Your username is [ec_user_login].

You can access your account area to view your orders and change your password here: [ec_account_link]. Thanks for your order on [ec_site_link].

To pay for this order please use the following link: [ec_pay_link] The order [ec_order] for [ec_firstname] [ec_lastname] has been cancelled. Welcome to [ec_site_name hide='container'] You have been partially refunded You have received an order from [ec_firstname] [ec_lastname].

Their order is as follows: [ec_order] You have received an order from [ec_firstname] [ec_lastname]. The order is as follows: You must be logged in Your account has been created Your order [ec_order] at [ec_site_name] has been completed.

We're just letting you know. No further action is required. Your order [ec_order] has been partially refunded. Thanks Your order [ec_order] has been received and is now being processed. Your order [ec_order] has been refunded. Thanks Your order has been fully refunded Your order has been received and is now being processed. Your order details are shown below for your reference: Your order has been refunded Your order is being processed Your order is complete Your order is on-hold until we confirm payment has been received. Your order details are shown below for your reference: Your password has been automatically generated: [ec_user_password] Project-Id-Version: email-control
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Language-Team: January 15th Ltd <adam@january15th.com>
X-Poedit-Basepath: ..
X-Poedit-KeywordsList: __;_e;_ex:1,2c;_n:1,2;_n_noop:1,2;_nx:1,2,4c;_nx_noop:1,2,3c;_x:1,2c;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c
X-Poedit-SourceCharset: UTF-8
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
POT-Creation-Date: 
PO-Revision-Date: 
X-Generator: Poedit 1.8.7.1
Last-Translator: 
Language: pl_PL
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 Zostało utworzone zamówienie na [ec_site_link]. Zapłać klikając tutaj: [ec_pay_link] Wygląd Wróć Dane rozliczeniowe Dane rozliczeniowe Zamówienie anulowane. Kliknij tutaj aby zresetować hasło Dane klienta Dane klienta Edytuj Zamówienie nieudane Stopka Treść stopki Nagłówek Cześć, właśnie została dodana uwaga do twojego zamówienia:

[ec_customer_note]

Dla Twojej informacji, szczegóły Twojego zamówienia są pokazane poniżej.

 Cześć, Twoje zamówienie na [ec_site_name] zostało częściowo zwrócone. Cześć, twoje zamówienie na [ec_site_name] zostało zwrócone. Cześć, twoje zamówienie na [ec_site_name] zostało zakończone. Szczegóły twojego zamówienia są pokazane poniżej w celach informacyjnych: Faktura dla zamówienia #[ec_order show="number"] Faktura dla zamówienia [ec_order] Tekst główny Nigdy Nowy Nowe zamówienie klienta Otrzymano nowe zamówienie! Uwaga Uwaga: Zamówienie #%s Data zamówienia: Szczegóły zamówienia Tabela produktów z zamówienia Numer zamówienia: Zamówienie [ec_order show='#,number' hide='container'] szczegóły Częściowy zwrot Zresetuj hasło Instrukcje do odzyskania hasła Zapłać teraz Płatność zakończona Płatność oczekująca Płatność za zamówienie [ec_order] od [ec_firstname] [ec_lastname] nieudana. Cena Nazwa produktu Zdjęcie produktu Zdjęcia produktu Zdjęcie produktu Kod zakupu Ilość Zwrot Zapisz Adres do wysyłki Adres do wysyłki Wysłano prośbę o zmianę hasła dla Twojego konta w sklepie Elektrobim.pl:

Login: [ec_user_login]

Jeżeli to nie Ty, to po prostu zignorują tę wiadomość..

Aby uzyskać nowe hasło, kliknij w poniższy link::
[ec_reset_password_link] Wysłano prośbę o zmianę hasła dla Twojego konta w sklepie Elektrobim.pl:

Login: [ec_user_login]

Jeżeli to nie Ty, to po prostu zignorują tę wiadomość..

Aby uzyskać nowe hasło, kliknij w poniższy link::
[ec_reset_password_link] Temat Tekst Dziękujemy za twoje zamówienie Dziękujemy za utworzenie konta na [ec_site_name].
 Twoja nazwa użytkownika [ec_user_login].

Wejdź na swoje konto żeby zobaczyć swoje zamówienia i zmienić hasło: [ec_account_link]. Dziękujemy za utworzenie konta na [ec_site_name]. Twoja nazwa użytkownika [ec_user_login].

Wejdź na swoje konto żeby zobaczyć swoje zamówienia i zmienić hasło: [ec_account_link]. Dziękujemy za zamówienie na [ec_site_link].

Zapłać tutaj: [ec_pay_link] Zamówienie [ec_order] dla [ec_firstname] [ec_lastname] zostało anulowane. Witaj na [ec_site_name hide='container'] Twoje zamówienie zostało częściowo zwrócone Otrzymałeś zamówienie od [ec_firstname] [ec_lastname].

Szczegóły zamówienia: [ec_order] Otrzymałęś zamówienie od [ec_firstname] [ec_lastname]. Szczegóły zamówienia: Musisz być zalogowany Twoje konto zostało utworzone Twoje zamówienie [ec_order] na [ec_site_name] zostało zakończone.

Po prostu cię informujemy. Żadne dalsze działania nie są wymagane. Twoje zamówienie [ec_order] ostało częściowo zwrócone. Dziękujemy Otrzymaliśmy twoje zamówienie [ec_order] i jest w trakcie realizacji. Twoje zamówienie [ec_order] zostało zwrócone. Dziękujemy Twoje zamówienie zostało w pełni zwrócone. Otrzymaliśmy Twoje zamówienie i jest w trakcie realizacji. Szczegóły twojego zamówienia są pokazane poniżej w celach informacyjnych Twoje zamówienie zostało zwrócone. Twoje zamówienie jest w trakcie realizacji Zamówienie zakończone Twoje zamówienie jest wstrzymane do momentu potwierdzenia płatności. Szczegóły twojego zamówienia są pokazane poniżej w celach informacyjnych: Twoje hasło zostało wygenerowane automatycznie : [ec_user_password] 