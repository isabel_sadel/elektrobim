<?php
/*
	Plugin Name: WooCommerce Active Payments
	Plugin URI: https://www.wpdesk.net/products/active-payments-woocommerce/
	Description: Allows to hide certain payment methods for selected shipping methods. Works great with <a href="https://www.wpdesk.net/products/flexible-shipping-pro-woocommerce/" target="_blank">Flexible Shipping for WooCommerce</a>.
	Version: 3.3.5
	Author: WP Desk
	Author URI: https://www.wpdesk.net/
	Text Domain: woocommerce_activepayments
	Domain Path: /lang/
	Requires at least: 4.5
    Tested up to: 5.0.0
    WC requires at least: 3.1.0
    WC tested up to: 3.6.0
    Requires PHP: 5.6

	Copyright 2017 WP Desk Ltd.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	$wpdesk_helper_text_domain = 'woocommerce_activepayments';

	require_once( plugin_basename( 'inc/wpdesk-functions.php' ) );

	$plugin_data = array(
        'plugin' => plugin_basename( __FILE__ ),
        'product_id' => 'WooCommerce Active Payments',
        'version'   => '3.3.5',
        'config_uri' => admin_url( 'admin.php?page=woocommerce_activepayments' )
        );
	$helper = new WPDesk_Helper_Plugin( $plugin_data );

	if ( $helper->is_active() && in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
		require_once('class/activepayments_admin.php');

		class ActivePayments extends inspirePlugin {
			protected $pluginNamespace = 'woocommerce_activepayments';
			protected $activepaymentsAdmin;

			protected $order = false;
			protected $change = false;

			/** @var array|null */
			protected $settings;

			public function __construct() {
				$this->initBaseVariables();

				add_filter( 'woocommerce_available_payment_gateways', array( $this, 'filterActiveGateways' ), 100 );

				add_action( 'woocommerce_cart_calculate_fees', array( $this, 'woocommerce_cart_calculate_fees' ), 100 );

				add_action( 'wp_enqueue_scripts', array($this, 'initFrontJs'), 75 );

				// load locales
				load_plugin_textdomain('woocommerce_activepayments', FALSE, dirname(plugin_basename(__FILE__)) . '/lang/');

				if ( is_admin() ) {
					$this->activepaymentsAdmin = new ActivePaymentsAdmin($this->is_without_any_settings());
				}
			}

			public function initBaseVariables() {
				parent::initBaseVariables();
				$this->settings = get_option('woocommerce_activepayments_options', array() );
			}

			static function getOptionsFromGateway(WC_Shipping_Flat_Rate $gateway) {
			    $woocommerce = WooCommerce::instance();

			    if ( version_compare( $woocommerce->version, '2.4.0', '>=' ) ) {
			        return explode( "\n", $gateway->settings['options'] );
			    } else {
			        return $gateway->options;
			    }
			}

			static function generateFlatIdFromTitle( $title ) {
			    return ( sanitize_title( trim ( $title ) ) );
			}

			/**
			 * @param WC_Cart $cart
			 */
			public function woocommerce_cart_calculate_fees( $cart ) {
				global $woocommerce;
				$ap_options_fees = get_option('woocommerce_activepayments_options_fees', array() );

				$current_gateway = $woocommerce->session->chosen_payment_method;

				$avaliable_gateways = $woocommerce->payment_gateways()->get_available_payment_gateways();

				if ( !in_array( $current_gateway, array_keys( $avaliable_gateways ) ) ) {
                    $current_gateway = '';
                    foreach ( $avaliable_gateways as $gateway_key => $gateway_val ) {
                        $current_gateway = $gateway_key;
                        break;
                    }
                }
				if ( isset( $ap_options_fees[$current_gateway] )
                    && isset( $ap_options_fees[$current_gateway]['enabled'] )
                    && $ap_options_fees[$current_gateway]['enabled'] == '1'
                ) {
					$taxable = true;
                    $calc_taxes = get_option('woocommerce_calc_taxes') == 'yes' ? true : false;
                    if ( !$calc_taxes ) {
                        $taxable = false;
                    }
					$tax_class = '-none-';
                    if ( isset( $ap_options_fees[$current_gateway]['tax_class'] ) ) {
                        $tax_class = $ap_options_fees[$current_gateway]['tax_class'];
                    }
                    if ( $tax_class == '-none-' ) {
                        $taxable = false;
                    }

					$title = $ap_options_fees[$current_gateway]['title'];
					$amount = $ap_options_fees[$current_gateway]['amount'];
					$type = $ap_options_fees[$current_gateway]['type'];
                    $total_in_cart = $cart->cart_contents_total + $cart->shipping_total;
					$prices_include_tax = get_option('woocommerce_prices_include_tax') == 'yes' ? true : false;
					if ( $calc_taxes && $prices_include_tax ) {
						if ( version_compare( WC_VERSION, '3.2', '<' ) ) {
							$taxes = $cart->taxes;
						}
						else {
							$taxes = $cart->get_taxes();
						}
						foreach ( $taxes as $tax_value ) {
							$total_in_cart = $total_in_cart + $tax_value;
						}
						if ( version_compare( WC_VERSION, '3.2', '<' ) ) {
							$shipping_taxes = $cart->shipping_taxes;
						}
						else {
							$shipping_taxes = $cart->get_shipping_taxes();
						}
						foreach ( $shipping_taxes as $tax_value ) {
							$total_in_cart = $total_in_cart + $tax_value;
						}
					}
					$min_order_total = $ap_options_fees[$current_gateway]['min_order_total'];
					if ( $min_order_total != '' ) {
						$min_order_total = apply_filters( 'wpdesk_value_in_currency', floatval( $min_order_total ) );
					}
					else {
						$min_order_total = 0;
					}
					$max_order_total = $ap_options_fees[$current_gateway]['max_order_total'];
					if ( $max_order_total != '' ) {
						$max_order_total = apply_filters( 'wpdesk_value_in_currency', floatval( $max_order_total ) );
					}
					if ( $total_in_cart >= $min_order_total && ( $max_order_total == '' || $total_in_cart <= $max_order_total ) ) {
						if ( $amount != '' && $amount != '0' ) {
							$amount = floatval( $amount );
							if ( $type == 'fixed' ) {
								$fee = apply_filters( 'wpdesk_value_in_currency', $amount );
							}
							if ( $type == 'percent' ) {
								$precision = get_option( 'woocommerce_price_num_decimals', 2 );
								$fee = apply_filters( 'wpdesk_value_in_currency', round( $total_in_cart * $amount / 100, $precision ) );
							}
                            if ( $taxable ) {
                                $cart->add_fee( $title, $fee, $taxable, $tax_class );
                            }
                            else {
                                $cart->add_fee( $title, $fee, $taxable );
                            }
                            $woocommerce->session->set( 'active_payments_fee', sanitize_title( $title ) );
						}
					}
				}
			}

			/**
			 * Checks if plugin have any settings saved
			 *
			 * @return bool
			 */
			protected function is_without_any_settings() {
				return empty($this->settings);
			}

			public function filterActiveGateways( $gateways ) {
				if ( !is_checkout() || $this->is_without_any_settings() ) {
					return $gateways;
				}
				global $woocommerce;

				if ( version_compare( WC_VERSION, '3.2', '<' ) ) {
					if ( wc_prices_include_tax() ) {
						$sum = WC()->cart->subtotal;
					}
					else {
						$sum = WC()->cart->subtotal_ex_tax;
					}
				}
				else {
					$sum = WC()->cart->get_subtotal();
					if ( wc_prices_include_tax() ) {
						$sum += WC()->cart->get_subtotal_tax();
					}
				}

				if ( version_compare( $woocommerce->version, '2.1.0', '>=' ) ) {
					$shippingMethod = @reset($woocommerce->session->get('chosen_shipping_methods'));
				} else {
					$shippingMethod = $woocommerce->session->chosen_shipping_method;
				}

				if ( !empty($shippingMethod) ) {

					if ( version_compare($woocommerce->version, '2.6', '>=') ) {
						if ( strpos( $shippingMethod, 'flexible_shipping_ups' ) === 0 ) {
							$shippingMethod = substr( $shippingMethod, 0, strpos( $shippingMethod, ':', strlen( 'flexible_shipping_ups:' ) ) );
						}
						$ap_options = $this->settings;
						foreach ($gateways as $gateway_id => $gateway) {
							if ( $shippingMethod == 'apaczka' || $shippingMethod == 'apaczka_cod' ) {
								if ( !isset( $ap_options[$shippingMethod . ':0'][$gateway_id] ) || $ap_options[$shippingMethod . ':0'][$gateway_id] == 0 ) {
									unset( $gateways[$gateway_id] );
								}
							}
							else if ( !isset( $ap_options[$shippingMethod][$gateway_id] ) || $ap_options[$shippingMethod][$gateway_id] == 0 ) {
								if ( !isset( $ap_options[$shippingMethod . ':0'][$gateway_id] ) || $ap_options[$shippingMethod . ':0'][$gateway_id] == 0 ) {
									unset( $gateways[$gateway_id] );
								}
							}
							if ( isset( $ap_options[$gateway_id] ) && isset( $ap_options[$gateway_id]['amount'] ) && $ap_options[$gateway_id]['amount'] != '' && floatval( $ap_options[$gateway_id]['amount'] ) < $sum ) {
								unset( $gateways[$gateway_id] );
							}
						}
						return $gateways;
					}
					else {
						$newGateways = array();

						$flat = strpos($shippingMethod, 'flat_rate') !== false;
						$shippingFS = false;

						if ($flat) {
							$shippingMethod = explode(':', $shippingMethod);
							$shippingTable = false;
						} else {
							// shipping table integration
							$shippingTable = strpos($shippingMethod, 'table_rate_shipping') !== false;
							$shippingFS = strpos($shippingMethod, 'flexible_shipping') !== false;

						}

						foreach ($gateways as $gateway_id => $gateway) {
							if ( $flat ) {
								if ( isset($shippingMethod[1]) && $shippingMethod[1] != '' ) {
									$shippingMethod[1] = ActivePayments::generateFlatIdFromTitle($shippingMethod[1]);
									$md5 = $shippingMethod[0] . '_' . $gateway_id . '_' . $shippingMethod[1];
								}
								else {
									$md5 = str_replace('-', '_', $shippingMethod[0] ) . '_' . $gateway_id;
								}
							}
							elseif ($shippingTable) {
							    $md5 = 'table_rate_shipping_' . $gateway_id . '_' . str_replace(array('table-rate-shipping-', 'table_rate_shipping_'), '', $shippingMethod);
							}
							elseif ($shippingFS) {
							    $md5 = 'flexible_shipping_' . $gateway_id . '_' . $shippingMethod;
							}
							else {
							    $md5 = str_replace('-', '_', $shippingMethod) . '_' . $gateway_id;
							}

							//
							$amount = $this->getSettingValue('pm_' . md5($gateway_id . '_amount'));

							$setting = 'pm_' . md5($md5);
							$isOk = $this->getSettingValue( $setting );

							if ( !empty($isOk) ) {
	    						if( empty($amount) || (!empty($amount) && $sum < $amount) ){
								    $newGateways[$gateway_id] = $gateway;
								}
							}
						}

						return $newGateways;
					}
				} else {
					return $gateways;
				}

			}

			public function initFrontJs() {
				if ( is_checkout() ) {
					wp_enqueue_script( 'woocommerce-activepayments-front', $this->getPluginUrl() . '/assets/js/front.js', array('jquery'), '2.9', true );
				}
			}

			/**
			 * action_links function.
			 *
			 * @access public
			 * @param mixed $links
			 * @return void
			 */
			public function action_links( $links ) {
                $docs_link = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/docs/aktywne-platnosci-woocommerce-docs/' : 'https://www.wpdesk.net/docs/active-payments-woocommerce-docs/';
                $support_link = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/support/' : 'https://www.wpdesk.net/support';

				$plugin_links = array(
				    '<a href="' . admin_url( 'admin.php?page=woocommerce_activepayments' ) . '">' . __( 'Settings', 'woocommerce_activepayments' ) . '</a>',
				    '<a href="' . $docs_link . '">' . __( 'Docs', 'woocommerce_activepayments' ) . '</a>',
				    '<a href="' . $support_link . '">' . __( 'Support', 'woocommerce_activepayments' ) . '</a>',
				);

				return array_merge( $plugin_links, $links );
			}

		}

		$_GLOBALS['woocommerce_activepayments'] = $activepayments = new ActivePayments();
	}

	function wpdesk_activepayments_notice() {

		$active_plugins = apply_filters( 'active_plugins', get_option('active_plugins' ) );
		if ( in_array( 'flexible-shipping/flexible-shipping.php', $active_plugins ) ) return;

		if ( in_array( 'woo-flexible-shipping/flexible-shipping.php', $active_plugins ) ) {
			$message = sprintf( wp_kses( __( 'You have an old version of Flexible Shipping. <a href="%s">Please read about the changes</a> we made and upgrade to the new version. Thanks :)', 'woocommerce_activepayments' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( 'https://www.wpdesk.pl/blog/nowy-flexible-shipping/' ) );
			echo '<div class="error fade"><p>' . $message . '</p></div>' . "\n";
		}

		if ( is_plugin_active( 'wpdesk-helper/wpdesk-helper.php' ) ) {
			$plugin = get_plugin_data( WP_PLUGIN_DIR . '/wpdesk-helper/wpdesk-helper.php' );

			$version_compare = version_compare( $plugin['Version'], '1.3' );
			if ( $version_compare < 0 ) {
				$class   = 'notice notice-error';
				$message = __( 'WooCommerce Active Payments requires at least version 1.3 of WP Desk Helper plugin.', 'woocommerce_activepayments' );
				//$message = __( 'Wtyczka WooCommerce eNadawca wymaga wtyczki WP Desk Helper w wersji nie niższej niż 1.3.', 'woocommerce-enadawca' );
				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
			}
		}

	}

	add_action( 'admin_notices', 'wpdesk_activepayments_notice' );


	if ( !function_exists( 'wpdesk_value_in_currency_wpml' ) ) {
		function wpdesk_value_in_currency_wpml( $value ) {
			global $woocommerce_wpml;
			if ( isset( $woocommerce_wpml ) ) {
				$value = apply_filters( 'wcml_raw_price_amount', $value );
			}
			return $value;
		}
		add_filter( 'wpdesk_value_in_currency', 'wpdesk_value_in_currency_wpml', 1 );
	}

	if ( !function_exists( 'wpdesk_value_in_currency_aelia' ) ) {
		function wpdesk_value_in_currency_aelia( $value ) {
			if ( class_exists( 'WC_Aelia_CurrencySwitcher' ) ) {
				$aelia          = WC_Aelia_CurrencySwitcher::instance();
				$aelia_settings = WC_Aelia_CurrencySwitcher::settings();
				$from_currency  = $aelia_settings->base_currency();
				$to_currency    = $aelia->get_selected_currency();
				$value          = $aelia->convert( $value, $from_currency, $to_currency );
			}
			return $value;
		}
		add_filter( 'wpdesk_value_in_currency', 'wpdesk_value_in_currency_aelia', 1 );
	}

	if ( !function_exists( 'wpdesk_value_in_currency_wmcs' ) ) {
		function wpdesk_value_in_currency_wmcs( $value ) {
			if ( function_exists( 'wmcs_convert_price' ) ) {
				$value = wmcs_convert_price( $value );
			}
			return $value;
		}
		add_filter( 'wpdesk_value_in_currency', 'wpdesk_value_in_currency_wmcs', 1 );
	}

	if ( !function_exists( 'wpdesk_value_in_currency_woocs' ) ) {
		function wpdesk_value_in_currency_woocs( $value ) {
			if ( isset( $GLOBALS['WOOCS'] ) ) {
				$value =  $GLOBALS['WOOCS']->woocs_exchange_value( $value );
			}
			return $value;
		}
		add_filter( 'wpdesk_value_in_currency', 'wpdesk_value_in_currency_woocs', 1 );
	}

	require_once( 'class/tracker.php' );


