<div class="active-payments wrap">
    <h2 class="nav-tab-wrapper">
       <?php foreach ( $tabs as $key => $tab ): ?>
           <a class="nav-tab <?php if ( $current_tab === $key): ?>nav-tab-active<?php endif; ?>" href="<?php echo admin_url( $tab['page'] ); ?>"><?php echo $tab['title']; ?></a>
       <?php endforeach; ?>
    </h2>

    <h2><?php _e( 'Active Payments', 'woocommerce_activepayments' ); ?></h2>
