<td class="checkbox">
    <?php
    $params = array(
        'id' 		=> 'payment_fees[' . $method_id . '][enabled]',
        'type'  	=> 'checkbox',
        'label' 	=> false,
    );
    if ( !isset( $ap_options_fees[$method_id]['enabled'] ) ) {
        $ap_options_fees[$method_id]['enabled'] = '0';
    }
    woocommerce_form_field(
        $params['id'],
        $params,
        $ap_options_fees[$method_id]['enabled']
    );
    ?>
</td>
<td>
    <?php
    $params = array(
        'id' 		=> 'payment_fees[' . $method_id . '][title]',
        'type'  	=> 'text',
        'label' 	=> false,
    );
    if ( !isset( $ap_options_fees[$method_id]['title'] ) ) {
        $ap_options_fees[$method_id]['title'] = '';
    }
    woocommerce_form_field(
        $params['id'],
        $params,
        $ap_options_fees[$method_id]['title']
    );
    ?>
</td>
<?php $calc_taxes = get_option('woocommerce_calc_taxes') == 'yes' ? true : false; ?>
<?php if ( $calc_taxes ) : ?>
    <td>
        <?php
        $tax_options = array(
            ''  => __( 'Standard', 'woocommerce' )
        );

        $tax_classes = WC_Tax::get_tax_classes();

        if ( ! empty( $tax_classes ) )
            foreach ( $tax_classes as $class ) {
                $tax_options[ sanitize_title( $class ) ] = esc_html( $class );
            }

        $tax_options['-none-'] = __( 'None', 'woocommerce_activepayments' );


        $params = array(
            'id' 		=> 'payment_fees[' . $method_id . '][tax_class]',
            'type'  	=> 'select',
            'label' 	=> false,
            'options'	=> $tax_options
        );
        if ( !isset( $ap_options_fees[$method_id]['tax_class'] ) ) {
            $ap_options_fees[$method_id]['tax_class'] = '';
        }
        woocommerce_form_field(
            $params['id'],
            $params,
            $ap_options_fees[$method_id]['tax_class']
        );
        ?>
    </td>
<?php endif; ?>
<td>
    <?php
    $params = array(
        'id' 		=> 'payment_fees[' . $method_id . '][min_order_total]',
        'type'  	=> 'text',
        'label' 	=> false,
        'custom_attributes' => array(
            'to_number' => 1,
            'step'		=> 'any'
        )
    );
    if ( !isset( $ap_options_fees[$method_id]['min_order_total'] ) ) {
        $ap_options_fees[$method_id]['min_order_total'] = '';
    }
    woocommerce_form_field(
        $params['id'],
        $params,
        $ap_options_fees[$method_id]['min_order_total']
    );
    ?>
</td>
<td>
    <?php
    $params = array(
        'id' 		=> 'payment_fees[' . $method_id . '][max_order_total]',
        'type'  	=> 'text',
        'label' 	=> false,
        'custom_attributes' => array(
            'to_number' => 1,
            'step'		=> 'any'
        )
    );
    if ( !isset( $ap_options_fees[$method_id]['max_order_total'] ) ) {
        $ap_options_fees[$method_id]['max_order_total'] = '';
    }
    woocommerce_form_field(
        $params['id'],
        $params,
        $ap_options_fees[$method_id]['max_order_total']
    );
    ?>
</td>
<td>
    <?php
    $params = array(
        'id' 		=> 'payment_fees[' . $method_id . '][type]',
        'type'  	=> 'select',
        'label' 	=> false,
        'options'	=> array(
            'fixed' 	=> __( 'Fixed', 'woocommerce_activepayments' ),
            'percent' 	=> __( 'Percent', 'woocommerce_activepayments' ),
        )
    );
    if ( !isset( $ap_options_fees[$method_id]['type'] ) ) {
        $ap_options_fees[$method_id]['type'] = '';
    }
    woocommerce_form_field(
        $params['id'],
        $params,
        $ap_options_fees[$method_id]['type']
    );
    ?>
</td>
<td>
    <?php
    $params = array(
        'id' 		=> 'payment_fees[' . $method_id . '][amount]',
        'type'  	=> 'text',
        'label' 	=> false,
        'custom_attributes' => array(
            'to_number' => 1,
            'step'		=> 'any'
        )
    );
    if ( !isset( $ap_options_fees[$method_id]['amount'] ) ) {
        $ap_options_fees[$method_id]['amount'] = '';
    }
    woocommerce_form_field(
        $params['id'],
        $params,
        $ap_options_fees[$method_id]['amount']
    );
    ?>
</td>
