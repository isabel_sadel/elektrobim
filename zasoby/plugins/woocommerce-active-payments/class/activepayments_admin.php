<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	require_once('inspire/plugin.php');

    class ActivePaymentsAdmin extends inspirePlugin {
    	protected $pluginNamespace = 'woocommerce_activepayments';

    	/** @var bool */
    	protected $is_without_any_settings;

	    /**
	     * ActivePaymentsAdmin constructor.
	     *
	     * @param bool $is_without_any_settings
	     */
        public function __construct($is_without_any_settings) {
        	$this->is_without_any_settings = $is_without_any_settings;

        	$this->initBaseVariables();

        	// security check
        	if ( !is_admin() ) die;

        	// Active Payments settings
        	add_action( 'admin_init', array($this, 'initSettingsAction') );
        	add_action( 'admin_init', array($this, 'updateSettingsAction') );
            add_action( 'admin_enqueue_scripts', array($this, 'admin_enqueue_scripts') );
        	add_action( 'admin_menu', array($this, 'initAdminMenuAction'), 70);
        	add_filter( 'woocommerce_screen_ids', array ($this, 'woocommerce_screen_ids' ) );
        }

        public function initSettingsAction() {

        }

        /**
         * Add Active Payments Page to WooCommerce Screen IDs (enqueues Woo CSS & JS)
         *
         */
        function woocommerce_screen_ids( $screen_ids ) {
        	$screen_ids[] = 'woocommerce_page_woocommerce_activepayments';
        	return $screen_ids;
        }

        /**
         * wordpress action
         *
         * should-be-protected method to save/update settings when changed by POST
         */
        public function updateSettingsAction() {
        	if (!empty($_POST)) {
        		// checkboxes
        		if (!empty($_POST['option_page'])) {

        			if ( in_array($_POST['option_page'], array('woocommerce_activepayments_settings') ) ) {
        				$plugin_woocommerce = get_plugin_data( WP_PLUGIN_DIR . '/woocommerce/woocommerce.php' );
        				$woocommerce_version_compare = version_compare( $plugin_woocommerce['Version'], '2.6' );

	        			$payment = new WC_Payment_Gateways();
	        			$paymentGateways = $payment->payment_gateways();

        				if ( $woocommerce_version_compare >= 0 ) {
        					update_option('woocommerce_activepayments_options', $_POST['payment_method'] );
        				} else {
	        				$shipping = new WC_Shipping();
	        				$shippingMethods = $shipping->load_shipping_methods();

	        				foreach ( $shippingMethods as $method ) {
	        					foreach ( $paymentGateways as $payment ) {
	        						update_option( 'woocommerce_activepayments_pm_' . md5($method->id . '_' . $payment->id), !empty($_POST['payment_method'][$method->id][$payment->id] ) );
	        					}

	        					if ( $method->id == 'flat_rate' ) {
	        					    $options = ActivePayments::getOptionsFromGateway($method);
		        					foreach ( $options as $methodOption ) {
		        					    $methodOptionArray = explode('|', $methodOption);
		        					    $fname = trim(reset($methodOptionArray));
		        						$fname_id = ActivePayments::generateFlatIdFromTitle($fname);
		        						foreach ( $paymentGateways as $payment ) {
		        							update_option(
		        									'woocommerce_activepayments_pm_' . md5($method->id . '_' . $payment->id . '_' . $fname_id),
		        									!empty($_POST['payment_method'][$method->id . ':' . $fname_id][$payment->id])
		        							);
		        						}
		        					}
	        					}

	        					if ( $method->id == 'table_rate_shipping' ) {
	        						$shippingTableMethods = get_option( 'woocommerce_table_rates', null );
	        						foreach ($shippingTableMethods as $stMethod) {
	        							foreach ($paymentGateways as $payment) {
		        							update_option(
			        							'woocommerce_activepayments_pm_' . md5($method->id . '_' . $payment->id . '_' . $stMethod['identifier']),
			        							!empty($_POST['payment_method'][$method->id . ':' . $stMethod['identifier']][$payment->id])
		        							);
	        							}
	        						}
	        					}

	        					if ( $method->id == 'flexible_shipping' ) {
	        						$shippingFSMethods = get_option( 'flexible_shipping_rates', null );
	        						if ( isset( $shippingFSMethods ) && is_array( $shippingFSMethods ) ) {
	        							foreach ( $shippingFSMethods as $stMethod ) {
	        								foreach ( $paymentGateways as $payment ) {
		        								update_option(
			        								'woocommerce_activepayments_pm_' . md5($method->id . '_' . $payment->id . '_' . $stMethod['identifier']),
			        								!empty($_POST['payment_method'][$method->id . ':' . $stMethod['identifier']][$payment->id])
		        								);
	        								}
	        							}
	        						}
	        						$shippingFSMethods = get_option( 'woocommerce_flexible_shipping_rates', null );
	        						if ( isset( $shippingFSMethods ) && is_array( $shippingFSMethods ) ) {
	        							foreach ($shippingFSMethods as $stMethod) {
	        								foreach ( $paymentGateways as $payment ) {
		        								update_option(
			        								'woocommerce_activepayments_pm_' . md5($method->id . '_' . $payment->id . '_' . $stMethod['identifier']),
			        								!empty($_POST['payment_method'][$method->id . ':' . $stMethod['identifier']][$payment->id])
		        								);
	        								}
	        							}
	        						}
	        					}

	        					if ( $method->id == 'kurjerzy_shipping_method' ) {
	    							foreach ( $paymentGateways as $payment ) {
	                                    foreach($method->couriers as $courier){
	            							update_option(
	    	        							'woocommerce_activepayments_pm_' . md5($method->id .'_'. $courier . '_' . $payment->id ),
	    	        							!empty($_POST['payment_method'][$method->id . '_' . $courier][$payment->id])
	            							);
	        							}
	    							}
	        					}

	        				} // foreach shipping
        				}

        				// save amount for each payment method
        				foreach( $paymentGateways as $payment ) {
            				update_option('woocommerce_activepayments_pm_' . md5($payment->id . '_amount'), @$_POST['payment_method'][$payment->id]['amount']);
                        }
				        $this->is_without_any_settings = false;
        			} // if

        			if ( in_array($_POST['option_page'], array('woocommerce_activepayments_settings_fees') ) ) {
        				$payment_fees = $_POST['payment_fees'];
						foreach ( $payment_fees as $key => $payment_fee ) {
							$payment_fees[$key]['enabled'] = isset( $payment_fees[$key]['enabled'] ) ? '1' : '0';
							$payment_fees[$key]['title'] = isset( $payment_fees[$key]['title'] ) ? wp_kses_post( trim( stripslashes( $payment_fees[$key]['title'] ) ) ) : '';
							$payment_fees[$key]['tax_class'] = isset( $payment_fees[$key]['tax_class'] ) ? wc_clean( stripslashes( $payment_fees[$key]['tax_class'] ) ) : '';
							$payment_fees[$key]['min_order_total'] = isset( $payment_fees[$key]['min_order_total'] ) ? wc_format_decimal( trim( stripslashes( $payment_fees[$key]['min_order_total'] ) ) ) : '';
							$payment_fees[$key]['max_order_total'] = isset( $payment_fees[$key]['max_order_total'] ) ? wc_format_decimal( trim( stripslashes( $payment_fees[$key]['max_order_total'] ) ) ) : '';
							$payment_fees[$key]['type'] = isset( $payment_fees[$key]['type'] ) ? wc_clean( stripslashes( $payment_fees[$key]['type'] ) ) : '';
							$payment_fees[$key]['amount'] = isset( $payment_fees[$key]['amount'] ) ? wc_format_decimal( trim( stripslashes( $payment_fees[$key]['amount'] ) ) ) : '';
						}
						update_option('woocommerce_activepayments_options_fees', $payment_fees );
        			}
        		}
        	}
        }

        /**
         * Enqueue Scripts & Styles
         *
         * Conditionally inits js & css
         */
        public function admin_enqueue_scripts( $hook ) {
        	if ( 'woocommerce_page_woocommerce_activepayments' === $hook ) {
        	    wp_enqueue_style( 'woocommerce-active-payments-admin', $this->getPluginUrl() . '../assets/css/admin.css' );
                wp_enqueue_script( 'woocommerce-active-payments-admin', $this->getPluginUrl() . '../assets/js/admin.js', array('jquery'), '', true );
        	}
        }

        /**
         * wordpress action
         *
         * inits menu
         */
        public function initAdminMenuAction() {
            $activepayments_page = add_submenu_page( 'woocommerce', __( 'Active Payments', 'woocommerce_activepayments' ),  __( 'Active Payments', 'woocommerce_activepayments' ) , 'manage_woocommerce', $this->getNamespace(), array( $this, 'renderActivePaymentsPage') );
        }

        /**
         * wordpress action
         *
         * renders activepayments submenu page
         */
        public function renderActivePaymentsPage() {
        	$tabs = array(
                'shipping' => array(
                    	'page' => 'admin.php?page=woocommerce_activepayments&tab=shipping',
                    	'title' => __( 'Shipping', 'woocommerce_activepayments' )
                ),
                'fees' => array(
                    	'page' => 'admin.php?page=woocommerce_activepayments&tab=fees',
                    	'title' => __( 'Fees', 'woocommerce_activepayments' )
                )
        	);
        	$current_tab = 'shipping';
        	if ( isset( $_GET['tab'] ) ) {
        		$current_tab = $_GET['tab'];
        	}
        	if ( $current_tab != 'shipping' && $current_tab != 'fees' ) {
        		$current_tab = 'shipping';
        	}
        	include( 'views/tabs.php' );
        	if ( $current_tab == 'shipping' ) {
        		$this->renderActivePaymentsShipping();
        	}
            if ( $current_tab == 'fees' ) {
        		$this->renderActivePaymentsFees();
        	}
        	include( 'views/tabs_end.php' );
        }

        public function renderActivePaymentsShipping() {

        	$shipping = new WC_Shipping();
        	$shippingMethods = $shipping->load_shipping_methods();

        	$shippingMethods_all = $shippingMethods;

        	unset( $shippingMethods['enadawca'] );
        	unset( $shippingMethods['paczka_w_ruchu'] );

        	$active_plugins = apply_filters( 'active_plugins', get_option('active_plugins' ) );
        	if ( in_array( 'woocommerce-paczkomaty-inpost/woocommerce-paczkomaty-inpost.php', $active_plugins ) ) {
        		$plugin_paczkomaty = get_plugin_data( WP_PLUGIN_DIR . '/woocommerce-paczkomaty-inpost/woocommerce-paczkomaty-inpost.php' );

        		$version_compare = version_compare( $plugin_paczkomaty['Version'], '3.0' );

        		if ( $version_compare >= 0 ) {
        			unset( $shippingMethods['paczkomaty_shipping_method'] );
        			unset( $shippingMethods['polecony_paczkomaty_shipping_method'] );
        		}
        	}

        	$plugin_woocommerce = get_plugin_data( WP_PLUGIN_DIR . '/woocommerce/woocommerce.php' );
        	$woocommerce_version_compare = version_compare( $plugin_woocommerce['Version'], '2.6' );

        	$payment = new WC_Payment_Gateways();
        	$paymentGateways = $payment->payment_gateways();

        	// unset unused gateways
        	$unsetKeys = array();

        	foreach ($paymentGateways as $key => $gateway) {
        		if ($gateway->enabled != 'yes') {
        			$unsetKeys[] = $key;
        		}
        	}
        	if (!empty($unsetKeys)) {
        		foreach ($unsetKeys as $key) {
        			unset($paymentGateways[$key]);
        		}
        	}

        	if ( $woocommerce_version_compare >= 0 ) {

        		$ap_options = get_option('woocommerce_activepayments_options', array() );

        		$shippingMethods = array();
        		$shippingFSMethods = array();
        		$shippingFSMethods_woo = array();
        		$shippingTableMethods = array();
        		$shippingZones = WC_Shipping_Zones::get_zones();
        		$worldwide = new WC_Shipping_Zone( 0 );
        		$shippingZones[0] = $worldwide->get_data();
        		$shippingZones[0]['shipping_methods'] = $worldwide->get_shipping_methods();
        		foreach ( $shippingZones as $shippingZoneKey => $shippingZone ) {
        			if ( !isset( $shippingZone['zone_id'] ) ) {
				        $shippingZone['zone_id'] = $shippingZone['id'];
				        $shippingZones[$shippingZoneKey] = $shippingZone;
			        }
        			$shippingMethods[$shippingZone['zone_id']] = $shippingZone['shipping_methods'];
					foreach ( $shippingMethods[$shippingZone['zone_id']] as $shipping_method ) {
						if ( $shipping_method->id == 'flexible_shipping' ) {
							$shippingFSMethods[$shipping_method->shipping_methods_option] = get_option( $shipping_method->shipping_methods_option, array() );
						}
					}
        		}
        		$shippingMethods_no_zone = array();
        		foreach ( $shippingMethods_all as $key => $shipping_method ) {
        			$zone_settings = false;
        			foreach ( $shipping_method->supports as $supports ) {
        				if ( in_array($supports, ['flexible-shipping', 'shipping-zones'] ) ) {
        					$zone_settings = true;
        				}
        			}
			        if ( ! $zone_settings ) {
				        if ( ! in_array( $shipping_method->id, array( 'paczkomaty_shipping_method', 'polecony_paczkomaty_shipping_method', 'enadawca', 'paczka_w_ruchu', 'dhl', 'dpd', 'furgonetka' ) ) ) {
					        if ( isset( $shipping_method->enabled ) && $shipping_method->enabled == 'yes' ) {
						        $shippingMethods_no_zone[ $key ] = $shipping_method;
					        }
				        }
			        }
        		}
        		if ( sizeof( $shippingMethods_no_zone ) > 0 ) {
        			$shippingZones['no_zone'] = array( 'zone_name' => __('Other (without shipping zone)', 'woocommerce_activepayments' ), 'zone_id' => 'no_zone' );
        			$shippingMethods['no_zone'] = $shippingMethods_no_zone;
        		}

        		$args = array(
						'ap_options' => $ap_options,
						'shippingZones' => $shippingZones,
						'shippingMethods' => $shippingMethods,
						'shippingTableMethods' => $shippingTableMethods,
						'shippingFSMethods' => $shippingFSMethods,
						'shippingFSMethods_woo' => $shippingFSMethods_woo,
						'paymentGateways' => $paymentGateways,
						'is_without_any_settings' => $this->is_without_any_settings
				);

        		include( 'views/activepayments_settings_2.6.php' );
        	}
        	else {
	        	$shippingTableMethods = get_option( 'woocommerce_table_rates', null );

	        	if ( in_array( 'flexible-shipping/flexible-shipping.php', $active_plugins ) ) {
	        		$shippingFSMethods = get_option( 'flexible_shipping_rates', array() );
	        	}
	        	else {
	        		$shippingFSMethods = array();
	        	}

	            if ( in_array( 'woo-flexible-shipping/flexible-shipping.php', $active_plugins ) ) {
	        		$shippingFSMethods_woo = get_option( 'woocommerce_flexible_shipping_rates', array() );
	        	}
	        	else {
	        		$shippingFSMethods_woo = array();
	        	}

	        	$args = array(
	        			'shippingMethods' => $shippingMethods,
	        			'shippingTableMethods' => $shippingTableMethods,
	        			'shippingFSMethods' => $shippingFSMethods,
	        			'shippingFSMethods_woo' => $shippingFSMethods_woo,
	        			'paymentGateways' => $paymentGateways,
				        'is_without_any_settings' => $this->is_without_any_settings
	        	);

	        	include( 'views/activepayments_settings.php' );
        	}
        }

        public function renderActivePaymentsFees() {

        	$payment = new WC_Payment_Gateways();
        	$paymentGateways = $payment->payment_gateways();

        	$ap_options_fees = get_option('woocommerce_activepayments_options_fees', array() );

       		include( 'views/activepayments_settings_fees.php' );

        }

        function ap_settings_fees_row( $ap_options_fees, $method_id ) {
            include( 'views/activepayments_settings_fees_row.php' );
        }
    }
