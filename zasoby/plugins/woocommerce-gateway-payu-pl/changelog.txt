*** WooCommerce PayU Gateway Changelog ***

2014.08.28 - version 3.2
 * Wsparcie dla UTF w nazwie zamówienia

2014.07.27 - version 3.1
 * Kosmetyczny fix numeracji zamowien

2014.06.30 - version 3.0
 * Fix - ograniczenie długości opisu przesyłanego do PayU do 50 znaków oraz wycięcie spacji
 * Nadanie wersji która nie konfliktuje się z WC Payu

2014.03.12 - version 1.2.1
 * Fix - poprawne pobieranie id

2014.02.02 - version 1.2
 * Dostosowanie do WooCommerce 2.1 i zmiany w obsłudze magazynu

2013.07.31 - version 1.1
 * Improved payment processing and error reporting

2013.07.15 - version 1.0
 * First Release!
