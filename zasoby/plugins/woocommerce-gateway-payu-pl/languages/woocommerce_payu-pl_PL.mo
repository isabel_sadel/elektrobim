��          �      |      �     �  k        }     �     �     �     �     �     �     �  ]   �  [   X  \   �  d     	   v  M   �  B   �  <     G   N     �  (   �    �  %   �  f   	     j	     o	     �	     �	     �	     �	     �	     �	  O   �	  X   C
  W   �
  U   �
     J  g   W  J   �  K   
  j   V     �     �                                                                             	         
           Cancel order &amp; restore cart Choose Polish Złoty as your store currency in <a href="%s">Pricing Options</a> to enable the PayU Gateway. Description Direct payment via PayU Enable PayU Enable Test Mode Enable/Disable Gateway Disabled Order %s from  Pay via PayU Please enter your MD5 key number; this number should be provided by PayU during registration! Please enter your POS auth key; this number should be provided by PayU during registration! Please enter your pos_id number; this number should be provided by PayU during registration! Please enter your second MD5 key number; this number should be provided by PayU during registration! Test Mode Thank you for your order. We are now redirecting you to PayU to make payment. This controls the description which the user sees during checkout. This controls the title which the user sees during checkout. This is just to test your account information. No payment will be made. Title Wrong status - we ask user to contact us Project-Id-Version: WooCommerce PayU Gateway v1.0
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-08-05 14:18+0100
PO-Revision-Date: 2014-08-05 14:18+0100
Last-Translator: Maciej Swoboda <maciek@inspirelabs.pl>
Language-Team: Inspire Labs
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Poedit 1.6.4
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: /Users/maciek/Sites/woocommercebeta
X-Textdomain-Support: yes
X-Poedit-SearchPath-0: wp-content/plugins/woocommerce-gateway-payu-pl
 Anuluj zamówienie i przywróc koszyk Wybierz walutę \"Polski Złoty\" w <a href="%s">ustawieniach sklepu</a>, aby włączyć bramkę PayU. Opis Płatność przez PayU Włącz PayU Włącz tryb testowy Włącz/Wyłącz Bramka wyłączona Zamówienie %s od  Zapłać przez PayU Wpisz klucz MD5 w systemie PayU, uzyskany podczas tworzenia punktu płatności. Wpisz klucz pos_auth_key w systemie PayU, uzyskany podczas tworzenia punktu płatności. Wpisz identyfikator pos w systemie PayU, uzyskany podczas tworzenia punktu płatności. Wpisz drugi klucz MD5 w systemie PayU, uzyskany podczas tworzenia punktu płatności. Tryb testowy Dziękujemy za złożenie zamówienia. Przekierujemy Cię na stronę PayU w celu dokonania płatności. Opis bramki płatności, którą klient widzi przy składaniu zamówienia. Nazwa bramki płatności, którą klient widzi przy składaniu zamówienia. Używany wyłącznie w celu przetestowania bramki płatności. Żadne płatności nie będą zrealizowane. Tytuł Błędny status 