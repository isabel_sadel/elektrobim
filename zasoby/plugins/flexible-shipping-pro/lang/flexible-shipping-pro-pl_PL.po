msgid ""
msgstr ""
"Project-Id-Version: Flexible Shipping PRO\n"
"POT-Creation-Date: 2018-12-13 10:37+0100\n"
"PO-Revision-Date: 2018-12-13 10:37+0100\n"
"Last-Translator: Piotr Jabłonowski <piotr.jablonowski@wpdesk.net>\n"
"Language-Team: Maciej Swoboda <maciej.swoboda@gmail.com>\n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.2\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: flexible-shipping-pro.php\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"
"X-Poedit-SearchPathExcluded-1: vendor\n"

#: classes/admin-notices.php:46
msgid ""
"Flexible Shipping PRO requires Flexible Shipping plugin in version 2.1 or "
"later."
msgstr ""
"Flexible Shipping PRO wymaga wtyczki Flexible Shipping w wersji 2.1 lub "
"nowszej."

#: classes/admin-notices.php:56
#, php-format
msgid ""
"Flexible Shipping PRO requires Flexible Shipping plugin. <a href=\"%s"
"\">Install Flexible Shipping →</a>"
msgstr ""
"Flexible Shipping PRO wymaga wtyczki Flexible Shipping. <a href=\"%s"
"\">Zainstaluj Flexible Shipping →</a>"

#: classes/admin-notices.php:62
#, php-format
msgid ""
"Flexible Shipping PRO requires Flexible Shipping plugin. <a href=\"%s"
"\">Activate Flexible Shipping →</a>"
msgstr ""
"Flexible Shipping PRO wymaga wtyczki Flexible Shipping. <a href=\"%s\">Włącz "
"Flexible Shipping →</a>"

#: classes/costs-calculation.php:655
msgid "Free"
msgstr "Bezpłatna"

#: classes/flexible-shipping-hooks.php:28
msgid "Free Shipping Requires"
msgstr "Darmowa wysyłka wymaga"

#: classes/flexible-shipping-hooks.php:32
msgid "Minimum order amount"
msgstr "Minimalnej wartości zamówienia"

#: classes/flexible-shipping-hooks.php:33
msgid "Free shipping coupon"
msgstr "Kuponu umożliwiającego darmową wysyłkę"

#: classes/flexible-shipping-hooks.php:34
msgid "Free shipping coupon or minimum order amount"
msgstr "Minimalnej wartości zamówienia lub kuponu"

#: classes/flexible-shipping-hooks.php:35
msgid "Free shipping coupon and minimum order amount"
msgstr "Minimalnej wartości zamówienia i kuponu"

#: classes/flexible-shipping-hooks.php:37
msgid "Condition for free shipping"
msgstr "Warunek do zastosowania darmowej wysyłki"

#: classes/flexible-shipping-hooks.php:43
msgid "Maximum Cost"
msgstr "Maksymalny koszt"

#: classes/flexible-shipping-hooks.php:46
msgid ""
"Set a maximum cost of shipping. This will override the costs configured "
"below."
msgstr ""
"Ustaw maksymalny koszt wysyłki. To nadpisze koszty ustawione w regułach."

#: classes/flexible-shipping-hooks.php:52
msgid "Lowest cost"
msgstr "Najniższy koszt"

#: classes/flexible-shipping-hooks.php:53
msgid "Highest cost"
msgstr "Najwyższy koszt"

#: classes/flexible-shipping-hooks.php:73
msgid "Item"
msgstr "Sztuka"

#: classes/flexible-shipping-hooks.php:74
msgid "Cart line item"
msgstr "Pozycja w koszyku"

#: classes/flexible-shipping-hooks.php:84
#: classes/flexible-shipping-hooks.php:96
msgid "All products"
msgstr "Wszystkie produkty"

#: classes/flexible-shipping-hooks.php:85
#: classes/flexible-shipping-hooks.php:97
msgid "Any class (must be set)"
msgstr "Dowolna klasa (musi być ustawiona)"

#: classes/flexible-shipping-hooks.php:86
#: classes/flexible-shipping-hooks.php:98
msgid "None"
msgstr "Brak"

#: classes/flexible-shipping-pro-plugin.php:114
#, php-format
msgid ""
"<a href=\"%s\">Install the WP Desk Helper plugin</a> to activate and get "
"updates for your WP Desk plugins."
msgstr ""
"<a href=\"%s\">Zainstaluj wtyczkę WP Desk Helper</a>, aby aktywować i "
"otrzymywać aktualizacje wtyczek WP Desk."

#: classes/flexible-shipping-pro-plugin.php:118
#, php-format
msgid ""
"<a href=\"%s\">Activate the WP Desk Helper plugin</a> to activate and get "
"updates for your WP Desk plugins."
msgstr ""
"<a href=\"%s\">Włącz wtyczkę WP Desk Helper</a>, aby aktywować i otrzymywać "
"aktualizacje wtyczek WP Desk."

#: classes/flexible-shipping-pro-plugin.php:140
msgid "Settings"
msgstr "Ustawienia"

#: classes/flexible-shipping-pro-plugin.php:141
msgid "Docs"
msgstr "Docs"

#: classes/flexible-shipping-pro-plugin.php:142
msgid "Support"
msgstr "Wsparcie"

#: classes/views/html-shipping-method-actions.php:3
msgid "Export selected"
msgstr "Eksportuj zaznaczone"

#: classes/views/html-shipping-method-rules-thead.php:3
msgid "Additional<br/>cost"
msgstr "Dodatkowy <br/> koszt"

#: classes/views/html-shipping-method-rules-thead.php:4
msgid "Enter additional shipping cost based on the Value field."
msgstr "Wpisz dodatkowy koszt bazujący na polu Wartość."

#: classes/views/html-shipping-method-rules-thead.php:7
msgid "Value"
msgstr "Wartość"

#: classes/views/html-shipping-method-rules-thead.php:8
msgid "Value for additional cost."
msgstr "Wartość dodatkowego kosztu."

#: classes/views/html-shipping-method-rules-thead.php:11
msgid "Shipping class"
msgstr "Klasa wysyłkowa"

#: classes/views/html-shipping-method-rules-thead.php:12
msgid "Select shipping class that the rule applies to."
msgstr "Wybierz klasę wysyłkową, do której ma być stosowana reguła."

#: classes/views/html-shipping-method-rules-thead.php:15
msgid "Stop"
msgstr "Zatrzymaj"

#: classes/views/html-shipping-method-rules-thead.php:16
msgid "When this rule is matched stop calculating the following rules."
msgstr ""
"Jeśli zostanie spełniona wybrana reguła, zatrzymaj obliczanie kolejnych "
"reguł."

#: classes/views/html-shipping-method-rules-thead.php:19
msgid "Cancel"
msgstr "Anuluj"

#: classes/views/html-shipping-method-rules-thead.php:20
msgid "When this rule is matched do not show it in the checkout."
msgstr "Jeśli zostanie spełniona wybrana reguła, nie pokazuj jej w zamówieniu."

#: classes/woocommerce-form-field.php:39
msgid "required"
msgstr "wymagane"

#: classes/woocommerce-form-field.php:92
msgid "Choose an option"
msgstr "Wybierz opcje"

#: classes/wpdesk/class-helper.php:52
#, php-format
msgid ""
"The %s%s%s License Key has not been activated, so the plugin is inactive! "
"%sClick here%s to activate the license key and the plugin."
msgstr ""
"Klucz licencyjny wtyczki %s%s%s nie został aktywowany, więc wtyczka jest "
"nieaktywna! %sKliknij tutaj%s, aby aktywować klucz licencyjny wtyczki."

#: inc/functions.php:20
#, php-format
msgid "Redirecting. If page not redirects click %s here %s."
msgstr ""
"Przekierowywanie. Jeśli strona nie przekierowuje cię automatycznie, kliknij "
"%s tu %s."

#. Plugin Name of the plugin/theme
msgid "Flexible Shipping PRO"
msgstr "Flexible Shipping PRO"

#. Plugin URI of the plugin/theme
msgid "https://flexibleshipping.com/table-rate/"
msgstr "https://www.wpdesk.pl/sklep/flexible-shipping-pro-woocommerce/"

#. Description of the plugin/theme
msgid ""
"Extends the free version of Flexible Shipping by adding advanced pro "
"features."
msgstr ""
"Rozszerza bezpłatną wersję Flexible Shipping poprzez dodanie zaawansowanych "
"ustawień."

#. Author of the plugin/theme
msgid "WP Desk"
msgstr "WP Desk"

#. Author URI of the plugin/theme
msgid "https://www.wpdesk.net/"
msgstr "https://www.wpdesk.pl/"

#~ msgid ""
#~ "The &#8220;%s&#8221; plugin cannot run on PHP versions older than %s. "
#~ "Please contact your host and ask them to upgrade."
#~ msgstr ""
#~ "Wtyczka &#8220;%s&#8221; nie może działać w wersjach PHP starszych niż "
#~ "%s. Skontaktuj się z administratorem hostingu i poproś o aktualizację."

#~ msgid ""
#~ "The &#8220;%s&#8221; plugin cannot run on WordPress versions older than "
#~ "%s. Please update WordPress."
#~ msgstr ""
#~ "Wtyczka &#8220;%s&#8221; nie może działać w wersjach WordPress starszych "
#~ "niż %s. Prosimy o aktualizację WordPress."

#~ msgid ""
#~ "The &#8220;%s&#8221; plugin cannot run on WooCommerce versions older than "
#~ "%s. Please update WooCommerce."
#~ msgstr ""
#~ "Wtyczka &#8220;%s&#8221; nie może działać w wersjach WooCommerce "
#~ "starszych niż %s. Prosimy o aktualizację WooCommerce."

#, fuzzy
#~| msgid ""
#~| "The &#8220;%s&#8221; plugin cannot run on WordPress versions older than "
#~| "%s. Please update WordPress."
#~ msgid ""
#~ "The &#8220;%s&#8221; plugin cannot run without OpenSSL module version at "
#~ "least %s. Please update OpenSSL module."
#~ msgstr ""
#~ "Wtyczka &#8220;%s&#8221; nie może działać w wersjach WordPress starszych "
#~ "niż %s. Prosimy o aktualizację WordPress."

#~ msgid ""
#~ "The &#8220;%s&#8221; plugin cannot run without %s active. Please install "
#~ "and activate %s plugin."
#~ msgstr ""
#~ "Wtyczka &#8220;%s&#8221; nie może działać bez aktywacji %s. Zainstaluj i "
#~ "aktywuj wtyczkę %s."

#~ msgid ""
#~ "The &#8220;%s&#8221; plugin cannot run without %s php module installed. "
#~ "Please contact your host and ask them to install %s."
#~ msgstr ""
#~ "Wtyczka &#8220;%s&#8221; nie może działać bez modułu php: %s. Skontaktuj "
#~ "się z administratorem hostingu i poproś o instalację %s."

#~ msgid ""
#~ "The &#8220;%s&#8221; plugin cannot run without %s php setting set to %s. "
#~ "Please contact your host and ask them to set %s."
#~ msgstr ""
#~ "Wtyczka &#8220;%s&#8221; nie może działać bez ustawienia php %s "
#~ "ustawionego na %s. Skontaktuj się z administratorem hostingu i poproś o "
#~ "ustawienie %s."

#~ msgid "Insert Image"
#~ msgstr "Wstaw obrazek"

#~ msgid "Select Image"
#~ msgstr "Wybierz obrazek"

#~ msgid "https://www.wpdesk.net/products/flexible-shipping-pro-woocommerce/"
#~ msgstr "https://www.wpdesk.pl/sklep/flexible-shipping-pro-woocommerce/"

#, fuzzy
#~| msgid "Settings"
#~ msgid "Settings updated."
#~ msgstr "Ustawienia"

#~ msgid ""
#~ "An Unexpected HTTP Error occurred during the API request.</p> <p><a href="
#~ "\"?\" onclick=\"document.location.reload(); return false;\">Try again</a>"
#~ msgstr ""
#~ "Wystąpił nieoczekiwany błąd HTTP podczas zapytania API.</p> <p><a "
#~ "href=„?” onclick=„document.location.reload(); return false;”>Spróbuj "
#~ "ponownie</a>"

#~ msgid "An unknown error occurred"
#~ msgstr "Wystąpił nieznany błąd"

#~ msgid "Ustawienia"
#~ msgstr "Ustawienia"

#~ msgid "Dokumentacja"
#~ msgstr "Dokumentacja"

#~ msgid "Wsparcie"
#~ msgstr "Wsparcie"

#~ msgid "Free shipping requires"
#~ msgstr "Darmowa wysyłka wymaga"

#~ msgid "Maximum cost"
#~ msgstr "Maksymalny koszt"
