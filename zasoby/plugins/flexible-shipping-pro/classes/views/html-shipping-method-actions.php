<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>

<button id="flexible_shipping_export_selected" class="button" data-instance-id="<?php echo $_GET['instance_id']; ?>" data-nonce="<?php echo wp_create_nonce( "flexible_shipping" ); ?>" disabled><?php _e( 'Export selected', 'flexible-shipping-pro' ); ?></button>
