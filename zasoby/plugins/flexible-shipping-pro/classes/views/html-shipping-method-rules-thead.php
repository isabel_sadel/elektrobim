<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>
<th class="cost_additional">
	<?php _e( 'Additional<br/>cost', 'flexible-shipping-pro' ); ?>
	<span class="woocommerce-help-tip" data-tip="<?php _e( 'Enter additional shipping cost based on the Value field.', 'flexible-shipping-pro' ); ?>"></span>
</th>
<th class="per_value">
    <?php _e( 'Value', 'flexible-shipping-pro' ); ?>
	<span class="woocommerce-help-tip" data-tip="<?php _e( 'Value for additional cost.', 'flexible-shipping-pro' ); ?>"></span>
</th>
<th class="shipping_class">
	<?php _e( 'Shipping class', 'flexible-shipping-pro' ); ?>
	<span class="woocommerce-help-tip" data-tip="<?php _e( 'Select shipping class that the rule applies to.', 'flexible-shipping-pro' ); ?>"></span>
</th>
<th class="stop">
	<?php _e( 'Stop', 'flexible-shipping-pro' ); ?><br/>
	<span class="woocommerce-help-tip" data-tip="<?php _e( 'When this rule is matched stop calculating the following rules.', 'flexible-shipping-pro' ); ?>"></span>
</th>
<th class="cancel">
	<?php _e( 'Cancel', 'flexible-shipping-pro' ); ?><br/>
	<span class="woocommerce-help-tip" data-tip="<?php _e( 'When this rule is matched do not show it in the checkout.', 'flexible-shipping-pro' ); ?>"></span>
</th>
