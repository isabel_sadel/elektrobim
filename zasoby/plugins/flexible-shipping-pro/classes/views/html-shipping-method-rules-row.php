<?php if ( ! defined( 'ABSPATH' ) ) exit; ?>

<td class="cost_additional">
	<?php 
		$key = 'method_rules[' . $count_rules . '][cost_additional]';
		$args = array(
				'type' 			=> 'text',
				'input_class'	=> array( 'wc_input_price' ),
		);
		$value = '';
		if ( isset( $rule['cost_additional'] ) ) {
			$value = $rule['cost_additional'];
		}
		woocommerce_form_field( $key, $args, wc_format_localized_price( $value ) ); 
	?>
</td>
<td class="per_value">
	<?php 
		$key = 'method_rules[' . $count_rules . '][per_value]';
		$args = array(
				'type' 			=> 'text',
				'input_class'	=> array( 'wc_input_price' ),
		);
		$value = '';
		if ( isset( $rule['per_value'] ) ) {
			$value = $rule['per_value'];
		}
		woocommerce_form_field( $key, $args, wc_format_localized_price( $value ) ); 
	?>
</td>
<td class="shipping_class">
	<?php
		$key = 'method_rules[' . $count_rules . '][shipping_class][]';
		$args = array(
			'type' 		    => 'select_multiple',
			'options' 	    => $shipping_classes,
            'input_class'   => array( 'fs-shipping-class' ),
		);
		$value = 'any';
		if ( isset( $rule['shipping_class'] ) ) {
			$value = $rule['shipping_class'];
		}
		woocommerce_form_field( $key, $args, $value );
	?>
</td>
<td class="stop">
	<?php
		$key = 'method_rules[' . $count_rules . '][stop]';
		$args = array(
			'type' 		=> 'checkbox',
		);
		$value = '';
		if ( isset( $rule['stop'] ) ) {
			$value = $rule['stop'];
		}
		woocommerce_form_field( $key, $args, $value ); 
	?>
</td>
<td class="cancel">
	<?php
		$key = 'method_rules[' . $count_rules . '][cancel]';
		$args = array(
			'type' 		=> 'checkbox',
		);
		$value = '';
		if ( isset( $rule['cancel'] ) ) {
			$value = $rule['cancel'];
		}
		woocommerce_form_field( $key, $args, $value ); 
	?>
</td>
