<?php 
	if ( ! defined( 'ABSPATH' ) ) exit; 
	
	$key = 'method_rules[xxx][shipping_class][]';
	$args = array(
			'type' 		    => 'select_multiple',
			'options' 	    => $shipping_classes,
			'input_class'   => array( 'fs-shipping-class' ),
			'return' 	    => true,
	);
	$value = 'all';
	$field_shipping_class = woocommerce_form_field( $key, $args, $value );
	
	$key = 'method_rules[xxx][stop]';
	$args = array(
		'type' 		=> 'checkbox',
		'return' 	=> true,
	);
	$value = 0;
	$field_stop = woocommerce_form_field( $key, $args, $value );
	
	$key = 'method_rules[xxx][cancel]';
	$args = array(
		'type' 		=> 'checkbox',
		'return' 	=> true,
	);
	$value = 0;
	$field_cancel = woocommerce_form_field( $key, $args, $value );
	
	$key = 'method_rules[xxx][repeat]';
	$args = array(
		'type' 		=> 'checkbox',
		'return' 	=> true,
	);
	$value = 0;
	$field_repeat = woocommerce_form_field( $key, $args, $value );
	
	$key = 'method_rules[xxx][cost_additional]';
	$args = array(
			'type' 		=> 'text',
			'return' 	=> true,
			'input_class'	=> array( 'wc_input_price' ),
	);
	$value = '';
	$field_cost_additional = woocommerce_form_field( $key, $args, $value );
	
	$key = 'method_rules[xxx][per_value]';
	$args = array(
			'type' 		=> 'text',
			'return' 	=> true,
			'input_class'	=> array( 'wc_input_price' ),
	);
	$value = '';
	$field_per_value = woocommerce_form_field( $key, $args, $value );
	
?><td class="cost_additional">\
   <?php echo str_replace( "'", '"', str_replace( "\r", "", str_replace( "\n", "", $field_cost_additional ) ) ); ?> \
</td>\
<td class="per_value">\
   <?php echo str_replace( "'", '"', str_replace( "\r", "", str_replace( "\n", "", $field_per_value ) ) ); ?> \
</td>\
<td class="shipping_class">\
	<?php echo str_replace( "'", '"', str_replace( "\r", "", str_replace( "\n", "", $field_shipping_class ) ) ); ?> \
</td>\
<td class="stop">\
    <?php echo str_replace( "'", '"', str_replace( "\r", "", str_replace( "\n", "", $field_stop ) ) ); ?> \
</td>\
<td class="cancel">\
    <?php echo str_replace( "'", '"', str_replace( "\r", "", str_replace( "\n", "", $field_cancel ) ) ); ?> \
</td>\
