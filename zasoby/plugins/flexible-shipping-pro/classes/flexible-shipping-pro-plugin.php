<?php

/**
 * Class WPDesk_Flexible_Shipping_Pro_Plugin
 */
class WPDesk_Flexible_Shipping_Pro_Plugin
	extends \WPDesk\PluginBuilder\Plugin\AbstractPlugin
	implements \WPDesk\PluginBuilder\Plugin\HookableCollection
{

	use \WPDesk\PluginBuilder\Plugin\HookableParent;
	use \WPDesk\PluginBuilder\Plugin\TemplateLoad;


	const HOOK_PRIORITY_AFTER_DEFAULT = 11;

	/**
	 * Plugin is active?
	 *
	 * @var bool
	 */
	private $plugin_is_active = true;

	/**
	 * WPDesk_Flexible_Shipping_Pro_Plugin constructor.
	 *
	 * @param WPDesk_Plugin_Info $plugin_info Plugin info.
	 */
	public function __construct( WPDesk_Plugin_Info $plugin_info ) {
		$this->plugin_info = $plugin_info;
		$this->check_activation();
		parent::__construct( $this->plugin_info );
		if ( $this->plugin_is_active ) {
			add_action( 'plugins_loaded', array( $this, 'init_flexible_shipping' ), self::HOOK_PRIORITY_AFTER_DEFAULT );
		}
	}

	/**
	 * Check plugin activation.
	 */
	private function check_activation() {
		if ( ! class_exists( 'WPDesk_Helper_Plugin' ) ) {
			require_once 'wpdesk/class-helper.php';
		}
		$plugin_data = array(
			'plugin'     => $this->plugin_info->get_plugin_file_name(),
			'product_id' => $this->plugin_info->get_product_id(),
			'version'    => $this->plugin_info->get_version(),
			'config_uri' => admin_url( 'admin.php?page=wc-settings&tab=shipping&section=flexible_shipping_connect' ),
		);

		$helper      = new WPDesk_Helper_Plugin( $plugin_data );
		if ( ! $helper->is_active() ) {
			$this->plugin_is_active = false;
		}
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		if ( $this->plugin_is_active ) {
			parent::hooks();
		}
	}

	/**
	 * Init base variables for plugin
	 */
	public function init_base_variables() {
		$this->plugin_url = $this->plugin_info->get_plugin_url();

		$this->plugin_path   = $this->plugin_info->get_plugin_dir();
		$this->template_path = $this->plugin_info->get_text_domain();

		$this->plugin_text_domain   = $this->plugin_info->get_text_domain();
		$this->plugin_namespace     = $this->plugin_info->get_text_domain();

	}


	public function init_flexible_shipping() {
		$fs = new WPDesk_Flexible_Shipping_Pro_FS_Hooks();

		$woocommerce_form_fields = new WPDesk_Flexible_Shipping_Pro_Woocommerce_Form_Field();
		$woocommerce_form_fields->hooks();

		$admin_notices = new WPDesk_Flexible_Shipping_Pro_Admin_Notices();
		$admin_notices->hooks();

		$costs_calculation = new WPDesk_Flexible_Shipping_Pro_Costs_Calculation();
		$costs_calculation->hooks();
	}

	/**
	 * @param mixed $links
	 *
	 * @return array
	 */
	public function links_filter( $links ) {
		$docs_link = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/docs/flexible-shipping-pro-woocommerce-docs/' : 'https://docs.flexibleshipping.com/collection/20-fs-table-rate/';
		$support_link = get_locale() === 'pl_PL' ? 'https://www.wpdesk.pl/support/' : 'https://flexibleshipping.com/support/';

		$plugin_links = array(
			'<a href="' . admin_url( 'admin.php?page=wc-settings&tab=shipping&section=flexible_shipping_connect') . '">' . __( 'Settings', 'flexible-shipping-pro' ) . '</a>',
			'<a href="' . $docs_link . '">' . __( 'Docs', 'flexible-shipping-pro' ) . '</a>',
			'<a href="' . $support_link . '">' . __( 'Support', 'flexible-shipping-pro' ) . '</a>',
		);

		return array_merge( $plugin_links, $links );
	}
}