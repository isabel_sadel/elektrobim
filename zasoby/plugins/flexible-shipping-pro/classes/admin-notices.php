<?php

class WPDesk_Flexible_Shipping_Pro_Admin_Notices {

	public function hooks() {
		add_filter( 'plugins_api', array( $this, 'flexible_shipping_install' ), 10, 3 );
		add_action( 'admin_notices', array( $this, 'admin_notices_plugin_flexible_shipping' ) );
	}


	function flexible_shipping_install( $api, $action, $args ) {
		$download_url = 'http://downloads.wordpress.org/plugin/flexible-shipping.latest-stable.zip';

		if ( 'plugin_information' != $action ||
		     false !== $api ||
		     ! isset( $args->slug ) ||
		     'wpdesk-helper' != $args->slug
		) {
			return $api;
		}

		$api                = new stdClass();
		$api->name          = 'Flexible Shipping';
		$api->version       = '1.6.2';
		$api->download_link = esc_url( $download_url );

		return $api;
	}


	function admin_notices_plugin_flexible_shipping() {
		$flexible_shipping_version = '0';
		if ( defined( 'FLEXIBLE_SHIPPING_VERSION' ) ) {
			$flexible_shipping_version = FLEXIBLE_SHIPPING_VERSION;
		}
		else {
			$active_plugins = apply_filters( 'active_plugins', get_option('active_plugins' ) );
			if ( in_array( 'flexible-shipping/flexible-shipping.php', $active_plugins ) ) {
				$plugin = get_plugin_data( WP_PLUGIN_DIR . '/flexible-shipping/flexible-shipping.php' );
				$flexible_shipping_version = $plugin['Version'];
			}
		}
		if ( $flexible_shipping_version != '0' ) {
			if ( version_compare( $flexible_shipping_version, '2.1', '<' ) ) {
				$class   = 'error notice-error';
				$message = __( 'Flexible Shipping PRO requires Flexible Shipping plugin in version 2.1 or later.', 'flexible-shipping-pro' );
				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
			}
			return;
		}

		$slug = 'flexible-shipping';
		$install_url = wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=' . $slug ), 'install-plugin_' . $slug );
		$activate_url = 'plugins.php?action=activate&plugin=' . urlencode( 'flexible-shipping/flexible-shipping.php' ) . '&plugin_status=all&paged=1&s&_wpnonce=' . urlencode( wp_create_nonce( 'activate-plugin_flexible-shipping/flexible-shipping.php' ) );

		$message = sprintf( wp_kses( __( 'Flexible Shipping PRO requires Flexible Shipping plugin. <a href="%s">Install Flexible Shipping →</a>', 'flexible-shipping-pro' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $install_url ) );
		$is_downloaded = false;
		$plugins = array_keys( get_plugins() );
		foreach ( $plugins as $plugin ) {
			if ( strpos( $plugin, 'flexible-shipping/flexible-shipping.php' ) === 0 ) {
				$is_downloaded = true;
				$message = sprintf( wp_kses( __( 'Flexible Shipping PRO requires Flexible Shipping plugin. <a href="%s">Activate Flexible Shipping →</a>', 'flexible-shipping-pro' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( admin_url( $activate_url ) ) );
			}
		}
		echo '<div class="error fade"><p>' . $message . '</p></div>' . "\n";
	}

}
