<?php

class WPDesk_Flexible_Shipping_Pro_FS_Hooks {

	private $scripts_version = '1';

	public function __construct() {

		add_filter( 'flexible_shipping_method_settings', array( $this, 'flexible_shipping_method_settings' ),  10, 2);
		add_filter( 'flexible_shipping_process_admin_options', array( $this, 'flexible_shipping_process_admin_options' ), 10, 1 );
		add_filter( 'flexible_shipping_method_rule_options_based_on', array( $this, 'flexible_shipping_method_rule_options_based_on' ) );
		add_action( 'flexible_shipping_method_rule_thead', array( $this, 'flexible_shipping_method_rule_thead' ) );
		add_action( 'flexible_shipping_method_rule_row', array( $this, 'flexible_shipping_method_rule_row' ), 10, 2 );
		add_action( 'flexible_shipping_method_rule_js', array( $this, 'flexible_shipping_method_rule_js' ) );
		add_filter( 'flexible_shipping_method_rule_save', array( $this, 'flexible_shipping_method_rule_save' ), 10, 2 );

        add_action( 'admin_enqueue_scripts', array( $this, 'enqueue_admin_scripts' ) );

        add_action( 'flexible_shipping_actions_row', array( $this, 'flexible_shipping_actions_row' ) );

    }

	public function flexible_shipping_method_settings( $flexible_shipping_settings, $shipping_method ) {
		$flexible_shipping_settings_new = array();
		foreach ( $flexible_shipping_settings as $key => $setting ) {
            if ( $key === 'method_free_shipping' ) {
                $flexible_shipping_settings_new['method_free_shipping_requires'] = array(
                    'title' 		=> __( 'Free Shipping Requires', 'flexible-shipping-pro' ),
                    'type' 			=> 'select',
                    'default' 		=> isset( $shipping_method['method_free_shipping_requires'] ) ? $shipping_method['method_free_shipping_requires'] : 'order_amount',
                    'options'       => array(
                        'order_amount'              => __( 'Minimum order amount', 'flexible-shipping-pro' ),
                        'coupon'                    => __( 'Free shipping coupon', 'flexible-shipping-pro' ),
                        'order_amount_or_coupon'    => __( 'Free shipping coupon or minimum order amount', 'flexible-shipping-pro' ),
                        'order_amount_and_coupon'   => __( 'Free shipping coupon and minimum order amount', 'flexible-shipping-pro' ),
                    ),
                    'description' 	=> __( 'Condition for free shipping', 'flexible-shipping-pro' ),
                    'desc_tip'		=> true
                );
            }
            if ( $key === 'method_calculation_method' ) {
                $flexible_shipping_settings_new['method_max_cost'] = array(
                    'title' 		=> __( 'Maximum Cost', 'flexible-shipping-pro' ),
                    'type' 			=> 'price',
                    'default' 		=> isset( $shipping_method['method_max_cost'] ) ? $shipping_method['method_max_cost'] : '',
                    'description' 	=> __( 'Set a maximum cost of shipping. This will override the costs configured below.', 'flexible-shipping-pro' ),
                    'desc_tip'		=> true
                );
            }
			$flexible_shipping_settings_new[$key] = $setting;
		}
		$flexible_shipping_settings_new['method_calculation_method']['options']['lowest'] = __( 'Lowest cost', 'flexible-shipping-pro' );
		$flexible_shipping_settings_new['method_calculation_method']['options']['highest'] = __( 'Highest cost', 'flexible-shipping-pro' );
		return $flexible_shipping_settings_new;
	}

    function enqueue_admin_scripts() {
        $current_screen = get_current_screen();
        if ( $current_screen->id === 'woocommerce_page_wc-settings' ) {
	        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
	        wp_enqueue_script( 'flexible-shipping-pro-admin', plugins_url( 'flexible-shipping-pro/assets/js/admin' . $suffix . '.js' ), array(), $this->scripts_version );
        }
    }


    public function flexible_shipping_process_admin_options( $shipping_method )	{
		$shipping_method['method_max_cost'] = wc_format_decimal( $_POST['woocommerce_flexible_shipping_method_max_cost'] );
        $shipping_method['method_free_shipping_requires'] = $_POST['woocommerce_flexible_shipping_method_free_shipping_requires'];
		return $shipping_method;
	}

	public function flexible_shipping_method_rule_options_based_on( $options_based_on ) {
		$options_based_on['item'] = __( 'Item', 'flexible-shipping-pro' );
		$options_based_on['cart_line_item'] = __( 'Cart line item', 'flexible-shipping-pro' );
		return $options_based_on;
	}

	public function flexible_shipping_method_rule_thead( $rule ) {
		include( 'views/html-shipping-method-rules-thead.php' );
	}

	public function flexible_shipping_method_rule_row( $rule, $count_rules ) {
		$shipping_classes = array();
		$shipping_classes['all'] = __( 'All products', 'flexible-shipping-pro' );
		$shipping_classes['any'] = __( 'Any class (must be set)', 'flexible-shipping-pro' );
		$shipping_classes['none'] = __( 'None', 'flexible-shipping-pro' );
		$wc_shipping_classes = WC()->shipping->get_shipping_classes();
		foreach ( $wc_shipping_classes as $shipping_class ) {
			$shipping_classes[$shipping_class->term_id] = $shipping_class->name;
		}
		include( 'views/html-shipping-method-rules-row.php' );
	}

	public function flexible_shipping_method_rule_js( $rule ) {
		$shipping_classes = array();
		$shipping_classes['all'] = __( 'All products', 'flexible-shipping-pro' );
		$shipping_classes['any'] = __( 'Any class (must be set)', 'flexible-shipping-pro' );
		$shipping_classes['none'] = __( 'None', 'flexible-shipping-pro' );
		$wc_shipping_classes = WC()->shipping->get_shipping_classes();
		foreach ( $wc_shipping_classes as $shipping_class ) {
			$shipping_classes[$shipping_class->term_id] = $shipping_class->name;
		}
		include( 'views/html-shipping-method-rules-js.php' );
	}

	public function flexible_shipping_actions_row() {
        include( 'views/html-shipping-method-actions.php' );
    }

	public function flexible_shipping_method_rule_save( $method_rule, $rule ) {
		$method_rule['shipping_class'] = $rule['shipping_class'];
		$method_rule['stop'] = 0;
		if ( isset( $rule['stop'] ) ) {
			$method_rule['stop'] = $rule['stop'];
		}
		$method_rule['cancel'] = 0;
		if ( isset( $rule['cancel'] ) ) {
			$method_rule['cancel'] = $rule['cancel'];
		}
		$method_rule['cost_additional'] = wc_format_decimal( $rule['cost_additional'] );
		$method_rule['per_value'] = wc_format_decimal( $rule['per_value'] );
		return $method_rule;
	}


}
