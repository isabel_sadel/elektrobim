<?php
/*
	Plugin Name: Flexible Shipping PRO
	Plugin URI: https://flexibleshipping.com/table-rate/
	Description: Extends the free version of Flexible Shipping by adding advanced pro features.
	Version: 1.7.5
	Author: WP Desk
	Author URI: https://www.wpdesk.net/
	Text Domain: flexible-shipping-pro
	Domain Path: /lang/
	Requires at least: 4.5
    Tested up to: 5.1.1
    WC requires at least: 3.1.0
    WC tested up to: 3.6
    Requires PHP: 5.6

	Copyright 2017 WP Desk Ltd.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

/* THESE TWO VARIABLES CAN BE CHANGED AUTOMATICALLY */
$plugin_version           = '1.7.5';
$plugin_release_timestamp = '2019-04-24';

$plugin_name        = 'WooCommerce Flexible Shipping PRO';
$plugin_class_name  = 'WPDesk_Flexible_Shipping_Pro_Plugin';
$plugin_text_domain = 'flexible-shipping-pro';
$product_id         = 'WooCommerce Flexible Shipping PRO';
$plugin_file        = __FILE__;
$plugin_dir         = dirname( __FILE__ );

$requirements = array(
    'php'     => '5.6',
    'wp'      => '4.5',
    'plugins' => array(
        array(
            'name'      => 'woocommerce/woocommerce.php',
            'nice_name' => 'WooCommerce',
        ),
        array(
            'name'      => 'flexible-shipping/flexible-shipping.php',
            'nice_name' => 'Flexible Shipping',
        ),
    ),
);

require __DIR__ . '/vendor/wpdesk/wp-plugin-flow/src/plugin-init-php52.php';