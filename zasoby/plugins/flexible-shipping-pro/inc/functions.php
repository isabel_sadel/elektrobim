<?php

if ( !function_exists( 'wpdesk_is_plugin_active' ) ) {
	function wpdesk_is_plugin_active( $plugin_file ) {

		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return in_array( $plugin_file, $active_plugins ) || array_key_exists( $plugin_file, $active_plugins );
	}
}

if ( !function_exists('wpdesk_redirect') ) {
	function wpdesk_redirect( $redirect ) {
		if ( 1==1 && headers_sent() ) {
			?>
			<span><?php printf(__('Redirecting. If page not redirects click %s here %s.', 'flexible-shipping-pro'), '<a href="' . $redirect . '" >', '</a>'); ?></span>

			<script>
                parent.location.replace('<?php echo $redirect; ?>');
			</script>
			<?php
		}
		else {
			wp_safe_redirect($redirect);
		}
		exit;
	}
}

if ( !function_exists( 'wpdesk__' ) ) {
	function wpdesk__( $text, $domain ) {
		if ( function_exists( 'icl_sw_filters_gettext' ) ) {
			return icl_sw_filters_gettext( $text, $text, $domain, $text );
		}
		if ( function_exists( 'pll__' ) ) {
			return pll__( $text );
		}
		return __( $text, $domain );
	}
}

if ( !function_exists( 'wpdesk__e' ) ) {
	function wpdesk__e( $text, $domain ) {
		echo wpdesk__( $text, $domain );
	}
}
