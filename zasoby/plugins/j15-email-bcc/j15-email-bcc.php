<?php
/**
 * Plugin Name:     Woocommerce Email Bcc
 * Plugin URI:      https://january15th.com
 * Description:     Dodaje BCC do maila W trakcie realizacji
 * Author:          January 15th Ltd
 * Author URI:      https://january15th.com
 * Text Domain:     j15-email-bcc
 * Domain Path:     /languages
 * Version:         1.0.0
 *
 * @package         J15\Email_Bcc
 * @version         1.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Adds BCC header to email.
 *
 * @param string $headers Default headers.
 * @param string $object Email type/object
 *
 * @return string
 *
 * @since 1.0.0
 */
function j15_add_bcc_to_woo_email( $headers, $object ) {

	$add_bcc_to = array(
		'customer_processing_order',
	);

	if ( in_array( $object, $add_bcc_to ) ) {

		$headers .=	'Cc: Elektrobim <biuro@elektrobim.pl>' ."\r\n";
	}
	return $headers;
}
add_filter( 'woocommerce_email_headers', 'j15_add_bcc_to_woo_email', 10, 2 );