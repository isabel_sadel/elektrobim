#Changelog

# 0.1.0

* initial release
* changed billing and shipping address format in WooCommerce emails, add NIP field
* add customer note field in WooCommerce emails