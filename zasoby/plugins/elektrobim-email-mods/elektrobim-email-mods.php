<?php
/**
 * Plugin Name:     Modyfikacje maili Elektrobim
 * Plugin URI:      https://elektrobim.pl
 * Description:     Modyfikuje maile Elektrobim
 * Author:          January 15th Ltd
 * Author URI:      https://january15th.com
 * Text Domain:     elektrobim-email-mods
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Elektrobim_Email_Mods
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_filter( 'woocommerce_order_formatted_shipping_address', 'ebim_formatted_shipping_address', 99, 2 );
add_filter( 'woocommerce_order_formatted_billing_address', 'ebim_formatted_billing_address', 99, 2 );
add_action( 'woocommerce_email_customer_details', 'ebim_email_order_comment', 50, 3 );



/**
 * @param $address_arr
 * @param WC_Order $order
 *
 * @return mixed
 */
function ebim_formatted_shipping_address( $address_arr, $order ) {

	$address_arr['first_name'] = $address_arr['company'] . " " . $address_arr['first_name'];
	$address_arr['company'] = "";
	$address_arr['address_1'] .= " " . $address_arr['address_2'];
	$address_arr['address_2'] = "";
	$address_arr['city'] = $address_arr['postcode'] . " " . $address_arr['city'];

	if( $phone = $order->get_billing_phone() ) $address_arr['postcode'] = "tel. " . $phone;
	if( $email = $order->get_billing_email() ) $address_arr['country'] = $email;

	return $address_arr;

}
/**
 * @param $address_arr
 * @param WC_Order $order
 *
 * @return mixed
 */
function ebim_formatted_billing_address( $address_arr, $order ) {


	if(!empty( $address_arr['company'] ) ) {
		$address_arr['first_name'] = "";
		$address_arr['last_name'] = "";
	}



	$address_arr['address_1'] .= " " . $address_arr['address_2'];
	$address_arr['address_2'] = "";
	$address_arr['city'] = $address_arr['postcode'] . " " . $address_arr['city'];
	$address_arr['postcode'] = "";
	if( $nip = get_post_meta( $order->get_id(), 'vat_number', true ) ) {
		$address_arr['country'] = "NIP: " . $nip;
	}
	//$address_arr['country'] = "email. ";

	return $address_arr;

}





function ebim_email_order_comment( $order, $sent_to_admin = false, $plain_text = false  ) {

	if ( ! is_a( $order, 'WC_Order' ) ) {
		return;
	}

	$note = $order->get_customer_note();

	//if( $note ) {

	if( !empty( $note ) ) {

		echo '<table class="addresses" style="font-family:
			Arial, sans-serif; line-height: 1.3em; color: #666666;" width="100%" cellspacing="0" cellpadding="0" border="0"> 
			<tbody>
			<tr style="font-family: Arial, sans-serif; line-height: 1.3em;">
			<td class="addresses-td" style="font-family: Arial, sans-serif; line-height: 1.3em;" width="50%" valign="top">
			 
			 <p style="margin: .6em 0;"><strong style="margin-bottom: 0.5rem;">Uwagi:</strong>  
			 
			 ' . nl2br( $note ) . '</p>   
			    
			</td>  
			</tr> 
			</tbody>
			</table>';



	}

}





/*add_filter( 'woocommerce_email_attachments', 'j15_woo_privacy_attach_terms_conditions_pdf_to_email', 10, 3);

function j15_woo_privacy_attach_terms_conditions_pdf_to_email ( $attachments , $id, $object ) {

	$allowed_statuses = array( 'on_hold_order', 'customer_on_hold_order', 'new_order', 'customer_invoice', 'customer_processing_order', 'customer_completed_order' );

	if( isset( $id ) && in_array ( $id, $allowed_statuses ) ) {

		$upload_dir = wp_upload_dir();

		$rules_pdf_path = $upload_dir['basedir'] . '/dokumenty/Regulamin.pdf';
		$complaint_pdf_path2 = $upload_dir['basedir'] . '/dokumenty/Formularz_Odstapienia.pdf';

		$attachments[] = $rules_pdf_path;
		$attachments[] = $complaint_pdf_path2;

	}

	return $attachments;

}*/

add_action( 'woocommerce_email_footer', 'j15_add_content_specific_email', 1, 1 );
//add_action( 'woocommerce_email_footer', 'j15_add_content_specific_email', 99, 1 );

function j15_add_content_specific_email( $email ) {

	$allowed_statuses = array( 'on_hold_order', 'customer_on_hold_order', 'new_order', 'customer_invoice', 'customer_processing_order', 'customer_completed_order' );

	if ( in_array( $email->id, $allowed_statuses) ) {
		printf(
			'<p style="text-align: center; font-size: 0.8em; padding-top: 2rem; margin-bottom: 0;"><a href="%s">Regulamin sklepu</a> <a href="%s">Formularz odstąpienia od umowy</a></p>',
			content_url().'/uploads/dokumenty/Regulamin.pdf',
			content_url().'/uploads/dokumenty/Formularz_Odstapienia.pdf'
		);
	}
}


function check_json_api_url() {

	if( 2261 === get_current_user_id() ) wp_die( esc_url_raw( rest_url( 'contact-form-7/v1' ) ) );

}

//add_action('template_redirect','check_json_api_url');
