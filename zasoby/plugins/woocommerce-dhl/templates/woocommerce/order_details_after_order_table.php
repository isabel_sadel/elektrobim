<?php
/**
 * Linki do śledzenia przesyłki
 *
 * This template can be overridden by copying it to yourtheme/woocommerce-dhl/woocommerce/email_after_order_table.php
 *
 * @author  WP Desk
 * @version 1.0.0
 */ 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<header>
	<h2><?php _e( 'Przesyłka', 'woocommerce-dhl' ); ?></h2>
</header>
<?php
	foreach ( $dhl_packages as $dhl_package ) {
		?>
		<p>
			<?php _e( 'Śledź przesyłkę: ', 'woocommerce-dhl' ); ?><a target="_blank" href="<?php echo $dhl_package['tracking_url']; ?>"><?php echo $dhl_package['shipment_id']; ?></a>
		</p>
		<?php
	}
