<?php
/**
 * Linki do śledzenia przesyłki
 *
 * This template can be overridden by copying it to yourtheme/woocommerce-dhl/woocommerce/email_after_order_table.php
 *
 * @author  WP Desk
 * @version 1.0.0
 */ 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<br/>
<?php echo ec_special_title( __( "Śledź przesyłkę DHL", 'email-control'), array("border_position" => "center", "text_position" => "center", "space_after" => "3", "space_before" => "3" ) ); ?>
<?php
	foreach ( $dhl_packages as $dhl_package ) {
		?>
		<p>
<table>
<tr>
<td width="25%">
<center><img src="http://www.elektrobim.pl/zasoby/uploads/2017/04/dhl-sledzenie.png"></center>
</td>
<td width="75%">
<p>Twoje zamówienie zostało wysłane kurierem DHL. Możesz na bieżąco śledzić swoją przesyłkę, wystarczy kliknąć w poniższy link z numerem monitorowania.</p>
<p><?php _e( 'Śledź przesyłkę: ', 'woocommerce-dhl' ); ?><a target="_blank" href="<?php echo $dhl_package['tracking_url']; ?>"><?php echo $dhl_package['shipment_id']; ?></a></p>
</td>
</tr>
</table>

			
			
		</p>
		<hr/>
		<?php
	}
