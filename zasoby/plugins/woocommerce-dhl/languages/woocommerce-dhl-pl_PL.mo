��    	      d      �       �   i   �   h   K     �     �     �     �  �   �     a    i  l   �  o   �     f  
   k  #   v     �  �   �     6            	                                  <a href="%s">Activate the WP Desk Helper plugin</a> to activate and get updates for your WP Desk plugins. <a href="%s">Install the WP Desk Helper plugin</a> to activate and get updates for your WP Desk plugins. Docs Settings Settings updated. Support The %s%s%s License Key has not been activated, so the plugin is inactive! %sClick here%s to activate the license key and the plugin. WP Desk Project-Id-Version: WooCommerce DHL
POT-Creation-Date: 2016-10-14 14:43+0200
PO-Revision-Date: 2016-10-14 14:43+0200
Last-Translator: Maciej Swoboda <maciej.swoboda@gmail.com>
Language-Team: Maciej Swoboda <support@wpdesk.pl>
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.7
X-Poedit-Basepath: ..
X-Poedit-WPHeader: woocommerce-dhl.php
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 <a href="%s">Włącz wtyczkę WP Desk Helper</a>, aby aktywować i otrzymywać aktualizacje wtyczek WP Desk. <a href="%s">Zainstaluj wtyczkę WP Desk Helper</a>, aby aktywować i otrzymywać aktualizacje wtyczek WP Desk. Docs Ustawienia Ustawienia zostały zaktualizowane. Wsparcie Klucz licencyjny wtyczki %s%s%s nie został aktywowany, więc wtyczka jest nieaktywna! %sKliknij tutaj%s, aby aktywować klucz licencyjny wtyczki. WP Desk 