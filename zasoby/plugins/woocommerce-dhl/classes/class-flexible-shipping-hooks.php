<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WPDesk_WooCommerce_DHL_FS_Hooks' ) ) {

	class WPDesk_WooCommerce_DHL_FS_Hooks {

		private $plugin = null;

		public function __construct( WPDesk_WooCommerce_DHL_Plugin $plugin ) {

		    $this->plugin = $plugin;

			add_filter( 'flexible_shipping_integration_options', array( $this, 'flexible_shipping_integration_options' ),  10 );
				
			add_filter( 'flexible_shipping_method_settings', array( $this, 'flexible_shipping_method_settings' ), 10, 2 );
				
			add_action( 'flexible_shipping_method_script', array( $this, 'flexible_shipping_method_script' ) );
		
			add_filter( 'flexible_shipping_process_admin_options', array( $this, 'flexible_shipping_process_admin_options' ), 10, 1 );
				
			add_filter( 'flexible_shipping_method_integration_col', array( $this, 'flexible_shipping_method_integration_col' ), 10, 2 );
				
			add_filter( 'flexible_shipping_method_rate_id', array( $this, 'flexible_shipping_method_rate_id' ), 10, 2 );

			add_filter( 'flexible_shipping_add_method', array( $this, 'flexible_shipping_add_method' ), 10, 3 );

			add_filter( 'flexible_shipping_shipping_data', array( $this, 'flexible_shipping_shipping_data' ) );

			add_filter( 'flexible_shipping_bulk_send', array( $this, 'flexible_shipping_bulk_send' ), 10, 2 );

			add_filter( 'flexible_shipping_bulk_label', array( $this, 'flexible_shipping_bulk_label' ), 10, 2  );

//			add_filter( 'flexible_shipping_integration', array( $this, 'flexible_shipping_integration' ), 10 );

		}
		
		function flexible_shipping_integration_options( $options ) {
			$options['dhl'] = __( 'DHL', 'woocommerce-dhl' );
			return $options;
		}
		
		public function flexible_shipping_method_settings( $flexible_shipping_settings, $shipping_method ) {
			$shipping_methods = WC()->shipping->get_shipping_methods();
			if ( 1 == 1 || $shipping_methods['dhl']->enabled == 'yes' ) { // always available
				$insurance_custom_attributes = array();
				if ( isset( $shipping_method['dhl_insurance'] ) && $shipping_method['dhl_insurance'] == '1' ) {
					$insurance_custom_attributes = array( 'checked' => 'checked' );
				}
				$cod_custom_attributes = array();
				if ( isset( $shipping_method['dhl_cod'] ) && $shipping_method['dhl_cod'] == '1' ) {
					$cod_custom_attributes = array( 'checked' => 'checked' );
				}
				$settings = array(
						'dhl_product' => array(
								'title' => __( 'Usługa DHL', 'woocommerce-dhl' ),
								'type' => 'select',
								'default' => isset( $shipping_method['dhl_product'] ) ? $shipping_method['dhl_product'] : '',
								'options' => WPDesk_WooCommerce_DHL::get_products(),
						),
						'dhl_package_type' => array(
								'title' => __( 'Rodzaj przesyłki', 'woocommerce-dhl' ),
								'type' => 'select',
								'default' => isset( $shipping_method['dhl_package_type'] ) ? $shipping_method['dhl_package_type'] : '',
								'options' => WPDesk_WooCommerce_DHL::get_package_types(),
						),
						'dhl_insurance' => array(
								'title' 			=> __( 'Ubezpieczenie', 'woocommerce-dhl' ),
								'type' 				=> 'checkbox',
								'custom_attributes' => $insurance_custom_attributes
						),
						'dhl_cod' => array(
								'title' 			=> __( 'Pobranie', 'woocommerce-dhl' ),
								'type' 				=> 'checkbox',
								'custom_attributes' => $cod_custom_attributes
						),
				);
				return array_merge( $flexible_shipping_settings, $settings );
			}
			return $flexible_shipping_settings;
		}
		
		public function flexible_shipping_method_script() {
			?>
				<script type="text/javascript">
					jQuery(document).ready(function() {
						function dhlOptions() {
							if ( jQuery('#woocommerce_flexible_shipping_method_integration').val() == 'dhl' ) {
								jQuery('#woocommerce_flexible_shipping_dhl_product').closest('tr').css('display','table-row');
								jQuery('#woocommerce_flexible_shipping_dhl_package_type').closest('tr').css('display','table-row');
								jQuery('#woocommerce_flexible_shipping_dhl_insurance').closest('tr').css('display','table-row');
								jQuery('#woocommerce_flexible_shipping_dhl_cod').closest('tr').css('display','table-row');
							}
							else {
								jQuery('#woocommerce_flexible_shipping_dhl_product').closest('tr').css('display','none');
								jQuery('#woocommerce_flexible_shipping_dhl_package_type').closest('tr').css('display','none');
								jQuery('#woocommerce_flexible_shipping_dhl_insurance').closest('tr').css('display','none');
								jQuery('#woocommerce_flexible_shipping_dhl_cod').closest('tr').css('display','none');
							}
						}
						jQuery('#woocommerce_flexible_shipping_method_integration').change(function() {
							dhlOptions();
						});
						dhlOptions();
					});
				</script>
			<?php
		}
		
		public function flexible_shipping_process_admin_options( $shipping_method )	{
			$shipping_methods = WC()->shipping->get_shipping_methods();
			$shipping_method['dhl_product'] = $_POST['woocommerce_flexible_shipping_dhl_product'];					
			$shipping_method['dhl_package_type'] = $_POST['woocommerce_flexible_shipping_dhl_package_type'];					
			$shipping_method['dhl_insurance'] = 0;
			if ( isset( $_POST['woocommerce_flexible_shipping_dhl_insurance'] ) ) {
				$shipping_method['dhl_insurance'] = $_POST['woocommerce_flexible_shipping_dhl_insurance'];
			}
			$shipping_method['dhl_cod'] = 0;
			if ( isset( $_POST['woocommerce_flexible_shipping_dhl_cod'] ) ) {
				$shipping_method['dhl_cod'] = $_POST['woocommerce_flexible_shipping_dhl_cod'];
			}
			return $shipping_method;
		}
		
		public function flexible_shipping_method_integration_col( $col, $shipping_method ) {
			$shipping_methods = WC()->shipping->get_shipping_methods();
			if ( $shipping_methods['dhl']->enabled == 'yes' ) {
				if ( isset( $shipping_method['method_integration'] ) && 'dhl' === $shipping_method['method_integration'] ) {
					ob_start();
					$tip = __( 'Brak', 'woocommerce-dhl' );
					$products = WPDesk_WooCommerce_DHL::get_products();
					if ( isset( $products[$shipping_method['dhl_product']] ) ) {
						$tip = $products[$shipping_method['dhl_product']];
					}
					$package_types = WPDesk_WooCommerce_DHL::get_package_types();
					if ( isset( $package_types[$shipping_method['dhl_package_type']] ) ) {
						$tip .= ', ' . $package_types[$shipping_method['dhl_package_type']];
					}
					?>
					<td width="1%" class="integration default">
						<span class="tips" data-tip="<?php echo $tip; ?>">
							<?php echo $shipping_methods['dhl']->title; ?>
						</span>
					</td>
					<?php
					$col = ob_get_contents();
					ob_end_clean();
				}
			}
			return $col;
		}
				
		public function flexible_shipping_method_rate_id( $rate_id, $shipping_method ) {
			if ( isset( $shipping_method['method_integration'] ) && 'dhl' === $shipping_method['method_integration'] ) {
				$rate_id = $rate_id . '_dhl_' . sanitize_title( $shipping_method['dhl_product'] ) . '_' . $shipping_method['dhl_package_type'] . '_' . $shipping_method['dhl_insurance'];
				if ( isset( $shipping_method['dhl_cod'] ) ) {
					$rate_id .=  '_' . $shipping_method['dhl_cod'];
				}
			}
			return $rate_id;
		}
		
		public function flexible_shipping_add_method( $add_method, $shipping_method, $package )	{
			if ( isset( $shipping_method['method_integration'] ) && 'dhl' === $shipping_method['method_integration'] 
				&& isset( $shipping_method['dhl_product'] ) 
			) {
			}
			return $add_method;
		}

		public function flexible_shipping_shipping_data( $shippings ) {
			global $post, $the_order;
			$order = $the_order;
			$_dhl = wpdesk_get_order_meta( $order, '_dhl', true );
			if ( is_array( $_dhl ) ) {
				$count = 0;
				foreach ( $_dhl as $key => $dhl_package ) {
					$count++;
					$shipping = array();
					$shipping['url'] = admin_url( 'post.php?post=' . $post->ID . '&action=edit#dhl_metabox"' );
					if ( isset( $dhl_package['dhl_status'] ) ) {
						if ( $dhl_package['dhl_status'] == 'ok' ) {
						    $shipping['order_id'] = $post->ID;
							$shipping['count'] = $count;
						    $shipping['integration'] = 'dhl';
						    $shipping['integration_data'] = $dhl_package;
							$shipping['status'] = 'confirmed';
							$shipping['tracking_number'] = $dhl_package['dhl_package_number'];
							$label_url = admin_url( 'admin-ajax.php?action=wpdesk_dhl&dhl_action=get_label&security=' );
							$label_url .= wp_create_nonce( 'dhl_ajax_nonce', 'dhl_ajax_nonce' );
							$label_url .= '&shipment_id=' . $dhl_package['dhl_package_number'];
							$label_url .= '&post_id=' . $post->ID;
							$label_url .= '&key=' . $key;
							$shipping['label_url'] = $label_url;
							$shipping['tracking_url'] = 'http://www.dhl.com.pl/sledzenieprzesylkikrajowej/szukaj.aspx?m=0&sn=' . $dhl_package['dhl_package_number'];
						}
						if ( $dhl_package['dhl_status'] == 'error' ) {
							$shipping['status'] = 'error';
							$shipping['error'] = $dhl_package['dhl_message'];
                        }
					}
					else {
						$shipping['status'] = 'new';
                    }
					$shippings[] = $shipping;
				}
			}
			return $shippings;
		}

		public function flexible_shipping_bulk_send( $message, $post_id ) {
			$order = wc_get_order( $post_id );
			$_dhl = wpdesk_get_order_meta( $order, '_dhl', true );
			if ( is_array( $_dhl ) ) {
			    try {
				    $this->plugin->dhl->create_shipments( $order );
				    return array( 'status'  => 'created',
				                  'message' => __( 'Przesyłka została utworzona.', 'paczkomaty_shipping_method' )
				    );
			    } catch ( Exception $e ) {
				    return array( 'status' => 'error', 'message' => $e->getMessage() );
			    }
			}
			return $message;
		}

		public function flexible_shipping_bulk_label( $labels, $post_id ) {
			$order = wc_get_order( $post_id );
			$_dhl = wpdesk_get_order_meta( $order, '_dhl', true );
			if ( is_array( $_dhl ) ) {
				foreach ( $_dhl as $key => $dhl_package ) {
					if ( isset( $dhl_package['dhl_status'] ) ) {
						if ( $dhl_package['dhl_status'] == 'ok' ) {
							$shipment_id = $dhl_package['dhl_package']->createShipmentsResult->item->shipmentId;
							try {
								$label_data = $this->plugin->dhl->get_label( $shipment_id );
								$labels[] = array(
									'message'   => __( 'Etykieta została pobrana.', 'paczkomaty_shipping_method' ),
									'content'   => base64_decode($label_data->getLabelsResult->item->labelData),
									'file_name' => 'dhl_' . $label_data->getLabelsResult->item->labelName
								);
							}
							catch ( Exception $e ) {
								$labels[] = array( 'status' => 'error', 'message' => $e->getMessage() );
							}
						}
                    }
					else {
						$labels[] = array( 'status' => 'error', 'message' => __( 'Przesyłka nie została odnaleziona (1).', 'woocommerce-dhl' ) );
					}
				}
			}
			return $labels;
		}

/*
		public function flexible_shipping_integration( $integrations ) {
		    $integrations['dhl'] = __( 'DHL', 'woocommerce-dhl' );
		    return $integrations;
        }
*/
	}

}