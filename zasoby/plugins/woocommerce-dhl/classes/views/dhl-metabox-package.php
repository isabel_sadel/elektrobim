<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
	
	$disabled = false;
	if ( isset( $package['dhl_status'] ) && $package['dhl_status'] == 'ok' ) {
		$disabled = true;
	}
?>
<div class="dhl-package">
	<input id="dhl_<?php echo $count; ?>_key" name="dhl[<?php echo $count; ?>][key]" type="hidden" value="<?php echo $key; ?>" />
	<input id="dhl_<?php echo $count; ?>_post_id" name="dhl[<?php echo $count; ?>][post_id]" type="hidden" value="<?php echo $post->ID; ?>" />
	<?php if ( count( $_dhl ) > 1 ) : ?>
		<h4><?php echo sprintf( __( 'Przesyłka %s', 'woocommerce-dhl' ), $count ); ?></h4>
	<?php endif; ?>
	<?php
		$key = 'dhl[' . $count . '][dhl_product]';
		$args = array(
				'label'		=> __( 'Usługa DHL', 'woocommerce-dhl' ),
				'id'		=>  'dhl_' . $count . '_dhl_product',
				'type' 		=> 'select',
				'options' 	=> WPDesk_WooCommerce_DHL::get_products(),
		);
		$value = '';
		if ( isset( $package['dhl_product'] ) ) {
			$value = $package['dhl_product'];
		}
		if ( $disabled ) {
			$args['custom_attributes'] = array( 'disabled' => 'disabled' );
		}
		woocommerce_form_field( $key, $args, $value );

    ?>
    <hr/>
    <div class="dhl_package_data">
        <h4><?php echo sprintf( __( 'Paczka %s', 'woocommerce-dhl' ), 1 ); ?></h4>
        <?php
            $key = 'dhl[' . $count . '][dhl_package_type]';
            $args = array(
                    'label'			=> __( 'Typ', 'woocommerce-dhl' ),
                    'id'			=>  'dhl_' . $count . '_dhl_package_type',
                    'type' 			=> 'select',
                    'options' 		=> WPDesk_WooCommerce_DHL::get_package_types(),
                    'input_class'	=> array( 'dhl-package-type' ),
            );
            $value = '';
            if ( isset( $package['dhl_package_type'] ) ) {
                $value = $package['dhl_package_type'];
            }
            if ( $disabled ) {
                $args['custom_attributes'] = array( 'disabled' => 'disabled' );
            }
            woocommerce_form_field( $key, $args, $value );
        ?>
        <div class="dhl-weight">
            <?php
                $key = 'dhl[' . $count . '][dhl_package_weight]';
                $args = array(
                        'label'		=> __( 'Waga [kg]', 'woocommerce-dhl' ),
                        'id'		=>  'dhl_' . $count . '_dhl_package_weight',
                        'type' 		=> 'number',
                );
                $value = '';
                if ( isset( $package['dhl_package_weight'] ) ) {
                    $value = $package['dhl_package_weight'];
                }
                else {
                    $value = $settings['package_weight'];
                }
                if ( $disabled ) {
                    $args['custom_attributes'] = array( 'disabled' => 'disabled' );
                }
                woocommerce_form_field( $key, $args, $value );
            ?>
        </div>
        <div class="dhl-dimensions">
            <label><?php _e( 'Wymiary (dł / sz / wys) [cm]' ); ?></label>
            <?php
                $key = 'dhl[' . $count . '][dhl_package_width]';
                $args = array(
                        'type' 		=> 'number',
                        'id'		=>  'dhl_' . $count . '_dhl_package_width',
                );
                $value = '';
                if ( isset( $package['dhl_package_width'] ) ) {
                    $value = $package['dhl_package_width'];
                }
                else {
                    $value = $settings['package_width'];
                }
                if ( $disabled ) {
                    $args['custom_attributes'] = array( 'disabled' => 'disabled' );
                }
                woocommerce_form_field( $key, $args, $value );
            ?>
            <?php
                $key = 'dhl[' . $count . '][dhl_package_length]';
                $args = array(
                        'label'		=> '/',
                        'type' 		=> 'number',
                        'id'		=>  'dhl_' . $count . '_dhl_package_length',
                );
                $value = '';
                if ( isset( $package['dhl_package_length'] ) ) {
                    $value = $package['dhl_package_length'];
                }
                else {
                    $value = $settings['package_length'];
                }
                if ( $disabled ) {
                    $args['custom_attributes'] = array( 'disabled' => 'disabled' );
                }
                woocommerce_form_field( $key, $args, $value );
            ?>
            <?php
                $key = 'dhl[' . $count . '][dhl_package_height]';
                $args = array(
                        'label'		=> '/',
                        'type' 		=> 'number',
                        'id'		=>  'dhl_' . $count . '_dhl_package_height',
                );
                $value = '';
                if ( isset( $package['dhl_package_height'] ) ) {
                    $value = $package['dhl_package_height'];
                }
                else {
                    $value = $settings['package_height'];
                }
                if ( $disabled ) {
                    $args['custom_attributes'] = array( 'disabled' => 'disabled' );
                }
                woocommerce_form_field( $key, $args, $value );
            ?>
        </div>
    </div>
    <hr/>
    <?php $count_additional = 1; ?>
	<?php if ( isset( $package['additional_packages'] ) ) : ?>
        <?php foreach ( $package['additional_packages'] as $additional_key => $additional_package ) : ?>
            <?php $count_additional++; ?>
            <?php $additional_package_label = sprintf( __( 'Paczka %s', 'woocommerce-dhl' ), $count_additional ); ?>
            <?php include( 'dhl-metabox-additional-package.php' ); ?>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php if ( !$disabled ) : ?>
        <p id="dhl_add_package_<?php echo $count; ?>">
            <a data-count="<?php echo $count; ?>" data-count_additional="<?php echo $count_additional; ?>" class="button dhl-button dhl-button-add-package"><?php _e( 'Dodaj paczkę', 'woocommerce-dhl' ); ?></a>
            <span class="dhl-spinner spinner"></span>
        </p>
        <hr/>
        <div class="dhl_additional_package_template" id="dhl_additional_package_template_<?php echo $count; ?>" style="display: none;">
            <?php $current_count = $count; ?>
            <?php $count = '_count_'; ?>
            <?php $additional_key = '_additional_key_'; ?>
            <?php $count_additional = '_count_additional_'; ?>
            <?php $additional_package = $package; ?>
            <?php $additional_package_label = __( 'Nowa paczka', 'woocommerce-dhl' ); ?>
            <?php include( 'dhl-metabox-additional-package.php' ); ?>
            <?php $count = $current_count; ?>
        </div>
    <?php endif; ?>
	<?php
		$key = 'dhl[' . $count . '][dhl_shipment_date]';
		$args = array(
				'label'				=> __( 'Data wysyłki', 'woocommerce-dhl' ),
				'id'				=>  'dhl_' . $count . '_dhl_shipment_date',
				'type' 				=> 'select',
				'options'			=> $shipment_date_options,
		);
		$value = $shipment_date_default;
		if ( isset( $package['dhl_shipment_date'] ) ) {
			$value = $package['dhl_shipment_date'];
		}
		if ( isset( $package['dhl_status'] ) && $package['dhl_status'] == 'ok' ) {
			$args['options'] = array( $package['dhl_shipment_date'] => $package['dhl_shipment_date'] );
			$value = $package['dhl_shipment_date'];
		}
		if ( $disabled ) {
			$args['custom_attributes'] = array( 'disabled' => 'disabled' );
		}
		woocommerce_form_field( $key, $args, $value );	
	?>
	<?php
		$key = 'dhl[' . $count . '][dhl_insurance]';
		$args = array(
				'label'			=> __( 'Ubezpieczenie', 'woocommerce-dhl' ),
				'id'			=>  'dhl_' . $count . '_dhl_insurance',
				'type' 			=> 'checkbox',
				'input_class'	=> array( 'dhl-insurance' ),
		);
		$value = '0';
		if ( isset( $package['dhl_insurance'] ) ) {
			$value = $package['dhl_insurance'];
		}
		else {
			$value = $settings['insurance'];
		}
		if ( $disabled ) {
			$args['input_class'][] = 'dhl-disabled';
		}
		woocommerce_form_field( $key, $args, $value );	
	?>
	<?php
		$key = 'dhl[' . $count . '][dhl_insurance_value]';
		$args = array(
				'label'			=> __( 'Kwota ubezpieczenia', 'woocommerce-dhl' ),
				'id'			=>  'dhl_' . $count . '_dhl_insurance_value',
				'type' 			=> 'number',
				'class'			=> array( 'dhl-insurance-value' ),
				'custom_attributes' => array(
						'min' 	=> 0,
						'step'	=> '0.01',
				)
		);
		$value = '';
		if ( isset( $package['dhl_insurance_value'] ) ) {
			$value = $package['dhl_insurance_value'];
		}
		else {
			$value = $order->get_total();
		}
		if ( $disabled ) {
			$args['custom_attributes']['disabled'] = 'disabled';
		}
		woocommerce_form_field( $key, $args, $value );	
	?>
	<?php
		$key = 'dhl[' . $count . '][dhl_cod]';
		$args = array(
				'label'			=> __( 'Pobranie', 'woocommerce-dhl' ),
				'id'			=>  'dhl_' . $count . '_dhl_cod',
				'type' 			=> 'checkbox',
				'input_class'	=> array( 'dhl-cod' ),
		);
		$value = '0';
		if ( isset( $package['dhl_cod'] ) ) {
			$value = $package['dhl_cod'];
		}
		if ( $disabled ) {
			$args['input_class'][] = 'dhl-disabled';
		}
		woocommerce_form_field( $key, $args, $value );	
	?>
	<?php
		$key = 'dhl[' . $count . '][dhl_cod_value]';
		$args = array(
				'label'			=> __( 'Kwota pobrania', 'woocommerce-dhl' ),
				'id'			=>  'dhl_' . $count . '_dhl_cod_value',
				'type' 			=> 'number',
				'class'			=> array( 'dhl-cod-value' ),
				'custom_attributes' => array(
						'min' 	=> 0,
						'step'	=> '0.01',
				)
		);
		$value = '';
		if ( isset( $package['dhl_cod_value'] ) ) {
			$value = $package['dhl_cod_value'];
		}
		else {
			$value = $order->get_total();
		}
		if ( $disabled ) {
			$args['custom_attributes']['disabled'] = 'disabled';
		}
		woocommerce_form_field( $key, $args, $value );	
	?>
	<?php
		$key = 'dhl[' . $count . '][dhl_content]';
		$args = array(
				'label'			=> __( 'Zawartość', 'woocommerce-dhl' ),
				'id'			=>  'dhl_' . $count . '_dhl_content',
				'type' 			=> 'text',
		);
		$value = $settings['package_contents'];
		if ( isset( $package['dhl_content'] ) ) {
			$value = $package['dhl_content'];
		}
		if ( $disabled ) {
			$args['custom_attributes'] = array( 'disabled' => 'disabled' );
		}
		woocommerce_form_field( $key, $args, $value );	
	?>
	<?php
		$key = 'dhl[' . $count . '][dhl_comment]';
		$args = array(
				'label'			=> __( 'Komentarz', 'woocommerce-dhl' ),
				'id'			=>  'dhl_' . $count . '_dhl_comment',
				'type' 			=> 'text',				
		);
		$value = $settings['package_comment'];
		if ( isset( $package['dhl_comment'] ) ) {
			$value = $package['dhl_comment'];
		}
		else {
			$value = str_replace( '[order_number]', $order->get_order_number(), $value );
			$value = str_replace( '[shop_name]', get_bloginfo( 'name' ), $value );
			$value = str_replace( '[shop_url]', home_url(), $value );
		}
		if ( $disabled ) {
			$args['custom_attributes'] = array( 'disabled' => 'disabled' );
		}
		woocommerce_form_field( $key, $args, $value );	
	?>
	<?php if ( isset( $package['dhl_one_time_message'] ) && $package['dhl_one_time_message'] != '' ) : ?>
		<p class="dhl_error">
			<?php echo $package['dhl_one_time_message']; ?>
		</p>
	<?php endif; ?>
	<?php if ( isset( $package['dhl_status'] ) && $package['dhl_status'] == 'error' ) : ?>
		<p class="dhl_error">
			<?php echo $package['dhl_message']; ?>
		</p>
	<?php endif; ?>
	<?php if ( isset( $package['dhl_status'] ) && $package['dhl_status'] == 'ok' ) : ?>
		<p>
			<?php _e( 'Przesyłka: ', 'woocommerce-dhl' ); ?> <a target="_blank" href="<?php echo $this->get_tracking_url( $package['dhl_package_number'] ); ?>"><?php echo $package['dhl_package_number']; ?></a>
		</p>
		<p>
			<a data-shipment-id="<?php echo $package['dhl_package']->createShipmentsResult->item->shipmentId; ?>" data-count="<?php echo $count; ?>" class="button button-primary dhl-button dhl-button-label"><?php _e( 'Pobierz etykietę', 'woocommerce-dhl' ); ?></a>
			<span class="spinner dhl-spinner"></span>
			<a data-count="<?php echo $count; ?>" class="dhl-button dhl-button-delete-created"><?php _e( 'Anuluj', 'woocommerce-dhl' ); ?></a>
        </p>
        <?php if ( apply_filters( 'flexible_printing', false ) && isset( $settings['flexible_printing_integration'] ) && $settings['flexible_printing_integration'] == 'yes' ) : ?>
            <p>
                <?php echo apply_filters( 'flexible_printing_print_button', '', 'dhl',
                    array(
                        'content' => 'print',
                        'id' => $package['dhl_package']->createShipmentsResult->item->shipmentId,
                        'icon'  => true,
                        'label' => 'Print on: %s',
                        'data' => array(
                            'post_id'       => $post->ID,
                            'count'         => $count,
                            'shipment_id'   => $package['dhl_package']->createShipmentsResult->item->shipmentId,
                        ),
                    )
                ); ?>
            </p>
        <?php endif; ?>
	<?php endif; ?>
	<?php if ( !isset( $package['dhl_status'] ) || $package['dhl_status'] == 'error' || $package['dhl_status'] == '' ) : ?>
		<p>
			<a data-count="<?php echo $count; ?>" class="button button-primary dhl-button dhl-button-create"><?php _e( 'Utwórz', 'woocommerce-dhl' ); ?></a>
			<a data-count="<?php echo $count; ?>" class="button dhl-button dhl-button-save"><?php _e( 'Zapisz', 'woocommerce-dhl' ); ?></a>
			<span class="spinner dhl-spinner"></span>
            <?php /* ?>
			<a data-count="<?php echo $count; ?>" class="dhl-button dhl-button-delete"><?php _e( 'Usuń', 'woocommerce-dhl' ); ?></a>
            <?php /* */ ?>
		</p>			
	<?php endif; ?>
</div>