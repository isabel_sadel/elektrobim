<div class="dhl_additional_package" id="dhl_additional_package_<?php echo $additional_key; ?>" data-key="<?php echo $additional_key; ?>" data-count="<?php echo $count; ?>">
    <h4><?php echo $additional_package_label; ?></h4>
    <?php
    $key = 'dhl[' . $count . '][additional_packages][' . $additional_key . '][dhl_package_type]';
    $args = array(
        'label'			=> __( 'Typ', 'woocommerce-dhl' ),
        'id'			=>  'dhl_' . $count . '_' . $additional_key . '_dhl_package_type',
        'type' 			=> 'select',
        'options' 		=> WPDesk_WooCommerce_DHL::get_package_types(),
        'input_class'	=> array( 'dhl-package-type' ),
    );
    $value = '';
    if ( isset( $additional_package['dhl_package_type'] ) ) {
        $value = $additional_package['dhl_package_type'];
    }
    if ( $disabled ) {
        $args['custom_attributes'] = array( 'disabled' => 'disabled' );
    }
    woocommerce_form_field( $key, $args, $value );
    ?>
    <div class="dhl-weight">
        <?php
        $key = 'dhl[' . $count . '][additional_packages][' . $additional_key . '][dhl_package_weight]';
        $args = array(
            'label'		=> __( 'Waga [kg]', 'woocommerce-dhl' ),
            'id'		=>  'dhl_' . $count . '_' . $additional_key . '_dhl_package_weight',
            'type' 		=> 'number',
        );
        $value = '';
        if ( isset( $additional_package['dhl_package_weight'] ) ) {
            $value = $additional_package['dhl_package_weight'];
        }
        else {
            $value = $settings['package_weight'];
        }
        if ( $disabled ) {
            $args['custom_attributes'] = array( 'disabled' => 'disabled' );
        }
        woocommerce_form_field( $key, $args, $value );
        ?>
    </div>
    <div class="dhl-dimensions">
        <label><?php _e( 'Wymiary (dł / sz / wys) [cm]' ); ?></label>
        <?php
        $key = 'dhl[' . $count . '][additional_packages][' . $additional_key . '][dhl_package_width]';
        $args = array(
            'type' 		=> 'number',
            'id'		=>  'dhl_' . $count . '_' . $additional_key . '_dhl_package_width',
        );
        $value = '';
        if ( isset( $additional_package['dhl_package_width'] ) ) {
            $value = $additional_package['dhl_package_width'];
        }
        else {
            $value = $settings['package_width'];
        }
        if ( $disabled ) {
            $args['custom_attributes'] = array( 'disabled' => 'disabled' );
        }
        woocommerce_form_field( $key, $args, $value );
        ?>
        <?php
        $key = 'dhl[' . $count . '][additional_packages][' . $additional_key . '][dhl_package_length]';
        $args = array(
            'label'		=> '/',
            'type' 		=> 'number',
            'id'		=>  'dhl_' . $count . '_' . $additional_key . '_dhl_package_length',
        );
        $value = '';
        if ( isset( $additional_package['dhl_package_length'] ) ) {
            $value = $additional_package['dhl_package_length'];
        }
        else {
            $value = $settings['package_length'];
        }
        if ( $disabled ) {
            $args['custom_attributes'] = array( 'disabled' => 'disabled' );
        }
        woocommerce_form_field( $key, $args, $value );
        ?>
        <?php
        $key = 'dhl[' . $count . '][additional_packages][' . $additional_key . '][dhl_package_height]';
        $args = array(
            'label'		=> '/',
            'type' 		=> 'number',
            'id'		=>  'dhl_' . $count . '_' . $additional_key . '_dhl_package_height',
        );
        $value = '';
        if ( isset( $additional_package['dhl_package_height'] ) ) {
            $value = $additional_package['dhl_package_height'];
        }
        else {
            $value = $settings['package_height'];
        }
        if ( $disabled ) {
            $args['custom_attributes'] = array( 'disabled' => 'disabled' );
        }
        woocommerce_form_field( $key, $args, $value );
        ?>
    </div>
    <a data-count="<?php echo $count; ?>" data-key="<?php echo $additional_key; ?>" class="dhl-button dhl-button-delete-package"><?php _e( 'Usuń', 'woocommerce-dhl' ); ?></a>
    <div style="clear: both;"></div>
    <hr/>
</div>
