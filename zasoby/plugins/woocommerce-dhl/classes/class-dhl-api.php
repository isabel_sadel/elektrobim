<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WPDesk_WooCommerce_DHL_API' ) ) {

	class WPDesk_WooCommerce_DHL_API {
		
		private $login;
		private $password;
		private $test_api;
		
		private $auth_data = array();
		
		private $client = false;
		
		public function __construct( $login, $password, $test_api = false ) {
			$this->login = $login;
			$this->password = $password;
			$this->test_api = $test_api;
			$this->auth_data = array( 'username' => $login, 'password' => $password );
		}
		
		public function get_client() {
			if ( ! $this->client ) {
				$dhl_webapi_url = 'https://dhl24.com.pl/webapi2';
				if ( $this->test_api ) {
					$dhl_webapi_url = 'https://sandbox.dhl24.com.pl/webapi2';
				}
				$context = stream_context_create( array('http' => array('header' => 'DHLApiOrgin: WOOCOMMERCE') ) );
				$this->client = new SoapClient(
						$dhl_webapi_url,
						array( 'keep-alive' => true, 'connection_timeout' => 30/*, 'stream_context' => $context */)
						);
			}
			return $this->client;
		}
		
		public function ping() {
			$client = $this->get_client();
			$params = array(
					'authData' 		=> $this->auth_data,
					'createdFrom'	=> date( 'Y-m-d', time() ),
					'createdTo'		=> date( 'Y-m-d', time()+5*DAY_IN_SECONDS ),
			);
			$ret = $client->getMyShipments( $params );
			return $ret;
		}
		
		public function create_shipment( $shipments ) {
			$client = $this->get_client();
			$params = array(
					'authData' 		=> $this->auth_data,
					'shipments'		=> $shipments,
			);			
			$ret = $client->createShipments( $params );
			
			return $ret;
		}
		
		public function get_postal_code_services( $post_code, $pickup_date, $cache = true ) {
			
			$transient_name = 'dhl_pc1_services_' . $post_code .'-' . $pickup_date . '_' . date('H');
			
			$ret = get_transient( $transient_name );
			
			if ( $ret == false ) {
			
				$client = $this->get_client();
	
				$params = array(
						'authData' 		=> $this->auth_data,
						'postCode'		=> $post_code,
						'pickupDate'	=> $pickup_date
				);			
				$ret = $client->getPostalCodeServices( $params );
				set_transient( $transient_name, $ret, HOUR_IN_SECONDS );
			}
			
			return $ret;
		
		}
		
		public function get_label( $shipment_id, $label_format ) {
		
			$client = $this->get_client();
	
			$params = array(
					'authData' 		=> $this->auth_data,
					'itemsToPrint'	=> array( array( 'labelType' => $label_format, 'shipmentId' => $shipment_id ) )
			);			
			
			$ret = $client->getLabels( $params );

			return $ret;
			
		}
	
		public function delete_shipment( $shipment_id ) {
		
			$client = $this->get_client();
	
			$params = array(
					'authData' 		=> $this->auth_data,
					'shipments'		=> array( $shipment_id )
			);			
			
			$ret = $client->deleteShipments( $params );

			return $ret;
			
		}
	
	}
	
}