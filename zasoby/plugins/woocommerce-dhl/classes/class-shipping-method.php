<?php 

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WPDesk_WooCommerce_DHL_Shipping_Method' ) ) {
	class WPDesk_WooCommerce_DHL_Shipping_Method extends WC_Shipping_Method {
		
		public $api = false;		

		/**
		 * Constructor for your shipping class
		 *
		 * @access public
		 * @return void
		 */
		public function __construct( $instance_id = 0 ) {
			
			$this->instance_id 			     	= absint( $instance_id );
			$this->id                 			= 'dhl';
						
			$this->method_title       			= __( 'DHL', 'woocommerce-dhl' );
			$this->method_description 			= __( 'Integracja WooCommerce z DHL. <a href="https://www.wpdesk.pl/docs/woocommerce-dhl-docs/" target="_blank">Zapoznaj się z instrukcją obsługi &rarr;</a>', 'woocommerce-dhl' );

			$this->enabled		    = 'yes';
			$this->title            = __( 'DHL', 'woocommerce-dhl' );

			$this->settings['enabled']  = 'yes';
						
			$this->init();

			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
			
		}
		
		/**
		 * Init your settings
		 *
		 * @access public
		 * @return void
		 */
		function init() {

			// Load the settings API
			$this->init_settings();
			$this->init_form_fields();

			// Define user set variables
			//$this->title        		= $this->get_option( 'title' );
			
			$this->login            	= $this->get_option( 'login' );
			$this->password         	= $this->get_option( 'password' );
			$this->api_key         		= $this->get_option( 'api_key' );							
			$this->test_mode         	= $this->get_option( 'test_mode', 'no' );
			
			$this->cost         		= $this->get_option( 'cost' );
			$this->cost_cod        		= $this->get_option( 'cost_cod' );

		}	
		
		public function get_api() {
			if ( !$this->api ) {
				$this->api = new WPDesk_WooCommerce_DHL_API( $this->login, $this->password, $this->test_mode == 'yes' );
			}
			return $this->api;
		}
		
		/**
		 * Initialise Settings Form Fields
		 */
		public function init_form_fields() {
			$this->form_fields = include( 'settings-dhl.php' );
		}
		
		public function generate_settings_html( $form_fields = array(), $echo = false ) {
			parent::generate_settings_html( $form_fields );
			$this->check_connection();
			?>
			<script type="text/javascript">
				jQuery(document).ready(function(){
				    function dhl_order_status() {
                        if (jQuery('#woocommerce_dhl_auto_create').val() == 'auto') {
                            jQuery('#woocommerce_dhl_order_status').closest('tr').show();
                        }
                        else {
                            jQuery('#woocommerce_dhl_order_status').closest('tr').hide();
                        }
                    }
                    dhl_order_status();
                    jQuery('#woocommerce_dhl_auto_create').change(function () {
                        dhl_order_status();
                    });
				})
			</script>
			<?php
		}
		
		public function check_connection() {
			if ( isset( $this->login ) && isset( $this->password ) && $this->login != '' && $this->password != '' ) {
				$api = $this->get_api();
				try {
					$ping = $api->ping();
				}
				catch ( Exception $e ) {
					$class = 'notice notice-error';
					$message = sprintf( __( 'Komunikat API DHL: %s - %s', 'woocommerce-dhl' ), $e->getCode(), $e->getMessage() );					
					printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
				}
			}
		}

		public function calculate_shipping( $package = array() ) {
		}

	}
}
