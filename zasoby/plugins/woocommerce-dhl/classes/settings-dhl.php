<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Settings for flexible shipping
 */

$options_hours = array(
);
for ( $h = 9; $h < 20; $h++ ) {
	$options_hours[$h . ':00'] = $h . ':00';
	if ( $h < 19 ) {
		$options_hours[$h . ':30'] = $h . ':30';
	}
}

$order_statuses = wc_get_order_statuses();

$flexible_printing = apply_filters( 'flexible_printing', false );

$auto_print_description         = '';
$auto_print_custom_attributes = array();

if ( $flexible_printing ) {
	if ( $this->get_option( 'auto_print', '' ) == 'yes' ) {
		$flexible_printing_integration_url = apply_filters( 'flexible_printing_integration_url', 'dhl' );
		$auto_print_description = sprintf( __( 'Aby przejść do ustawień wydruku kliknij %stutaj%s.', 'woocommerce-dhl' ), '<a target="_blank" href="' . $flexible_printing_integration_url . '">', '</a>' );
	}
	else {
		$auto_print_description = __( 'Konfiguracja wydruku dostępna po włączeniu opcji i zapisaniu ustawień.', 'woocommerce-dhl' );
	}
}
else {
	$this->settings['auto_print'] = 'no';
	$flexible_printing_buy_url = 'https://www.wpdesk.pl/sklep/flexible-printing/';
	$auto_print_description     = sprintf(
		__( 'Drukuj etykiety bezpośrnio na drukarce (bez pobierania pliku) lub zapisuj automatycznie na dysku. %sKup Flexible Printing &rarr;%s.', 'woocommerce-dhl' ),
		'<a href="' . $flexible_printing_buy_url . '" target="_blank">',
		'</a>'
	);
	$auto_print_custom_attributes = array( 'disabled' => 'disabled' );
}

$settings = array(
	array(
		'title'     => __( 'Logowanie', 'woocommerce-dhl' ),
		'type'     => 'title',
		'description'     => '',
	),
	'login' => array(
		'title' 		=> __( 'Login', 'woocommerce-dhl' ),
		'type' 			=> 'text',
		'description' 	=> __( 'Login do serwisu DHL.', 'woocommerce-dhl' ),
		'default'		=> '',
		'desc_tip'		=> true,
		'custom_attributes' => array(
				'required' => 'required'
		)
	),
	'password' => array(
		'title' 		=> __( 'Hasło', 'woocommerce-dhl' ),
		'type' 			=> 'password',
		'description' 	=> __( 'Hasło do serwisu DHL.', 'woocommerce-dhl' ),
		'default'		=> '',
		'desc_tip'		=> true,
		'custom_attributes' => array(
				'required' => 'required'
		)
	),
	'test_mode' => array(
		'title' 		=> __( 'Tryb testowy', 'woocommerce-dhl' ),
		'label'			=> __( 'Włącz tryb testowy', 'woocommerce-dhl' ),
		'type' 			=> 'checkbox',
		'description'	=> __( 'Jeśli włączasz tryb testowy - wpisz Login i Hasło API do serwisu testowego, a nie do produkcyjnego. Serwis testowy dostępny jest pod adresem: <a target="_blank" href="https://sandbox.dhl24.com.pl/">https://sandbox.dhl24.com.pl/</a>', 'woocommerce-dhl' ),
		'default' 		=> 'no',
		'desc_tip'		=> false
	),
	array(
		'title'     	=> __( 'Tworzenie przesyłek', 'woocommerce-dhl' ),
		'type'     		=> 'title',
		'description'   => '',
	),
	'auto_create' => array(
		'title' 		=> __( 'Tworzenie przesyłek', 'woocommerce-dhl' ),
		'type' 			=> 'select',
		'default'		=> 'manual',
		'options'       => array(
			'manual'    => __( 'Ręczne', 'woocommerce-dhl' ),
			'auto'      => __( 'Automatyczne', 'woocommerce-dhl' ),
		),
	),
	'order_status' => array(
		'title' 		=> __( 'Status zamówienia', 'woocommerce-dhl' ),
		'type' 			=> 'select',
		'default'		=> 'wc-completed',
		'options'       => $order_statuses,
		'description'	=> __( 'Status zamówienia, przy którym do zamówienia zostanie automatycznie utworzona przesyłka.', 'woocommerce-dhl' ),
	),

    'complete_order' => array(
        'title' => __('Zrealizuj zamówienie', 'woocommerce-dhl'),
        'type' => 'checkbox',
        'label' => __('Włącz zmianę statusu zamówienia na Zrealizowane', 'woocommerce-dhl'),
        'default' => 'no',
        'description' => __('Po nadaniu przesyłek status zamówienia zostanie automatycznie zmieniony na Zrealizowane.', 'woocommerce-dhl'),
    ),

	array(
		'title'     	=> __( 'Domyślne ustawienia przesyłki', 'woocommerce-dhl' ),
		'type'     		=> 'title',
		'description'   => '',
	),
	'package_width' => array(
			'title' 		=> __( 'Długość paczki [cm]', 'woocommerce-dhl' ),
			'type' 			=> 'number',
			'default'		=> '',
			'custom_attributes' => array(
					'min' 	=> 0,
					'max' 	=> 10000,
					'step'	=> 1,
					'required' => 'required'
			)
	),
	'package_length' => array(
			'title' 		=> __( 'Szerokość paczki [cm]', 'woocommerce-dhl' ),
			'type' 			=> 'number',
			'default'		=> '',
			'custom_attributes' => array(
					'min' 	=> 0,
					'max' 	=> 10000,
					'step'	=> 1,
					'required' => 'required'
			)
	),
	'package_height' => array(
			'title' 		=> __( 'Wysokość paczki [cm]', 'woocommerce-dhl' ),
			'type' 			=> 'number',
			'default'		=> '',
			'custom_attributes' => array(
					'min' 	=> 0,
					'max' 	=> 10000,
					'step'	=> 1,
					'required' => 'required'
			)
	),
	'package_weight' => array(
			'title' 		=> __( 'Waga paczki [kg]', 'woocommerce-dhl' ),
			'type' 			=> 'number',
			'default'		=> '',
			'custom_attributes' => array(
					'min' 	=> 0,
					'max' 	=> 10000,
					'step'	=> 'any',
					'required' => 'required'
			)
	),
	'package_contents' => array(
			'title' 		=> __( 'Zawartość paczki', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> ''
	),
	'package_comment' => array(
			'title' 		=> __( 'Komentarz', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'description' 	=> __( 'Komentarz widoczny na etykiecie. Dostępne shortcody [order_number], [shop_name], [shop_url]', 'woocommerce-dhl' ),
			'default'		=> __( 'Zamówienie [order_number], [shop_name], [shop_url]' , 'woocommerce-dhl' ),
			'desc_tip'		=> false,
	),
	array(
			'title'     	=> __( 'Etykiety', 'woocommerce-dhl' ),
			'type'     		=> 'title',
			'description'   => ''
	),
	'label_format' => array(
		'title' 		=> __( 'Format etykiet', 'woocommerce-dhl' ),
		'type' 			=> 'select',
		'options'		=> array(
				'BLP'	=> __( 'Etykieta BLP' , 'woocommerce-dhl' ),
				'LBLP'	=> __( 'Etykieta BLP w formacie PDF A4', 'woocommerce-dhl' ),
				'ZBLP'	=> __( 'Etykieta BLP w formacie dla drukarek Zebra', 'woocommerce-dhl' ),
		),
		'default'		=> 'LBLP',
	),
	'auto_print' => array(
		'title' 		=> __( 'Drukowanie', 'woocommerce-dhl' ),
		'label'			=> __( 'Włącz automatyczne drukowanie', 'woocommerce-dhl' ),
		'type' 			=> 'checkbox',
		'description'	=> $auto_print_description,
		'custom_attributes' => $auto_print_custom_attributes,
		'default' 		=> 'no',
		'desc_tip'		=> false
	),
	array(
			'title'     	=> __( 'Dane nadawcy', 'woocommerce-dhl' ),
			'type'     		=> 'title',
			'description'   => '',
	),
	'account_number' => array(
		'title' 		=> __( 'Numer klienta (SAP)', 'woocommerce-dhl' ),
		'type' 			=> 'text',
		'default'		=> ''
	),
	'sender_name' => array(
			'title' 		=> __( 'Nazwa', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> '',
			'custom_attributes' => array(
					'required' => 'required'
			)
	),
	'sender_street' => array(
			'title' 		=> __( 'Adres', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> '',
			'custom_attributes' => array(
					'required' => 'required'
			)
	),
	'sender_house_number' => array(
			'title' 		=> __( 'Nr budynku', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> ''
	),
	'sender_apartment_number' => array(
			'title' 		=> __( 'Nr lokalu', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> ''
	),
	'sender_postal_code' => array(
			'title' 		=> __( 'Kod pocztowy', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> '',
			'custom_attributes' => array(
					'required' => 'required'
			)
	),
	'sender_city' => array(
			'title' 		=> __( 'Miasto', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> ''
	),
	'sender_contact_person' => array(
			'title' 		=> __( 'Osoba kontaktowa', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> '',
			'custom_attributes' => array(
					'required' => 'required'
			)
	),
	'sender_contact_phone' => array(
			'title' 		=> __( 'Telefon', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> '',
			'custom_attributes' => array(
					'required' => 'required'
			)
	),
	'sender_contact_email' => array(
			'title' 		=> __( 'E-mail', 'woocommerce-dhl' ),
			'type' 			=> 'text',
			'default'		=> get_option( 'admin_email' ),
			'custom_attributes' => array(
					'required' => 'required'
			)
	),
);
/*
if ( apply_filters( 'flexible_printing', false ) ) {
    $settings[] = 	array(
        'title'     	=> __( 'Drukowanie', 'woocommerce-dhl' ),
        'type'     		=> 'title',
        'description'   => '',
    );
    $settings['auto_print'] =  array(
        'title' 		=> __( 'Etykiety', 'woocommerce-dhl' ),
        'label'			=> __( 'Włącz automatyczne drukowanie', 'woocommerce-dhl' ),
        'type' 			=> 'checkbox',
        'description'	=> __( 'Etykieta zostanie automatycznie wydrukowana na wybranej drukarce', 'woocommerce-dhl' ),
        'default' 		=> 'no',
        'desc_tip'		=> false
    );
    $settings['printer'] =  array(
        'title' 		=> __( 'Drukarka', 'woocommerce-dhl' ),
        'type' 			=> 'select',
        'options'       => apply_filters( 'flexible_printing_printers', array() ),
        'description'	=> __( 'Drukarka, na której zostanie wydrukowana etykieta', 'woocommerce-dhl' ),
        'default' 		=> 'no',
        'desc_tip'		=> false
    );
}
*/

return $settings;
