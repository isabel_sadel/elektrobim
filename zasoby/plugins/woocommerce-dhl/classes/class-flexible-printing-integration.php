<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WPDesk_WooCommerce_DHL_Flexible_Printing_Integration' ) ) {
	class WPDesk_WooCommerce_DHL_Flexible_Printing_Integration extends Flexible_Printing_Integration {

		private $plugin = null;

		public function __construct( WPDesk_WooCommerce_DHL_Plugin $plugin ) {
			$this->plugin = $plugin;
			$this->id = 'dhl';
			$this->title = 'DHL';

			add_action( 'woocommerce_dhl_shipment_created', array( $this, 'woocommerce_dhl_shipment_created' ), 10, 2 );
			add_action( 'flexible_shipping_shipping_actions_html', array( $this, 'flexible_shipping_shipping_actions_html' ) );
		}

		public function woocommerce_dhl_shipment_created( WC_Order $order, $shipment ) {
			$shipping_method = $this->plugin->dhl->get_shipping_method();
			if ( $shipping_method->get_option( 'auto_print', 'no' ) == 'yes'
			) {
				$label_data = $this->plugin->dhl->get_label( $shipment->createShipmentsResult->item->shipmentId );
				try {
					$content_type = 'application/pdf';
					if ( $label_data->getLabelsResult->item->labelType == 'zpl' ) {
						$content_type = 'application/zpl';
					}
					$this->do_print(
						'dhl_' . $label_data->getLabelsResult->item->labelType . '_' . $label_data->getLabelsResult->item->labelName,
						base64_decode( $label_data->getLabelsResult->item->labelData ),
						$content_type,
						false
					);
				}
				catch ( Exception $e ) {
					error_log( sprintf( __( 'Blad drukowania: %s', 'woocommerce-dhl' ), $e->getMessage() ) );
				}
			}
		}

		public function options() {
			$options = array();
/*
			$options[] = array(
				'id' => 'auto_print',
				'name' => __('Automatyczne drukowanie', 'woocommerce-dhl'),
				'desc' => __('Włącz (po utworzeniu etykieta zostanie automatycznie wydrukowana)', 'woocommerce-dhl'),
				'type' => 'checkbox',
				'std' => '',
			);
*/
			return $options;
		}

		public function do_print_action( $args ) {
			$shipment_id = $args['data']['shipment_id'];
			$label_data = $this->plugin->dhl->get_label( $shipment_id );
			$content_type = 'application/pdf';
			if ( $label_data->getLabelsResult->item->labelType == 'ZBLP' ) {
				$content_type = 'application/zpl';
			}
			$args = array(
				'title'         => 'dhl_' . $label_data->getLabelsResult->item->labelType . '_' . $label_data->getLabelsResult->item->labelName,
				'content'       => base64_decode( $label_data->getLabelsResult->item->labelData ),
				'content_type'  => $content_type,
				'silent'        => false
			);
			do_action( 'flexible_printing_print', 'dhl', $args );
		}

		public function flexible_shipping_shipping_actions_html( $shipping ) {
			if ( isset( $shipping['integration'] ) && $shipping['integration'] == 'dhl' ) {
				$package = $shipping['integration_data'];
				if ( $package['dhl_status'] == 'ok' ) {
					echo apply_filters( 'flexible_printing_print_button', '', 'dhl',
						array(
							'content' => 'print',
							'icon'    => true,
							'id'      => $package['dhl_package']->createShipmentsResult->item->shipmentId,
							'tip'   => 'Print on: %s',
							'data'    => array(
								'post_id'     => $shipping['order_id'],
								'count'       => $shipping['count'],
								'shipment_id' => $package['dhl_package']->createShipmentsResult->item->shipmentId,
							),
						)
					);
				}
			}
		}

	}
}