<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WPDesk_WooCommerce_DHL' ) ) {
	class WPDesk_WooCommerce_DHL {

		static $instance = null;

		private $plugin = null;

		public static function get_instance( WPDesk_WooCommerce_DHL_Plugin $plugin ) {
			if ( self::$instance == null ) {
				self::$instance = new self( $plugin );
			}
			return self::$instance;
		}

		public static function get_products() {
			return array(
					'AH' 		=> __( 'Przesyłka krajowa', 'woocommerce-dhl' ),
					'09'		=> __( 'DHL Domestic Express 9', 'woocommerce-dhl' ),
					'12'		=> __( 'DHL Domestic Express 12', 'woocommerce-dhl' ),
					'EK'		=> __( 'Przesyłka 2Europe', 'woocommerce-dhl' ),
			);
		}

		public static function get_package_types() {
			return array(
					'ENVELOPE' 		=> __( 'Koperta', 'woocommerce-dhl' ),
					'PACKAGE'		=> __( 'Paczka', 'woocommerce-dhl' ),
					'PALLET'		=> __( 'Paleta', 'woocommerce-dhl' ),
			);
		}

		public function __construct( WPDesk_WooCommerce_DHL_Plugin $plugin ) {
			$this->plugin = $plugin;
			$this->hooks();
		}

		public function hooks() {

			add_filter( 'woocommerce_shipping_methods', array( $this, 'woocommerce_shipping_methods' ), 20, 1 );

			add_action( 'add_meta_boxes_shop_order', array( $this, 'add_meta_boxes_shop_order' ), 10, 2 );

			add_action( 'woocommerce_email_after_order_table', array( $this, 'woocommerce_email_after_order_table' ), 10, 2 );

			add_action( 'woocommerce_order_details_after_order_table', array( $this, 'woocommerce_order_details_after_order_table' ) );

			if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
				add_action( 'woocommerce_checkout_update_order_meta', array(
					$this,
					'woocommerce_checkout_update_order_meta'
				) );
			}
			else {
				add_action( 'woocommerce_checkout_create_order', array( $this, 'woocommerce_checkout_create_order' ) );
            }

			add_action( 'wp_ajax_wpdesk_dhl', array( $this, 'wp_ajax_wpdesk_dhl' ) );

			add_action( 'woocommerce_order_status_changed', array( $this, 'woocommerce_order_status_changed' ), 10, 3 );

            add_action( 'woocommerce_dhl_shipment_created', array( $this, 'woocommerce_dhl_shipment_created' ), 10, 2 );

		}

		public function woocommerce_order_status_changed( $order_id, $old_status, $new_status ) {
			$shipping_method = $this->get_shipping_method();
			$settings = $shipping_method->settings;
			if ( isset( $settings['auto_create'] ) && $settings['auto_create'] == 'auto' ) {
				if ( isset( $settings['order_status'] ) && 'wc-' . $new_status == $settings['order_status'] ) {
				    $order = wc_get_order( $order_id );
                    $_dhl = wpdesk_get_order_meta( $order, '_dhl', true );
                    if ( $_dhl != '' ) {
                        $this->create_shipments($order);
                    }
				}
			}
		}

		public function create_shipments( $order ) {
			$_dhl = wpdesk_get_order_meta( $order, '_dhl', true );
			if ( is_array( $_dhl ) ) {
				$shipping_method = $this->get_shipping_method();
				$settings = $shipping_method->settings;
				foreach ( $_dhl as $key => $package ) {
					$dhl_package = $package;
					if ( ! isset( $dhl_package['dhl_status'] ) || $dhl_package['dhl_status'] != 'ok' ) {
						try {
							$api = $this->get_api();
							for ( $i = -1; $i < 5; $i++ ) {
								$shipment_date = date( 'Y-m-d', time()+DAY_IN_SECONDS*$i );
								$week_day = date( 'D', strtotime( $shipment_date ) );
								if ( $week_day != 'Sun' ) {
									try {
										$shipment_date_postal_code = $api->get_postal_code_services( trim( str_replace( '-', '', $settings['sender_postal_code'] ) ), $shipment_date );
										if ( $shipment_date_postal_code->getPostalCodeServicesResult->drPickupFrom != 'brak' && $shipment_date_postal_code->getPostalCodeServicesResult->drPickupTo != 'brak' ) {
											$package['dhl_shipment_date'] = $shipment_date;
											break;
										}
									}
									catch ( Exception $e ) {
									}
								}
							}
							$dhl_package                   = $this->create_shipment( $order, $package );
							$package['dhl_status']         = 'ok';
							$package['dhl_package']        = $dhl_package;
							$package['dhl_package_number'] = $dhl_package->createShipmentsResult->item->shipmentId;
							$_dhl[ $key ]                  = $package;
							wpdesk_update_order_meta( $order, '_dhl', $_dhl );
							$order->add_order_note( sprintf( __( 'Do zamówienia została utworzona przesyłka %s. Szczegóły poniżej.', 'woocommerce-dhl' ), $package['dhl_package_number'] ), true );
						} catch ( Exception $e ) {
							$package['dhl_status']  = 'error';
							$package['dhl_message'] = $e->getMessage();
							$_dhl[ $key ]           = $package;
							wpdesk_update_order_meta( $order, '_dhl', $_dhl );
							wpdesk_update_order_meta( $order, '_flexible_shipping_status', 'failed' );
						}
					}
				}
			}
        }


		public function get_shipping_method() {
			$shipping_methods = WC()->shipping()->shipping_methods;
			if ( empty( $shipping_methods ) || !is_array( $shipping_methods ) || count( $shipping_methods ) == 0 ) {
				$shipping_methods = WC()->shipping()->load_shipping_methods();
			}
			return $shipping_methods['dhl'];
		}

		public function wp_ajax_wpdesk_dhl() {

			check_ajax_referer( 'dhl_ajax_nonce', 'security' );

			$ret = array();
			$ret['post'] = $_POST;
			$ret['get'] = $_GET;

			$one_time_message = false;

			$post = get_post( $_REQUEST['post_id'] );

			$dhl_action = $_REQUEST['dhl_action'];

			if ( $post ) {
				if ( $dhl_action == 'add_package_package' ) {
					$_dhl = get_post_meta( $post->ID, '_dhl', true);
					$package = false;
					$key = false;
					if ( $_dhl != '' ) {
						foreach ( $_dhl as $key => $package ) {
						}
					}
					if ( $package == false ) {
						$package = array();
					}
					if ( isset( $package['additional_packages'] ) ) {
						$additional_packages = $package['additional_packages'];
					}
					else {
						$additional_packages = array();
					}
					$additional_packages[] = array();
					$package['additional_packages'] = $additional_packages;
					if ( $key == false ) {
						$_dhl[] = $package;
					}
					else {
						$_dhl[$key] = $package;
					}
					update_post_meta( $post->ID, '_dhl', $_dhl );
				}

				if ( $dhl_action == 'add_package' ) {
					$_dhl = get_post_meta( $post->ID, '_dhl', true);
					$package = false;
					if ( $_dhl != '' ) {
						foreach ( $_dhl as $key => $package ) {
						}
					}
					if ( $package == false ) {
						$package = array();
					}
					unset( $package['dhl_status'] );
					unset( $package['dhl_message'] );
					$_dhl[] = $package;
					update_post_meta( $post->ID, '_dhl', $_dhl );
				}

				if ( $dhl_action == 'delete_package' ) {
					$one_time_message = __( 'Usunięcie przesyłki: przesyłka nie została odnaleziona.', 'woocommerce-dhl' );
					$_dhl = get_post_meta( $post->ID, '_dhl', true);
					$key = $_REQUEST['key'];
					if ( isset( $_dhl[$key] ) ) {
						unset( $_dhl[$key] );
						update_post_meta( $post->ID, '_dhl', $_dhl );
						$one_time_message = __( 'Przesyłka została usunięta.', 'woocommerce-dhl' );
					}
				}

				if ( $dhl_action == 'delete_package_package' ) {
					$one_time_message = __( 'Usunięcie przesyłki: przesyłka nie została odnaleziona.', 'woocommerce-dhl' );
					$_dhl = get_post_meta( $post->ID, '_dhl', true);
					$key = $_REQUEST['key'];
					$count = $_REQUEST['count'];
					if ( isset( $_dhl[$count] ) ) {
						if ( isset( $_dhl[$count]['additional_packages'] ) && isset( $_dhl[$count]['additional_packages'][$key] ) ) {
							unset( $_dhl[ $count ]['additional_packages'][ $key ] );
							update_post_meta( $post->ID, '_dhl', $_dhl );
							$one_time_message = __( 'Paczka została usunięta.', 'woocommerce-dhl' );
						}
					}
				}

				if ( $dhl_action == 'delete_created_package' ) {
					$_dhl = get_post_meta( $post->ID, '_dhl', true);
					$key = $_REQUEST['key'];
					if ( isset( $_dhl[$key] ) ) {
						$package = $_dhl[$key];
						if ( isset( $package['dhl_status'] ) &&  $package['dhl_status'] == 'ok' ) {
							$shipment_id = $package['dhl_package']->createShipmentsResult->item->shipmentId;;
							try {
								$delete_shipment = $this->delete_shipment( $shipment_id );
								if ( $delete_shipment->deleteShipmentsResult->item->error && $delete_shipment->deleteShipmentsResult->item->error ) {
									$package['dhl_one_time_message'] = $delete_shipment->deleteShipmentsResult->item->error;
									$_dhl[$key] = $package;
									update_post_meta( $post->ID, '_dhl', $_dhl );
									$one_time_message = false;
								}
								else {
									$package['dhl_one_time_message'] = sprintf( __( 'Przesyłka %s została anulowana', 'woocommerce-dhl' ), $shipment_id );
									unset( $package['dhl_status'] );
									unset( $package['dhl_package'] );
									unset( $package['dhl_package_number'] );
									$_dhl[$key] = $package;
									update_post_meta( $post->ID, '_dhl', $_dhl );
								}
							}
							catch ( Exception $e ) {
								$package['dhl_one_time_message'] = $e->getMessage();
								$_dhl[$key] = $package;
								update_post_meta( $post->ID, '_dhl', $_dhl );
							}
						}
					}
				}

				if ( $dhl_action == 'save_package' || $dhl_action == 'create_package' ) {
					$_dhl = get_post_meta( $post->ID, '_dhl', true);
					$key = $_REQUEST['key'];
					if ( isset( $_dhl[$key] ) ) {
						$package = $_dhl[$key];
						$package['dhl_product'] = $_REQUEST['dhl_product'];
						$package['dhl_package_type'] = $_REQUEST['dhl_package_type'];
						$package['dhl_package_weight'] = $_REQUEST['dhl_package_weight'];
						$package['dhl_package_width'] = $_REQUEST['dhl_package_width'];
						$package['dhl_package_length'] = $_REQUEST['dhl_package_length'];
						$package['dhl_package_height'] = $_REQUEST['dhl_package_height'];
						$package['dhl_insurance'] = $_REQUEST['dhl_insurance'];
						$package['dhl_insurance_value'] = $_REQUEST['dhl_insurance_value'];
						$package['dhl_cod'] = $_REQUEST['dhl_cod'];
						$package['dhl_cod_value'] = $_REQUEST['dhl_cod_value'];
						$package['dhl_shipment_date'] = $_REQUEST['dhl_shipment_date'];
						$package['dhl_content'] = $_REQUEST['dhl_content'];
						$package['dhl_comment'] = $_REQUEST['dhl_comment'];
						$additional_packages = array();
						if ( isset( $_REQUEST['additional_packages'] ) ) {
                            foreach ( $_REQUEST['additional_packages'] as $additional_package ) {
                                $additional_packages[] = $additional_package;
                            }
						}
                        $package['additional_packages'] = $additional_packages;
						$_dhl[$key] = $package;
						update_post_meta( $post->ID, '_dhl', $_dhl );

						if ( $dhl_action == 'create_package' ) {
							$order = wc_get_order( $post->ID );
							try {
								$dhl_package = $this->create_shipment( $order, $package );
								$package['dhl_status'] = 'ok';
								$package['dhl_package'] = $dhl_package;
								$package['dhl_package_number'] = $dhl_package->createShipmentsResult->item->shipmentId;
								$_dhl[$key] = $package;
								wpdesk_update_order_meta( $order, '_dhl', $_dhl );
								wpdesk_update_order_meta( $order, '_flexible_shipping_status', 'confirmed' );
								$order->add_order_note( sprintf( __( 'Do zamówienia została utworzona przesyłka %s. Szczegóły poniżej.', 'woocommerce-dhl' ), $package['dhl_package_number'] ), true );
							}
							catch ( Exception $e ) {
								$package['dhl_status'] = 'error';
								$package['dhl_message'] = $e->getMessage();
								$_dhl[$key] = $package;
								wpdesk_update_order_meta( $order, '_dhl', $_dhl );
								wpdesk_update_order_meta( $order, '_flexible_shipping_status', 'failed' );
							}
						}
					}
				}

				if ( $dhl_action == 'get_label' ) {
				    try {
                        $_dhl = get_post_meta( $post->ID, '_dhl', true );
                        $key = $_REQUEST['key'];
                        if ( isset( $_dhl[$key] ) ) {
                            $package = $_dhl[$key];
                            if ($package['dhl_status'] == 'ok') {
                                $shipment_id = $package['dhl_package']->createShipmentsResult->item->shipmentId;
                                $label_data = $this->get_label($shipment_id);
                                header('Content-Disposition: attachment; filename="' . $label_data->getLabelsResult->item->labelType . '_' . $label_data->getLabelsResult->item->labelName . '"');
                                header('Content-type: application/octet-stream');
                                echo base64_decode($label_data->getLabelsResult->item->labelData);
                            } else {
                                _e('Przesyłka nie została odnaleziona (1).', 'woocommerce-dhl');
                            }
                        }
                        else {
                            _e('Przesyłka nie została odnaleziona (2).', 'woocommerce-dhl');
                        }
                    }
                    catch ( Exception $e ) {
				        echo $e->getMessage();
                    }
					wp_die();
				}
			}

			ob_start();

			global $dhl_one_time_message;
			$dhl_one_time_message = $one_time_message;

			$this->dhl_meta_box( $post );

			$ret['content'] = ob_get_clean();

			echo json_encode( $ret );
			wp_die();
		}

		public function add_meta_boxes_shop_order( $post ) {
			$_dhl = get_post_meta( $post->ID, '_dhl', true);
			if ( $_dhl ) {
				add_meta_box( 'dhl_metabox', __( 'DHL', 'woocommerce-dhl' ), array( $this, 'dhl_meta_box'), 'shop_order', 'side' );
			}
		}

		public function dhl_meta_box( $post ) {
			global $dhl_one_time_message;
			$one_time_message = false;
			if ( !empty( $dhl_one_time_message ) ) {
				$one_time_message = $dhl_one_time_message;
			}
			if ( ! function_exists( 'woocommerce_form_field' ) ) {
				$wc_template_functions = trailingslashit( dirname( __FILE__) ) . '../../woocommerce/includes/wc-template-functions.php';
				if ( file_exists( $wc_template_functions ) ) {
					include_once( $wc_template_functions );
				}
			}

			wp_nonce_field( 'dhl_ajax_nonce', 'dhl_ajax_nonce' );

			$shipping_method = $this->get_shipping_method();
			$settings = $shipping_method->settings;

			if ( !isset( $settings['sender_postal_code'] ) ) {
				$settings['sender_postal_code'] = '';
			}

			if ( !isset( $settings['insurance'] ) ) {
				$settings['insurance'] = '';
			}

			if ( !isset( $settings['package_contents'] ) ) {
				$settings['package_contents'] = '';
			}

			if ( !isset( $settings['package_comment'] ) ) {
				$settings['package_comment'] = '';
			}

			$time = time();

			$shipment_date_options = array();
			$shipment_date_options_ex = array();

			$shipment_date_default = false;
			$shipment_date_default_ex = false;

			$api = $this->get_api();

			$order = wc_get_order( $post->ID );

			for ( $i = -1; $i < 5; $i++ ) {
				$shipment_date = date( 'Y-m-d', time()+DAY_IN_SECONDS*$i );
				$week_day = date( 'D', strtotime( $shipment_date ) );
				if ( $week_day != 'Sun' ) {
					try {
						$shipment_date_postal_code = $api->get_postal_code_services( trim( str_replace( '-', '', $settings['sender_postal_code'] ) ), $shipment_date );
						if ( $shipment_date_postal_code->getPostalCodeServicesResult->drPickupFrom != 'brak' && $shipment_date_postal_code->getPostalCodeServicesResult->drPickupTo != 'brak' ) {
							$shipment_date_options[$shipment_date] = $shipment_date;
						}
						if ( $shipment_date_postal_code->getPostalCodeServicesResult->exPickupFrom != 'brak' && $shipment_date_postal_code->getPostalCodeServicesResult->exPickupTo != 'brak' ) {
							$shipment_date_options_ex[$shipment_date] = $shipment_date;
						}
					}
					catch ( Exception $e ) {
					}
				}
			}

			foreach ( $shipment_date_options as $shipment_date ) {
				$week_day = date( 'D', strtotime( $shipment_date ) );
				if ( $week_day != 'Sat' ) {
					$shipment_date_default = $shipment_date;
					break;
				}
			}

			if ( count( $shipment_date_options ) == 0 ) {
				for ($i = 0; $i < 5; $i++ ) {
					$shipment_date = date( 'Y-m-d', time()+DAY_IN_SECONDS*$i );
					$shipment_date_options[$shipment_date] = $shipment_date;
				}
			}

			foreach ( $shipment_date_options_ex as $shipment_date ) {
				$week_day = date( 'D', strtotime( $shipment_date ) );
				if ( $week_day != 'Sat' ) {
					$shipment_date_default_ex = $shipment_date;
					break;
				}
			}

			if ( count( $shipment_date_options_ex ) == 0 ) {
				for ($i = 0; $i < 5; $i++ ) {
					$shipment_date = date( 'Y-m-d', time()+DAY_IN_SECONDS*$i );
					$shipment_date_options_ex[$shipment_date] = $shipment_date;
				}
			}

			$_dhl = get_post_meta( $post->ID, '_dhl', true);
			$count = 0;
			if ( is_array( $_dhl ) ) {
				foreach ( $_dhl as $key => $package ) {
					$count++;
					include('views/dhl-metabox-package.php');
				}
			}
			include('views/dhl-metabox-add-package.php');

			if ( is_array( $_dhl ) ) {
				foreach ( $_dhl as $key => $package ) {
					if ( isset( $package['dhl_one_time_message'] ) ) {
						unset( $package['dhl_one_time_message'] );
						$_dhl[$key] = $package;
						update_post_meta( $post->ID, '_dhl', $_dhl );
					}
				}
			}

		}

		public function get_tracking_url( $shipment_id ) {
			return 'http://www.dhl.com.pl/sledzenieprzesylkikrajowej/szukaj.aspx?m=0&sn=' . $shipment_id;
		}

		public function get_api() {
			$shipping_method = $this->get_shipping_method();
			return $shipping_method->get_api();
		}

		public function get_shipper( $settings ) {
			$shipper = array();

			$shipper['name'] = $settings['sender_name'];
			$shipper['postalCode'] = str_replace( '-', '', $settings['sender_postal_code'] );
			$shipper['city'] = $settings['sender_city'];
			$shipper['street'] = $settings['sender_street'];
			$shipper['houseNumber'] = $settings['sender_house_number'];
			$shipper['apartmentNumber'] = $settings['sender_apartment_number'];
			$shipper['contactPerson'] = $settings['sender_contact_person'];
			$shipper['contactPhone'] = $settings['sender_contact_phone'];
			$shipper['contactEmail'] = $settings['sender_contact_email'];

			return $shipper;
		}

		public function get_receiver( WC_Order $order ) {

			$receiver = array();

			$receiver['country'] = wpdesk_get_order_meta( $order, '_shipping_country', true );
			$receiver['name'] = '';
			if ( wpdesk_get_order_meta( $order, '_shipping_company', true ) != '' ) {
				$receiver['name'] .= wpdesk_get_order_meta( $order, '_shipping_company', true );
				$receiver['name'] .= ' ';
			}
			$receiver['name'] .= wpdesk_get_order_meta( $order, '_shipping_first_name', true );
			$receiver['name'] .= ' ';
			$receiver['name'] .= wpdesk_get_order_meta( $order, '_shipping_last_name', true );
			$receiver['postalCode'] = str_replace( '-', '', wpdesk_get_order_meta( $order, '_shipping_postcode', true ) );
			$receiver['city'] = wpdesk_get_order_meta( $order, '_shipping_city', true );
			$address = wpdesk_get_order_meta( $order, '_shipping_address_1', true );
			if ( wpdesk_get_order_meta( $order, '_shipping_address_2', true ) != '' ) {
				$address .= ' ' . wpdesk_get_order_meta( $order, '_shipping_address_2' , true );
			}
			$address_array = explode( ' ', trim( $address ) );
			$receiver['street'] = $address_array[0];
            $address_element = 1;
            while ( $address_element < count( $address_array ) - 1 ) {
                $receiver['street'] .= ' ' . $address_array[$address_element];
                $address_element++;
            }
            $receiver['houseNumber'] = $address_array[ count( $address_array ) - 1 ];
			$receiver['apartmentNumber'] = '';
			$receiver['contactPerson'] = wpdesk_get_order_meta( $order, '_shipping_first_name', true ) . ' ' . wpdesk_get_order_meta( $order, '_shipping_last_name', true );
			$receiver['contactPhone'] = wpdesk_get_order_meta( $order, '_billing_phone', true );
			$receiver['contactEmail'] = wpdesk_get_order_meta( $order, '_billing_email', true );

			return $receiver;

		}

		public function get_piece_list( $package ) {

			$piece_list = array();

			$piece = array();

			$piece['type'] = $package['dhl_package_type'];
			$piece['width'] = $package['dhl_package_width'];
			$piece['height'] = $package['dhl_package_height'];
			$piece['length'] = $package['dhl_package_length'];
			$piece['weight'] = $package['dhl_package_weight'];
			$piece['quantity'] = 1;

			$piece_list[] = $piece;

			if ( isset( $package['additional_packages'] ) ) {
				foreach ( $package['additional_packages'] as $additional_key => $additional_package ) {
					$piece = array();
					$piece['type'] = $additional_package['dhl_package_type'];
					$piece['width'] = $additional_package['dhl_package_width'];
					$piece['height'] = $additional_package['dhl_package_height'];
					$piece['length'] = $additional_package['dhl_package_length'];
					$piece['weight'] = $additional_package['dhl_package_weight'];
					$piece['quantity'] = 1;
					$piece_list[] = $piece;
				}
			}
			return $piece_list;
		}

		public function get_service( $package ) {
			$service = array();

			$service['product'] = $package['dhl_product'];
			$service['preaviso'] = true;

			if ( $package['dhl_cod'] == 1 ) {
				$service['collectOnDelivery'] = true;
				$service['collectOnDeliveryValue'] = $package['dhl_cod_value'];
				$service['collectOnDeliveryForm'] = 'BANK_TRANSFER';
			}

			if ( $package['dhl_insurance'] == 1 ) {
				$service['insurance'] = true;
				$service['insuranceValue'] = $package['dhl_insurance_value'];
			}

			return $service;
		}

		public function get_payment( $settings, $package ) {
			$payment = array();

			$payment['paymentMethod'] = 'BANK_TRANSFER';
			$payment['payerType'] = 'SHIPPER';
			$payment['accountNumber'] = $settings['account_number'];

			return $payment;
		}

		public function get_label( $shipment_id ) {
			$shipping_method = $this->get_shipping_method();
			$settings = $shipping_method->settings;

			$api = $this->get_api();

			$ret = $api->get_label( $shipment_id, $settings['label_format'] );

			return $ret;
		}

		public function delete_shipment( $shipment_id ) {

			$api = $this->get_api();

			$ret = $api->delete_shipment( $shipment_id );

			return $ret;

		}

		public function create_shipment( WC_Order $order, array $package ) {

			$shipping_method = $this->get_shipping_method();
			$settings = $shipping_method->settings;

			$shipment = array();

			$shipment['shipper'] = $this->get_shipper( $settings );

			$shipment['receiver'] = $this->get_receiver( $order );

			$shipment['pieceList'] = $this->get_piece_list( $package );

			$shipment['service'] = $this->get_service( $package );

			$shipment['payment'] = $this->get_payment( $settings, $package );

			if ( isset( $package['dhl_shipment_date'] ) ) {
				$shipment['shipmentDate'] = $package['dhl_shipment_date'];
			}

			$shipment['content'] = $package['dhl_content'];

			if ( isset( $package['dhl_comment'] ) ) {
				$shipment['comment'] = $package['dhl_comment'];
			}

			$shipment['skipRestrictionCheck'] = true;

			$api = $this->get_api();

			$ret = $api->create_shipment( array ( $shipment  ) );

			do_action( 'woocommerce_dhl_shipment_created', $order, $ret );

			wpdesk_update_order_meta( $order, '_flexible_shipping_status', 'confirmed' );

            return $ret;

		}

		public function woocommerce_email_after_order_table( $order, $sent_to_admin ) {
			$_dhl = get_post_meta( wpdesk_get_order_id( $order ), '_dhl', true );
			$dhl_packages = array();
			if ( $_dhl != '' ) {
				foreach ( $_dhl as $dhl_package ) {
					if ( isset( $dhl_package['dhl_status'] ) && $dhl_package['dhl_status'] == 'ok' ) {
						$dhl_packages[] = array(
								'shipment_id' 	=> $dhl_package['dhl_package_number'],
								'tracking_url'	=> $this->get_tracking_url( $dhl_package['dhl_package_number'] )
						);
					}
				}
			}
			if ( count( $dhl_packages ) ) {
				$args = array( 'dhl_packages' => $dhl_packages );
				echo $this->plugin->load_template( 'email_after_order_table', 'woocommerce', $args );
			}
		}

		public function woocommerce_order_details_after_order_table( $order ) {
			$_dhl = get_post_meta( wpdesk_get_order_id( $order ), '_dhl', true );
			$dhl_packages = array();
			if ( $_dhl != '' ) {
				foreach ( $_dhl as $dhl_package ) {
					if ( isset( $dhl_package['dhl_status'] ) && $dhl_package['dhl_status'] == 'ok' ) {
						$dhl_packages[] = array(
								'shipment_id' 	=> $dhl_package['dhl_package_number'],
								'tracking_url'	=> $this->get_tracking_url( $dhl_package['dhl_package_number'] )
						);
					}
				}
			}
			if ( count( $dhl_packages ) ) {
				$args = array( 'dhl_packages' => $dhl_packages );
				echo $this->plugin->load_template( 'order_details_after_order_table', 'woocommerce', $args );
			}
		}

		public function woocommerce_checkout_update_order_meta( $order_id ) {
			if ( function_exists( 'flexible_shipping_method_selected' ) ) {
				$shipping_method = flexible_shipping_method_selected( $order_id, 'dhl' );
				if ( false !== $shipping_method ) {
					$settings = $this->get_shipping_method()->settings;
					$order = wc_get_order( $order_id );
					$dhl_data = array( '1' => array() );
					$dhl_data['1']['dhl_product'] = $shipping_method['dhl_product'];
					$dhl_data['1']['dhl_package_type'] = $shipping_method['dhl_package_type'];
					$dhl_data['1']['dhl_insurance'] = $shipping_method['dhl_insurance'];
					$dhl_data['1']['dhl_insurance_value'] = $order->get_total();
					$dhl_data['1']['dhl_cod'] = $shipping_method['dhl_cod'];
					$dhl_data['1']['dhl_cod_value'] = $order->get_total();
					$dhl_data['1']['dhl_package_width'] = $settings['package_width'];
					$dhl_data['1']['dhl_package_length'] = $settings['package_length'];
					$dhl_data['1']['dhl_package_height'] = $settings['package_height'];
					$dhl_data['1']['dhl_package_weight'] = $settings['package_weight'];
					$dhl_data['1']['dhl_content'] = $settings['package_contents'];
					if ( wpdesk_get_order_meta( $order, '_payment_method', true ) == 'cod' ) {
						$dhl_data['1']['dhl_cod_value'] = $order->get_total();
						$dhl_data['1']['dhl_cod'] = 1;
					}
					update_post_meta( $order_id, '_dhl', $dhl_data );
					update_post_meta( $order_id, '_flexible_shipping_status', 'new' );
				}
			}
		}

		public function woocommerce_checkout_create_order( WC_Order $order ) {
			if ( function_exists( 'flexible_shipping_method_selected' ) ) {
				$shipping_method = flexible_shipping_method_selected( $order, 'dhl' );
				if ( false !== $shipping_method ) {
					$settings = $this->get_shipping_method()->settings;
					$dhl_data = array( '1' => array() );
					$dhl_data['1']['dhl_product'] = $shipping_method['dhl_product'];
					$dhl_data['1']['dhl_package_type'] = $shipping_method['dhl_package_type'];
					$dhl_data['1']['dhl_insurance'] = $shipping_method['dhl_insurance'];
					$dhl_data['1']['dhl_insurance_value'] = $order->get_total();
					$dhl_data['1']['dhl_cod'] = $shipping_method['dhl_cod'];
					$dhl_data['1']['dhl_cod_value'] = $order->get_total();
					$dhl_data['1']['dhl_package_width'] = $settings['package_width'];
					$dhl_data['1']['dhl_package_length'] = $settings['package_length'];
					$dhl_data['1']['dhl_package_height'] = $settings['package_height'];
					$dhl_data['1']['dhl_package_weight'] = $settings['package_weight'];
					$dhl_data['1']['dhl_content'] = $settings['package_contents'];
					if ( wpdesk_get_order_meta( $order, '_payment_method', true ) == 'cod' ) {
						$dhl_data['1']['dhl_cod_value'] = $order->get_total();
						$dhl_data['1']['dhl_cod'] = 1;
					}
					$order->update_meta_data( '_dhl', $dhl_data );
					$order->add_meta_data( '_flexible_shipping_status', 'new' );
				}
			}
        }


		public function woocommerce_shipping_methods( $methods ) {
			include_once( 'class-shipping-method.php' );
			$methods['dhl'] = 'WPDesk_WooCommerce_DHL_Shipping_Method';
			return $methods;
		}

        public function woocommerce_dhl_shipment_created( WC_Order $order, $shipment ) {
            $all_shipping_methods = WC()->shipping()->get_shipping_methods();
            $shipping_method = $all_shipping_methods['dhl'];
            if ( $shipping_method->get_option( 'complete_order', 'no' ) == 'yes' ) {
                $order->update_status( 'completed', __( 'Status zmieniony automatycznie po utworzeniu przesyłki - wtyczka DHL.', 'woocommerce-dhl' ) );
            }
        }


    }
}
