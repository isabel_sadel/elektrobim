<?php
/*
	Plugin Name: WooCommerce DHL
	Plugin URI: https://www.wpdesk.pl/sklep/dhl-woocommerce/
	Description: Integracja WooCommerce z DHL.
	Version: 1.1.1
	Author: WP Desk
	Author URI: https://www.wpdesk.net/
	Text Domain: woocommerce-dhl
	Domain Path: /languages/

	Copyright 2017 WP Desk Ltd.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

require_once( plugin_basename( 'inc/wpdesk-woo27-functions.php' ) );

$dhl_plugin_data = array(
    'plugin' => plugin_basename( __FILE__ ),
    'product_id' => 'WooCommerce DHL',
    'version'   => '1.1.1',
    'config_uri' => admin_url( 'admin.php?page=wc-settings&tab=shipping&section=dhl' )
);

require_once( plugin_basename( 'classes/wpdesk/class-plugin.php' ) );

class WPDesk_WooCommerce_DHL_Plugin extends WPDesk_Plugin_1_2 {

    private $script_version = '26';

    public $dhl = null;

    public $flexible_printing_integration = false;

	public function __construct( $plugin_data ) {
		$this->_plugin_namespace = 'woocommerce-dhl';
		$this->_plugin_text_domain = 'woocommerce-dhl';
		$this->_settings_url = admin_url( 'admin.php?page=wc-settings&tab=shipping&section=dhl' );

		$this->_plugin_has_settings = false;

		parent::__construct( $plugin_data );
	//	if ( $this->plugin_is_active() ) {WAŻNE
			$this->init();
	//	}WAŻNE
	}

	public function init() {
		include_once( 'classes/class-dhl-api.php' );
		include_once( 'classes/class-flexible-shipping-hooks.php' );
		include_once( 'classes/class-woocommerce-dhl.php' );
		$this->dhl = WPDesk_WooCommerce_DHL::get_instance( $this );
		new WPDesk_WooCommerce_DHL_FS_Hooks( $this );

	}

	public function hooks() {
		parent::hooks();
		add_action( 'admin_notices', array( $this, 'admin_notices' ) );
		add_filter( 'flexible_printing_integrations', array( $this, 'flexible_printing_integrations' ) );
	}

	public function flexible_printing_integrations( array $integrations ) {
		include_once( 'classes/class-flexible-printing-integration.php' );
		$this->flexible_printing_integration                      = new WPDesk_WooCommerce_DHL_Flexible_Printing_Integration( $this );
		$integrations[ $this->flexible_printing_integration->id ] = $this->flexible_printing_integration;
		return $integrations;
	}

	public function admin_enqueue_scripts( $hooq ) {
		$current_screen = get_current_screen();
        $suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
		wp_register_style( 'dhl_admin_css', $this->get_plugin_assets_url() . 'css/admin' . $suffix . '.css', array(), $this->script_version );
        if ( in_array( $current_screen->id, array( 'shop_order', 'edit-shop_order' ) ) ) {
		    wp_enqueue_style( 'dhl_admin_css' );

            wp_enqueue_script( 'dhl_admin_order_js', $this->get_plugin_assets_url() . 'js/admin-order' . $suffix . '.js' , array('jquery'), $this->script_version, true );

            wp_localize_script( 'dhl_admin_order_js', 'dhl_ajax_object', array(
                    'ajax_url' => admin_url( 'admin-ajax.php' ),
            ));
        }
	}

    public function admin_notices() {

        if ( is_plugin_active( 'flexible-shipping/flexible-shipping.php' ) ) return;

        $active_plugins = apply_filters( 'active_plugins', get_option('active_plugins' ) );

        $slug = 'flexible-shipping';
        $install_url = wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=' . $slug ), 'install-plugin_' . $slug );
        $activate_url = 'plugins.php?action=activate&plugin=' . urlencode( 'flexible-shipping/flexible-shipping.php' ) . '&plugin_status=all&paged=1&s&_wpnonce=' . urlencode( wp_create_nonce( 'activate-plugin_flexible-shipping/flexible-shipping.php' ) );

        $message = sprintf( wp_kses( __( 'Wtyczka WooCommerce DHL wymaga wtyczki Flexible Shipping. <a href="%s">Zainstaluj Flexible Shipping →</a>', $this->get_text_domain() ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $install_url ) );
        $is_downloaded = false;
        $plugins = array_keys( get_plugins() );
        foreach ( $plugins as $plugin ) {
            if ( strpos( $plugin, 'flexible-shipping/flexible-shipping.php' ) === 0 ) {
                $is_downloaded = true;
                $message = sprintf( wp_kses( __( 'Wtyczka WooCommerce DHL wymaga wtyczki Flexible Shipping. <a href="%s">Włącz Flexible Shipping →</a>', $this->get_text_domain() ), array(  'a' => array( 'href' => array() ) ) ), esc_url( admin_url( $activate_url ) ) );
            }
        }
        echo '<div class="error fade"><p>' . $message . '</p></div>' . "\n";
    }

	/**
	 * action_links function.
	 *
	 * @access public
	 * @param mixed $links
	 * @return void
	 */
	 public function links_filter( $links ) {

        $plugin_links = array(
            '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=shipping&section=dhl') . '">' . __( 'Ustawienia', 'woocommerce-dhl' ) . '</a>',
            '<a href="https://www.wpdesk.pl/docs/woocommerce-dhl-docs/">' . __( 'Dokumentacja', 'woocommerce-dhl' ) . '</a>',
            '<a href="https://www.wpdesk.pl/support/">' . __( 'Wsparcie', 'woocommerce-dhl' ) . '</a>',
        );

	     return array_merge( $plugin_links, $links );
    }

}

function dhl_init() {
	global $dhl_plugin_data;
	$GLOBALS['woocommerce_dhl'] = new WPDesk_WooCommerce_DHL_Plugin( $dhl_plugin_data );
}

add_action( 'plugins_loaded', 'dhl_init', 1 );

function dhl_plugin_activate() {
	global $wpdb;
	if ( get_option( 'dhl_convert_fs', '0' ) != '1' ) {
		$sql = "insert into {$wpdb->postmeta} ( post_id, meta_key, meta_value )
				( select post_id, '_flexible_shipping_integration', 'dhl'
					from {$wpdb->postmeta} m
					where m.meta_key = '_dhl'
					and not exists ( select 1 from {$wpdb->postmeta} m2 where m.post_id = m2.post_id and m2.meta_key = '_flexible_shipping_integration' and m2.meta_value = 'dhl' ) )";
		$wpdb->query( $sql );
		$sql = "insert into {$wpdb->postmeta} ( post_id, meta_key, meta_value )
					( select post_id, '_flexible_shipping_status', 'confirmed'
					from {$wpdb->postmeta} m where m.meta_key = '_dhl' and m.meta_value like '%\"dhl_status\";s:2:\"ok\"%'
					and not exists ( select 1 from {$wpdb->postmeta} m2 where m.post_id = m2.post_id and m2.meta_key = '_flexible_shipping_status' ) )";
		$wpdb->query( $sql );
		$sql = "insert into {$wpdb->postmeta} ( post_id, meta_key, meta_value )
					( select post_id, '_flexible_shipping_status', 'failed'
					from {$wpdb->postmeta} m where m.meta_key = '_dhl' and m.meta_value like '%\"dhl_status\";s:5:\"error\"%'
					and not exists ( select 1 from {$wpdb->postmeta} m2 where m.post_id = m2.post_id and m2.meta_key = '_flexible_shipping_status' ) )";
		$wpdb->query( $sql );
		$sql = "insert into {$wpdb->postmeta} ( post_id, meta_key, meta_value )
					( select post_id, '_flexible_shipping_status', 'new'
					from {$wpdb->postmeta} m where m.meta_key = '_dhl'
					and not exists ( select 1 from {$wpdb->postmeta} m2 where m.post_id = m2.post_id and m2.meta_key = '_flexible_shipping_status' ) )";
		$wpdb->query( $sql );
		update_option( 'dhl_convert_fs', '1' );
	}
}
register_activation_hook( __FILE__, 'dhl_plugin_activate' );
