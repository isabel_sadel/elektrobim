function dhl_ajax( dhl_action, count ) {
	var dhl_insurance = 0;
	if ( jQuery("#dhl_"+count+"_dhl_insurance").is(':checked') ) {
		dhl_insurance = jQuery("#dhl_"+count+"_dhl_insurance").val();
	}
	var dhl_cod = 0;
	if ( jQuery("#dhl_"+count+"_dhl_cod").is(':checked') ) {
		dhl_cod = jQuery("#dhl_"+count+"_dhl_cod").val();
	}

	var dhl_additional_packages = [];

	var additional_package_count = 0;

	jQuery(".dhl_additional_package").each(function(){
		var additional_key = jQuery(this).attr('data-key');
		var additional_package = {};
		additional_package.dhl_package_type = jQuery("#dhl_"+count+"_"+additional_key+"_dhl_package_type").val();
        additional_package.dhl_package_weight = jQuery("#dhl_"+count+"_"+additional_key+"_dhl_package_weight").val();
        additional_package.dhl_package_width = jQuery("#dhl_"+count+"_"+additional_key+"_dhl_package_width").val();
        additional_package.dhl_package_length = jQuery("#dhl_"+count+"_"+additional_key+"_dhl_package_length").val();
        additional_package.dhl_package_height = jQuery("#dhl_"+count+"_"+additional_key+"_dhl_package_height").val();
        dhl_additional_packages[additional_package_count] = additional_package;
        additional_package_count++;
	});

    console.log(JSON.stringify(dhl_additional_packages));

	var ajax_data = {
			'action'					: 'wpdesk_dhl',
			'dhl_action'				: dhl_action,
			'security'					: jQuery("#dhl_ajax_nonce").val(),
			'post_id'					: jQuery("#dhl_"+count+"_post_id").val(),
			'key'						: jQuery("#dhl_"+count+"_key").val(),
			'dhl_product'				: jQuery("#dhl_"+count+"_dhl_product").val(),
			'dhl_package_type'			: jQuery("#dhl_"+count+"_dhl_package_type").val(),
			'dhl_package_weight'		: jQuery("#dhl_"+count+"_dhl_package_weight").val(),
			'dhl_package_width'			: jQuery("#dhl_"+count+"_dhl_package_width").val(),
			'dhl_package_length'		: jQuery("#dhl_"+count+"_dhl_package_length").val(),
			'dhl_package_height'		: jQuery("#dhl_"+count+"_dhl_package_height").val(),
			'dhl_shipment_date'			: jQuery("#dhl_"+count+"_dhl_shipment_date").val(),
			'dhl_insurance'				: dhl_insurance,
			'dhl_insurance_value'		: jQuery("#dhl_"+count+"_dhl_insurance_value").val(),
			'dhl_cod'					: dhl_cod,
			'dhl_cod_value'				: jQuery("#dhl_"+count+"_dhl_cod_value").val(),
			'dhl_content'				: jQuery("#dhl_"+count+"_dhl_content").val(),
			'dhl_comment'				: jQuery("#dhl_"+count+"_dhl_comment").val(),
			'additional_packages' 		: dhl_additional_packages,
	};

	jQuery.ajax({ 
		url		: dhl_ajax_object.ajax_url,
		data	: ajax_data,
		method	: 'POST',
		dataType: 'JSON',
		success: function( data ) {
			console.log(data);
			if ( data != 0 ) {
				jQuery('#dhl_metabox>.inside').html(data.content);
				dhl_init();
			}			
		},
		error: function ( xhr, ajaxOptions, thrownError ) {
	        alert( xhr.status + ': ' + thrownError );
	    },
	    complete: function() {
	    	jQuery('.dhl-button').removeAttr('disabled','disabled');
	    	jQuery('.dhl-spinner').css({visibility: 'hidden'});	    	
	    }
	});
	
} 

jQuery(document).on('click','.dhl-button-create', function() {
	if ( jQuery(this).attr('disabled') == 'disabled' ) {
		return;
	} 
	jQuery('.dhl-button').attr('disabled','disabled');
	jQuery(this).parent().find('.spinner').css({visibility: 'visible'});
	
	var count = jQuery(this).attr('data-count');
	
	dhl_ajax( 'create_package', count );
})

jQuery(document).on('click','.dhl-button-save', function() {
	if ( jQuery(this).attr('disabled') == 'disabled' ) {
		return;
	} 
	jQuery('.dhl-button').attr('disabled','disabled');
	jQuery(this).parent().find('.spinner').css({visibility: 'visible'});
	
	var count = jQuery(this).attr('data-count');
	
	dhl_ajax( 'save_package', count );
	
})

jQuery(document).on('click','.dhl-button-delete', function() {
	if ( jQuery(this).attr('disabled') == 'disabled' ) {
		return;
	} 
	jQuery('.dhl-button').attr('disabled','disabled');
	jQuery(this).parent().find('.spinner').css({visibility: 'visible'});
	
	var count = jQuery(this).attr('data-count');
	
	dhl_ajax( 'delete_package', count );
	
})

jQuery(document).on('click','.dhl-button-delete-package', function() {
    if ( jQuery(this).attr('disabled') == 'disabled' ) {
        return;
    }
    jQuery(this).parent().remove();
})

jQuery(document).on('click','.dhl-button-delete-created', function() {
	if ( jQuery(this).attr('disabled') == 'disabled' ) {
		return;
	} 
	jQuery('.dhl-button').attr('disabled','disabled');
	jQuery(this).parent().find('.spinner').css({visibility: 'visible'});
	
	var count = jQuery(this).attr('data-count');
	
	dhl_ajax( 'delete_created_package', count );
	
})

jQuery(document).on('click','.dhl-button-add', function() {
	if ( jQuery(this).attr('disabled') == 'disabled' ) {
		return;
	} 
	jQuery('.dhl-button').attr('disabled','disabled');
	jQuery(this).parent().find('.spinner').css({visibility: 'visible'});

	var ajax_data = {
				'action'		: 'wpdesk_dhl',
				'dhl_action'	: 'add_package',
				'security'		: jQuery("#dhl_ajax_nonce").val(),
				'post_id'		: jQuery("input[name=post_id]").val(),
	};
	
	jQuery.ajax({ 
		url		: dhl_ajax_object.ajax_url,
		data	: ajax_data,
		method	: 'POST',
		dataType: 'JSON',
		success: function( data ) {
			console.log(data);
			if ( data != 0 ) {
				jQuery('#dhl_metabox>.inside').html(data.content);
				dhl_init();
			}
		},
		error: function ( xhr, ajaxOptions, thrownError ) {
	        alert( xhr.status + ': ' + thrownError );
	    },
	    complete: function() {
	    	jQuery('.dhl-button').removeAttr('disabled','disabled');
	    	jQuery('.dhl-spinner').css({visibility: 'hidden'});	    	
	    }
	});
})

jQuery(document).on('click','.dhl-button-add-package', function() {
	var count = jQuery(this).attr('data-count');
    var count_additional = parseInt(jQuery(this).attr('data-count_additional'))+1;
    jQuery(this).attr('data-count_additional',count_additional);
    var html = jQuery('#dhl_additional_package_template_' + count).html()
    html = html.split('_additional_key_').join(count_additional);
    html = html.split('_count_').join(count);
    jQuery(this).parent().before(html);
    jQuery(this).parent().before('<hr/>');
})

jQuery(document).on('click','.dhl-button-print', function() {
    if ( jQuery(this).attr('disabled') == 'disabled' ) {
        return;
    }
    jQuery('.dhl-button').attr('disabled','disabled');
    jQuery(this).parent().find('.spinner').css({visibility: 'visible'});
    var count = jQuery(this).attr('data-count');

    var ajax_data = {
        'action'		: 'wpdesk_dhl',
        'dhl_action'	: 'print_label',
        'security'		: jQuery("#dhl_ajax_nonce").val(),
        'post_id'		: jQuery("input[name=post_id]").val(),
        'shipment_id'	: jQuery(this).attr('data-shipment-id'),
		'key'			: jQuery("#dhl_"+count+"_key").val(),
    };

    jQuery.ajax({
        url		: dhl_ajax_object.ajax_url,
        data	: ajax_data,
        method	: 'POST',
        dataType: 'JSON',
        success: function( data ) {
        	console.log(data);
			jQuery('#print_message_' + data.post.shipment_id).html(data.message);
        },
        error: function ( xhr, ajaxOptions, thrownError ) {
            alert( xhr.status + ': ' + thrownError );
        },
        complete: function() {
            jQuery('.dhl-button').removeAttr('disabled','disabled');
            jQuery('.dhl-spinner').css({visibility: 'hidden'});
        }
    });
})

jQuery(document).on('click','.dhl-button-label', function() {
	var shipment_id = jQuery(this).attr('data-shipment-id');
	var url = dhl_ajax_object.ajax_url + '?action=wpdesk_dhl&dhl_action=get_label&security=' + jQuery("#dhl_ajax_nonce").val();
	url = url + '&shipment_id=' + shipment_id;
	url = url + '&post_id=' + jQuery("input[name=post_id]").val();
	var count = jQuery(this).attr('data-count');
	url = url + '&key=' + jQuery("#dhl_"+count+"_key").val(),
	window.open( url, '_blank');
})

jQuery(document).on('change','.dhl-package-type', function() {
	if ( jQuery(this).val() == 'ENVELOPE' ) {
		jQuery(this).parent().parent().find('.dhl-weight').hide();
		jQuery(this).parent().parent().find('.dhl-dimensions').hide();
	}
	else {
		jQuery(this).parent().parent().find('.dhl-weight').show();
		jQuery(this).parent().parent().find('.dhl-dimensions').show();
	}
})

jQuery(document).on('change','.dhl-insurance', function() {
	if ( jQuery(this).is(':checked') ) {
		jQuery(this).closest('.dhl-package').find('.dhl-insurance-value').show();
	}
	else {
		jQuery(this).closest('.dhl-package').find('.dhl-insurance-value').hide();
	}
})

jQuery(document).on('change','.dhl-cod', function() {
	if ( jQuery(this).is(':checked') ) {
		jQuery(this).closest('.dhl-package').find('.dhl-cod-value').show();
	}
	else {
		jQuery(this).closest('.dhl-package').find('.dhl-cod-value').hide();
	}
})

function dhl_init() {
	jQuery('.dhl-package-type').each(function(){
		jQuery(this).change();
	})
	jQuery('.dhl-insurance').each(function(){
		jQuery(this).change();
	})
	jQuery('.dhl-cod').each(function(){
		jQuery(this).change();
	})
	jQuery('.dhl-disabled').each(function(){
		jQuery(this).prop('disabled',true);
	})
}

dhl_init();
