*** WooCommerce DHL Changelog ***

1.1.1 - 2017-04-13
* Poprawienie aktywacji wtyczki w WP Desk Helper

1.1 - 2017-04-06
* Dodanie wsparcia dla WooCommerce 3.0
* Porzucenie wsparcia dla wersji WooCommerce poniżej 2.5 (wtyczka może dalej działać z wcześniejszymi wersjami, ale nie deklarujemy oficjalnego wsparcia i nie testowaliśmy wtyczki z tymi wersjami)
* Dodanie możliwości automatycznego nadawania przesyłek
* Dodanie możliwości nadawania przesyłek przez Masowe działania
* Dodanie możliwości automatycznej zmiany statusu zamówienia na Zrealizowane po nadaniu przesyłki
* Dodanie obsługi wielu paczek dla jednej przesyłki
* Dodanie integracji z Flexible Printing

1.0.2 - 2016-12-20
* Poprawki związane z nadawcą paczek (niekompletne dane adresowe, dane płatnika)
* Warunkowe ładowanie CSS i JS w panelu admina, tylko wtedy, gdy są używane

1.0.1 - 2016-10-14
* Drobne poprawki kompatybilności

1.0 - 2016-10-14
* Pierwsze wydanie!
