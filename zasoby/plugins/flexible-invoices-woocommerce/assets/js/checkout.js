jQuery('#invoice_ask').change(function() {
	if (jQuery('#invoice_ask').is(':checked') ) {
		jQuery('#vat_number').parent().slideDown();
	}
	else {
		jQuery('#vat_number').parent().slideUp();
	}
});
jQuery(document).ready(function() {
	jQuery('#invoice_ask').trigger('change');
})
