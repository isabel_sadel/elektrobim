<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    class invoiceWoocommerceOrder extends inspire_pluginDependant3
    {
        public function __construct($plugin)
        {
            parent::__construct($plugin);

            // for ajax billing field load

            add_filter( 'woocommerce_admin_order_actions', array( $this, 'orderStatusFilter') ,10,3);

            add_action('wp_ajax_woocommerce-invoice-generate-invoice', array( $this, 'generateInvoiceAction') );
            add_action('wp_ajax_woocommerce-invoice-get-invoice', array( $this, 'getInvoiceAction') );
            add_action('wp_ajax_woocommerce-invoice-get-pdf-invoice', array( $this, 'getInvoicePdfAction') );

            add_action('woocommerce_order_status_changed', array($this, 'generateAndSendEmailWhenOrderStatusChangedAction'), 80, 3);
            add_action('woocommerce_order_status_changed', array($this, 'changeInvoiceStatusWhenOrderStatusChangesAction'), 85, 3);

            add_action( 'woocommerce_admin_order_data_after_billing_address', array( $this, 'woocommerce_admin_order_data_after_billing_address' ) );
            add_action( 'woocommerce_process_shop_order_meta', array( $this, 'woocommerce_process_shop_order_meta' ), 60, 2 );

            add_action( 'admin_enqueue_scripts', array( $this, 'admin_enqueue_scripts' ) );

            // free the order :)
            add_action( 'before_delete_post', array( $this, 'freeTheOrderAction' ) );

            add_action( 'add_meta_boxes', array( $this, 'add_meta_boxes'), 10 );
        }

        function admin_enqueue_scripts() {
        	$screen = get_current_screen();
			if ( $screen->id == 'edit-shop_order' ) {
				wp_register_script( 'inspire_invoice_orders', $this->getPlugin()->getPluginUrl() . 'assets/js/orders.js', array('jquery') ,'2.1', true);
				wp_enqueue_script( 'inspire_invoice_orders' );
			}
        }

        function woocommerce_process_shop_order_meta( $post_id, $post ) {
        	$_billing_invoice_ask = 0;
        	if ( isset( $_POST['_billing_invoice_ask'] ) && $_POST['_billing_invoice_ask'] == 'yes' ) {
        		$_billing_invoice_ask = 1;
        	}
        	wpdesk_update_order_meta( $post_id, '_billing_invoice_ask', $_billing_invoice_ask );
        	if ( isset( $_POST['_billing_vat_number'] ) ) {
        		$_billing_vat_number = $_POST['_billing_vat_number'];
        	}
	        wpdesk_update_order_meta( $post_id, '_billing_vat_number', $_billing_vat_number );
        	$_billing_vat_number = '';
        }

        function woocommerce_admin_order_data_after_billing_address( $order ) {
        	$billing_invoice_ask_display = __( 'No', 'flexible-invoices-woocommerce' );
        	$billing_vat_number = wpdesk_get_order_meta( $order, '_billing_vat_number', true);
        	$billing_invoice_ask = wpdesk_get_order_meta( $order, '_billing_invoice_ask', true);
        	if ( $billing_invoice_ask == '1' ) {
        		$billing_invoice_ask_display = __( 'Yes', 'flexible-invoices-woocommerce' );
        	}
        	$billing_invoice_ask_field = array(
        		'id'		=> '_billing_invoice_ask',
        		'label' 	=> __('I want an invoice', 'flexible-invoices-woocommerce'),
                'show'  	=> true,
            	'type'		=> 'checkbox',
            	'class'   	=> 'select short',
        		'value'		=> ( $billing_invoice_ask == '1' ) ? 'yes' : 'no',
        		'style' 	=> 'width:auto;',
            );
        	$billing_vat_number_field = array(
        		'id'			=> '_billing_vat_number',
                'label' 		=> $this->getSettingValue( 'nip_label' ),
            	'wrapper_class' => '_billing_last_name_field',
        		'show'  		=> true
            );

        	include( 'views/admin_order_data_after_billing_address.php' );
        }

        public function freeTheOrderAction($id)
        {
            global $post_type;
            if ( $post_type == 'inspire_invoice' )
            {
                $order_id = get_post_meta($id, '_wc_order_id', true);
                delete_post_meta($order_id, '_invoice_generated');
            }
        }

        public function changeInvoiceStatusWhenOrderStatusChangesAction($id, $old, $new)
        {
            if ($new == 'completed' && $this->getSettingValue('auto_paid_status') == 'on')
            {
                $order = new WC_Order($id);
                $invoice = InvoicesWoocommerce::getInstance()->invoiceWoocommerceOrder->fetchInvoiceForOrder($order, false);
                if (!empty($invoice))
                {
                    $invoice->setTotalPaid($invoice->getTotalPrice());
                    $invoice->setPaymentStatus('paid');
                    $invoice->save();
                }
            }
        }

        public function generateAndSendEmailWhenOrderStatusChangedAction($id, $old, $new)
        {
            //if ( get_post_meta($id, '_invoice_generated', true) == 0 )
	        if ( wpdesk_get_order_meta( $id, '_invoice_generated', true ) == 0 )
            {
                if ($this->getSettingValue('send_invoice_mail_when_status') == $new)
                {
                	if ( $new == 'completed' ) {
                		//$_completed_date = get_post_meta( $id, '_completed_date', true );
		                $_completed_date = wpdesk_get_order_meta( $id, '_completed_date', true );
                		if ( $_completed_date == '' ) {
                			//update_post_meta( $id, '_completed_date', current_time('mysql') );
			                wpdesk_update_order_meta( $id, '_completed_date', current_time('mysql') );
                		}
                	}
                	if ( $this->getSettingValue('add_invoice_ask_field') != 'on'
                		|| ( $this->getSettingValue('add_invoice_ask_field') == 'on'
		                 //    && get_post_meta($id, '_billing_invoice_ask', true) == '1'
		                     && wpdesk_get_order_meta( $id, '_billing_invoice_ask', true ) == '1'
	                     )
                	) {
	                    $order = new WC_Order($id);

	                    if ( $this->getSettingValue('zero_invoice') != 'on'
	                    	|| ($this->getSettingValue('zero_invoice') == 'on' && $order->get_total() != 0 )
	                    ) {

		                    $invoice = $this->getPlugin()->generateInvoiceForOrder($order);
		                    //update_post_meta(wpdesk_get_order_id( $order ), '_invoice_generated', $invoice->getId());
		                    wpdesk_update_order_meta( $order, '_invoice_generated', $invoice->getId() );

		                    $email = wpdesk_get_order_meta( $order, 'billing_email', true );
		                    if (empty($email))
		                    {
		                        //$client_id = get_post_meta( $id, '_customer_user', true);
			                    $client_id = wpdesk_get_order_meta( $id, '_customer_user', true );
		                        $user = new WP_User($client_id);
		                        $email = $user->user_email;
		                    }
		                    $invoice->sendByEmail($email);
	                    }
                	}
                }
            }


        }

        /**
         *
         * @param bool $billInvoice true if invoice
         */
        private function generateInvoice($billInvoice = true)
        {
            $orderId = intval($_GET['order_id']);
            //$order = new WC_Order($orderId);
	        $order = wc_get_order( $orderId );

            //if (get_post_meta($orderId, '_invoice_generated', true) > 0)
	        if ( wpdesk_get_order_meta( $orderId, '_invoice_generated', true ) > 0 )
            {
                $result = array(
                        'code' => 101,
                        'invoice_number' => $invoice_number,
                        'result' => 'OK'
                );
            } else { // save invoice_number
                $invoice = $this->getPlugin()->generateInvoiceForOrder($order);

                //update_post_meta($orderId, '_invoice_generated', $invoice->getId());
	            wpdesk_update_order_meta( $order, '_invoice_generated', $invoice->getId() );

                $result = array(
                        'code' => 100,
                        'invoice_number' => $invoice->getNumber(),
                        'result' => 'OK'
                );
            }

            if ($invoice instanceof InvoicePost) // success invoice
            {
            	if ( isset( $_GET['single_order'] ) ) {
            		//$edit_url = admin_url( 'post.php?post=' . get_post_meta( $orderId, '_invoice_generated', true ) . '&action=edit' );
		            $edit_url = admin_url( 'post.php?post=' . wpdesk_get_order_meta( $orderId, '_invoice_generated', true ) . '&action=edit' );
            		$invoice_number = $invoice->getFormattedInvoiceNumber();
            		$result['newbtn_edit'] = '<a target="_blank" href="' . $edit_url . '" class="edit-invoice">' . $invoice_number . '</a>';
            		$result['newbtn'] = '<a target="_blank" href="' . site_url() . '/wp-admin/admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&amp;order_id=' . $orderId . '" class="button view-invoice">' . __( 'View Invoice', 'flexible-invoices-woocommerce' ) . '</a>';
            		$result['newbtn_download'] = '<a target="_blank" href="' . site_url() . '/wp-admin/admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&amp;order_id=' . $orderId . '&save_file=1" class="button get-invoice">' . __( 'Download Invoice', 'flexible-invoices-woocommerce' ) . '</a>';
            	}
            	else {
            		$result['newbtn'] = '<a target="_blank" href="' . site_url() . '/wp-admin/admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&amp;order_id=' . $orderId . '" class="button tips dashicons view-invoice">' . __( 'View Invoice', 'flexible-invoices-woocommerce' ) . '</a>';
            		$result['newbtn_download'] = '<a target="_blank" href="' . site_url() . '/wp-admin/admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&amp;order_id=' . $orderId . '&save_file=1" class="button tips dashicons get-invoice">' . __( 'Download Invoice', 'flexible-invoices-woocommerce' ) . '</a>';
            	}
            }

            return $result;
        }

        /**
         * generates new invoice and invoice_number
         *
         * admin ajax action
         */
        public function generateInvoiceAction()
        {
            header('Content-Type: application/json');
            $result = $this->generateInvoice(true);
            die(json_encode($result));
        }

        /**
         * admin ajax action
         */
        public function getInvoiceAction()
        {
            $order = new WC_Order(intval($_GET['order_id']));
            $invoice = $this->fetchInvoiceForOrder($order);

            echo $this->loadTemplate('generated_invoice', 'invoice', array(
                'order' => $order,
                'invoice' => $invoice
            ));
            die();
        }

        /**
         *
         * @param WC_Order $order
         * @return InvoicePost
         */
        public function fetchInvoiceForOrder(WC_Order $order, $force = true)
        {

            if ($order instanceof WC_Order)
            {
                $order_id = wpdesk_get_order_id( $order );
            } else {
                $order_id = $order;
            }

            //$id = get_post_meta($order_id, '_invoice_generated', true);
	        $id = wpdesk_get_order_meta($order_id, '_invoice_generated', true);

            $post = get_post($id);
            if (empty($post) || empty($post->ID))
            {
                $id = null;

                if ($force)
                {


                    //update_post_meta($order_id, '_invoice_generated', 0);
	                wpdesk_update_order_meta( $order_id, '_invoice_generated', 0 );

                    $invoice = $this->getPlugin()->generateInvoiceForOrder( new WC_Order($order_id) );
                    //update_post_meta($order_id, '_invoice_generated', $invoice->getId());
	                wpdesk_update_order_meta( $order_id, '_invoice_generated', $invoice->getId() );
                    $id = $invoice->getId();
                }
            }

            if (!empty($id))
            {
                $post = get_post( $id );
                if ( ! $post ) {
            		wp_die( __( 'This document does not exist or was deleted.', 'flexible-invoices-woocommerce' ) );
            	}
            	return Invoice::getInstance()->invoicePostType->invoiceFactory($id);
            }

        }

        /**
         * admin ajax action
         */
        public function getInvoicePdfAction()
        {
        	$id = $_GET['order_id'];
        	$invoice = $this->fetchInvoiceForOrder( new WC_Order($id) );
            do_action('wp_ajax_invoice-get-pdf-invoice', $invoice->getId() );
        }

        /**
         *
         * @param WC_Order $order
         * @return string
         */
        public function orderStatusFilter($actions, WC_Order $order)
        {
            global $woocommerce;


            //if ( get_post_meta( wpdesk_get_order_id( $order ), '_invoice_generated', true ) > 0 )
	        if ( wpdesk_get_order_meta( $order, '_invoice_generated', true ) > 0 )
            {
                $actions[] = array(
                        'url' => wp_nonce_url( admin_url('admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&order_id=' . wpdesk_get_order_id( $order ) ) ),
                        'name' => __( 'View Invoice', 'flexible-invoices-woocommerce' ),
                        'action' => 'dashicons view-invoice'
                );
                $actions[] = array(
                        'url' => wp_nonce_url( admin_url('admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&order_id=' . wpdesk_get_order_id( $order ) . '&save_file=1' )),
                        'name' => __( 'Download Invoice', 'flexible-invoices-woocommerce' ),
                        'action' => 'dashicons get-invoice'
                );
            } else {
                $actions[] = array(
                        'url' => wp_nonce_url( admin_url('admin-ajax.php?action=woocommerce-invoice-generate-invoice&order_id=' . wpdesk_get_order_id( $order ) ) ),
                        'name' => __( 'Issue Invoice', 'flexible-invoices-woocommerce' ),
                        'action' => 'dashicons generate-invoice'
                );
            }

            return $actions;
        }

        public function add_meta_boxes(){

        	add_meta_box(
        			'flexible-invoices-woocommerce',
        			__( 'Invoice', 'flexible-invoices-woocommerce' ),
        			array( $this, 'order_meta_box' ),
        			'shop_order',
        			'side',
        			'core'
        	);

        }

        public function order_meta_box( $post ) {

        	$order = new WC_Order( $post->ID );

        	if (wpdesk_get_order_meta( $order, '_invoice_generated', true) > 0)
        	{        		
        		$actions[] = array(
        				'url' => wp_nonce_url( admin_url('admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&order_id=' . wpdesk_get_order_id( $order ))),
        				'name' => __( 'View Invoice', 'flexible-invoices-woocommerce' ),
        				'action' => 'button view-invoice'
        		);
        		$actions[] = array(
        				'url' => wp_nonce_url( admin_url('admin-ajax.php?action=woocommerce-invoice-get-pdf-invoice&order_id=' . wpdesk_get_order_id( $order ) . '&save_file=1' )),
        				'name' => __( 'Download Invoice', 'flexible-invoices-woocommerce' ),
        				'action' => 'button get-invoice'
        		);
        	} else {
        		$actions[] = array(
        				'url' => wp_nonce_url( admin_url('admin-ajax.php?action=woocommerce-invoice-generate-invoice&order_id=' . wpdesk_get_order_id( $order ) . '&single_order=1')),
        				'name' => __( 'Issue Invoice', 'flexible-invoices-woocommerce' ),
        				'action' => 'button generate-invoice'
        		);
        	}

        	if ( wpdesk_get_order_meta( $order, '_invoice_generated', true ) > 0) {
        		$invoice = Invoice::getInstance()->invoicePostType->invoiceFactory( wpdesk_get_order_meta( $order, '_invoice_generated', true ) );
        		$edit_url = admin_url( 'post.php?post=' . wpdesk_get_order_meta( $order, '_invoice_generated', true ) . '&action=edit' );
        		$invoice_number = $invoice->getFormattedInvoiceNumber();        		
        		echo '<a href="' . $edit_url . '" class="edit-invoice">';
        		echo $invoice_number;
        		echo '</a> ';
        		echo '<br/><br/>';
        	}
        	
        	foreach ( $actions as $action ) {
        		echo '<a target="blank" href="' . $action['url'] . '" class="' . $action['action'] . '">';
        		echo $action['name'];
        		echo '</a> ';
        	}

        }

    }
