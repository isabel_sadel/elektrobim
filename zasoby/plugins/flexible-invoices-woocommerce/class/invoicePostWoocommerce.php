<?php

	class InvoicePostWoocommerce extends InvoicePost
	{
		protected $_wc_order; // WC_Order
		protected $_wc_order_id;
		protected $_add_order_id;

		public static function initWoocommercePostType()
		{
		    // inject class
		    add_filter('inspire_invoices_invoice_post_class', array('InvoicePostWoocommerce', 'injectInvoicePostClass'));

		    // hook into woocommerce emails
		    add_action('woocommerce_email_before_order_table', array('InvoicePostWoocommerce', 'addInvoiceToWoocommerceEmailAction'), 80, 3);

		    // payment methods
		    add_filter('inspire_invoices_payment_methods', array( 'InvoicePostWoocommerce', 'addWoocommercePaymentMethodsFilter') );

		    // currencies
		    add_filter('inspire_invoices_payment_currencies', array( 'InvoicePostWoocommerce', 'addWoocommerceCurrenciesFilter') );

		    // vat rates
		    add_filter('inspire_invoices_vat_types', array( 'InvoicePostWoocommerce', 'addWoocommerceCurrenciesVatTypesFilter' ));

		    // client data
		    add_filter('inspire_invoices_client_data', array( 'InvoicePostWoocommerce', 'addWoocommerceClientDataFilter' ), 80, 2 );

		    // save custom fields
		    add_action('inspire_invoices_before_save_invoice_custom_fields', array( 'InvoicePostWoocommerce', 'beforeSaveInvoiceCustomFieldsAction' ), 80, 1);

		    // add send mail option
		    add_action('inspire_invoices_after_display_options_metabox_actions', array( 'InvoicePostWoocommerce', 'addSendMailOptionAction'));

		    // add 'add order id' option
		    add_action('inspire_invoices_after_display_payment_metabox_actions', array( 'InvoicePostWoocommerce', 'addPaymentOptionsAction'));

		    // add order id in invoices list column
		    add_action( 'inspire_invoices_display_order_column_for_invoice', array( 'InvoicePostWoocommerce', 'displayOrderColumnAction' ), 20, 1);
		}

		public static function displayOrderColumnAction($invoice)
		{
		    $order = $invoice->getOrder();
		    if (!empty($order))
		    {
		        echo '<a href="' . admin_url('post.php?post=' . wpdesk_get_order_id( $order ) . '&action=edit') . '">' . $order->get_order_number() . '</a>';
		    }

		}

		public static function addPaymentOptionsAction($invoice)
		{
		    echo '<div class="form-field lonely"><label><input type="checkbox" name="add_order_id" value="1" ' . ($invoice->getAddOrderId()? 'checked="checked"': '') . '/>' . __( 'Add order number to an invoice', 'flexible-invoices-woocommerce' ) . '</label> </div>';
		}

		public static function beforeSaveInvoiceCustomFieldsAction($invoice)
		{
		    if (!empty($_POST))
		    {
		    	$add_order_id = 0;
		    	if ( isset( $_POST['add_order_id'] ) ) {
				    $add_order_id = $_POST['add_order_id'];
			    }
		        $invoice->setAddOrderId($add_order_id == 1);
		    }
		}

		public static function addWoocommerceClientDataFilter($user, $id)
		{
		    $user = new WP_User($id);
		    return array(
                'name' => empty($user->billing_company)? ($user->billing_first_name . ' ' . $user->billing_last_name): $user->billing_company,
	            'street' => $user->billing_address_1 . (empty($user->billing_address_2)? '': (' ' . $user->billing_address_2)),
	            'postcode' => $user->billing_postcode,
	            'city' => $user->billing_city,
                'nip' => $user->vat_number,
	            'country' => $user->billing_country,
	            'phone' => $user->billing_phone,
                'email' => $user->user_email
            );
		}

		public static function  addWoocommerceCurrenciesVatTypesFilter() {
			global $wpdb;

	        $tax_classes = WC_Tax::get_tax_classes();
	        foreach ( $tax_classes as $key => $tax_class ) {
	        	$tax_classes[$key] = sanitize_title( $tax_class );
	        }

	        $found_rates = $wpdb->get_results( "
	        		SELECT distinct tax_rates.*
	        		FROM {$wpdb->prefix}woocommerce_tax_rates as tax_rates
	        		ORDER BY CAST(tax_rate as DECIMAL(10,5)) desc, tax_rate_priority, tax_rate_order
	        		" );

	        $matched_tax_rates = array();

	        foreach ( $found_rates as $found_rate ) {
	        	if ( trim($found_rate->tax_rate_class) == '' || in_array( $found_rate->tax_rate_class, $tax_classes ) ) {
	        		$matched_tax_rates[ $found_rate->tax_rate_id ] = array(
	        			'rate'     => $found_rate->tax_rate,
	        			'label'    => $found_rate->tax_rate_name,
	        			'shipping' => $found_rate->tax_rate_shipping ? 'yes' : 'no',
	        			'compound' => $found_rate->tax_rate_compound ? 'yes' : 'no'
	        		);
	        	}
	        }
	        $rates = $matched_tax_rates;

	        $types = array();
	        if (!empty($rates))
	        {
    	        foreach ($rates as $index => $rate)
    	        {
    	        	$name = $rate['label'];
    	        	$types[$index] = array('index' => $index, 'rate' => floatval($rate['rate']), 'name' => $name );
    	        }
	        }
	        return $types;
		}

		public static function addWoocommerceCurrenciesFilter()
		{
		    return get_woocommerce_currencies();
		}

		public static function addWoocommercePaymentMethodsFilter()
		{
		    global $woocommerce;

		    $gateways = $woocommerce->payment_gateways->payment_gateways();

		    $methods = array();
		    foreach ($gateways as $gateway)
		    {
		        $methods[$gateway->id] = $gateway->title;
		    }
		    return $methods;
		}

		public static function addInvoiceToWoocommerceEmailAction($order, $sent_to_admin, $plain_text)
		{
		    $invoice = InvoicesWoocommerce::getInstance()->invoiceWoocommerceOrder->fetchInvoiceForOrder($order, false);
		    if (!empty($invoice) && $invoice->getId() > 0)
		    {
				echo '<a name=faktura></a>';
				echo ec_special_title( __( "E-FAKTURA", 'email-control'), array("border_position" => "center", "text_position" => "center", "space_after" => "3", "space_before" => "3" ) );
		        echo '<p><a href="' . admin_url('admin-ajax.php?action=invoice-get-pdf-invoice&id=' . $invoice->getId() . '&hash=' . md5(NONCE_SALT . $invoice->getId()), 'admin') . '"><img src=http://elektrobim.shop.pl/zasoby/uploads/2016/02/e-faktura.png></a></p>';
				echo '<p><font size=2>Nie widzisz obrazków? <a href="' . admin_url('admin-ajax.php?action=invoice-get-pdf-invoice&id=' . $invoice->getId() . '&hash=' . md5(NONCE_SALT . $invoice->getId()), 'admin') . '">Pobierz fakturę PDF</a></font></p>';
		    }
		}

		public static function injectInvoicePostClass()
		{
		    return 'InvoicePostWoocommerce';
		}

		public static function addSendMailOptionAction($invoice)
		{
		    echo '<button data-id="' . $invoice->getId() . '" data-hash="' . md5(NONCE_SALT . $invoice->getId()) . '" class="button button-primary button-large send_invoice">' . __( 'Send by e-mail', 'flexible-invoices-woocommerce' ) . '</button>';
		}

		protected function _getMetaFields()
		{
		    $meta = parent::_getMetaFields();
		    $meta['_add_order_id'] = '_add_order_id';
		    $meta['_wc_order_id'] = '_wc_order_id';

            return $meta;
		}

		static function createFromOrder($order, $plugin, $force = false)
		{
		    if ($order instanceof WC_Order)
		    {
		        $order_id = wpdesk_get_order_id( $order );
		    } else {
		        $order_id = $order;
		    }

		    $id = wpdesk_get_order_meta( $order, '_invoice_generated', true);

		    if (!empty($id))
		    {
		        $invoice = Invoice::getInstance()->invoicePostType->invoiceFactory($id);
		    }


		    return $invoice;
		}

		public function refresh()
		{
		    parent::refresh();

		    // refresh zamowienia
		    if (!empty($this->_wc_order_id))
		    {
		        $this->_wc_order = new WC_Order($this->_wc_order_id);
		    }
		}

		public function save()
		{
		    parent::save();
		}

		/**
		 * @return WC_Order
		 */
		public function getOrder()
		{
		    return $this->_wc_order;
		}

		public function isOrder()
		{
		    return !empty($this->_wc_order);
		}

		public function getAddOrderId()
		{
		    return $this->_add_order_id;
		}

		/**
		 *
		 * @param WC_Order $order
		 */
		public function setOrder(WC_Order $order)
		{
		    $this->_wc_order = $order;
		    $this->_wc_order_id = wpdesk_get_order_id( $order );
		}



		public function setDefaultValuesIfNumberEmpty()
		{
		    if (empty($this->_number))
		    {
		        parent::setDefaultValuesIfNumberEmpty();
		        $this->setAddOrderId(InvoicesWoocommerce::getInstance()->getSettingValue('add_order_id') == 'on');
		        $this->_currency = get_woocommerce_currency();
		    }
		}

		protected function _getVatRateFromSettingsFromValue($value)
		{
		    $vatTypes = Invoice::getInstance()->invoicePostType->getVatTypes();
		    foreach ($vatTypes as $vatType)
		    {
		        if ($value == $vatType['rate'])
		        {
		            return $vatType;
		        }
		    }
		}

		/**
		 *
		 * @param WC_Order $order
		 */
		public function setInvoiceDataFromOrder(WC_Order $order)
		{
		    global $woocommerce;

		    $this->setOrder($order);
		    $this->_type = 'invoice';
            $this->_pay_status = 'topay';

            $this->_date_sale = strtotime( wpdesk_get_order_meta( $order, 'order_date', true ) );

            if ( InvoicesWoocommerce::getInstance()->getSettingValue( 'date_of_sale', 'order_date' ) == 'order_completed' ) {
            	$_completed_date = wpdesk_get_order_meta( $order, '_completed_date', true );
            	if ( $_completed_date != '' ) {
            		if ( ! is_numeric( $_completed_date ) ) {
			            $this->_date_sale = strtotime( $_completed_date );
		            }
		            else {
			            $this->_date_sale = $_completed_date;
		            }
            	}
            }

		    $this->_date_issue = strtotime( current_time( 'mysql' ) );

			if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
				$this->_currency = $order->get_order_currency();
			}
			else {
				$this->_currency = $order->get_currency();
			}

		    $this->_payment_method = wpdesk_get_order_meta( $order, '_payment_method', true);

		    $this->_payment_method_name = wpdesk_get_order_meta( $order, '_payment_method_title', true);

		    // if method is empty try to get id from name
		    if ($this->_payment_method === false || $this->_payment_method === '' || $this->_payment_method === null)
		    {
		        $title = wpdesk_get_order_meta( $order, '_payment_method_title', true );

		        $methods = $this->addWoocommercePaymentMethodsFilter();
		        foreach ($methods as $id => $name)
		        {
		            if ($name == $title)
		            {
		                $this->_payment_method = $id;
		            }
		        }
		    }

		    // try to get from order and user
		    $nip = wpdesk_get_order_meta( $order, '_billing_vat_number', true);
		    if (empty($nip))
		    {
		        $user = new WP_User( wpdesk_get_order_meta( $order, '_customer_user', true ) );
		        $nip = $user->vat_number;
		    }


		    $this->_client = array(
                'wc_id' => wpdesk_get_order_meta( $order, '_customer_user', true),
	            'street' => wpdesk_get_order_meta( $order, '_billing_address_1', true ) . (empty(wpdesk_get_order_meta( $order, '_billing_address_2', true ))? '': (' ' . wpdesk_get_order_meta( $order, '_billing_address_2', true ))),
	            'postcode' => wpdesk_get_order_meta( $order, '_billing_postcode', true ),
	            'city' => wpdesk_get_order_meta( $order, '_billing_city', true ),
                'nip' => $nip,
	            'country' => wpdesk_get_order_meta( $order, '_billing_country', true ),
	            'phone' => wpdesk_get_order_meta( $order, '_billing_phone', true ),
                'email' => wpdesk_get_order_meta( $order, '_billing_email', true)
		    );
		    if (empty(wpdesk_get_order_meta( $order, '_billing_company', true ) ) )
		    {
		        $this->_client['type'] = 'individual';
		        $this->_client['name'] = wpdesk_get_order_meta( $order, '_billing_first_name', true ) . ' ' . wpdesk_get_order_meta( $order, '_billing_last_name', true);
		    } else{
		        $this->_client['type'] = 'company';
		        $this->_client['name'] = wpdesk_get_order_meta( $order, '_billing_company', true );
		    }
		    $this->_client_filter_field = $this->_client['name'];

		    $this->_products = array();
		    $items = $order->get_items();

		    // unit, quantity, net_price, discount, net_price_discount, net_price_sum, vat_rate, vat_sum, total_price, wc_product_id
		    foreach ($items as $item_id => $item)
		    {
		    	if ( InvoicesWoocommerce::getInstance()->getSettingValue('zero_product') == 'on' ) {
		    		if ( $item['line_total'] == 0 ) {
		    			continue;
		    		}
		    	}

		        if ( $item['line_subtotal'] > 0 )
		        {
                    $vatRateValue = 23; // WAŻNE
		        } else {
		            $vatRateValue = 23; // WAŻNE
		        }
		        $vatRate = $this->_getVatRateFromSettingsFromValue($vatRateValue);

		        $productObject = new WC_Product($item['product_id']);

		        $variation_data = '';
		        if ( InvoicesWoocommerce::getInstance()->getSettingValue('add_variant_info') == 'on'
		        	//&& $metadata = $order->has_meta( $item_id ) ) {
		             && $metadata = wpdesk_get_order_item_meta_data( $order, $item_id, true ) ) {
		        	foreach ( $metadata as $meta ) {
		        		// Skip hidden fields
		        		if ( strpos( $meta['meta_key'], '_' ) === 0 ) {
		        			continue;
		        		}

		        		// Skip serialised meta
		        		if ( is_serialized( $meta['meta_value'] ) ) {
		        			continue;
		        		}

		        		// Get attribute data
		        		if ( taxonomy_exists( wc_sanitize_taxonomy_name( $meta['meta_key'] ) ) ) {
		        			$term               = get_term_by( 'slug', $meta['meta_value'], wc_sanitize_taxonomy_name( $meta['meta_key'] ) );
		        			$meta['meta_key']   = wc_attribute_label( wc_sanitize_taxonomy_name( $meta['meta_key'] ) );
		        			$meta['meta_value'] = isset( $term->name ) ? $term->name : $meta['meta_value'];
		        		} else {
		        			$meta['meta_key']   = apply_filters(
		        				'woocommerce_attribute_label',
						        wc_attribute_label(
						        	$meta['meta_key'],
							        $productObject
						        ),
						        $meta['meta_key']
					        );
		        		}
		        		$variation_data .= $meta['meta_key'] . ': ' . $meta['meta_value'] . ', ';
		        	}
		        }
		        if ( $variation_data != '' ) {
		        	$variation_data = ' (' . trim( trim( $variation_data ), ',' ) . ')';
		        }

		        $product = array(
		            'name' => $item['name'] . $variation_data,

		            //'sku' => $productObject->get_sku(),
		        	'unit' => __( 'item', 'flexible-invoices-woocommerce' ),
		            'quantity' => $item['qty'], // ilosc

		            'net_price' => $item['line_total'] / intval($item['qty']) / 1.23, // bazowa kwota per 1 szt WAŻNE

		            'discount' => $item['line_subtotal'] - $item['line_total'] / intval($item['qty']), // obnizka

	                'net_price_discount' => $item['line_total'] / intval($item['qty']), // calkowita kwota bazowa, bez znizki
	                'net_price_sum' => $item['line_total'] / 1.23, // calkowita kwota, z wliuczonymi zniżkami

	                'vat_rate' => $vatRateValue, // vat per 1 szt
					$wartosc_netto = $item['line_total'] / 1.23, // WAŻNE
	                'vat_sum' => $item['line_total'] - $wartosc_netto, // całkowity vat
		            'total_price' => $item['line_total'], // kwota całkowita z podatkami i wszystkim WAŻNE było: 'total_price' => $item['line_tax'] + $item['line_total'],

		            'vat_type' => $vatRateValue,
	                'vat_type_name' => empty($vatRate)? $vatRateValue . '%': $vatRate['name'],
		            'vat_type_index' => empty($vatRate)? 0: $vatRate['index'],

		            'wc_item_type' => $item['type'],
		            'wc_order_item_id' => $item['item_meta'],
		            'wc_product_id' => $item['product_id'],
		            'wc_variation_id' => $item['variation_id']
		        );
		        if (InvoicesWoocommerce::getInstance()->getSettingValue('get_sku'))
		        {
		            $product['sku'] = $productObject->get_sku();
		        }
		        $this->_products[] = $product;
		    }

            $items = $order->get_items( 'fee' );

            // unit, quantity, net_price, discount, net_price_discount, net_price_sum, vat_rate, vat_sum, total_price, wc_product_id
            foreach ($items as $item_id => $item)
            {
                if ( InvoicesWoocommerce::getInstance()->getSettingValue('zero_product') == 'on' ) {
                    if ( $item['line_total'] == 0 ) {
                        continue;
                    }
                }

                if ( $item['line_total'] > 0 )
                {
                    $vatRateValue = round($item['line_tax'] / $item['line_total'] * 100);
                } else {
                    $vatRateValue = 0;
                }
                $vatRate = $this->_getVatRateFromSettingsFromValue($vatRateValue);

                $product = array(
                    'name' => $item['name'],

                    'unit' => __( 'item', 'flexible-invoices-woocommerce' ),
                    'quantity' => 1, // ilosc

                    'net_price' => $item['line_total'] / 1, // bazowa kwota per 1 szt

                    'discount' => 0, // obnizka

                    'net_price_discount' => $item['line_total'] / 1, // calkowita kwota bazowa, bez znizki
                    'net_price_sum' => $item['line_total'], // calkowita kwota, z wliuczonymi zniżkami

                    'vat_rate' => $vatRateValue, // vat per 1 szt
                    'vat_sum' => $item['line_tax'], // całkowity vat
                    'total_price' => $item['line_tax'] + $item['line_total'], // kwota całkowita z podatkami i wszystkim

                    'vat_type' => $vatRateValue,
                    'vat_type_name' => empty($vatRate)? $vatRateValue . '%': $vatRate['name'],
                    'vat_type_index' => empty($vatRate)? 0: $vatRate['index'],

                    'wc_item_type' => $item['type'],
                    'wc_order_item_id' => $item['item_meta'],
                );

                $this->_products[] = $product;
            }

		    // shipping
			$items = $order->get_items( 'shipping' );

			// unit, quantity, net_price, discount, net_price_discount, net_price_sum, vat_rate, vat_sum, total_price, wc_product_id
			foreach ($items as $item_id => $item)
			{
				if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
					if ( InvoicesWoocommerce::getInstance()->getSettingValue( 'zero_product' ) == 'on' ) {
						if ( $item['cost'] == 0 ) {
							continue;
						}
					}
					$total_tax = 0;
					if ( is_array( unserialize( $item['taxes'] ) ) ) {
						foreach ( unserialize( $item['taxes'] ) as $tax ) {
							$total_tax = $total_tax + floatval( $tax );
						}
					}
					if ( $item['cost'] > 0 ) {
						$vatRateValue = round( $total_tax / $item['cost'] * 100 );
					} else {
						$vatRateValue = 0;
					}
					$vatRate = $this->_getVatRateFromSettingsFromValue( $vatRateValue );

					$product = array(
						'name' => $item['name'],

						'unit'     => __( 'item', 'flexible-invoices-woocommerce' ),
						'quantity' => 1, // ilosc

						'net_price' => $item['cost'] / 1, // bazowa kwota per 1 szt

						'net_price_sum'      => $item['cost'], // calkowita kwota, z wliuczonymi zniżkami

						'vat_rate'    => $vatRateValue, // vat per 1 szt
						'vat_sum'     => $total_tax, // całkowity vat
						'total_price' => $total_tax + $item['cost'], // kwota całkowita z podatkami i wszystkim

						'vat_type'       => $vatRateValue,
						'vat_type_name'  => empty( $vatRate ) ? $vatRateValue . '%' : $vatRate['name'],
						'vat_type_index' => empty( $vatRate ) ? 0 : $vatRate['index'],

					);
				}
				else {
					if ( InvoicesWoocommerce::getInstance()->getSettingValue( 'zero_product' ) == 'on' ) {
						if ( $item['total'] == 0 ) {
							continue;
						}
					}
					if ( $item['total'] > 0 ) {
						$vatRateValue = 23; // WAŻNE - VAT DLA WYSYŁKI
					} else {
						$vatRateValue = 23;
					}
					$vatRate = $this->_getVatRateFromSettingsFromValue( $vatRateValue );

					$product = array(
						'name' => $item['name'],

						'unit'     => __( 'item', 'flexible-invoices-woocommerce' ),
						'quantity' => 1, // ilosc

						'net_price' => $item['total'] / 1.23, // bazowa kwota per 1 szt

						'net_price_sum'      => $item['total'] / 1.23, // calkowita kwota, z wliuczonymi zniżkami

						'vat_rate'    => $vatRateValue, // vat per 1 szt
						$wysylka_netto = $item['total'] / 1.23,
						'vat_sum'     => $item['total'] - $wysylka_netto, // całkowity vat
						'total_price' => $item['total'], // kwota całkowita z podatkami i wszystkim

						'vat_type'       => $vatRateValue,
						'vat_type_name'  => empty( $vatRate ) ? $vatRateValue . '%' : $vatRate['name'],
						'vat_type_index' => empty( $vatRate ) ? 0 : $vatRate['index'],

					);
				}
				$this->_products[] = $product;
			}


		    $this->_shipping = array();
/*
		    $shipping = $order->get_shipping_method();

		    if (!empty($shipping))
		    {
		    	$add_shipping = true;
		    	if ( InvoicesWoocommerce::getInstance()->getSettingValue('zero_product') == 'on' ) {
		    		if ( $order->get_shipping_tax() + $order->get_total_shipping() == 0 ) {
		    			$add_shipping = false;
		    		}
		    	}

		    	if ( $add_shipping ) {
			    	$vatRateValue = round($order->get_shipping_tax() / ($order->get_total_shipping() == 0? 1: $order->get_total_shipping()) * 100);
			        $vatRate = $this->_getVatRateFromSettingsFromValue($vatRateValue);

	    		    $shipping = array(
	    		        'name' => $order->get_shipping_method(),
	    	            'unit' => __( 'service', 'flexible-invoices-woocommerce' ),

			            'vat_type' => $vatRateValue,
		                'vat_type_name' => empty($vatRate)? $vatRateValue . '%': $vatRate['name'],
			            'vat_type_index' => empty($vatRate)? 0: $vatRate['index'],

	    	            'quantity' => 1, // ilosc

	    		        'vat_sum' => $order->get_shipping_tax(), // całkowity vat

	    		        'net_price' => 1,
	    		        'net_price_sum' => $order->get_total_shipping(),

	    		        'total_price' => $order->get_shipping_tax() + $order->get_total_shipping()
	                );

	    		    $this->_products[] = $shipping;
		    	}
		    }
*/
		    //$this->_discount = $order->get_total_discount();
		    $this->_discount = 0;
		    if ( isset( $order->order_discount ) ) {
		    	$this->_discount = (double) $order->order_discount;
		    }
		    if ($this->_discount != 0)
		    {
		        $this->_products[] = array(
		                'name'          => __( 'Discount', 'flexible-invoices-woocommerce' ),
		                'unit'          => __( 'service', 'flexible-invoices-woocommerce' ),
		                'vat_type'      => 0,
		                'vat_type_name' => '0%',
		                'quantity'      => 1, // ilosc
		                'vat_sum'       => 0, // całkowity vat
		                'net_price'     => - $this->_discount,
		                'net_price_sum' => - $this->_discount,
		                'total_price'   => - $this->_discount
		        );
		    }

		    $this->setAddOrderId(InvoicesWoocommerce::getInstance()->getSettingValue('add_order_id') == 'on');
		    $this->setDefaultDatePay();


		    $this->refreshTotals();

		    //$this->setDefaultPaymentStatus();

		    if (method_exists($order, 'get_status'))
		    {
    		    if ( in_array( $order->get_status(), array( 'processing', 'completed' ) )  && InvoicesWoocommerce::getInstance()->getSettingValue('auto_paid_status') == 'on' )
    		    {
    		        $this->setPaymentStatus('paid');
    		        $this->setTotalPaid($this->getTotalPrice());
    		    } else {
    		        $this->setDefaultPaymentStatus();
    		    }
		    } else {
		        $this->setDefaultPaymentStatus();
		    }
		}

		public function setAddOrderId($value)
		{
		    $this->_add_order_id = $value;
		}

		public function sendByEmail($recipient)
		{
		    $action = 'send_email_customer_invoice';
		    $order = $this->getOrder();

		    // set recipient email
		    add_filter('woocommerce_email_recipient_customer_invoice', create_function('', 'return \'' . $recipient . '\';'));

		    do_action( 'woocommerce_before_resend_order_emails', $order );

		    // Ensure gateways are loaded in case they need to insert data into the emails
		    WC()->payment_gateways();
		    WC()->shipping();

		    // Load mailer
		    $mailer = WC()->mailer();

		    $email_to_send = str_replace( 'send_email_', '', $action );

		    $mails = $mailer->get_emails();

		    if ( ! empty( $mails ) ) {
		        foreach ( $mails as $mail ) {
		            if ( $mail->id == $email_to_send ) {
		                $mail->trigger( wpdesk_get_order_id( $order ) );
		            }
		        }
		    }

		    do_action( 'woocommerce_after_resend_order_email', $order, $email_to_send );
		}
	}
