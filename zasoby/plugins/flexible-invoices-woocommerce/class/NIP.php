<?php 
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
	    
    class NIP {
    	
    	public static function validate( $nip ) {
		    if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
			    $country = WC()->customer->get_country();
		    }
		    else {
			    $country = WC()->customer->get_billing_country();
		    }
		    if ( !in_array( $country, array( 'AT','BE','BG','CHE','CY','CZ','DE','DK','EE','EL','ES','EU','FI','FR','GB','GR','HR','HU','IE','IT','LV','LT','LU','MT','NL','NO','PL','PT','RO','RS','SI','SK','SE' ) ) ) {
    	    	return true;
    		}
    		return self::isValid( $nip, $country );
    	}
    	// http://www.synet.sk/php/en/350-EU-VAT-validator
    	/**
    	 * Return TRUE if supplied tax ID is valid for supplied country.
    	 * @param string $id Taxation ID, e.g. ATU99999999 for Austria.
    	 * @param string $country Country code, e.g. AT
    	 */
    	public static function isValid( $id, $customer_country ) {
    		$id = strtoupper( $id );
    		$id = preg_replace('/[ -,.]/', '', $id);
    		if ( strlen( $id ) < 8 ) {
    			return false;
    		}
    		$country = substr( $id, 0, 2 );
    		$woocommerce_default_country = get_option( 'woocommerce_default_country', 0 );
    		if ( !in_array( $customer_country, array( 'AT','BE','BG','CHE','CY','CZ','DE','DK','EE','EL','ES','EU','FI','FR','GB','GR','HR','HU','IE','IT','LV','LT','LU','MT','NL','NO','PL','PT','RO','RS','SI','SK','SE' ) ) ) {    			
    			//if ( $woocommerce_default_country == $customer_country ) {
    				$id = $customer_country . $id;
    				$country = $customer_country;
    			//}
    		}
    		else {
    			if ( $woocommerce_default_country == $customer_country ) {
    				if ( $country != $customer_country ) {
    					$id = $customer_country . $id;
    					$country = $customer_country;
    				}
    			}
    		}
    		if ( $country != $customer_country ) {
    			return false;
    		}
    		switch ( $country ) {
    			case 'AT': // AUSTRIA
    				$isValid = (bool) preg_match('/^(AT)U(\d{8})$/', $id);
    				break;
    			case 'BE': // BELGIUM
    				$isValid = (bool) preg_match('/(BE)(0?\d{9})$/', $id);
    				break;
    			case 'BG': // BULGARIA
    				$isValid = (bool) preg_match('/(BG)(\d{9,10})$/', $id);
    				break;
    			case 'CHE': // Switzerland
    				$isValid = (bool) preg_match('/(CHE)(\d{9})(MWST)?$/', $id);
    				break;
    			case 'CY': // CYPRUS
    				$isValid = (bool) preg_match('/^(CY)([0-5|9]\d{7}[A-Z])$/', $id);
    				break;
    			case 'CZ': // CZECH REPUBLIC
    				$isValid = (bool) preg_match('/^(CZ)(\d{8,10})(\d{3})?$/', $id);
    				break;
    			case 'DE': // GERMANY
    				$isValid = (bool) preg_match('/^(DE)([1-9]\d{8})/', $id);
    				break;
    			case 'DK': // DENMARK
    				$isValid = (bool) preg_match('/^(DK)(\d{8})$/', $id);
    				break;
    			case 'EE': // ESTONIA
    				$isValid = (bool) preg_match('/^(EE)(10\d{7})$/', $id);
    				break;
    			case 'EL': // GREECE
    				$isValid = (bool) preg_match('/^(EL)(\d{9})$/', $id);
    				break;
    			case 'ES': // SPAIN
    				$isValid = (bool) preg_match('/^(ES)([A-Z]\d{8})$/', $id)
    				|| preg_match('/^(ES)([A-H|N-S|W]\d{7}[A-J])$/', $id)
    				|| preg_match('/^(ES)([0-9|Y|Z]\d{7}[A-Z])$/', $id)
    				|| preg_match('/^(ES)([K|L|M|X]\d{7}[A-Z])$/', $id);
    				break;
    			case 'EU': // EU type
    				$isValid = (bool) preg_match('/^(EU)(\d{9})$/', $id);
    				break;
    			case 'FI': // FINLAND
    				$isValid = (bool) preg_match('/^(FI)(\d{8})$/', $id);
    				break;
    			case 'FR': // FRANCE
    				$isValid = (bool) preg_match('/^(FR)(\d{11})$/', $id)
    				|| preg_match('/^(FR)([(A-H)|(J-N)|(P-Z)]\d{10})$/', $id)
    				|| preg_match('/^(FR)(\d[(A-H)|(J-N)|(P-Z)]\d{9})$/', $id)
    				|| preg_match('/^(FR)([(A-H)|(J-N)|(P-Z)]{2}\d{9})$/', $id);
    				break;
    			case 'GB': // GREAT BRITAIN
    				$isValid = (bool) preg_match('/^(GB)?(\d{9})$/', $id)
    				|| preg_match('/^(GB)?(\d{12})$/', $id)
    				|| preg_match('/^(GB)?(GD\d{3})$/', $id)
    				|| preg_match('/^(GB)?(HA\d{3})$/', $id);
    				break;
    			case 'GR': // GREECE
    				$isValid = (bool) preg_match('/^(GR)(\d{8,9})$/', $id);
    				break;
    			case 'HR': // CROATIA
    				$isValid = (bool) preg_match('/^(HR)(\d{11})$/', $id);
    				break;
    			case 'HU': // HUNGARY
    				$isValid = (bool) preg_match('/^(HU)(\d{8})$/', $id);
    				break;
    			case 'IE': // IRELAND
    				$isValid = (bool) preg_match('/^(IE)(\d{7}[A-W])$/', $id)
    				|| preg_match('/^(IE)([7-9][A-Z\*\+)]\d{5}[A-W])$/', $id)
    				|| preg_match('/^(IE)(\d{7}[A-W][AH])$/', $id);
    				break;
    			case 'IT': // ITALY
    				$isValid = (bool) preg_match('/^(IT)(\d{11})$/', $id);
    				break;
    			case 'LV': // LATVIA
    				$isValid = (bool) preg_match('/^(LV)(\d{11})$/', $id);
    				break;
    			case 'LT': // LITHUNIA
    				$isValid = (bool) preg_match('/^(LT)(\d{9}|\d{12})$/', $id);
    				break;
    			case 'LU': // LUXEMBOURG
    				$isValid = (bool) preg_match('/^(LU)(\d{8})$/', $id);
    				break;
    			case 'MT': // MALTA
    				$isValid = (bool) preg_match('/^(MT)([1-9]\d{7})$/', $id);
    				break;
    			case 'NL': // NETHERLAND
    				$isValid = (bool) preg_match('/^(NL)(\d{9})B\d{2}$/', $id);
    				break;
    			case 'NO': // NORWAY
    				$isValid = (bool) preg_match('/^(NO)(\d{9})$/', $id);
    				break;
    			case 'PL': // POLAND
    				$isValid = (bool) preg_match('/^(PL)(\d{10})$/', $id);
    				break;
    			case 'PT': // PORTUGAL
    				$isValid = (bool) preg_match('/^(PT)(\d{9})$/', $id);
    				break;
    			case 'RO': // ROMANIA
    				$isValid = (bool) preg_match('/^(RO)([1-9]\d{1,9})$/', $id);
    				break;
    			case 'RS': // SERBIA
    				$isValid = (bool) preg_match('/^(RS)(\d{9})$/', $id);
    				break;
    			case 'SI': // SLOVENIA
    				$isValid = (bool) preg_match('/^(SI)([1-9]\d{7})$/', $id);
    				break;
    			case 'SK': // SLOVAK REPUBLIC
    				$isValid = (bool) preg_match('/^(SK)([1-9]\d[(2-4)|(6-9)]\d{7})$/', $id);
    				break;
    			case 'SE': // SWEDEN
    				$isValid = (bool) preg_match('/^(SE)(\d{10}01)$/', $id);
    				break;
    			default:
    				$isValid = false;
    		}
    	
    		return $isValid;
    	}
    	 
    	
        public static function validate_old($nip) 
        {
            $eu_countries = array('AT', 'BE', 'BG', 'CY', 'CZ', 'DE', 'DK', 'EE', 'EL', 'ES', 'FI', 'FR', 'GB', 'HR', 'HU', 'IE', 'IT', 'LT', 'LU', 'LV', 'MT', 'NL', 'PL', 'PT', 'RO', 'SE', 'SI', 'SK');
            $prefix = substr($nip, 0, 2);

            if (!is_numeric($prefix)) {
                if (!in_array($prefix, $eu_countries)) {
                    return false;
                } else {
                    $nip = substr($nip, 2);
                }
            } 

            if (!is_numeric($nip)) {
                return false;
            }

            $nip_bez_kresek = preg_replace("/-/","",$nip);
            $reg = '/^[0-9]{10}$/';
            if(preg_match($reg, $nip_bez_kresek)==false)
                return false;
            else
            {
                $dig = str_split($nip_bez_kresek);
                $kontrola = (6*intval($dig[0]) + 5*intval($dig[1]) + 7*intval($dig[2]) + 2*intval($dig[3]) + 3*intval($dig[4]) + 4*intval($dig[5]) + 5*intval($dig[6]) + 6*intval($dig[7]) + 7*intval($dig[8]))%11;
                if(intval($dig[9]) == $kontrola)
                    return true;
                else
                    return false;
            }
        }
        
    }  
