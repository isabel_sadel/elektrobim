<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    class invoiceWoocommerceCheckout extends inspire_pluginDependant3
    {
        public function __construct($plugin)
        {
            parent::__construct($plugin);

            // add vat field for users
            add_filter( 'woocommerce_checkout_fields' , array( $this, 'addVatCheckoutFieldFilter'), 10, 1);
            add_filter( 'woocommerce_billing_fields' , array( $this, 'addVatBillingFieldFilter'), 10, 1);

            add_action( 'wp_enqueue_scripts', array( $this, 'wp_enqueue_scripts' ) );

            add_action('woocommerce_checkout_update_user_meta', array( $this, 'saveCustomerVatField'), 10, 2);
            add_action('woocommerce_checkout_update_order_meta', array( $this, 'saveOrderVatField'), 10, 2);
            if ($this->getSettingValue('add_nip_field') == 'on')
                add_action('woocommerce_checkout_process', array( $this, 'validateFields'), 10, 1);
        }

        public function wp_enqueue_scripts() {
        	if ( is_checkout() && $this->getSettingValue('add_invoice_ask_field') == 'on' && $this->getSettingValue('nip_required') != 'on' ) {
        		wp_register_script( 'inspire_invoice_checkout', $this->getPlugin()->getPluginUrl() . 'assets/js/checkout.js', array('jquery') ,'2.1', true);
        		wp_enqueue_script( 'inspire_invoice_checkout' );
        	}
        }

        public function saveCustomerVatField($user_id, $post_data)
        {
            if (!empty($post_data['vat_number']))
            {
                update_user_meta( $user_id, 'vat_number', $post_data['vat_number'] );
            }
        }

        public function saveOrderVatField($order_id, $post_data)
        {
            if (!empty($post_data['invoice_ask']))
            {
                //update_post_meta( $order_id, '_billing_invoice_ask', $post_data['invoice_ask'] );
                wpdesk_update_order_meta( $order_id, '_billing_invoice_ask', $post_data['invoice_ask'] );
            }
        	if (!empty($post_data['vat_number']))
            {
                //update_post_meta( $order_id, '_billing_vat_number', $post_data['vat_number'] );
	            wpdesk_update_order_meta( $order_id, '_billing_vat_number', $post_data['vat_number'] );
            }
        }

        public function addVatBillingFieldFilter($fields)
        {
        	if($this->getSettingValue('add_invoice_ask_field') == 'on') {
        		$fields['invoice_ask'] = array(
        				'label'     	=> __( 'I want an invoice', 'flexible-invoices-woocommerce' ),
        				'class'     	=> array('form-row-wide'),
        				'clear'     	=> true,
        				'type'			=> 'checkbox'
        		);
        	}

            if($this->getSettingValue('add_nip_field') == 'on') {
                $fields['vat_number'] = array(
                        'label'       => $this->getSettingValue( 'nip_label' ),
                        'placeholder' => $this->getSettingValue( 'nip_placeholder', '' ),
                        'required'    => $this->getSettingValue( 'nip_required' ) == 'on' ? true : false,
                        'class'       => array('form-row-wide'),
                        'clear'       => true
                );
            }

            return $fields;
        }

        /**
         *
         * @param unknown_type $fields
         * @return multitype:boolean multitype:string  Ambigous <string, mixed>
         */
        public function addVatCheckoutFieldFilter($fields)
        {
            if($this->getSettingValue('add_invoice_ask_field') == 'on') {
        		$fields['billing']['invoice_ask'] = array(
        				'label'       => __( 'I want an invoice', 'flexible-invoices-woocommerce' ),
        				'class'       => array('form-row-wide'),
        				'clear'       => true,
        				'type'		  => 'checkbox'
        		);
        	}
        	if($this->getSettingValue('add_nip_field') == 'on') {
                $fields['billing']['vat_number'] = array(
                        'label'       => $this->getSettingValue( 'nip_label' ),
                        'placeholder' => $this->getSettingValue( 'nip_placeholder', '' ),
                        'required'    => $this->getSettingValue( 'nip_required' ) == 'on' ? true : false,
                        'class'       => array('form-row-wide'),
                        'clear'       => true
                );
            }

            return $fields;
        }

        function validateFields( $args ) {        	
            if ( isset( $_POST['vat_number'] ) && !empty($_POST['vat_number']) ) {
                if ( $this->getSettingValue('validate_nip') == 'on' ) {
                    if ( !NIP::validate( $_POST['vat_number'] ) ) {
	                    if ( version_compare( WC_VERSION, '2.7', '<' ) ) {
		                    $country = WC()->customer->get_country();
	                    }
	                    else {
		                    $country = WC()->customer->get_billing_country();
	                    }
                    	$woocommerce_default_country = get_option( 'woocommerce_default_country', 0 );
                    	if ( !in_array( $woocommerce_default_country, array( 'AT','BE','BG','CHE','CY','CZ','DE','DK','EE','EL','ES','EU','FI','FR','GB','GR','HR','HU','IE','IT','LV','LT','LU','MT','NL','NO','PL','PT','RO','RS','SI','SK','SE' ) )
                    		|| $woocommerce_default_country == $country
                    	) {
                    		wc_add_notice(  sprintf ( __( 'Please enter a valid %s. Do not enter hyphens or spaces. Optionally add country prefix (EU VAT Number).', 'flexible-invoices-woocommerce' ), $this->getSettingValue( 'nip_label' ) ), 'error' );
                    	}
                    	else {
                    		wc_add_notice(  sprintf ( __( 'Please enter a valid %s without hyphens and spaces, with valid country prefix (EU VAT Number).', 'flexible-invoices-woocommerce' ), $this->getSettingValue( 'nip_label' ) ), 'error' );
                    	}
                    }
                }
            }
        }

    }
