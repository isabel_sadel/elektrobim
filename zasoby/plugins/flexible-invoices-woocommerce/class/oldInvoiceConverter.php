<?php

class oldInvoiceConverter extends inspire_pluginDependant3
{
    public function convertAllOldInvoices()
    {
        function invoice_filter_where( $where = '' ) {
            $where .= " AND post_date >= '" . date('Y-m-d 00:00:00', strtotime($_GET['start_date'])) . "' AND post_date <= '" . date('Y-m-d 23:59:59', strtotime($_GET['end_date'])) . "'";
            return $where;
        }
        //add_filter( 'posts_where', 'invoice_filter_where' );
        
        $raportQuery = new WP_Query( array(
                'post_type' => 'woocommerce_invoices',
                'orderby' => 'date',
                'order' => 'ASC',
                'post_status' => 'publish',
                'nopaging' => true,
                 
        ) );
        
        $invoices = $raportQuery->get_posts();
        
        //remove_filter( 'posts_where', 'filter_where' );
        
        foreach ($invoices as $item)
        {
            
            if ($item->post_parent > 0)
            {
                $converted = get_post_meta($item->ID, '_invoice_converted', true);
                if (empty($converted))
                {
                    $invoice_number = get_post_meta($item->ID, 'invoice_number', true);
                    $now = get_post_meta($item->ID, 'date_of_issue', true);;
                    $order = new WC_Order( $item->post_parent );

                    // create invoice
                    $invoicePost = array(
                            'post_title'    => InvoicePost::generateFormattedInvoiceNumberForData($invoice_number, $now),
                            'post_content'  => 'Invoice',
                            'post_status'   => 'publish',
                            'post_type' => 'inspire_invoice',
                            'post_date' => date('Y-m-d G:i:s', $now)
                    );
                    
                    // Insert the post into the database
                    $invoiceId = wp_insert_post( $invoicePost );
                    	
                    $invoice = Invoice::getInstance()->invoicePostType->invoiceFactory($invoiceId);
                    $invoice->setOwnerFromDefault();
                    $invoice->setInvoiceDataFromOrder($order);
                    $invoice->setNumber($invoice_number);
                    $invoice->setNotesFromDefault();
                    
                    if ( in_array($order->status, array('processing', 'completed')) )
                    {
                        $invoice->setTotalPaid($invoice->getTotalPrice());
                        $invoice->setPaymentStatus('paid');
                    }
                    
                     
                    //$invoice->setCurrency($order->get_order_currency());
                     
                    $invoice->save();
                    update_post_meta($item->ID, '_invoice_converted', true);
                }
            }
        }
        
        
    }
    

}