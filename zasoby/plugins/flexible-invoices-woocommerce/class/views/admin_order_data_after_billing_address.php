<div class="address">
	<p class="form-field form-field-wide">
		<?php if ( $this->getSettingValue('add_invoice_ask_field') == 'on' ): ?>
   			<strong><?php _e( 'I want an invoice:', 'flexible-invoices-woocommerce' ); ?></strong> <?php echo $billing_invoice_ask_display; ?><br />
   		<?php endif; ?>
   		<?php if ( ! empty ( $billing_vat_number ) ): ?>
	   		<strong><?php echo $this->getSettingValue( 'nip_label' ); ?>:</strong> <?php echo $billing_vat_number; ?>
   		<?php endif; ?>
	</p>
</div>

<div class="edit_address">
	<?php
		woocommerce_wp_checkbox( $billing_invoice_ask_field );
		woocommerce_wp_text_input( $billing_vat_number_field );
	?>
</div>
