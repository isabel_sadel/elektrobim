<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    class invoiceWoocommerceMyAccount extends inspire_pluginDependant3
    {
        public function __construct($plugin)
        {
            parent::__construct($plugin);

            // add vat field for users
            add_filter( 'woocommerce_my_account_my_orders_actions' , array( $this, 'addGetInvoiceFilter'), 10, 2);
            //add_action('user_profile_update_errors',array( $this, 'validateFields'),10,1);
        }

        public function addGetInvoiceFilter($actions, WC_Order $order)
        {
            //if (get_post_meta(wpdesk_get_order_id( $order ), '_invoice_generated', true) > 0)
	        if ( wpdesk_get_order_meta( $order, '_invoice_generated', true ) > 0 )
            {
                $invoice = InvoicesWoocommerce::getInstance()->invoiceWoocommerceOrder->fetchInvoiceForOrder ($order);
                if (!empty($invoice) && $invoice->getId() > 0)
                {
                    $actions['invoice'] = array(
                        'url' => admin_url('admin-ajax.php?action=invoice-get-pdf-invoice&id=' . $invoice->getId() . '&hash=' . md5(NONCE_SALT . $invoice->getId()), 'admin'),
                        'name' => __( 'Invoice', 'flexible-invoices-woocommerce' )
                    );
                }


            }
            return $actions;
        }

        function validateFields($args)
        {
            /*if ( $this->getSettingValue('validate_nip') == 'on' && isset( $_POST['vat_number'] ) && !empty($_POST['vat_number']) )
            {
                if(!NIP::validate($_POST['vat_number']))
                    $args->add( 'error', __( 'Wpisz poprawny numer ' . $this->getSettingValue('nip_label') . ' bez kresek, spacji, opcjonalnie poprzedź numer prefiksem kraju UE (tzw. NIP europejski)', 'flexible-invoices-woocommerce' ),'');
            } else if ($this->getSettingValue('nip_required') == 'on' && empty($_POST['vat_number'])){
                $args->add( 'error', __( $this->getSettingValue('nip_label') . ' jest wymaganym polem', 'flexible-invoices-woocommerce' ),'');
            }*/

        }
    }
