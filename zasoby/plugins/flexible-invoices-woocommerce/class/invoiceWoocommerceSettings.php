<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

    class invoiceWoocommerceSettings extends inspire_pluginDependant3
    {

        public function __construct($plugin)
        {
            parent::__construct($plugin);

            add_action( 'inspire_invoices_after_display_settings', array( $this, 'renderWooCommerceSettingsAction' ));
            add_action( 'admin_init', array($this, 'updateSettingsAction') );

            add_action('admin_footer', array($this, 'WooCommerceScripts'));

            add_filter('inspire_invoices_settings_pages', array( $this, 'WooCommerceSettingsTabFilter') );
        }

        public function WooCommerceScripts() { ?>
            <script type="text/javascript">
                jQuery(document).ready(function($) {
                    $("#inspire_invoices_add_nip_field").change(function() {
                        if ($(this).is(':checked')) {
                            $(".nip-additional-fields").show();
                        } else {
                            $(".nip-additional-fields").hide();
                        }
                    });

                    $("#inspire_invoices_add_nip_field").trigger("change");

                    $('label[for="vatNumber"]').text('<?php echo $this->getSettingValue('nip_label') ?>');

                });
            </script>
  <?php }

        public function WooCommerceSettingsTabFilter($pages)
        {
            if (class_exists( 'WooCommerce' ))
            {
                $pages['woocommerce'] = array(
                    'page' => 'edit.php?post_type=inspire_invoice&page=invoices_settings&tab=woocommerce',
                    'title' => __( 'WooCommerce', 'flexible-invoices-woocommerce' )
                );
            }

            return $pages;
        }


        /**
         * wordpress action
         *
         * renders invoices submenu page
         */
        public function renderWooCommerceSettingsAction($tab)
        {
            if ($tab == 'woocommerce')
            {
                echo $this->loadTemplate('submenu_invoices_woocommerce', 'settings', array(
                    'plugin' => $this->getPlugin()
                ));
            }
        }

        /**
         * wordpress action
         *
         * should-be-protected method to save/update settings when changed by POST
         */
        public function updateSettingsAction()
        {
            if (!empty($_POST))
            {
                // checkboxes
                if (!empty($_POST['option_page']) && $_POST['option_page'] === 'inspire_invoices_settings' && @$_REQUEST['tab'] == 'woocommerce')
                {
                    update_option('inspire_invoices_woocommerce_sequential_orders', '');
                    update_option('inspire_invoices_woocommerce_add_order_id', '');
                    update_option('inspire_invoices_woocommerce_add_variant_info', 'off');
                    update_option('inspire_invoices_woocommerce_zero_invoice', 'off');
                    update_option('inspire_invoices_woocommerce_zero_product', 'off');
                    update_option('inspire_invoices_woocommerce_get_sku', '');
                    update_option('inspire_invoices_woocommerce_auto_paid_status', '');

                    update_option('inspire_invoices_woocommerce_add_invoice_ask_field', 'off');
                    update_option('inspire_invoices_woocommerce_add_nip_field', 'default');
                    update_option('inspire_invoices_woocommerce_nip_label', '');
                    update_option('inspire_invoices_woocommerce_nip_placeholder', '');
                    update_option('inspire_invoices_woocommerce_nip_required', 'off');
                    update_option('inspire_invoices_woocommerce_validate_nip', 'off');

                    foreach ($_POST[$this->getNamespace()] as $name => $value)
                    {
                        update_option($this->getNamespace() . '_' . $name, $value);
                    }
                }
            }
        }


    }
