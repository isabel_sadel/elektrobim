<?php
/*
	Plugin Name: Flexible Invoices for WooCommerce
	Plugin URI: https://www.wpdesk.net/products/flexible-invoices-woocommerce/
	Description: Automatically issue and send invoices for WooCommerce orders. Extension for Flexible Invoices for WordPress.
	Version: 2.4
	Author: WP Desk
	Author URI: https://www.wpdesk.net/
	Text Domain: flexible-invoices-woocommerce
	Domain Path: /lang/

	Copyright 2017 WP Desk Ltd.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

	$wpdesk_helper_text_domain = 'flexible-invoices-woocommerce';
	require_once( plugin_basename( 'inc/wpdesk-functions.php' ) );

	$plugin_data = array(
        'plugin' => plugin_basename( __FILE__ ),
        'product_id' => 'WooCommerce Invoices',
        'version'   => '2.4',
        'config_uri' => admin_url( 'edit.php?post_type=inspire_invoice&page=invoices_settings' )
    );

	$helper = new WPDesk_Helper_Plugin( $plugin_data );

	require_once( plugin_basename( 'inc/wpdesk-woo27-functions.php' ) );


//if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && in_array( 'inspire-invoices/invoices.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )
	{
		require_once('class/inspire/plugin3.php');
		require_once('class/inspire/pluginDependant3.php');
		require_once('class/inspire/pluginPostTypeFactory3.php');
		require_once('class/inspire/pluginPostType3.php');

		if ( in_array( 'flexible-invoices/flexible-invoices.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {

    		require_once( plugin_dir_path( __FILE__ ) . '../' . 'flexible-invoices/class/invoicePost.php' );

    		require_once('class/invoicePostWoocommerce.php');
    		require_once('class/invoiceWoocommerceCheckout.php');
    		require_once('class/invoiceWoocommerceOrder.php');
    		require_once('class/invoiceWoocommerceSettings.php');
    		require_once('class/invoiceWoocommerceMyAccount.php');
    		require_once('class/NIP.php');
		}

		class InvoicesWoocommerce extends inspire_Plugin3 {
			protected $_pluginNamespace = 'inspire_invoices_woocommerce';
			protected $_textDomain = 'flexible-invoices-woocommerce';

			protected $_adminNotices;

			private static $_oInstance = false;

			//protected $_invoicesAdmin;
			public $invoiceWoocommerceOrder;

			public function __construct() {
			    add_action( 'admin_notices', array($this, 'showAdminNoticesAction' ) );

			    // Activate
			    register_activation_hook( __FILE__, array( $this, 'pluginActivated' ) );

			    // Load Locales
			    load_plugin_textdomain( 'flexible-invoices-woocommerce', FALSE, dirname ( plugin_basename ( __FILE__ ) ) . '/lang/' );

			    if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && in_array( 'flexible-invoices/flexible-invoices.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    				$this->_initBaseVariables();
    				$this->injectDependenciesIntoInvoices();

                    // Templates Path
                    $this->_templatePath = 'templates';

    				if ( class_exists('invoiceWoocommerceSettings') ) {
        				$this->invoiceWoocommerceSettings = new invoiceWoocommerceSettings($this);

        				$this->invoiceWoocommerceOrder = new invoiceWoocommerceOrder($this);
        				$this->invoiceWoocommerceCheckout = new invoiceWoocommerceCheckout($this);
        				$this->invoiceWoocommerceMyAccount = new invoiceWoocommerceMyAccount($this);


        				// add custom order numbering
        				if ( $this->getSettingValue( 'sequential_orders') == 'on' ) {
        				    add_action( 'wp_insert_post', array( $this, 'setOrderNumAction'), 10, 2 );
        				    add_action( 'woocommerce_process_shop_order_meta', array( $this, 'setOrderNumAction'), 10, 2 );

        				    add_filter( 'woocommerce_order_number', array( $this, 'orderNumberFilter' ), 10, 2);

        				    $this->installNumbering();
        				}
    				}
			    } else {
			        $this->_adminNotices = __( 'Flexible Invoices for WooCommerce requires activating WooCommerce and <a href="https://wordpress.org/plugins/flexible-invoices/" target="_blank">Flexible Invoices for WordPress</a>.', 'flexible-invoices-woocommerce' );
			    }
			}

			public function showAdminNoticesAction() {
			    if ( !empty( $this->_adminNotices ) ) {
    			    echo '<div class="error">';
    			    echo '<p>' . $this->_adminNotices . '</p>';
    			    echo '</div>';
			    }
			}

			public function pluginActivated() {
                if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) && in_array( 'flexible-invoices/flexible-invoices.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
    			    require_once('class/oldInvoiceConverter.php');
    			    $converter = new oldInvoiceConverter($this);
    			    $converter->convertAllOldInvoices();
                }
			}

			/**
			 *
			 * @param WC_Order $order
			 * @return InvoicePost
			 */
			public function generateInvoiceForOrder(WC_Order $order) {
			    $invoicePlugin = Invoice::getInstance();

			    $invoice_number = $invoicePlugin->invoicePostType->generateNextInvoiceNumber();
		        $now = strtotime( current_time( 'mysql' ) );
		        $now_mysql = current_time( 'mysql' );

			    // create invoice
			    $invoicePost = array(
			            'post_title'    => InvoicePost::generateFormattedInvoiceNumberForData($invoice_number, $now),
			            'post_content'  => 'Invoice',
			            'post_status'   => 'publish',
			            'post_type' => 'inspire_invoice',
			    		'post_date' => $now_mysql
//			            'post_date' => date('Y-m-d G:i:s', $now)
			    );

			    // Insert the post into the database
			    $invoiceId = wp_insert_post( $invoicePost );

			    $invoice = $invoicePlugin->invoicePostType->invoiceFactory($invoiceId);
			    $invoice->setOwnerFromDefault();
			    $invoice->setInvoiceDataFromOrder($order);
			    $invoice->setNumber($invoice_number);
			    $invoice->setNotesFromDefault();

			    //$invoice->setCurrency($order->get_order_currency());

			    $invoice->save();

			    do_action('inspire_invoices_after_generate_invoice', $invoice);
			    do_action('inspire_invoices_after_generate_order_invoice', $invoice, $order);

			    return $invoice;

			}

			public function installNumbering() {
			    $installed_version = get_option( $this->getNamespace() );

			    if ( ! $installed_version ) {
			        // initial install, set the order number for all existing orders to the post id
			        $orders = get_posts( array( 'numberposts' => '', 'post_type' => 'shop_order', 'nopaging' => true ) );
			        if ( is_array( $orders ) ) {
				        foreach( $orders as $order ) {
					        if ( get_post_meta( $order->ID, '_order_number', true ) == '' ) {
						        add_post_meta( $order->ID, '_order_number', $order->ID );
					        }
				        }
			        }
			    }

			    if ( $installed_version != 1 ) {
			        update_option( $this->getNamespace(), 1 );
			    }
			}

			public function orderNumberFilter($order_number, $order) {
			    $replaced_num = wpdesk_get_order_meta( $order, '_order_number', true);
			    if ( !empty( $replaced_num ) ) {
			        return '#' . $replaced_num;
			    }
			    return $order_number;
			}

			public function injectDependenciesIntoInvoices() {
			    if ( class_exists( 'InvoicePostWoocommerce' ) ) {
                    InvoicePostWoocommerce::initWoocommercePostType();
			    }
			}

			public function setOrderNumAction( $post_id, $post ) {
			    global $wpdb;
			    if ( $post->post_type == 'shop_order' && $post->post_status != 'auto-draft' ) {
			        $order_number = get_post_meta( $post_id, '_order_number', true );
			        if ( $order_number == "" ) {

			            // attempt the query up to 3 times for a much higher success rate if it fails (due to Deadlock)
			            $success = false;
			            for ( $i = 0; $i < 3 && ! $success; $i++ ) {
			                // this seems to me like the safest way to avoid order number clashes
			                $success = $wpdb->query( 'INSERT INTO ' . $wpdb->postmeta . ' (post_id,meta_key,meta_value) SELECT ' . $post_id . ',"_order_number",if(max(cast(meta_value as UNSIGNED)) is null,1,max(cast(meta_value as UNSIGNED))+1) from ' . $wpdb->postmeta . ' where meta_key="_order_number"' );
			            }
			        }
			    }
			}

			/**
			 * action_links function.
			 *
			 * @access public
			 * @param mixed $links
			 * @return void
			 */
			 public function linksFilter( $links ) {

			     $plugin_links = array(
			     		'<a href="' . admin_url( 'edit.php?post_type=inspire_invoice&page=invoices_settings&tab=woocommerce') . '">' . __( 'Settings', 'flexible-invoices-woocommerce' ) . '</a>',
			     		'<a href="' . __( 'https://www.wpdesk.net/docs/flexible-invoices-woocommerce-docs/', 'flexible-invoices-woocommerce' ) . '">' . __( 'Docs', 'flexible-invoices-woocommerce' ) . '</a>',
			     		'<a href="' . __( 'https://www.wpdesk.net/support/', 'flexible-invoices-woocommerce' ) . '">' . __( 'Support', 'flexible-invoices-woocommerce' ) . '</a>',
			     );

			     return array_merge( $plugin_links, $links );
			 }

			public static function getInstance() {
			    if( self::$_oInstance == false ) {
			        self::$_oInstance = new InvoicesWoocommerce();
			    }
			    return self::$_oInstance;
			}
		}

		//if ( $helper->is_active() ) {
			$_GLOBALS['inspire_invoices_woocommerce'] = $invoices_woocommerce = InvoicesWoocommerce::getInstance();
		//}
	}
