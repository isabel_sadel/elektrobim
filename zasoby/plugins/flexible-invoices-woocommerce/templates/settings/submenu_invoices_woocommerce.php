<?php
	global $woocommerce;
?>
<form action="" method="post">
	<?php settings_fields( 'inspire_invoices_settings' ); ?>

 	<?php if (!empty($_POST['option_page']) && $_POST['option_page'] === 'inspire_invoices_settings'): ?>
		<div id="message" class="updated fade"><p><strong><?php _e( 'Settings saved.', 'flexible-invoices-woocommerce' ); ?></strong></p></div>
	<?php endif; ?>

	<h3><?php _e( 'WooCommerce Integration Settings', 'flexible-invoices-woocommerce' ); ?></h3>

	<p><a href="<?php _e( 'https://www.wpdesk.net/docs/flexible-invoices-woocommerce-docs/', 'flexible-invoices-woocommerce' ); ?>" target="_blank"><?php _e( 'Read WooCommerce integration manual &rarr;', 'flexible-invoices-woocommerce' ); ?></a></p>

	<table class="form-table">
		<tbody>
			<tr valign="top">
			    <th class="titledesc" scope="row"><?php _e( 'Sequential Order Numbers', 'flexible-invoices-woocommerce' ); ?></th>

			    <td class="forminp forminp-checkbox">
			    	<label for="inspire_invoices_sequential_orders"> <input <?php if($this->getSettingValue('sequential_orders') == 'on'):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_sequential_orders" name="inspire_invoices_woocommerce[sequential_orders]" type="checkbox" /> <?php _e( 'Enable', 'flexible-invoices-woocommerce' ); ?></label>
			    	<br />
			    	<span class="description"><?php _e( 'In new stores order numbers begin from 1. In existing stores numbers continue from the last order number.', 'flexible-invoices-woocommerce' ); ?></span>
			    </td>
			</tr>

	        <tr valign="top">
	            <th class="titledesc" scope="row"><?php _e( 'Invoice Status', 'flexible-invoices-woocommerce' ); ?></th>

	            <td class="forminp forminp-checkbox">
	            	<label for="inspire_invoices_auto_paid_status"> <input <?php if($this->getSettingValue('auto_paid_status') == 'on'):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_auto_paid_status" name="inspire_invoices_woocommerce[auto_paid_status]" type="checkbox" /> <?php _e( 'Change invoice status to paid after order is completed', 'flexible-invoices-woocommerce' ); ?></label>
	            </td>
	        </tr>
		</tbody>
	</table>

	<h3><?php _e( 'Invoices Settings', 'flexible-invoices-woocommerce' ); ?></h3>

	<table class="form-table">
		<tbody>
			<tr valign="top">
			    <th class="titledesc" scope="row">
			    	<label for="inspire_invoices_send_invoice_mail_when_status"><?php _e( 'Automatic sending', 'flexible-invoices-woocommerce' ); ?></label>
			    </th>

			    <?php
			    	$statuses = wc_get_order_statuses();
			    	$statusVal = $this->getSettingValue('send_invoice_mail_when_status');
			    ?>
			    <td class="forminp forminp-text">
			    	<select id="inspire_invoices_send_invoice_mail_when_status" name="inspire_invoices_woocommerce[send_invoice_mail_when_status]">
			    		<option value=""><?php _e( 'Do not send', 'flexible-invoices-woocommerce' ); ?></option>
			    		<?php foreach ( $statuses as $status => $status_display ): ?>
                            <?php $status = str_replace ( 'wc-', '', $status ); ?>
                            <option value="<?php echo $status; ?>" <?php if ( $status == $statusVal ): ?>selected="selected"<?php endif; ?>><?php echo $status_display; ?></option>
			    		<?php endforeach; ?>
			    	</select>

			    	<span class="description"><?php _e( 'If you want to send invoices automatically to the customer select order status. When order status is changed to selected an invoice link will be attached to e-mail.', 'flexible-invoices-woocommerce' ); ?></span>
			    </td>
			</tr>

			<tr valign="top">
			    <th class="titledesc" scope="row">
			    	<label for="inspire_invoices_date_of_sale"><?php _e( 'Date of sale on the invoice', 'flexible-invoices-woocommerce' ); ?></label>
			    </th>

			    <?php
			    	$date_of_sale_options = array(
			    			'order_date' => __( 'Use order date', 'flexible-invoices-woocommerce' ),
			    			'order_completed' => __( 'Use order completed date', 'flexible-invoices-woocommerce' ),
			    	);
			    	$date_of_sale_val = $this->getSettingValue('date_of_sale');
			    ?>
			    <td class="forminp forminp-text">
			    	<select id="inspire_invoices_date_of_sale" name="inspire_invoices_woocommerce[date_of_sale]">
			    		<?php foreach ( $date_of_sale_options as $key => $date_of_sale ): ?>
			    		   <option value="<?php echo $key; ?>" <?php if( $key == $date_of_sale_val): ?>selected="selected"<?php endif; ?>><?php echo $date_of_sale; ?></option>
			    		<?php endforeach; ?>
			    	</select>

			    	<span class="description"><?php _e( 'Set which date will be date of sale on the invoice.', 'flexible-invoices-woocommerce' ); ?></span>
			    </td>
			</tr>

            <tr valign="top">
                <th class="titledesc" scope="row">
                	<?php _e( 'Variations', 'flexible-invoices-woocommerce' ); ?>
                </th>

                <td class="forminp forminp-text">
                	<label><input <?php if ( $this->getSettingValue('add_variant_info') == 'on' ):  ?>checked="checked"<?php endif; ?>  id="inspire_invoices_product_variants" name="inspire_invoices_woocommerce[add_variant_info]" type="checkbox" /> <?php _e( 'Add variations to invoices', 'flexible-invoices-woocommerce' ); ?></label>
                </td>
            </tr>

	        <tr valign="top">
	            <th class="titledesc" scope="row"><?php _e( 'Free Orders', 'flexible-invoices-woocommerce' ); ?></th>

	            <td class="forminp forminp-checkbox">
	            	<label for="inspire_invoices_zero_invoice"> <input <?php if($this->getSettingValue('zero_invoice') == 'on'):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_zero_invoice" name="inspire_invoices_woocommerce[zero_invoice]" type="checkbox" /> <?php _e( 'Do not automatically issue invoices for free orders', 'flexible-invoices-woocommerce' ); ?></label>
	            </td>
	        </tr>

	        <tr valign="top">
	            <th class="titledesc" scope="row"><?php _e( 'Free line items', 'flexible-invoices-woocommerce' ); ?></th>

	            <td class="forminp forminp-checkbox">
	            	<label for="inspire_invoices_zero_product"> <input <?php if($this->getSettingValue('zero_product') == 'on'):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_zero_product" name="inspire_invoices_woocommerce[zero_product]" type="checkbox" /> <?php _e( 'Do not add free line items to invoices (includes free products and free shipping)', 'flexible-invoices-woocommerce' ); ?></label>
	            </td>
	        </tr>

            <tr valign="top">
                <th class="titledesc" scope="row">
                	<?php _e( 'Order number', 'flexible-invoices-woocommerce' ); ?>
                </th>

                <td class="forminp forminp-text">
                	<label><input <?php if($this->getSettingValue('add_order_id') == 'on'):  ?>checked="checked"<?php endif; ?>  id="inspire_invoices_order_add_order_id" name="inspire_invoices_woocommerce[add_order_id]" type="checkbox" /> <?php _e( 'Add order number to an invoice', 'flexible-invoices-woocommerce' ); ?></label>
                </td>
            </tr>

	        <tr valign="top">
	            <th class="titledesc" scope="row"><?php _e( 'SKU', 'flexible-invoices-woocommerce' ); ?></th>

	            <td class="forminp forminp-checkbox">
	            	<label for="inspire_invoices_get_sku"> <input <?php if($this->getSettingValue('get_sku') == 'on'):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_get_sku" name="inspire_invoices_woocommerce[get_sku]" type="checkbox" /> <?php _e( 'Use SKU numbers on invoices', 'flexible-invoices-woocommerce' ); ?></label>
	            </td>
	        </tr>
        </tbody>
	</table>

    <h3><?php _e( 'Checkout', 'flexible-invoices-woocommerce' ); ?></h3>

    <p><strong><?php _e( 'Warning.', 'flexible-invoices-woocommerce' ); ?></strong> <?php printf( __( 'If you use <a href="%s" title="Flexible Checkout Fields WooCommerce" target="_blank">a plugin for editing checkout fields</a> it may override the following settings.', 'flexible-invoices-woocommerce' ),  'http://www.wpdesk.pl/sklep/woocommerce-checkout-fields/' ); ?></p>

	<table class="form-table">
	    <tbody>
            <tr valign="top">
				<th class="titledesc" scope="row"><?php _e( 'Ask the customer if he wants an invoice', 'flexible-invoices-woocommerce' ); ?></th>

				<td class="forminp forminp-checkbox">
					<?php $add_invoice_ask = $this->getSettingValue('add_invoice_ask_field'); ?>
					<label for="inspire_invoices_add_invoice_ask_field"> <input <?php if (($add_invoice_ask == 'on')):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_add_invoice_ask_field" name="inspire_invoices_woocommerce[add_invoice_ask_field]" type="checkbox" /> <?php _e( 'Enable', 'flexible-invoices-woocommerce' ); ?></label>
            		<br />
            		<span class="description"><?php _e( 'If enabled the customer can choose to get an invoice. If automatinc sending is enabled invoices will be issued only for these orders.', 'flexible-invoices-woocommerce' ); ?></span>
				</td>
			</tr>

            <tr valign="top">
				<th class="titledesc" scope="row"><?php _e( 'Add VAT Number field to checkout', 'flexible-invoices-woocommerce' ); ?></th>

				<td class="forminp forminp-checkbox">
					<?php $add_nip = $this->getSettingValue('add_nip_field'); ?>
					<label for="inspire_invoices_add_nip_field"> <input <?php if (($add_nip == 'on') || is_null($add_nip)):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_add_nip_field" name="inspire_invoices_woocommerce[add_nip_field]" type="checkbox" /> <?php _e( 'Enable', 'flexible-invoices-woocommerce' ); ?></label>
				</td>
			</tr>

            <tr valign="top" class="nip-additional-fields">
            	<th class="titledesc" scope="row"><label for="inspire_invoices_nip_label"><?php _e( 'Label', 'flexible-invoices-woocommerce' ); ?></label></th>

            	<td class="forminp forminp-text">
            		<?php $nip_label = $this->getSettingValue('nip_label'); ?>
            		<input id="inspire_invoices_nip_label" class="regular-text" name="inspire_invoices_woocommerce[nip_label]" value="<?php echo (isset($nip_label) && !empty($nip_label)) ? $nip_label : __( 'VAT Number', 'flexible-invoices-woocommerce' ) ?>" type="text" />
            	</td>
            </tr>

            <tr valign="top" class="nip-additional-fields">
            	<th class="titledesc" scope="row"><label for="inspire_invoices_nip_placeholder"><?php _e( 'Placeholder', 'flexible-invoices-woocommerce' ); ?></label></th>

            	<td class="forminp forminp-text">
            		<?php $nip_placeholder = $this->getSettingValue('nip_placeholder'); ?>
            		<input id="inspire_invoices_nip_placeholder" class="regular-text" name="inspire_invoices_woocommerce[nip_placeholder]" value="<?php echo (!empty($nip_placeholder) || $nip_placeholder == '') ? $nip_placeholder : __( 'Enter VAT Number', 'flexible-invoices-woocommerce' ) ?>" type="text" />
            	</td>
            </tr>

            <tr valign="top" class="nip-additional-fields">
            	<th class="titledesc" scope="row"><?php _e( 'VAT Number field required', 'flexible-invoices-woocommerce' ); ?></th>

            	<td class="forminp forminp-checkbox">
            		<label for="inspire_invoices_nip_required"> <input <?php if($this->getSettingValue('nip_required') == 'on'):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_nip_required" name="inspire_invoices_woocommerce[nip_required]" type="checkbox" /> <?php _e( 'Enable', 'flexible-invoices-woocommerce' ); ?></label>
            	</td>
            </tr>

            <tr valign="top" class="nip-additional-fields">
            	<th class="titledesc" scope="row"><?php _e( 'Validate VAT Number', 'flexible-invoices-woocommerce' ); ?></th>

            	<td class="forminp forminp-checkbox">
            		<label for="inspire_invoices_validate_nip"> <input <?php if($this->getSettingValue('validate_nip') == 'on'):  ?>checked="checked"<?php endif; ?> id="inspire_invoices_validate_nip" name="inspire_invoices_woocommerce[validate_nip]" type="checkbox" /> <?php _e( 'Enable', 'flexible-invoices-woocommerce' ); ?></label>
            		<br />
            		<span class="description"><?php _e( 'VAT Number will have to be entered without hyphens, spaces and optionally can be prefixed with country code.', 'flexible-invoices-woocommerce' ); ?></span>
            	</td>
            </tr>
		</tbody>
	</table>

	<?php do_action('inspire_invoices_after_display_tab_woocommerce'); ?>

	<p class="submit"><input type="submit" value="<?php _e( 'Save changes', 'flexible-invoices-woocommerce' ); ?>" class="button button-primary" id="submit" name=""></p>
</form>

