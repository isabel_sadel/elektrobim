<?php
/**
 * Plugin Name:     Ulepszenie SEO
 * Plugin URI:      https://january15th.com
 * Description:     Dopasowuje Wordpressa pod część wymogów SEO
 * Author:          January 15th Ltd
 * Author URI:      https://january15th.com
 * Text Domain:     j15-seo-improve
 * Domain Path:     /languages
 * Version:         1.0
 *
 * @package         J15_Seo_Improve
 * @version         1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_filter( 'wpseo_canonical', 'j15_fix_canonical_url', 10, 1 );

/**
 * Remove canonical url.
 *
 * @param string $url Canonical URL.
 *
 * @return bool
 *
 * @since 1.0.0
 */
function j15_fix_canonical_url( $url ) {


	if ( ! is_archive() ) {
		global $post;

		if ( get_post_permalink( $post ) !== $url ) {
			$url = get_post_permalink( $post );
		}

	} else {
		$category = get_queried_object();
		if ( get_category_link( $category->term_id ) ) {
			$url = get_category_link( $category->term_id );
		}
	}

	return $url;
}

