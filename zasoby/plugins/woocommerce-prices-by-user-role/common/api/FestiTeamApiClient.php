<?php

class FestiTeamApiClient
{
    public static function addInstallStatistics($idPlugin)
    {
        $url = "/statistics/plugin/".$idPlugin."/install/";
        
        $params = array();

        $ip = self::getIP();
        if ($ip) {
            $params['ip'] = $ip;
        }

        if (!empty($_SERVER['SERVER_NAME'])) {
            $params['host'] = $_SERVER['SERVER_NAME'];
        }
        
        if (function_exists('get_option')) {
            $params['admin_email'] = get_option('admin_email');
        }
        
        static::_api($url, $params);
    } // end addInstallStatictics
    
    private static function _api($url, $params)
    {
        $url = 'https://api.festi.team'.$url;
        
        $params = array('http' => array(
            'method' => 'POST',
            'content' => http_build_query($params)
        ));
        
        $ctx = stream_context_create($params);
        $fp = @fopen($url, 'rb', false, $ctx);
        if ($fp) {
            @stream_get_contents($fp);
        }
    } // end _api

    public static function getIP()
    {
        $ip = false;
        if (isset($_SERVER)) {
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
                $ip = $_SERVER['HTTP_CLIENT_IP'];
            } else if (isset($_SERVER['REMOTE_ADDR'])) {
                $ip = $_SERVER['REMOTE_ADDR'];
            }
        } else {
            if (getenv('HTTP_X_FORWARDED_FOR')) {
                $ip = getenv('HTTP_X_FORWARDED_FOR');
            } elseif (getenv('HTTP_CLIENT_IP')) {
                $ip = getenv('HTTP_CLIENT_IP');
            } else {
                $ip = getenv('REMOTE_ADDR');
            }
        }

        return $ip;
    } // end getIP
}