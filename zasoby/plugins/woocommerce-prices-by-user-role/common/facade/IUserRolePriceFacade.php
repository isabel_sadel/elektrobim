<?php

interface IUserRolePriceFacade
{
    public function updatePriceFilterQueryForProductsSearch(
        $wordPressQuery,
        $eCommerceQuery,
        $instance
    );
}
