<?php
    
class WooUserRoleHideProductModule extends AbstractWooUserRoleModule
{
    public function onHideProductByUserRole($query)
    {
        $hideProducts = $this->engine->getOptions(
            PRICE_BY_ROLE_HIDDEN_PRODUCT_OPTIONS
        );

        if (!$hideProducts) {
            $hideProducts = array();
        }

        if ($this->_hasHideProductByUserRole($hideProducts)) {
            $userRole = $this->engine->userRole;
            if (!$userRole) {
                $userRole = key($hideProducts);
            }

            $idProduct = $hideProducts[$userRole];
            $query->set('post__not_in', $idProduct);
        }
    } // end onHideProductByUserRole
    
    private function _hasHideProductByUserRole($hideProducts)
    {
        $userRole = $this->engine->userRole;

        if (!$userRole && !empty($hideProducts)) {
            $userRole = key($hideProducts);
        }

        return !is_admin() &&
                $userRole && 
                array_key_exists($userRole, $hideProducts);
    } // end _hasHideProductByUserRole
}