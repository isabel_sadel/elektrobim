<?php

/**
 * Plugin Name: Lee Framework
 * Plugin URI: http://leetheme.com
 * Description: Add shortcodes and custom post types for Leetheme's Themes
 * Version: 1.0
 * Author: Derry Vu
 * Author URI: derryvu@gmail.com
 * License: http://www.gnu.org/licenses/old-licenses/gpl-2.0.html
 * Text Domain: leeframework
 * Domain Path: /languages
 */

define('LEE_FRAMEWORK_ACTIVED', true, true);
define('LEE_FRAMEWORK_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('LEE_FRAMEWORK_PLUGIN_URL', plugin_dir_url(__FILE__));

// Back-end
if (is_admin()) {
    foreach (glob(LEE_FRAMEWORK_PLUGIN_PATH . 'admin/incls/*.php') as $file) {
        include_once $file;
    }
}

// Includes
foreach (glob(LEE_FRAMEWORK_PLUGIN_PATH . 'includes/incls/*.php') as $file) {
    include_once $file;
}

// Include post-type
foreach (glob(LEE_FRAMEWORK_PLUGIN_PATH . 'post-type/incls/*.php') as $file) {
    include_once $file;
}

add_action('plugins_loaded', 'lee_framework_load_textdomain');
function lee_framework_load_textdomain() {
    load_plugin_textdomain('lee_framework', false, LEE_FRAMEWORK_PLUGIN_PATH . 'languages/');
}