<?php if($count = wp_count_posts()->publish):?>
<div class="lt-sc-blogs-list">
    <?php while($recentPosts->have_posts()) :
        $recentPosts->the_post();
        $id = get_the_ID();?>
        <div class="row lt-sc-blogs-row">
            <div class="large-2 small-2 columns">
                <div class="post-date">
                    <span class="post-date-day"><?php echo get_the_time('d', $id); ?></span>
                    <span class="post-date-month"><?php echo get_the_time('M', $id); ?></span>
                </div>
            </div>
            <div class="large-10 small-10 columns">
                <div class="post-content">
                    <h5><a href="<?php the_permalink();?>" title="<?php the_title();?>"><?php the_title();?></a></h5>
                    <div class="entry-blog">
                        <div class="clearfix"></div>
                        <div class="lt-info-short">
                            <?php echo lt_limit_words(get_the_content(), 15); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile;?>
</div>
<?php endif;?>