<div class="row group-blogs">
    <div class="large-12 columns">
        <div class="blog-grid blog-grid-style-2">
            <ul class="small-block-grid-2 medium-block-grid-3 large-block-grid-4 grid" data-product-per-row="4">
                <?php
                    $k = 0;
                    $count = wp_count_posts()->publish;
                    if ($count > 0) {
                        while ( $recentPosts->have_posts() ) {
                            echo "<li>";
                            $recentPosts->the_post();
                            if ($k % 2 == 0) {
                ?>
                                <div class="lt-blog-info lt-blog-img-bottom">
                                    <div class="blog_title">
                                        <h2>
                                            <a href="<?php the_permalink() ?>" title="<?php echo trim( strip_tags( get_the_title() ) );?>"><?php the_title(); ?></a>
                                        </h2>
                                    </div>
                                    <div class="post-date">
                                        <span class="lt-post-date primary-color">
                                            <?php echo get_the_time('d', get_the_ID()); ?>
                                            / <?php echo get_the_time('F', get_the_ID()); ?>
                                        </span>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="lt-info-short">
                                        <?php echo lt_limit_words(get_the_content(), 15); ?>
                                    </div>
                                </div>
                                <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                    <div class="entry-blog">
                                        <div class="blog-image">
                                            <div class="blog-image-attachment" style="overflow:hidden;">
                                                <?php
                                                if (has_post_thumbnail() ):
                                                    the_post_thumbnail('lt-list-thumb', array(
                                                        'alt' => trim( strip_tags( get_the_title() ) )
                                                    ));
                                                else:
                                                    echo '<img src="'.get_template_directory_uri().'/images/placeholder.png" alt="" />';
                                                endif;
                                                ?>
                                                <div class="image-overlay"></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                <?php
                            } else{
                ?>
                                <a href="<?php the_permalink();?>" title="<?php the_title();?>">
                                    <div class="entry-blog">
                                        <div class="blog-image">
                                            <div class="blog-image-attachment" style="overflow:hidden;">
                                                <?php
                                                if (has_post_thumbnail() ):
                                                    the_post_thumbnail('lt-list-thumb', array(
                                                        'alt' => trim( strip_tags( get_the_title() ) )
                                                    ));
                                                else:
                                                    echo '<img src="'.get_template_directory_uri().'/images/placeholder.png" alt="" />';
                                                endif;
                                                ?>
                                                <div class="image-overlay"></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <div class="lt-blog-info lt-blog-img-top">
                                    <div class="blog_title">
                                        <h2>
                                            <a href="<?php the_permalink() ?>" title="<?php echo trim( strip_tags( get_the_title() ) );?>"><?php the_title(); ?></a>
                                        </h2>
                                    </div>
                                    <span class="lt-post-date primary-color">
                                        <?php echo get_the_time('d', get_the_ID()); ?>
                                        / <?php echo get_the_time('F', get_the_ID()); ?>
                                    </span>
                                    <div class="clearfix"></div>
                                    <div class="lt-info-short">
                                        <?php echo lt_limit_words(get_the_content(), 15); ?>
                                    </div>
                                </div>
                <?php
                            }
                            echo '</li>';
                            $k++;
                        }
                    }
                ?>
            </ul>
        </div>
    </div>
</div> 