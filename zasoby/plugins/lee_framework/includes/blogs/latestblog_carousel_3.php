<div class="row group-slider">
    <div class="group-blogs lt-blog-carousel-3 lt-slider owl-carousel" data-columns="<?php echo esc_attr($columns_number); ?>" data-columns-small="<?php echo esc_attr($columns_number_small); ?>" data-columns-tablet="<?php echo esc_attr($columns_number_tablet); ?>">
        <?php while ($recentPosts->have_posts()) {
            $recentPosts->the_post();
            ?>
            <div class="blog_item">

                <div class="large-12 columns">
                    <div class="lt-content-group">
                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                            <div class="entry-blog">
                                <div class="blog-image img_left">
                                    <div class="blog-image-attachment" style="overflow:hidden;">
                                        <?php
                                        if (has_post_thumbnail()):
                                            the_post_thumbnail('lt-list-thumb', array(
                                                'alt' => trim(strip_tags(get_the_title()))
                                            ));
                                        else:
                                            echo '<img src="' . get_template_directory_uri() . '/images/placeholder.png" alt="" />';
                                        endif;
                                        ?>
                                        <div class="image-overlay"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="lt-blog-info-slider">
                            <span class="lt-post-date">
    <?php echo get_the_time('d', get_the_ID()); ?>
                                / <?php echo get_the_time('F', get_the_ID()); ?>
                            </span>
                            <div class="blog_title"><h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2></div>
                            <div class="lt-info-short">
    <?php echo lt_limit_words(get_the_content(), 15); ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
<?php } ?>
    </div> 
</div> 