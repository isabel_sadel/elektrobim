<?php

add_shortcode("lt_tag_cloud", "lt_sc_tag_cloud");

function lt_sc_tag_cloud($atts, $content = null) {
    extract(shortcode_atts(array(
        'number' => '5',
        'title' => '',
        'parent' => '0',
        'disp_type' => 'product_cat',
        'style' => '',
        'el_class' => ''
    ), $atts));

    
    $ids = (isset($atts['ids']) && $atts['ids'] != '') ? array_map('trim', explode(',', $atts['ids'])) : array();
    $disp_type = in_array($disp_type, array('product_cat', 'product_tag')) ? $disp_type : 'product_cat';

    $content = '';
    $args = array(
        'taxonomy' => $disp_type,
        'echo' => false
    );

    $tag_class = ' lt-tag-cloud';
    $tag_class .= ($disp_type == 'product_tag') ? ' lt-tag-products-cloud' : '';

    if ((int) $number) {
        $args['number'] = (int) $number;
    }

    $tag_cloud = wp_tag_cloud(apply_filters('widget_tag_cloud_args', $args));
    $el_class = (trim($el_class) != '') ? ' ' . esc_attr($el_class) : '';
    $style = (trim($style) != '') ? ' ' . $style : '';
    $content .= '<div class="widget_tag_cloud' . $el_class . $style . '">';
    if ($title) {
        $content .= '<div class="row">';
        $content .= '<div class="large-12 columns">';
        $content .= '<h3 class="section-title"><span>' . esc_attr($title) . '</span></h3>';
        $content .= '<div class="bery-hr full"></div>';
        $content .= '</div>';
        $content .= '</div>';
    }

    $content .= '<div class="tagcloud' . $tag_class . '" data-taxonomy="' . $disp_type . '">' . $tag_cloud . '</div>';
    $content .= '</div>';

    return $content;
}
