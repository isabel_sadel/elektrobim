<?php

if (!function_exists('lt_feedburner_subscription_shortcode')) {

    function lt_feedburner_subscription_shortcode($atts) {
        extract(shortcode_atts(array(
            'title' => 'Newsletter',
            'intro_text' => '',
            'intro_text_2' => '',
            'button_text' => 'Subscribe',
            'placeholder_text' => 'Enter your email address',
            'feedburner_id' => '',
            'style' => 'style-1',
            'el_class' => ''
        ), $atts));

        if (!class_exists('LT_Feedburner_Subscription_Widget')) {
            return '';
        }

        $instance = compact('title', 'intro_text', 'intro_text_2', 'button_text', 'placeholder_text', 'feedburner_id');

        ob_start();
        echo '<div class="lt-feedburner-subscription-shortcode ' . $style . ' ' . $el_class . '">';
        the_widget('LT_Feedburner_Subscription_Widget', $instance);
        echo '</div>';

        return ob_get_clean();
    }

}
add_shortcode('lt_feedburner_subscription', 'lt_feedburner_subscription_shortcode');
