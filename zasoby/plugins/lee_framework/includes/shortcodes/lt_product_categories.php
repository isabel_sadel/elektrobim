<?php
add_shortcode("lt_product_categories", "lt_sc_product_categories");
function lt_sc_product_categories($atts, $content = null) {
    global $lt_opt;
    $_delay_item = (isset($lt_opt['delay_overlay']) && (int) $lt_opt['delay_overlay']) ? (int) $lt_opt['delay_overlay'] : 100;
    $delay_animation_product = $_delay_item;
    extract(shortcode_atts(array(
        'number' => '5',
        'title' => '',
        'orderby' => 'name',
        'order' => 'ASC',
        'hide_empty' => 1,
        'parent' => '0',
        'infinitive' => 'false',
        'disp_type' => 'Horizontal',
        'columns_number' => '4',
        'columns_number_small' => '1',
        'columns_number_tablet' => '3',
        'number_vertical' => '2',
        'el_class' => ''
    ), $atts));

    $el_class = trim($el_class) != '' ? ' ' . esc_attr($el_class) : '';
    $ids = (isset($atts['ids']) && $atts['ids'] != '') ? 
        array_map('trim', explode(',', $atts['ids'])) : array();
    $hide_empty = (bool) $hide_empty ? 1 : 0;

    $args = array(
        'orderby' => $orderby,
        'order' => $order,
        'hide_empty' => $hide_empty,
        'include' => $ids,
        'pad_counts' => true,
        'child_of' => $parent
    );

    $product_categories = get_terms('product_cat', $args);
    $product_categories = ($parent !== "") ? wp_list_filter(
        $product_categories,
        array('parent' => $parent)
    ) : $product_categories;
    $product_categories = $number ? array_slice($product_categories, 0, $number) : $product_categories;

    ob_start();
    if (function_exists('wc_print_notices') && $product_categories) :
        if ($title): ?>
            <div class="row<?php echo $el_class; ?>">
                <div class="large-12 columns">
                    <h3 class="section-title"><span><?php echo esc_attr($title); ?></span></h3>
                    <div class="bery-hr full"></div>
                </div>
            </div>
        <?php endif; ?>

        <?php switch ($disp_type) {
            case 'Vertical':
                ?>
                <div class="vertical-slider lt-category-slider-vertical lt-mgr-y-20 wow fadeInUp<?php echo $el_class; ?>" data-wow-duration="1s" data-wow-delay="<?php echo esc_attr($delay_animation_product); ?>ms">
                    <div class="lt-vertical-slider" data-show="<?php echo esc_attr($number_vertical); ?>">
                        <?php
                        foreach ($product_categories as $category) :
                            include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/product_layouts/content-product_cat_vertical.php';
                        endforeach;
                        ?>
                    </div> 
                </div>
                <?php
                break;
            case 'Horizontal1':
            case 'Horizontal2':
            default:
                $data_margin = $disp_type == 'Horizontal2' ? '20' : '10';
                $class_hozi = $disp_type == 'Horizontal2' ? 'lt-category-horizontal-2' : 'lt-category-horizontal-1';
                $disable_nav = $disp_type == 'Horizontal2' ? 'false' : 'true';
                $file = $disp_type == 'Horizontal2' ? '2' : '1';
                ?>
                <div class="group-slider category-slider lt-category-slider-horizontal<?php echo $el_class; ?>">
                    <div class="lt-slider products-group owl-carousel <?php echo $class_hozi; ?>" data-autoplay="false" data-loop="true" data-disable-nav="<?php echo $disable_nav; ?>" data-columns="<?php echo esc_attr($columns_number); ?>" data-columns-small="<?php echo esc_attr($columns_number_small); ?>" data-columns-tablet="<?php echo esc_attr($columns_number_tablet); ?>" data-margin="<?php echo $data_margin; ?>">
                        <?php
                        foreach ($product_categories as $category) :
                            include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/product_layouts/content-product_cat_horizontal_' . $file . '.php';
                            $delay_animation_product += $_delay_item;
                        endforeach;
                        ?>
                    </div> 
                </div>
                <?php
                break;
        }
        woocommerce_reset_loop();
    endif;

    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

function lt_category_thumbnail($category, $type = 'lt-category-vertical') {
    $small_thumbnail_size = apply_filters('subcategory_archive_thumbnail_size', $type);
    $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);

    if ($thumbnail_id) {
        $image = wp_get_attachment_image_src($thumbnail_id, $small_thumbnail_size);
        $image = $image[0];
    } else {
        $image = wc_placeholder_img_src();
    }

    if ($image) {
        // Prevent esc_url from breaking spaces in urls for image embeds
        // Ref: https://core.trac.wordpress.org/ticket/23605
        $image = str_replace(' ', '%20', $image);

        echo '<img src="' . esc_url($image) . '" alt="' . esc_attr($category->name) . '" />';
    }
}
