<?php

function lt_sc_posts($atts, $content = null) {
    extract(shortcode_atts(array(
        "title" => '',
        "align" => '',
        'show_type' => '0',
        "posts" => '8',
        "category" => '',
        'columns_number' => '4',
        'columns_number_small' => '1',
        'columns_number_tablet' => '2',
    ), $atts));
    
    ob_start();
    $align = ($align == 'center') ? ' text-center' : '';
    if ($title != '') { ?> 
        <div class="row">
            <div class="large-12 columns<?php echo esc_attr($align); ?>">
                <h3 class="section-title"><span><?php echo esc_attr($title); ?></span></h3>
                <div class="bery-hr medium"></div>
            </div>
        </div>
    <?php
    }
    $args = array(
        'post_status' => 'publish',
        'post_type' => 'post',
        'category_name' => $category,
        'posts_per_page' => $posts
    );

    $recentPosts = new WP_Query($args);

    if ($recentPosts->have_posts()) {
        switch ($show_type) {
            case 1:
                include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/blogs/latestblog_carousel_2.php';
                break;
            case 2:
                include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/blogs/latestblog_carousel_3.php';
                break;
            case 3:
                include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/blogs/latestblog_grid_1.php';
                break;
            case 4:
                include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/blogs/latestblog_grid_2.php';
                break;
            case 5:
                include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/blogs/latestblog_list.php';
                break;
            default:
                include LEE_FRAMEWORK_PLUGIN_PATH . 'includes/blogs/latestblog_carousel_1.php';
                break;
        }
    }

    wp_reset_postdata();
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}
add_shortcode("recent_post", "lt_sc_posts");

if (!function_exists('lt_limit_words')) {

    function lt_limit_words($string, $word_limit) {
        $words = explode(' ', $string, ($word_limit + 1));
        if (count($words) <= $word_limit) {
            return $string;
        }
        array_pop($words);
        return implode(' ', $words) . ' ...';
    }

}