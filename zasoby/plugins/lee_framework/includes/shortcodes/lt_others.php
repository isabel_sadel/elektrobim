<?php
/* SERVICE BOX */
function lt_sc_service_box($atts, $content = null) {
    extract(shortcode_atts(array(
        'service_icon' => '',
        'service_title' => '',
        'service_desc' => '',
        'service_style' => 'style-1',
        'service_hover' => '',
        'el_class' => ''
    ), $atts));
    ob_start();
    ?>
    <div class="service-block <?php echo esc_attr($service_style); ?> <?php echo esc_attr($el_class); ?>">
        <div class="box">
            <div class="service-icon <?php echo esc_attr($service_hover); ?> <?php echo esc_attr($service_icon) ?>"></div>
            <div class="service-text">
                <?php if (isset($service_title) && $service_title != '') { ?>
                    <div class="service-title"><?php echo esc_attr($service_title); ?></div>
                <?php } ?>
                <?php if (isset($service_desc) && $service_desc != '') { ?>
                    <div class="service-desc"><?php echo esc_attr($service_desc); ?></div>
                <?php } ?>
            </div>
        </div>
    </div>

    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

function lt_sc_client($atts, $content) {
    extract(shortcode_atts(array(
        "img_src" => '',
        "name" => '',
        "company" => '',
        "text_color" => '#fff',
        "content" => $content,
        'text_align' => 'center',
        'el_class' => ''
    ), $atts));

    $content = (trim($content) != '') ? lt_fixShortcode($content) : '';
    $el_class = (trim($el_class) != '') ? ' ' . asc_attr($el_class) : '';

    switch ($text_align) {
        case 'right':
        case 'left':
        case 'justify':
            $el_class .= ' text-' . $text_align;
            break;
        case 'center':
        default:
            $el_class .= ' text-center';
            break;
    }

    $image = '';
    if ($img_src != '') {
        $image = wp_get_attachment_image_src($img_src, 'full');
        $image = '<img class="wow fadeInUp" data-wow-delay="200ms" data-wow-duration="1s" src="' . esc_url($image[0]) . '" alt="" />';
    }

    $text_color = esc_attr($text_color);
    $client = 
        '<div class="client large-12' . $el_class . '">' .
            '<div class="client-inner" style="color:' . $text_color . '">' .
                '<div class="client-info wow fadeInUp" data-wow-delay="100ms" data-wow-duration="1s">' .
                    '<div class="client-content" style="color:' . $text_color . '">' . $content . '</div>' .
                    '<div class="client-img-info">' .
                        '<div class="client-img">' . $image . '</div>' .
                        '<div class="client-name-post">' .
                            '<h4 class="client-name">' . $name . '</h4>' .
                            '<span class="client-pos" style="color:' . $text_color . '">' . $company . '</span>' .
                        '</div>' .
                    '</div>' .
                '</div>' .
            '</div>' .
        '</div>';

    return $client;
}

/* CONTACT US ELEMENT */
function lt_sc_contact_us($atts, $content = null) {
    extract(shortcode_atts(array(
        'contact_logo' => '',
        'contact_address' => '',
        'contact_phone' => '',
        'service_desc' => '',
        'contact_email' => '',
        'el_class' => ''
    ), $atts));
    ob_start();
    ?>
    <ul class="contact-information <?php echo esc_attr($el_class); ?>">
    <?php if (isset($contact_logo) && $contact_logo) { ?>
        <li class="contact-logo">
            <img src="<?php echo esc_attr($contact_logo); ?>" alt="Logo" />
        </li>
    <?php } ?>

    <?php if (isset($contact_address) && $contact_address) { ?>
        <li class="media">
            <div class="contact-icon"><i class="pe-7s-map-marker"></i></div>
            <div class="contact-text"><span><?php echo esc_attr($contact_address); ?></span></div>
        </li>
    <?php } ?>

    <?php if (isset($contact_email) && $contact_email) { ?>
        <li class="media">
            <div class="contact-icon"><i class="pe-7s-mail"></i></div>
            <div class="contact-text"><span><?php echo esc_attr($contact_email); ?></span></div>
        </li>
    <?php } ?>

    <?php if (isset($contact_phone) && $contact_phone) { ?>
        <li class="media">
            <div class="contact-icon"><i class="pe-7s-call"></i></div>
            <div class="contact-text"><span><?php echo esc_attr($contact_phone); ?></span></div>
        </li>
    <?php } ?>
    </ul>

    <?php
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
}

/* INSTAGRAM */
function lt_instagram($atts, $content = null) {
    extract(shortcode_atts(array(
        'photos' => '10',
        'username' => '',
        'username_show' => '',
        'instagram_text' => '',
        'el_class' => ''
    ), $atts));

    if (class_exists('null_instagram_widget')) {
        ob_start();
        the_widget(
            'null_instagram_widget', 
            array(
                'username' => $username,
                'target' => '_blank', 'number' => $photos
            ),
            array(
                'before_widget' => '<div class="lt-instagram items-' . $photos . ' ' . $el_class . '"><div class="username-text text-center hide-for-small"><i class="fa fa-instagram"></i><span class="primary-color">' . $username_show . '</span> ' . $instagram_text . ' </div>',
                'after_widget' => '</div>'
            )
        );

        $l = ob_get_contents();
        ob_end_clean();
        return $l;
    } else {
        esc_html_e('Please active Instagram Widget plugin to use this featured', 'lee_framework');
    }
}

/* TITLE */

function lt_title($atts, $content = null) {
    extract(shortcode_atts(array(
        'title_text' => '',
        'title_style' => 'style-1',
        'title_align' => '',
        'el_class' => '',
        'text' => '',
        'style' => '',
        'align' => ''
    ), $atts));

    $style_output = ($title_style != '') ? ' title-' . $title_style : '';
    $align_output = ($title_align != '') ? ' ' . $title_align : '';

    return '<h5 class="lt-title clearfix' . $style_output . $align_output . ' ' . $el_class . '"><span>' . $atts['title_text'] . '</span></h5>';
}

/* Opening Time */
function lt_opening_time($atts, $content = null) {
    extract(shortcode_atts(array(
        'weekdays_start' => '08:00',
        'weekdays_end' => '20:00',
        'sat_start' => '09:00',
        'sat_end' => '21:00',
        'sun_start' => '13:00',
        'sun_end' => '22:00'
    ), $atts));

    $content = '<ul class="lt-opening-time">';
        $content .= '<li><span class="lt-day-open">' . esc_html__('Monday - Friday', 'lee_framework') . '</span><span class="lt-time-open">' . $weekdays_start . ' - ' . $weekdays_end . '</span></li>';
        $content .= '<li><span class="lt-day-open">' . esc_html__('Saturday', 'lee_framework') . '</span><span class="lt-time-open">' . $sat_start . ' - ' . $sat_end . '</span></li>';
        $content .= '<li><span class="lt-day-open">' . esc_html__('Sunday', 'lee_framework') . '</span><span class="lt-time-open">' . $sun_start . ' - ' . $sun_end . '</span></li>';
    $content .= '</ul>';

    return $content;
}

add_shortcode("service_box", "lt_sc_service_box");
add_shortcode('client', 'lt_sc_client');
add_shortcode("contact_us", "lt_sc_contact_us");
add_shortcode('lee_instagram', 'lt_instagram');
add_shortcode('lee_title', 'lt_title');
add_shortcode('lt_opening_time', 'lt_opening_time');
