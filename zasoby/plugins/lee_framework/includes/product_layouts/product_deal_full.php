<?php
$style = '';
$style .= is_numeric($position_top) ? 'padding-top: ' . $position_top . '%;' : 'padding-top: ' . $position_top . ';';
$style = ($style) ? ' style="' . esc_attr($style) . '"' : '';

$style_content = '';
$style_content .= is_numeric($padding_content) ? 'padding: ' . $padding_content . '%;' : 'padding: ' . $padding_content . ';';
$style_content = ($style_content) ? ' style="' . esc_attr($style_content) . '"' : '';

?>
<div class="lt-sc-pdeal lt-sc-pdeal-full" data-id="<?php echo $_id;?>">
    <div class="lt-sc-p-info"<?php echo ($bg_color) ? $bg_color : '';?>>
        <div class="lt-sc-block-content for-small-padding-10 row"<?php echo $style_content;?>>
            <div class="large-6 columns">
                <div class="row">
                    <div class="large-7 medium-9 small-7 columns">
                        <div class="product-images-slider images-popups-gallery">
                            <div class="y-main-images-<?php echo $_id;?>">
                                <?php if($image_pri):?>
                                    <a href="<?php echo esc_url($link);?>" title="<?php echo esc_attr($title);?>" class="woocommerce-additional-image product-image">
                                        <img class="lt-pri-img lt-pri-<?php echo $_id;?> lazyOwl" src="<?php echo esc_attr($image_pri['link']);?>" alt="<?php echo esc_attr($title);?>" />
                                    </a>
                                <?php endif;?>
                                <?php
                                if($count_imgs) :
                                    foreach($img_disp as $key => $img): ?>
                                        <a href="<?php echo esc_url($link);?>" title="<?php echo esc_attr($title);?>" class="woocommerce-additional-image product-image">
                                            <img class="lt-pri-img lt-pri-<?php echo $_id;?> lazyOwl" src="<?php echo esc_attr($img['link']);?>" alt="<?php echo esc_attr($title);?>" />
                                        </a>
                                    <?php endforeach;
                                else :
                                    echo sprintf('<a href="%s" class="active-thumbnail"><img src="%s" /></a>', wc_placeholder_img_src(), wc_placeholder_img_src());
                                endif;?>
                            </div>
                        </div>
                    </div>
                    <div class="large-5 medium-3 small-5 columns"><?php echo $thumbs; ?></div>
                </div>
            </div>
            
            <div class="large-6 columns"<?php echo $style;?>>
                <div class="lt-sc-info">
                    <h5><?php echo '<span class="lt-text">'.esc_html__('From ', 'lee_framework').'</span>'.$product->get_categories();?></h5>
                    <h3>
                        <a href="<?php echo esc_url($link); ?>" title="<?php echo esc_attr($title); ?>">
                            <?php echo $title; ?>
                        </a>
                    </h3>
                    <div class="lt-sc-p-price"><?php echo $product->get_price_html(); ?></div>
                </div>
                
                <?php if($time_sale):?>
                    <div class="lt-sc-pdeal-countdown">
                        <span class="countdown" data-fomart="dhms" data-countdown="<?php echo esc_attr(date('M j Y H:i:s O',$time_sale)); ?>"></span>
                    </div>
                <?php endif?>
                
                <div class="lt-sc-desc"><?php echo apply_filters('woocommerce_short_description', $product->post->post_excerpt); ?></div>
                
                <?php lt_sc_product_group_button($product, false); ?>
                
            </div>
        </div>
    </div>
</div>