<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$products_cats = $product->get_categories();
list($fistpart) = explode(',', $products_cats);
$classTip = $toltip ? ' tip-top' : '';

?>
<div class="product-summary">
    <div class="product-interactions">
        <?php if(defined('YITH_WCWL')):?>
            <div class="btn-wishlist<?php echo $classTip;?>" data-prod="<?php echo (int) $product->id; ?>" data-tip="<?php esc_html_e('Wishlist', 'lee_framework'); ?>" title="<?php esc_html_e('Wishlist', 'lee_framework'); ?>">
                <div class="btn-link ">
                    <div class="wishlist-icon">
                        <span class="pe-icon pe-7s-like"></span>
                        <span class="hidden-tag lt-icon-text"><?php esc_html_e('Wishlist', 'lee_framework'); ?></span>
                    </div>
                </div>
            </div>
        <?php endif;?>
        
        <span class="hidden-tag lt-seperator"></span>
        
        <?php if(defined('YITH_WOOCOMPARE')):?>
            <div class="btn-compare<?php echo $classTip;?>" data-prod="<?php echo (int) $product->id; ?>" data-tip="<?php esc_html_e('Compare', 'lee_framework'); ?>" title="<?php esc_html_e('Compare', 'lee_framework'); ?>">
                <div class="btn-link">
                    <div class="compare-icon">
                        <span class="pe-icon pe-7s-repeat"></span>
                        <span class="lt-icon-text"><?php esc_html_e('Compare', 'lee_framework'); ?></span>
                    </div>
                </div>
            </div>
        <?php endif;?>
        
        <div class="add-to-link hidden-tag">
            <?php echo defined('YITH_WCWL') ? do_shortcode('[yith_wcwl_add_to_wishlist]') : ''; ?>
            <div class="woocommerce-compare-button">
                <?php echo defined('YITH_WOOCOMPARE') ? do_shortcode('[yith_compare_button]') : ''; ?>
            </div>
        </div>
    </div>
    
    <div class="clear"></div>
</div>
