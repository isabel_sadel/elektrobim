<?php
/* Plugin update */
require_once LEE_FRAMEWORK_PLUGIN_PATH . 'admin/plugin-updates/plugin-update-checker.php';

class LT_FW_Update {

    public static $_instance;
    private $check_url;

    public static function instance() {
        if (is_null(self::$_instance)) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __construct() {
        $this->check_url = 'http://leetheme.com/server_update/techstore/info.json';
        $this->update_plugin();
    }

    private function update_plugin() {
        $updatechecker = PucFactory::buildUpdateChecker(
            $this->check_url, __FILE__
        );
        $updatechecker->addQueryArgFilter(array($this, 'get_secretkey'));
    }

    public function get_secretkey($query) {
        $query['secret'] = 'foo';
        return $query;
    }

}
LT_FW_Update::instance();