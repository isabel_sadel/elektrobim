<?php

function lt_register_block() {
    $labels = array(
        'name' => __('Static blocks', 'lee_framework'),
        'singular_name' => __('Static blocks', 'lee_framework'),
        'add_new' => __('Add New Block', 'lee_framework'),
        'add_new_item' => __('Add New Block', 'lee_framework'),
        'edit_item' => __('Edit Block', 'lee_framework'),
        'new_item' => __('New Block', 'lee_framework'),
        'view_item' => __('View Block', 'lee_framework'),
        'search_items' => __('Search Blocks', 'lee_framework'),
        'not_found' => __('No Blocks found', 'lee_framework'),
        'not_found_in_trash' => __('No Blocks found in Trash', 'lee_framework'),
        'parent_item_colon' => __('Parent Block:', 'lee_framework'),
        'menu_name' => __('Static blocks', 'lee_framework')
    );

    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'description' => __('List Blocks', 'lee_framework'),
        'supports' => array('title', 'editor'),
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_position' => 5,
        'show_in_nav_menus' => false,
        'publicly_queryable' => false,
        'exclude_from_search' => false,
        'has_archive' => false,
        'query_var' => true,
        'can_export' => true,
        'rewrite' => false
    );
    register_post_type('lt_block', $args);

    if ($options = get_option('wpb_js_content_types')) {
        $check = true;
        foreach ($options as $key => $value) {
            if ($value == 'lt_block') {
                $check = false;
            }
        }
        if ($check) {
            $options[] = 'lt_block';
        }
    } else {
        $options = array('page', 'lt_block');
    }
    update_option('wpb_js_content_types', $options);
}
add_action('init', 'lt_register_block');

add_action('manage_blocks_posts_custom_column', 'my_manage_blocks_columns', 10, 2);
function my_manage_blocks_columns($column, $post_id) {
    switch ($column) {
        case 'shortcode' :
        default :
            echo (int) $post_id ? '<span style="background:#eee;font-weight:bold;"> [static_block id="' . $post_id . '"] </span>' : '';
            break;
    }
}

function lt_block_scripts() {
    global $typenow;
    if ('lt_block' == $typenow) {
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                <?php if (isset($_GET["post"]) && $_GET["post"]): ?>
                    var block_id = $('input#post_ID').val();
                    if ($('#original_post_status').val() === 'publish') {
                        $('#submitpost #minor-publishing').append('<div class="misc-pub-section shortcode-info"><span><i class="fa fa-code" style="font-size: 1.2em; margin-right: 5px;"></i> Shortcode: <b>[static_block id="' + block_id + '"]</b></span></div>');
                    }
                <?php endif; ?>
                if ($('#posts-filter').length) {
                    if ($('input[name="post_status"]').val() !== 'trash') {
                        $('#posts-filter table.wp-list-table thead tr').append('<td scope="col" id="shortcode" class="manage-column" style="width: 170px;"><span>Shortcode</span></td>');
                        $('#posts-filter table.wp-list-table tfoot tr').append('<td scope="col" id="shortcode" class="manage-column"><span>Shortcode</span></td>');

                        $('#posts-filter table.wp-list-table tbody tr').each(function () {
                            if ($(this).hasClass('status-publish')) {
                                var _post_id = ($(this).attr('id')).replace('post-', '');
                                $(this).append('<td data-colname="Shortcode"><b>[static_block id="' + _post_id + '"]</b></td>');
                            } else {
                                $(this).append('<td></td>');
                            }
                        });
                    }
                }
            });
        </script>
        <?php
    }
}
add_action('admin_head', 'lt_block_scripts');

function lt_block_shortcode($atts, $content = null) {
    extract(shortcode_atts(array(
        'id' => ''
    ), $atts));

    $content = get_post_field('post_content', (int) $id);
    
    return $content ? do_shortcode($content) : '';
}
add_shortcode('static_block', 'lt_block_shortcode');
