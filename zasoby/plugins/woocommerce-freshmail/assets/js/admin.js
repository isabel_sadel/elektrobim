jQuery(document).ready(function($) {
    $("#woocommerce_woocommerce_freshmail_user_agreement").change(function() {
        if ($(this).find("option:checked").val() == "yes") {
            $("#woocommerce_woocommerce_freshmail_custom_check_agreement").removeAttr('disabled');
            $("#woocommerce_woocommerce_freshmail_custom_check_agreement").closest('tr').show();
            $("#woocommerce_woocommerce_freshmail_checkbox_text").removeAttr('disabled');
            $("#woocommerce_woocommerce_freshmail_checkbox_text").closest('tr').show();
            $("#woocommerce_woocommerce_freshmail_checkbox_description").removeAttr('disabled');
            $("#woocommerce_woocommerce_freshmail_checkbox_description").closest('tr').show();
            $("#woocommerce_woocommerce_freshmail_checkbox_position").removeAttr('disabled');
            $("#woocommerce_woocommerce_freshmail_checkbox_position").closest('tr').show();
        } else {
            $("#woocommerce_woocommerce_freshmail_custom_check_agreement").attr('disabled', 'disabled');
            $("#woocommerce_woocommerce_freshmail_custom_check_agreement").closest('tr').hide();
            $("#woocommerce_woocommerce_freshmail_checkbox_text").attr('disabled', 'disabled');
            $("#woocommerce_woocommerce_freshmail_checkbox_text").closest('tr').hide();
            $("#woocommerce_woocommerce_freshmail_checkbox_description").attr('disabled', 'disabled');
            $("#woocommerce_woocommerce_freshmail_checkbox_description").closest('tr').hide();
            $("#woocommerce_woocommerce_freshmail_checkbox_position").attr('disabled', 'disabled');
            $("#woocommerce_woocommerce_freshmail_checkbox_position").closest('tr').hide();

        }
    });
    $("#woocommerce_woocommerce_freshmail_user_agreement").trigger("change");
});
