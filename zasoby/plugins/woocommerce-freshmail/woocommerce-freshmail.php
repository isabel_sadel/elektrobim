<?php
/*
  Plugin Name: WooCommerce Freshmail
  Description: Integracja WooCommerce z FreshMailem.
  Version: 1.6
  Author: WP Desk
  Plugin URI: https://www.wpdesk.pl/sklep/freshmail-woocommerce/
  Author URI: https://www.wpdesk.pl/
  Text Domain: woocommerce-freshmail
  Domain Path: /lang/
  Tested up to: 4.6.1

  Copyright 2016 WP Desk Ltd.

  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

/**
 * Required functions
 */
$wpdesk_helper_text_domain = 'woocommerce-freshmail';
require_once( plugin_basename( 'inc/wpdesk-functions.php' ) );

$plugin_data = array(
	'plugin'     => plugin_basename( __FILE__ ),
	'product_id' => 'WooCommerce Freshmail',
	'version'    => '1.6',
	'config_uri' => admin_url( 'admin.php?page=wc-settings&tab=integration&section=woocommerce_freshmail' )
);

$helper = new WPDesk_Helper_Plugin( $plugin_data );

define( 'WC_FRESHMAIL_VERSION', $plugin_data['version'] );
define( 'WC_FRESHMAIL_FILE', __FILE__ );

/**
 * Init the extension
 */
//if ( $helper->is_active() && in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins'))) ) { WAŻNE1

	/**
	 * Init the Freshmail API
	 */
	if ( ! class_exists( 'InspireFmRestApi' ) ) {
    	require_once( 'api/class.rest.php' );
	}

    function init_freshmail_integration() {

        if ( ! class_exists( 'WC_Freshmail_Integration' ) ) {

            class WC_Freshmail_Integration extends WC_Integration {

                private $rest = false;

                public function __construct() {

                    $this->id = 'woocommerce_freshmail';
                    $this->method_title = __( 'FreshMail', 'woocommerce' );
                    $this->method_description = __( 'Integracja WooCommerce z programem do wysyłki newsletterów. <a href="http://www.wpdesk.pl/docs/woocommerce-freshmail-docs/" target="_blank">Instrukcja konfiguracji &rarr;</a>', 'woocommerce-freshmail' );

					/**
					 * Load translation
					 */
                    if ( !function_exists( '__woocommerce_freshmail_localization' ) ) {
                    	function __woocommerce_freshmail_localization() {
                    		load_plugin_textdomain( 'woocommerce-freshmail', false, dirname( plugin_basename( __FILE__ ) ) . '/lang' );
                    	}
                    }

                    $this->enabled = $this->get_option('enabled');
                    $this->list_key = $this->get_option('list_key');
                    $this->api_secret = $this->get_option('api_secret');
                    $this->api_key = $this->get_option('api_key');
                    $this->save_to_list = $this->get_option('save_to_list');
                    $this->user_agreement = $this->get_option('user_agreement');
                    $this->field_name = $this->get_option('field_name');
                    $this->custom_check_agreement = $this->get_option('custom_check_agreement');
                    $this->checkbox_text = $this->get_option('checkbox_text');
					$this->checkbox_description = $this->get_option('checkbox_description');
                    $this->checkbox_position = $this->get_option('checkbox_position');
                    $this->products_main_list = $this->get_option('products_main_list');

					/**
					 * Admin functions
					 */
					if ( is_admin() ) {
						include_once( 'class/class-wc-freshmail-admin.php' );
					}

                    // Load the settings.
                    $this->init_form_fields();
                    $this->init_settings();

                    // Actions
                    add_action( 'woocommerce_update_options_integration_' . $this->id, array( $this, 'process_admin_options' ) );

                    // Add Checkbox to Checkout
					if ( $this->get_option( 'checkbox_position' ) == 'before' ) {
					    add_action( 'woocommerce_review_order_before_submit', array( $this, 'add_freshmail_checkbox' ) );
					} elseif ( $this->get_option('checkbox_position' ) == 'after_billing' ) {
					 add_action( 'woocommerce_after_checkout_billing_form', array( $this, 'add_freshmail_checkbox' ) );
					} else {
					    add_action( 'woocommerce_review_order_after_submit', array( $this, 'add_freshmail_checkbox' ) );
					}

					// Order Actions
					add_action( 'woocommerce_checkout_update_order_meta', array( $this, 'update_order_meta') );
					add_action( 'woocommerce_checkout_order_processed', array( $this, 'save_order') );
					add_action( 'woocommerce_order_status_changed', array( $this, 'change_order_status') );

                    // Add Action Links
                    add_filter( 'plugin_action_links_' . plugin_basename( __FILE__ ), array( $this, '__woocommerce_freshmail_action_links' ) );
                }

                public function fmCheckConnection() {
                    $rest = new InspireFmRestApi();
                    $rest->setApiKey($this->api_key);
                    $rest->setApiSecret($this->api_secret);
                    try {
                        $response = $rest->doRequest('ping');
                        return true;
                    } catch (Exception $e) {
                        error_log('InspireFmRestApi Exception: ' . print_r($e, true));
                        return false;
                    }
                }

                public function admin_options() {
                    parent::admin_options();

                    $status = '<span style="color:red;font-weight:bold;">' . __( 'Niepoprawne dane.', 'woocommerce-freshmail' )  . '</span> ' . __( 'Uzupełnij klucz API, API sekret i klucz listy, aby integracja działała poprawnie.', 'woocommerce-freshmail' );
                    if ( $this->api_key != '' && $this->api_secret != '' ) {
                        if ( $this->fmCheckConnection() ) {
                            $status = '<span style="color:green;font-weight:bold;">' . __( 'OK', 'woocommerce-freshmail' ) . '</span>';
                        }
                    }
                    ?>
                    <br/><?php printf( __( 'Status połączenia: %s', 'woocommerce-freshmail' ), $status ); ?>
                    <?php
                }

                public function fmListExists() {
                    $rest = new InspireFmRestApi();
                    $rest->setApiKey($this->api_key);
                    $rest->setApiSecret($this->api_secret);

                    try {
                        $response = $rest->doRequest('subscribers_list/lists');
                        return true;
                    } catch (Exception $e) {
                        return false;
                    }
                }

                public function fmAddSubscriber( $email, $name, $order_id ) {
                    $rest = new InspireFmRestApi();
                    $rest->setApiKey($this->api_key);
                    $rest->setApiSecret($this->api_secret);

                    $data = array(
                        'email' => $email,
                        //'list' => $this->list_key,
                    );

                    $freshmail_lists = array();

                    if ( $this->can_save_to_main_freshmail_list( $order_id ) ) {
                    	$freshmail_lists[] = $this->list_key;
                    }

                    foreach ( $this->get_product_freshmail_lists( $order_id ) as $product_list ) {
                    	$freshmail_lists[] = $product_list;
                    }



                    foreach ( $freshmail_lists as $freshmail_list ) {

                    	$data['list'] = $freshmail_list;

	                    if ($this->field_name != "") {
	                        $data["custom_fields"][$this->field_name] = $name;
	                    }

	                    $try_without_custom_fields = false;

	                    try {

	                        $response = $rest->doRequest('subscriber/add', $data);

	                        error_log('InspireFmRestApi Add Subscriber Response: ' . print_r($response, true));

	                    } catch (Exception $e) {
	                        error_log('InspireFmRestApi Add Subscriber Exception: ' . print_r($e, true));

	                        $try_without_custom_fields = true;
	                    }

	                    if ( $try_without_custom_fields ) {

	                        unset( $data['custom_fields'] );
	                        try {
	                            $response = $rest->doRequest('subscriber/add', $data);

	                            error_log('InspireFmRestApi Add Subscriber Response: ' . print_r($response, true));

	                        } catch (Exception $e) {

	                            error_log('InspireFmRestApi Add Subscriber Exception: ' . print_r($e, true));

	                        }
	                    }
	                }

	                return true;
                }

				/**
				 * Settings for the addon are defined here
				 */
				public function init_form_fields() {

				    $this->form_fields = array(
				        'enabled' => array(
				            'title'   => __( 'Włącz/Wyłącz', 'woocommerce_freshmail' ),
				            'label'   => __( 'Włącz integrację z FreshMail', 'woocommerce_freshmail' ),
				            'type'    => 'checkbox',
				            'default' => 'no'
				        ),
				        'api_key' => array(
				            'title'   => __( 'Klucz API', 'woocommerce_freshmail' ),
				            'type'    => 'text',
				            'default' => ''
				        ),
				        'api_secret' => array(
				            'title'   => __( 'API Sekret', 'woocommerce_freshmail' ),
				            'type'    => 'text',
				            'default' => ''
				        ),
				        'list_key' => array(
				            'title'   => __( 'Klucz listy głównej', 'woocommerce_freshmail' ),
				            'type'    => 'text',
				            'default' => ''
				        ),
				        'field_name' => array(
				            'title'       => __( 'Pole imię (opcjonalnie)', 'woocommerce_freshmail' ),
				            'description' => __( 'Pole określające imię subskrybenta w liście FreshMail', 'woocommerce_freshmail' ),
				            'desc_tip'    => true,
				            'type'        => 'text'
				        ),
				        'save_to_list' => array(
				            'title'   => __( 'Zapisuj do listy', 'woocommerce_freshmail' ),
				            'type'    => 'select',
				            'options' => array(
				                'after_order'     => __('Po złożeniu zamówienia', 'woocommerce_freshmail' ),
				                'after_completed' => __('Po zrealizowaniu zamówienia', 'woocommerce_freshmail' ),
				            ),
				            'default' => 'after_order',
				        ),
				        'products_main_list' => array(
				            'label'   => __( 'Zawsze, również gdy klient został zapisany do listy na podstawie zakupionych produktów.', 'woocommerce_freshmail' ),
				            'title'   => __( 'Zapisuj do listy głównej', 'woocommerce_freshmail' ),
				            'description' => __( 'W edycji produktu w zakładce "Zaawansowane" możesz ustawić listę subskrypcji dla każdego produktu.', 'woocommerce_freshmail' ),
				            'desc_tip'    => true,
				            'type'    => 'checkbox',
				            'default' => 'no'
				        ),
				        'user_agreement' => array(
				            'title'   => __( 'Zgoda użytkownika', 'woocommerce_freshmail' ),
				            'type'    => 'select',
				            'options' => array(
				                'yes' => __('Pokaż checkbox potwierdzenia zapisu na newsletter', 'woocommerce_freshmail' ),
				                'no'  => __('Nie pokazuj checkboksa (zapisz automatycznie)', 'woocommerce_freshmail' ),
				            ),
				            'default' => 'yes',
				        ),
				        'custom_check_agreement' => array(
				            'label'   => __( 'Domyślnie zaznaczaj zgodę na newsletter', 'woocommerce_freshmail' ),
				            'title'   => "",
				            'type'    => 'checkbox',
				            'default' => 'no'
				        ),
				        'checkbox_text' => array(
				            'title'   => __( 'Tekst przy checkboksie', 'woocommerce_freshmail' ),
				            'type'    => 'text',
				            'default' => __( 'Zapisz się do newslettera', 'woocommerce_freshmail' ),
				        ),
				        'checkbox_description' => array(
				            'title' => __( 'Opis pod checkboksem', 'woocommerce_freshmail' ),
				            'type'  => 'text'
				        ),
				        'checkbox_position' => array(
				            'title'   => __( 'Położenie checkboxa', 'woocommerce_freshmail' ),
				            'type'    => 'select',
				            'options' => array(
				                'before'         => __( 'Nad przyciskiem "kupuję i płacę"', 'woocommerce_freshmail' ),
				                'after'          => __( 'Pod przyciskiem "kupuję i płacę"', 'woocommerce_freshmail' ),
				                'after_billing'  => __( 'Pod sekcją "szczegóły płatności"', 'woocommerce_freshmail' ),
				            ),
				            'default' => 'before',
				        ),
				        'admin_show_status' => array(
				            'label'   => __('Wyświetlaj informację o zapisie w edycji zamówienia', 'woocommerce_freshmail' ),
				            'title'   => __('Informacja dla admina', 'woocommerce_freshmail' ),
				            'type'    => 'checkbox'
				        )
				    );

				    if ($this->user_agreement == "no") {
				        $this->form_fields['custom_check_agreement']['disabled'] = 'disabled';
				        $this->form_fields['checkbox_position']['disabled'] = 'disabled';
				    }
				}

                /**
                 * Show FreshMail checkbox in the checkout
                 *
                 */
				public function add_freshmail_checkbox() {
				    if ( $this->user_agreement == "yes" && $this->enabled == "yes" ) {
						wc_get_template( 'freshmail-checkbox.php', array(
							'label'       => $this->checkbox_text,
							'description' => $this->checkbox_description,
							'checked'     => $this->custom_check_agreement,
						), 'woocommerce-freshmail', untrailingslashit( plugin_dir_path( __FILE__ ) ) . '/templates/' );
				    }
				}

				/**
				 * Update Order Meta
				 *
				 */
				public function update_order_meta( $order_id ) {
				    if ( isset($_POST['freshmail_subscribe']) && $_POST['freshmail_subscribe'] == 1 ) {
				        update_post_meta( $order_id, '_freshmail_subscribe', esc_attr( $_POST['freshmail_subscribe'] ) );
				    } elseif ( $this->user_agreement == "no" ) {
				        update_post_meta( $order_id, '_freshmail_subscribe', 1 );
				    }
				}

                /**
                 * Lists for Products
                 *
                 */
				public function get_product_freshmail_lists( $order_id ) {
					$order = new WC_Order( $order_id );

					$products = array();

					foreach ( $order->get_items() as $product ) {
						$freshmail_list = get_post_meta( $product['product_id'], '_product_freshmail_list', true );

						if ( $freshmail_list ) {
							$products[ $product['product_id'] ] = $freshmail_list;
						}

					}

					return $products;
				}

                /**
                 * Maybe Subscribe to Main List
                 *
                 */
				public function can_save_to_main_freshmail_list( $order_id ) {

					if ( $this->products_main_list == "no" && count( $this->get_product_freshmail_lists( $order_id ) ) ) {
						return false;
					} else {
						return true;
					}

				}

				/**
				 * Subscribe After Placing Order
				 *
				 */
				public function save_order( $order_id ) {
					$order = new WC_Order($order_id);

				    if ( $this->save_to_list == "after_order" && $this->enabled == "yes" && get_post_meta( $order_id, '_freshmail_subscribe', true ) == 1 ) {
				        $email = get_post_meta( $order_id, '_billing_email', true );
				        $name = get_post_meta( $order_id, '_billing_first_name', true );
				        $this->fmAddSubscriber( $email, $name, $order_id );
				    }
				}

				/**
				 * Subscribe After Order Completed
				 *
				 */
				public function change_order_status( $order_id ) {
				    $order = new WC_Order($order_id);

				    if ( $this->save_to_list == "after_completed" && $this->enabled == "yes" && get_post_meta( $order_id, '_freshmail_subscribe', true ) == 1 && $order->status == "completed" ) {
				        $email = get_post_meta( $order_id, '_billing_email', true );
				        $name = get_post_meta( $order_id, '_billing_first_name', true );
				        $this->fmAddSubscriber( $email, $name, $order_id );
				    }
				}

				/**
				 * Add custom action links on the plugin screen.
				 *
				 */
                public function __woocommerce_freshmail_action_links( $links ) {

                    $plugin_links = array(
                        '<a href="' . admin_url( 'admin.php?page=wc-settings&tab=integration&section=woocommerce_freshmail' ) . '">' . __( 'Ustawienia', 'woocommerce-freshmail' ) . '</a>',
                        '<a href="https://www.wpdesk.pl/docs/woocommerce-freshmail-docs/">' . __( 'Docs', 'woocommerce-freshmail' ) . '</a>',
                        '<a href="https://www.wpdesk.pl/support/">' . __( 'Support', 'woocommerce-freshmail' ) . '</a>',
                    );

                    return array_merge( $plugin_links, $links );
                }
            }

        }
    }

    function add_freshmail_integration($integrations) {
        $integrations[] = 'WC_Freshmail_Integration';
        return $integrations;
    }

    add_action( 'woocommerce_integrations_init', 'init_freshmail_integration' );
    add_filter( 'woocommerce_integrations', 'add_freshmail_integration' );
//} WAŻNE2
