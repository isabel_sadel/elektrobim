<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * WC_FRESHMAIL_ADMIN class
 */
class WC_FRESHMAIL_ADMIN {

	/**
	 * Constructor
	 */
	public static function init() {
		add_action( 'woocommerce_admin_order_data_after_billing_address', array( __CLASS__, 'admin_freshmail_checkbox'), 10, 1 );
		add_action( 'admin_enqueue_scripts', array( __CLASS__, 'scripts' ) );

        // Add FreshMail Product Field to Data Tabs
		add_action( 'woocommerce_product_options_advanced', array( __CLASS__, 'output_product_meta_fields' ) );

		// Save FreshMail Product Field
		add_action( 'woocommerce_process_product_meta', array( __CLASS__, 'save_product_meta_fields' ) );
	}

	/**
	 * Add Freshmail Subscribe Info to Admin Billing Fields
	 *
	 */
	public static function admin_freshmail_checkbox( $order ) {
	    $freshmail_subscribe = get_post_meta( $order->id, '_freshmail_subscribe', true );
		$freshmail_value = $freshmail_subscribe == 1 ? __( 'Tak', 'woocommerce-freshmail' ) : __( 'Nie', 'woocommerce-freshmail' );
		$freshmail_options = get_option( 'woocommerce_woocommerce_freshmail_settings' );

	    if ( ! empty( $freshmail_subscribe ) && $freshmail_options[ 'admin_show_status' ] == 'yes' ) {
	        echo '<p><strong>' . __( 'Freshmail', 'woocommerce-freshmail' ) . ':</strong> ' . $freshmail_value . '</p>';
	    }
	}

	/**
	 * Enqueue scripts
	 */
	public static function scripts() {
		$is_integration = isset($_GET['page']) && $_GET['page'] == 'wc-settings' && isset($_GET['tab']) && $_GET['tab'] == "integration";

		if ($is_integration) {
			wp_enqueue_script( 'wc_freshmail_admin_js', plugins_url( 'assets/js/admin.js', WC_FRESHMAIL_FILE ), array(), WC_FRESHMAIL_VERSION );
		}
	}

    /**
     * Output FreshMail Product Field in Advanced Tab
     *
     */
	public static function output_product_meta_fields( $post ) {
	    global $post;

        woocommerce_wp_text_input( array(
            'id'          => '_product_freshmail_list',
            'label'       => __( 'Klucz listy FreshMail', 'woocommerce_freshmail' ),
            'description' => __( 'Wklej klucz listy do której będzie zapisany użytkownik, jeśli kupi produkt.', 'woocommerce_freshmail' ),
            'desc_tip'    => true,
        ));
	}

    /**
     * Save FreshMail Product Field
     *
     */
	public static function save_product_meta_fields( $post_id ) {
	    $field = sanitize_text_field( $_POST['_product_freshmail_list'] );

	    if ( isset( $field ) )
	    	update_post_meta( $post_id, '_product_freshmail_list', $field );
	}
}

WC_FRESHMAIL_ADMIN::init();
