<?php
	woocommerce_form_field(
		'freshmail_subscribe',
		array(
			'type'        => 'checkbox',
			'class'       => array( 'freshmail' ),
			'label'       => $label,
			'label_class' => 'freshmail-subscribe',
			'description' => wp_kses_post( $description )
		),
		$checked == 'yes' ? 1 : 0
	);
?>
