<?php
/**
 * Plugin Name:     Elektrobim Extra CSS
 * Plugin URI:      https://elektrobim.pl
 * Description:     Modyfikuje wizualne
 * Author:          January 15th Ltd
 * Author URI:      https://january15th.com
 * Text Domain:     elektrobim-email-mods
 * Domain Path:     /languages
 * Version:         0.1.0
 *
 * @package         Elektrobim_Styles
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}



function ebim_styles_scripts() {
	wp_enqueue_style( 'ebim-extra', plugins_url( '/assets/css/style.min.css', __FILE__ ) , [], '0.1' );
}
add_action( 'wp_enqueue_scripts', 'ebim_styles_scripts' );