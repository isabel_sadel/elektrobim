<?php
/*
	Plugin Name: WooCommerce Paczkomaty InPost
	Plugin URI: http://www.wpdesk.pl/sklep/paczkomaty-inpost-woocommerce/
	Description: Wtyczka do WooCommerce. Umożliwia wysyłkę za pomocą Paczkomatów InPost.
	Version: 3.0.2
	Author: WP Desk
	Author URI: http://www.wpdesk.pl/
	Text Domain: paczkomaty_shipping_method
	Domain Path: /lang/
	Tested up to: 4.5.2

	Copyright 2016 WP Desk Ltd.

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; either version 2 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

*/

$wpdesk_helper_text_domain = 'paczkomaty_shipping_method';
require_once( plugin_basename( 'inc/wpdesk-functions.php' ) );

$plugin_data = array(
    'plugin' => plugin_basename( __FILE__ ),
    'product_id' => 'WooCommerce Paczkomaty InPost',
    'version'   => '3.0.2',
    'config_uri' => admin_url( 'admin.php?page=wc-settings&tab=shipping&section=wc_paczkomaty_shipping_method' )
    );
$helper = new WPDesk_Helper_Plugin( $plugin_data );


//if ( $helper->is_active()
//	 && in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins') ) )
//	 &&	in_array( 'flexible-shipping/flexible-shipping.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) )
//	) //{ WAŻNE

    function paczkomatyShippingInit() {

        require_once(plugin_dir_path(__FILE__) . "api/functions.php");
        require_once(plugin_dir_path(__FILE__) . "api/inpost.php");

        require_once('class/flexible_shipping_hooks.php');

        $woo_fs = new WPDesk_Paczkomaty_FS();

        global $inpost_data_dir, $inpost_api_url;

        $inpost_data_dir = plugin_dir_path(__FILE__) . 'api/data';
        $inpost_api_url = 'http://api.paczkomaty.pl';

        if (inpost_check_environment() != 1) {

        	add_action( 'admin_notices',  'paczkomaty_check_notice' );

            function paczkomaty_check_notice() {
				ob_start();
				inpost_check_environment(1);
				$out = ob_get_clean();
				if ( $out ) {
					$class = "error";
					echo"<div class=\"$class\"> <p>$out</p></div>";
				}
            }

        }

        if (!class_exists('WC_Paczkomaty_Shipping_Method')) {

            class WC_Paczkomaty_Shipping_Method extends WC_Shipping_Method {

                public $machines;
                public $machinesInfo;
                public $packageSizes;
                public $packageSizesKeys;
                public $translateDomain;
                public $howToGive;
                public $dispatch_points;
                public $dispatch_points_options;
                public $templatesDir;

                public function __construct( $instance_id = 0 ) {
                   	$this->id = 'paczkomaty_shipping_method';
                   	$this->instance_id 	= absint( $instance_id );
            		$this->supports = array(
            				'settings',
            		);

            		$this->returnSaveOrderBoxAjax = false;
                    $this->templatesDir = plugin_dir_path(__FILE__) . "woocommerce-paczkomaty-inpost-templates/";

                    //init languages
                    $plugin_dir = plugin_dir_path(__FILE__) . "/lang/";
                    load_plugin_textdomain($this->id, false, $plugin_dir);


                    $this->packageSizes = array('0' => '-- ' . __('wybierz', 'paczkomaty_shipping_method') . ' --');
                    $this->packageSizes['A'] = 'Rozmiar A - 8 x 38 x 64 cm';
                    $this->packageSizes['B'] = 'Rozmiar B - 19 x 38 x 64 cm';
                    $this->packageSizes['C'] = 'Rozmiar C - 41 x 38 x 64cm';
                    //$this->packageSizesKeys = array('A', 'B', 'C');

                    $this->howToGive[0] = '-- ' . __('wybierz', 'paczkomaty_shipping_method') . ' --';
                    $this->howToGive[1] = __('Paczkomat', 'paczkomaty_shipping_method');
                    //$this->howToGive[2] = __('Oddział', 'paczkomaty_shipping_method');
                    //8267
                    $this->howToGive[3] = __('Kurier', 'paczkomaty_shipping_method');


                    $this->translateDomain = 'paczkomaty';
                    $this->method_title = __('Paczkomaty', 'paczkomaty_shipping_method');
                    $this->method_description = __('Wysyłka przez Paczkomaty InPost. <a href="http://www.wpdesk.pl/docs/woocommerce-paczkomaty-docs/" target="_blank">Instrukcja konfiguracji &rarr;</a>'); //

					$this->init();

                    add_action('woocommerce_update_options_shipping_' . $this->id, array($this, 'process_admin_options'));

                }

                public function process_admin_options() {
                	parent::process_admin_options();
                	delete_transient( 'paczkomaty_dp' );
                }

                public function init_shipping_method() {

                }

                public function initMachinesList()
                {
                    if (empty($this->allMachines))
                    {
                        $machines = inpost_get_machine_list();


                        $this->machines = array('' => '-- ' . __('wybierz', 'paczkomaty_shipping_method') . ' --');
                        $this->allMachines = array();
                        foreach ($machines as $machine) {
                            $this->machines[$machine['name']] = $machine['town'] . ", " . $machine['street'] . " " . $machine['buildingnumber'];
                            $this->adminMachines[$machine['name']] = "[" . $machine['name'] . "] " . $machine['town'] . ", " . $machine['street'] . " " . $machine['buildingnumber'];
                            $this->allMachines[$machine['name']] = $machine['town'] . ", " . $machine['street'] . " " . $machine['buildingnumber'];
                            $this->machinesInfo[$machine['name']] = $machine;
                        }
                    }
                }

                function init() {

                	$this->init_settings();

                	// Load the settings API
                    $this->enabled = $this->get_option('enabled');
                    $this->title = $this->get_option('title');

                    $this->email = $this->get_option('email');
                    $this->password = $this->get_option('password');
                    $this->default_package_size = $this->get_option('default_package_size');
                    $this->tax_status = $this->get_option('tax_status');
                    $this->cost_per_order = $this->get_option('cost_per_order');
                    $this->free_shipping_cost = $this->get_option('free_shipping_cost');
                    $this->self_send = $this->get_option('self_send');
                    $this->chosen_disable = $this->get_option('chosen_disable');
                    $this->product_count = $this->get_option('product_count');

                    //$this->polecony_enabled = $this->get_option('polecony_enabled');
                    $this->polecony_title = $this->get_option('polecony_title');
                    $this->polecony_cost_per_order = $this->get_option('polecony_cost_per_order');
                    $this->polecony_free_shipping_cost = $this->get_option('polecony_free_shipping_cost');
                    $this->polecony_id = 'polecony_' . $this->id;

                    $this->polecony_sender_name = $this->get_option('polecony_sender_name');
                    $this->polecony_sender_surName = $this->get_option('polecony_sender_surName');
                    $this->polecony_sender_companyName = $this->get_option('polecony_sender_companyName');
                    $this->polecony_sender_email = $this->get_option('polecony_sender_email');
                    $this->polecony_sender_phoneNum = $this->get_option('polecony_sender_phoneNum');
                    $this->polecony_sender_street = $this->get_option('polecony_sender_street');
                    $this->polecony_sender_street_number = $this->get_option('polecony_sender_street_number');
                    $this->polecony_sender_town = $this->get_option('polecony_sender_town');
                    $this->polecony_sender_zipCode = $this->get_option('polecony_sender_zipCode');

                    //add cost for package size
                    /* foreach ($this->packageSizesKeys as $key) {
                      $kName = 'package_cost_' . $key;
                      $this->$kName = $this->get_option($kName);
                      } */

                    if( $this->chosen_disable != 'yes' ) {
                    	add_action( 'woocommerce_checkout_order_review', array($this, 'initPublicChosen'), 75 );
                    }

                    $this->dispatch_points = get_transient( 'paczkomaty_dp' );
                    if ( $this->dispatch_points == false ) {
                    	$this->dispatch_points = inpost_get_dispatch_points( $this->email, $this->password );
                    	if ( is_array( $this->dispatch_points ) ) {
                    		if ( isset( $this->dispatch_points['error'] ) ) {
                    			$this->dispatch_points = array( 'paczkomaty' => array( 'result' => array() ) );
                    		}
                    		else {
                    			set_transient( 'paczkomaty_dp', $this->dispatch_points, WEEK_IN_SECONDS );
                    		}
                    	}
                    }

                    $this->dispatch_points_options = array( '' => __( 'Wybierz punkt', 'paczkomaty_shipping_method' ) );
                    if ( isset( $this->dispatch_points ) && isset( $this->dispatch_points['paczkomaty'] ) && isset( $this->dispatch_points['paczkomaty']['result'] ) ) {
	                    foreach ( $this->dispatch_points['paczkomaty']['result'] as $dispatch_point ) {
	                    	if ( $dispatch_point['dispatchPointStatus']['value'] == 'ACTIVE' ) {
	                    		$this->dispatch_points_options[$dispatch_point['name']['value']] = $dispatch_point['name']['value'];
	                    	}
	                    }
                    }

					$this->init_form_fields();
                }

                public function register_hooks() {

                	add_action('woocommerce_review_order_after_shipping', array($this, 'shippingMethodAfter'));
                	add_filter('woocommerce_shipping_methods', array($this, 'paczkomatyShippingMethod'));
                	add_action('woocommerce_checkout_update_order_review', array($this, 'updateOrderReview'));
                	add_action('woocommerce_checkout_process', array($this, 'checkoutFieldProcess'));
                	add_action('woocommerce_checkout_update_order_meta', array($this, 'checkoutUpdateOrderMeta'));
                	add_action('woocommerce_order_details_after_order_table', array($this, 'orderItemsTable'));
                	add_action( 'woocommerce_email_order_meta', array($this, 'orderItemsTable'), 195);

                	if ( is_admin() ) {
                		add_action( 'admin_enqueue_scripts', array($this, 'initAdminJsAction'), 75 );
                	}

                	/* ADMIN ACTIONS */
                	add_action('add_meta_boxes', array($this, 'addAdminBox'));
                	add_action('admin_head', array($this, 'loadOrderBoxScript'));
                	add_action('admin_init', array($this, 'adminInit'));
                	add_action('save_post', array($this, 'adminSave'));
                	//add_action('edit_post', array($this, 'adminEdit'));

                	add_action('wp_ajax_send_package', array($this, 'sendPackageAjax'));
                	add_action('wp_ajax_save_order_box', array($this, 'saveOrderBoxAjax'));

                	add_action('wp_ajax_send_package_polecony', array($this, 'sendPackagePoleconyAjax'));
                	add_action('wp_ajax_save_order_polecony_box', array($this, 'saveOrderPoleconyBoxAjax'));

                	add_filter('plugin_action_links_' . plugin_basename(__FILE__), array($this, 'actionLinks'));

                	add_action( 'woocommerce_product_options_shipping', array( $this, 'woocommerce_product_options_shipping' ) );

                }

                public function initPublicChosen() {
                    echo '
                    <script type="text/javascript">
                        jQuery(document).ready(function($) {
                            $("#paczkomat_id").select2();
                        });
                    </script>
                    ';
                }

                public function initAdminJsAction() {
                    $reflection = new ReflectionClass($this);

                    wp_enqueue_script( 'jquery' );
                    wp_enqueue_script( 'paczkomaty_admin_js', plugin_dir_url($reflection->getFileName()) . 'js/admin.js' );
                }

                public function actionLinks($links) {
                	global $woocommerce;

					if (version_compare($woocommerce->version, '2.1.0', '>=')) // WC 2.1)
					{
	                    $plugin_links = array(
	                        '<a href="' . admin_url('admin.php?page=wc-settings&tab=shipping&section=wc_paczkomaty_shipping_method') . '">' . __('Ustawienia', 'woocommerce') . '</a>',
	                        '<a href="http://www.wpdesk.pl/docs/woocommerce-paczkomaty-docs/">' . __('Docs', 'woocommerce') . '</a>',
	                        '<a href="http://www.wpdesk.pl/support/">' . __('Support', 'woocommerce') . '</a>',
	                    );
					} else {
						$plugin_links = array(
							'<a href="' . admin_url('admin.php?page=woocommerce_settings&tab=shipping&section=WC_Paczkomaty_Shipping_Method') . '">' . __('Ustawienia', 'woocommerce') . '</a>',
							'<a href="http://www.wpdesk.pl/docs/woocommerce-paczkomaty-docs/">' . __('Docs', 'woocommerce') . '</a>',
							'<a href="http://www.wpdesk.pl/support/">' . __('Support', 'woocommerce') . '</a>',
						);
					}

                    return array_merge($plugin_links, $links);
                }

                public function adminInit() {
                    if (isset($_GET['post']) && is_numeric($_GET['post'])) {
                        $post = get_post($_GET['post']);
                    }
                    if (!empty($post) && isset($post->post_type) && $post->post_type == 'shop_order') {
                        //if first edit set default values if not exists
                        $postData = get_post_meta($post->ID);

                        if ( isset( $postData['_shipping_method'] ) && $postData['_shipping_method'][0] == $this->id ) {
                            if (!isset($postData['_paczkomat_size'][0]) && !empty($this->default_package_size)) {

                                update_post_meta($post->ID, '_paczkomat_size', $this->default_package_size);
                            }
                            if (!isset($postData['_paczkomat_how_to_give'][0]) && !empty($this->self_send)) {
                                update_post_meta($post->ID, '_paczkomat_how_to_give', $this->self_send);
                            }
                        }
                    }
                    if (isset($_GET['generate_inpost_sticker']) && $_GET['generate_inpost_sticker'] == true && isset($_GET['post']) && $_GET['post'] > 0) {
                        $post_id = (int) $_GET['post'];
                        $packcode = get_post_meta($post_id, '_paczkomat_packcode', true);

                        if (!empty($packcode)) {
                            if ($result = inpost_get_sticker($this->email, $this->password, $packcode)) {

                                if (is_array($result) && isset($result['error'])) {
                                    echo __("Error: " . $result['error']['message'], 'paczkomaty_shipping_method');
                                } else {
                                    header("Content-type: application/pdf");
                                    header("Content-Disposition: attachment; filename=e-$packcode.pdf");
                                    echo $result;
                                }
                            } else {
                                echo __("Error: Nie udało się wygenerować etykiety", 'paczkomaty_shipping_method');
                            }
                        } else {
                            echo __("Error: Niepoprawny numer paczki", 'paczkomaty_shipping_method');
                        }
                        die;
                    } elseif (isset($_GET['generate_inpost_confirm']) && $_GET['generate_inpost_confirm'] == true && isset($_GET['post']) && $_GET['post'] > 0) {
                        $post_id = (int) $_GET['post'];
                        $packcode = get_post_meta($post_id, '_paczkomat_packcode', true);

                        if (!empty($packcode)) {
                            if ($result = inpost_get_confirm_printout($this->email, $this->password, array($packcode))) {

                                if (is_array($result) && isset($result['error'])) {
                                    echo __("Error: " . $result['error']['message'], 'paczkomaty_shipping_method');
                                } else {
                                    header("Content-type: application/pdf");
                                    header("Content-Disposition: attachment; filename=pn-$packcode.pdf");
                                    echo $result;
                                }
                            } else {
                                echo __("Error: Nie udało się wygenerować potwierdzenia", 'paczkomaty_shipping_method');
                            }
                        } else {
                            echo __("Error: Niepoprawny numer paczki", 'paczkomaty_shipping_method');
                        }
                        die;
                    }
                }

                public function saveOrderBoxAjax() {

                    $post_id = (int) $_POST['post_id'];

                    if (isset($_POST['paczkomat_id'])) {
                        update_post_meta($post_id, '_paczkomat_id', esc_attr($_POST['paczkomat_id']));
                    }
                    if (isset($_POST['paczkomat_size'])) {
                        update_post_meta($post_id, '_paczkomat_size', esc_attr($_POST['paczkomat_size']));
                    }
                    if (isset($_POST['paczkomat_how_to_give'])) {
                        update_post_meta($post_id, '_paczkomat_how_to_give', esc_attr($_POST['paczkomat_how_to_give']));
                    }
                    if (isset($_POST['paczkomat_customer_ref'])) {
                        update_post_meta($post_id, '_paczkomat_customer_ref', esc_attr($_POST['paczkomat_customer_ref']));
                    }
                    /*
                    if (isset($_POST['paczkomat_punkt_nadania'])) {
                        update_post_meta($post_id, '_paczkomat_punkt_nadania', esc_attr($_POST['paczkomat_punkt_nadania']));
                    }
                    */
                    if ($this->returnSaveOrderBoxAjax == false) {
                        $json['status'] = 'success';
                        $json['html'] = $this->adminBoxContent($post_id, false);
                        echo json_encode($json);
                        die;
                    }
                }

                public function sendPackageAjax() {
                    $post_id = (int) $_POST['post_id'];

                    $json = array('status' => 'fail');
                    $this->returnSaveOrderBoxAjax = true;
                    $this->saveOrderBoxAjax();
                    $this->returnSaveOrderBoxAjax = false;
                    $postInfo = get_post_meta($post_id);
                    $order = new WC_Order($post_id);

                    if (!empty($postInfo)) {
                        $packData = array();
                        $packData['senderEmail'] = $this->email;
                        $packData['adreseeEmail'] = $postInfo['_billing_email'][0];
                        $packData['boxMachineName'] = $postInfo['_paczkomat_id'][0];
                        $packData['customerRef'] = $postInfo['_paczkomat_customer_ref'][0];
                        $packData['phoneNum'] = $postInfo['_billing_phone'][0];

error_log(print_r($postInfo,true));
                        $selfSend = 0;
                        if (isset($postInfo['_paczkomat_how_to_give'][0]) && $postInfo['_paczkomat_how_to_give'][0] == 1) {
                            $selfSend = 1;
                        } elseif (isset($postInfo['_paczkomat_how_to_give'][0]) && $postInfo['_paczkomat_how_to_give'][0] == 2) {
                            $selfSend = 0;
                            //$packData['dispatchPointName'] = '';
                        } elseif (isset($postInfo['_paczkomat_how_to_give'][0]) && $postInfo['_paczkomat_how_to_give'][0] == 3) {
                            $selfSend = 0;
                            //$packData['dispatchPointName'] = $this->get_option( 'dispatch_point' );
                        } elseif ((int) $this->self_send == 1) {
                            $selfSend = (int) $this->self_send;
                        }

                        if (strlen($postInfo['_paczkomat_size'][0]) > 0) {
                            $packData['packType'] = $postInfo['_paczkomat_size'][0];
                        } else {
                            $packData['packType'] = $this->default_package_size;
                        }

                        $_allegro_journal_deal = get_post_meta( $post_id, '_allegro_journal_deal', true );
                        $_allegro_post_buy_form_data = get_post_meta( $post_id, '_allegro_post_buy_form_data', true );
                       	if ( $_allegro_journal_deal != '' && $_allegro_post_buy_form_data != '' && $_allegro_post_buy_form_data->postBuyFormData->item->postBuyFormPayId ) {
                       		$packData['allegro'] = array();
                       		$packData['allegro']['userId'] = $_allegro_journal_deal->dealSellerId;
                       		$packData['allegro']['transactionId'] = $_allegro_post_buy_form_data->postBuyFormData->item->postBuyFormPayId;
                        }
error_log('--------pack data-------------');
error_log(print_r($packData,true));
//return;
						$response = inpost_send_packs($this->email, $this->password, array($packData), 1, $selfSend);
                        //print_r($response);
                        if (!empty($response[0]['error_key']) || !empty($response[0]['error_message'])) {
                            $json['message'] = $response[0]['error_message'];

                        } elseif (isset($response[0]['packcode'])) {
                            //save in order info
                            update_post_meta($post_id, "_paczkomat_packcode", $response[0]['packcode']);
                            //$json['message'] = __('', 'paczkomaty_shipping_method');
                            $json['status'] = 'success';
                            $json['html'] = $this->adminBoxContent($post_id, false);
                        }
                        else{
                            $json['message'] = 'Wystąpił błąd podczas przetwarzania żądania.';
                        }
                    } else {
                        $json['message'] = __('Niepoprawne ID zamówienia', 'paczkomaty_shipping_method');
                    }
                    echo json_encode($json);
                    die;
                }

                public function saveOrderPoleconyBoxAjax() {

                    $post_id = (int) $_POST['post_id'];

                    if (isset($_POST['paczkomat_size'])) {
                        update_post_meta($post_id, '_paczkomat_size', esc_attr($_POST['paczkomat_size']));
                    }
                    if (isset($_POST['paczkomat_customer_ref'])) {
                        update_post_meta($post_id, '_paczkomat_customer_ref', esc_attr($_POST['paczkomat_customer_ref']));
                    }
                    if (isset($_POST['paczkomat_punkt_nadania'])) {
                        update_post_meta($post_id, '_paczkomat_punkt_nadania', esc_attr($_POST['paczkomat_punkt_nadania']));
                        update_option('paczkomat_punkt_nadania', esc_attr($_POST['paczkomat_punkt_nadania']));
                    }
                    if ($this->returnSaveOrderBoxAjax == false) {
                        $json['status'] = 'success';
                        $json['html'] = $this->adminBoxContentPolecony($post_id, false);
                        echo json_encode($json);
                        die;
                    }
                }

                public function sendPackagePoleconyAjax() {

                	$post_id = (int) $_POST['post_id'];

                    $json = array('status' => 'fail');
                    $this->returnSaveOrderBoxAjax = true;
                    $this->saveOrderPoleconyBoxAjax();
                    $this->returnSaveOrderBoxAjax = false;
                    $postInfo = get_post_meta($post_id);
                    $order = new WC_Order($post_id);

                    if (!empty($postInfo)) {
                    	if (isset($_POST['paczkomat_customer_ref'])) {
                    		update_post_meta($post_id, '_paczkomat_customer_ref', esc_attr($_POST['paczkomat_customer_ref']));
                    	}
                    	if (isset($_POST['paczkomat_punkt_nadania'])) {
                    		update_post_meta($post_id, '_paczkomat_punkt_nadania', esc_attr($_POST['paczkomat_punkt_nadania']));
                    		update_option('paczkomat_punkt_nadania', esc_attr($_POST['paczkomat_punkt_nadania']));
                    	}
                    	$address = trim( $order->shipping_address_1.' '.$order->shipping_address_2 );
                    	$address_2 = trim( substr( $address, strripos( $address, ' ') ) );
                    	$address_1 = trim( substr( $address, 0, strripos( $address, ' ') ));
                        $packData = array();
                        $packData['senderEmail'] = $this->email;
                        $packData['adreseeEmail'] = $postInfo['_billing_email'][0];
                        $packData['phoneNum'] = $postInfo['_billing_phone'][0];
                        //$packData['customerRef'] = 'Zamówienie nr '. $order->get_order_number();
                        //$packData['customerRef'] = $_POST['paczkomat_customer_ref'][0];
                        $packData['senderBoxMachineName'] = $_POST['paczkomat_punkt_nadania'];
                        $packData['receiverAddress'] = array();
                        $packData['receiverAddress']['name'] = $order->shipping_first_name;
                        $packData['receiverAddress']['surName'] = $order->shipping_last_name;
                        $packData['receiverAddress']['companyName'] = $order->shipping_company;
                        $packData['receiverAddress']['email'] = $order->billing_email;
                        $packData['receiverAddress']['phoneNum'] = $order->billing_phone;
                        $packData['receiverAddress']['street'] = $address_1;
                        $packData['receiverAddress']['buildingNo'] = $address_2;
                        $packData['receiverAddress']['town'] = $order->shipping_city;
                        $packData['receiverAddress']['zipCode'] = $order->shipping_postcode;
                        //
                        $packData['senderAddress'] = array();
                        $packData['senderAddress']['name'] = $this->polecony_sender_name;
                        $packData['senderAddress']['surName'] = $this->polecony_sender_surName;
                        $packData['senderAddress']['companyName'] = $this->polecony_sender_companyName;
                        $packData['senderAddress']['email'] = $this->polecony_sender_email;
                        $packData['senderAddress']['phoneNum'] = $this->polecony_sender_phoneNum;
                        $packData['senderAddress']['street'] = $this->polecony_sender_street;
                        $packData['senderAddress']['buildingNo'] = $this->polecony_sender_street_number;
                        $packData['senderAddress']['town'] = $this->polecony_sender_town;
                        $packData['senderAddress']['zipCode'] = $this->polecony_sender_zipCode;

                        $selfSend = 0;

                        $_allegro_journal_deal = get_post_meta( $post_id, '_allegro_journal_deal', true );
                        $_allegro_post_buy_form_data = get_post_meta( $post_id, '_allegro_post_buy_form_data', true );
                       	if ( $_allegro_journal_deal != '' && $_allegro_post_buy_form_data != '' && $_allegro_post_buy_form_data->postBuyFormData->item->postBuyFormPayId ) {
                       		$packData['allegro'] = array();
                       		$packData['allegro']['userId'] = $_allegro_journal_deal->dealSellerId;
                       		$packData['allegro']['transactionId'] = $_allegro_post_buy_form_data->postBuyFormData->item->postBuyFormPayId;
                        }

                        $response = inpost_send_letters($this->email, $this->password, array($packData), 1);
                        //print_r($response);
                        if (!empty($response['error']['key'])) {
                            $json['message'] = $response['error']['message'];

                        } elseif (isset($response[0]['packcode'])) {
                            //save in order info
                            update_post_meta($post_id, "_paczkomat_packcode", $response[0]['packcode']);
                            //$json['message'] = __('', 'paczkomaty_shipping_method');
                            $json['status'] = 'success';
                            $json['html'] = $this->adminBoxContentPolecony($post_id, false);
                        }
                        else{
                            $json['message'] = 'Wystąpił błąd podczas przetwarzania żądania.';
                        }
                    } else {
                        $json['message'] = __('Niepoprawne ID zamówienia', 'paczkomaty_shipping_method');
                    }
                    echo json_encode($json);
                    die;
                }

                public function adminSave() {
                    global $post;

                    if (isset($post) && $post->post_type == "shop_order" && $post->ID) {
                        /* if (isset($_POST['paczkomat_id'])) {
                          update_post_meta($post->ID, '_paczkomat_id', esc_attr($_POST['paczkomat_id']));
                          }
                          if (isset($_POST['paczkomat_size'])) {
                          update_post_meta($post->ID, '_paczkomat_size', esc_attr($_POST['paczkomat_size']));
                          }
                          if (isset($_POST['paczkomat_how_to_give'])) {
                          update_post_meta($post->ID, '_paczkomat_how_to_give', esc_attr($_POST['paczkomat_how_to_give']));
                          } */
                    } elseif (isset($post) && $post->post_type == "product" && $post->ID) {
                        update_post_meta($post->ID, '_paczkomat_disabled', esc_attr($_POST['_paczkomat_disabled']));
                        update_post_meta($post->ID, '_paczkomat_polecony_disabled', esc_attr($_POST['_paczkomat_polecony_disabled']));
                    }
                }

                public function addAdminBox() {
                    global $post_id;
                    global $woocommerce;

                    $postInfo = get_post($post_id);

                    if ($postInfo && $postInfo->post_type == 'product') {
//                        add_meta_box('woocommerce-paczkomaty-product-box', __('Paczkomaty Ustawienia', 'paczkomaty_shipping_method'), array($this, 'adminProductBox'), 'product', 'side', 'default');
//                        add_meta_box('woocommerce-paczkomaty-polecony-product-box', __('Paczkomaty Polecony Ustawienia', 'paczkomaty_shipping_method'), array($this, 'adminProductPoleconyBox'), 'product', 'side', 'default');
                    } elseif ($postInfo && $postInfo->post_type == 'shop_order') {
                    	$order = new WC_Order($post_id);

                    	$_paczkomaty_usluga = get_post_meta( $order->id, '_paczkomaty_usluga', true );

   	                  	if ( $_paczkomaty_usluga == 'paczkomaty' ) {
   	                  		add_meta_box('woocommerce-paczkomaty-box', __('Paczkomaty', 'paczkomaty_shipping_method'), array($this, 'adminBox'), 'shop_order', 'side', 'default');
	                    }
	                    if ( $_paczkomaty_usluga == 'polecony' ) {
	                      	add_meta_box('woocommerce-paczkomaty-box', __('Polecony Paczkomaty', 'paczkomaty_shipping_method'), array($this, 'adminBoxPolecony'), 'shop_order', 'side', 'default');
	                    }
                    }
                }

                public function adminBox() {
                    global $post_id;
                    $this->adminBoxContent($post_id);
                }

                public function adminProductBox() {
                    global $post_id;
                    $paczkomat_disabled = get_post_meta($post_id, '_paczkomat_disabled', true);
                    ?>
                    <p>
                        <input id="paczkomat_disabled" class="input-checkbox" type="checkbox" value="1" <?php echo ($paczkomat_disabled == 1) ? 'checked="checked"' : ''; ?> name="paczkomat_disabled"/>
                        <label for="paczkomat_disabled"> <?php _e('Jeśli ten produkt znajduje się w koszyku wyłącz wysyłkę za pomocą Paczkomatów', 'paczkomaty_shipping_method'); ?></label>
                    </p>
                    <?php
                }

                public function adminProductPoleconyBox() {
                    global $post_id;
                    $paczkomat_polecony_disabled = get_post_meta($post_id, '_paczkomat_polecony_disabled', true);
                    ?>
                    <p>
                        <input id="paczkomat_polecony_disabled" class="input-checkbox" type="checkbox" value="1" <?php echo ($paczkomat_polecony_disabled == 1) ? 'checked="checked"' : ''; ?> name="paczkomat_polecony_disabled"/>
                        <label for="paczkomat_polecony_disabled"> <?php _e('Jeśli ten produkt znajduje się w koszyku wyłącz wysyłkę za pomocą Paczkomat Polecony', 'paczkomaty_shipping_method'); ?></label>
                    </p>
                    <?php
                }

                function woocommerce_product_options_shipping()	{
                	global $woocommerce, $post;
                	echo '</div>';
                	echo '<div class="options_group">';

                	woocommerce_wp_checkbox(
                			array(
                					'id'                => '_paczkomat_disabled',
                					'label'             => __( 'Paczkomaty', 'paczkomaty_shipping_method' ),
                					'description'       => __( 'Jeśli ten produkt znajduje się w koszyku wyłącz wysyłkę za pomocą Paczkomatów', 'paczkomaty_shipping_method' ),
                			)
           			);

                	woocommerce_wp_checkbox(
                			array(
                					'id'                => '_paczkomat_polecony_disabled',
                					'label'             => __( 'E-commerce Polecony', 'paczkomaty_shipping_method' ),
                					'description'       => __( 'Jeśli ten produkt znajduje się w koszyku wyłącz wysyłkę za pomocą Paczkomat Polecony', 'paczkomaty_shipping_method' ),
                			)
           			);
                }


                public function punktyNadaniaOptions()
                {
                    $punkty_nadania = inpost_get_dispatch_points($this->email, $this->password);
                    $punkty_nadania_options = array();
                    foreach ( $punkty_nadania['paczkomaty']['result'] as $punkt_nadania )
                    {
                    	$punkty_nadania_options[$punkt_nadania['name']['value']] = $punkt_nadania['name']['value'];
                    }
                    asort($punkty_nadania_options);
                    return $punkty_nadania_options;
                }

                public function loadTemplate($templateName, $args = array()) {

                    $templateFile = $this->templatesDir . $templateName . ".php";

                    if (file_exists($templateFile)) {
                        ob_start();
                        include_once($templateFile);
                        return ob_get_clean();
                    }
                    return "";
                }

                public function adminBoxContent($post_id, $echo = true) {

                    $args = array();
                    $args['paczkomat_id'] = get_post_meta($post_id, '_paczkomat_id', true);
                    $args['paczkomat_size'] = get_post_meta($post_id, '_paczkomat_size', true);
                    $args['phone'] = get_post_meta($post_id, '_billing_phone', true);
                    $args['paczkomat_packcode'] = get_post_meta($post_id, '_paczkomat_packcode', true);
                    $args['paczkomat_how_to_give'] = get_post_meta($post_id, '_paczkomat_how_to_give', true);
                    $args['post_id'] = $post_id;

                    $args['select2'] = ! ($this->chosen_disable == 'yes');

                    $order = wc_get_order( $post_id );

                    $args['paczkomat_customer_ref'] = get_post_meta($post_id, '_paczkomat_customer_ref', true);

                    if ( $args['paczkomat_customer_ref'] == '' ) {
                    	$args['paczkomat_customer_ref'] = sprintf( __( 'Zamówienie %s', 'paczkomaty_shipping_method'), $order->get_order_number() );
                    }

                    $this->initMachinesList();
					/*
                    $punkty_nadania = $this->punktyNadaniaOptions();
                    $args['punkty_nadania'] = $punkty_nadania;
                    $args['punkt_nadania'] = get_post_meta($post_id, '_paczkomat_punkt_nadania', true);
					*/
                    $content = $this->loadTemplate('admin-box-content', $args);
                    if ($echo != true) {
                        return $content;
                    } else {
                        echo $content;
                    }
                }

                public function adminBoxPolecony() {
                    global $post_id;
                    $this->adminBoxContentPolecony($post_id);
                }

                public function adminBoxContentPolecony($post_id, $echo = true) {

                    $args = array();
                    $args['paczkomat_size'] = get_post_meta($post_id, '_paczkomat_size', true);
                    $args['phone'] = get_post_meta($post_id, '_billing_phone', true);
                    $args['paczkomat_packcode'] = get_post_meta($post_id, '_paczkomat_packcode', true);
                    $args['paczkomat_how_to_give'] = get_post_meta($post_id, '_paczkomat_how_to_give', true);
                    $args['post_id'] = $post_id;

                    $args['select2'] = ! ($this->chosen_disable == 'yes');

                    $args['punkty_nadania'] = inpost_listmachines('t');
                    $args['punkt_nadania'] = get_post_meta( $post_id, '_paczkomat_punkt_nadania', true );
                    if ( $args['punkt_nadania'] == '' ) {
                    	$args['punkt_nadania'] = get_option('paczkomat_punkt_nadania' ,'' );
                    }

                    $order = wc_get_order( $post_id );

                    $args['paczkomat_customer_ref'] = get_post_meta($post_id, '_paczkomat_customer_ref', true);

                    if ( $args['paczkomat_customer_ref'] == '' ) {
                    	$args['paczkomat_customer_ref'] = sprintf( __( 'Zamówienie %s', 'paczkomaty_shipping_method'), $order->get_order_number() );
                    }

                    $content = $this->loadTemplate('admin-box-content-polecony', $args);
                    if ($echo != true) {
                        return $content;
                    } else {
                        echo $content;
                    }
                }

                public function loadOrderBoxScript() {

                    if (isset($_GET['post']) && isset($_GET['action']) && $_GET['action'] == 'edit') {
                        ?>
                        <script type="text/javascript">
                            jQuery(document).ready(function($) {
                                //jQuery("body").on("#sendPackage", 'click', function() {

                                var sendPackageStatus = true;

                                $("body").on('click', '#sendPackage', function(e) {
                                	e.stopImmediatePropagation();
                                    if(sendPackageStatus == true){
                                        sendPackageStatus = false;
                                        $('#sendPackage').css('opacity', 0.5);

                                        var paczkomat_id = $("#paczkomat_id").val();
                                        var paczkomat_size = $("#paczkomat_size").val();
                                        var paczkomat_how_to_give = $("#paczkomat_how_to_give").val();
                                        var paczkomat_customer_ref = $("#paczkomat_customer_ref").val();
                                        //var paczkomat_punkt_nadania = $("#paczkomat_punkt_nadania").val();

                                        $.ajax({
                                            url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                            type: 'POST',
                                            data: {
                                                action: 'send_package',
                                                post_id: <?php echo $_GET['post']; ?>,
                                                paczkomat_id: paczkomat_id,
                                                paczkomat_size: paczkomat_size,
                                                paczkomat_how_to_give: paczkomat_how_to_give,
                                                paczkomat_customer_ref : paczkomat_customer_ref,
                                                //paczkomat_punkt_nadania: paczkomat_punkt_nadania,
                                            },
                                            dataType: 'json',
                                            success: function(response) {
                                                if (response) {
                                                    if (response.status == 'success') {
                                                        $("#woocommerce-paczkomaty-box .inside").html(response.html);
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                }
                                                $('#sendPackage').css('opacity', 1);
                                                sendPackageStatus = true;
                                            }
                                        });

                                    }
                                });
                                $("body").on('click', '#sendPackagePolecony', function(e) {
                                	e.stopImmediatePropagation();
                                    if(sendPackageStatus == true){
                                        sendPackageStatus = false;
                                        $('#sendPackage').css('opacity', 0.5);

                                        var paczkomat_customer_ref = $("#paczkomat_customer_ref").val();
                                        //var paczkomat_size = $("#paczkomat_size").val();
                                        var paczkomat_punkt_nadania = $("#paczkomat_punkt_nadania").val();

                                        $.ajax({
                                            url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                            type: 'POST',
                                            data: {
                                                action: 'send_package_polecony',
                                                post_id: <?php echo $_GET['post']; ?>,
                                                paczkomat_punkt_nadania: paczkomat_punkt_nadania,
                                                paczkomat_customer_ref : paczkomat_customer_ref,
                                            },
                                            dataType: 'json',
                                            success: function(response) {
                                                if (response) {
                                                    if (response.status == 'success') {
                                                        $("#woocommerce-paczkomaty-box .inside").html(response.html);
                                                    } else {
                                                        alert(response.message);
                                                    }
                                                }
                                                $('#sendPackagePolecony').css('opacity', 1);
                                                sendPackageStatus = true;
                                            }
                                        });

                                    }
                                });
                                $("body").on('click', '#saveOrderBox', function(e) {
                                	e.stopImmediatePropagation();
                                    var paczkomat_id = $("#paczkomat_id").val();
                                    var paczkomat_size = $("#paczkomat_size").val();
                                    var paczkomat_how_to_give = $("#paczkomat_how_to_give").val();
                                    var paczkomat_customer_ref = $("#paczkomat_customer_ref").val();
                                    //var paczkomat_punkt_nadania = $("#paczkomat_punkt_nadania").val();

                                    $.ajax({
                                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                        type: 'POST',
                                        data: {
                                            action: 'save_order_box',
                                            post_id: <?php echo $_GET['post']; ?>,
                                            paczkomat_id: paczkomat_id,
                                            paczkomat_size: paczkomat_size,
                                            paczkomat_how_to_give: paczkomat_how_to_give,
                                            paczkomat_customer_ref : paczkomat_customer_ref,
                                            //paczkomat_punkt_nadania: paczkomat_punkt_nadania,
                                        },
                                        dataType: 'json',
                                        success: function(response) {
                                            if (response) {
                                                if (response.status == 'success') {
                                                    $("#woocommerce-paczkomaty-box .inside").html(response.html);
                                                } else {
                                                    alert(response.message);
                                                }
                                            }
                                        }
                                    });
                                });
                                $("body").on('click', '#saveOrderPoleconyBox', function(e) {
                                	e.stopImmediatePropagation();
                                    var paczkomat_size = $("#paczkomat_size").val();
                                    var paczkomat_customer_ref = $("#paczkomat_customer_ref").val();

                                    $.ajax({
                                        url: "<?php echo admin_url('admin-ajax.php'); ?>",
                                        type: 'POST',
                                        data: {
                                            action: 'save_order_polecony_box',
                                            post_id: <?php echo $_GET['post']; ?>,
                                            paczkomat_size: paczkomat_size,
                                            paczkomat_customer_ref : paczkomat_customer_ref,
                                        },
                                        dataType: 'json',
                                        success: function(response) {
                                            if (response) {
                                                if (response.status == 'success') {
                                                    $("#woocommerce-paczkomaty-box .inside").html(response.html);
                                                } else {
                                                    alert(response.message);
                                                }
                                            }
                                        }
                                    });
                                });
                                /*$("body").on('change', '#paczkomat_id,#paczkomat_size,#paczkomat_how_to_give', function() {

                                 });*/
                            });
                        </script>
                        <?php
                    }
                }

                public function orderItemsTable($order) {
                    $paczkomat = get_post_meta($order->id, '_paczkomat_id', true);
					if (!empty($paczkomat))
					{
                        $args = array('paczkomat_id' => $paczkomat);
                    }
                    $this->initMachinesList();
                    echo $this->loadTemplate('order-items-table', $args);
                }

                public function checkoutUpdateOrderMeta($order_id) {
                	if ( $this->isChoosenShippingMethodInSession() ) {
                		if ($_POST['paczkomat_id']) {
                			update_post_meta($order_id, '_paczkomat_id', esc_attr($_POST['paczkomat_id']));
                		}
                		update_post_meta($order_id, '_paczkomaty_usluga', 'paczkomaty' );
                	}
                	if ( $this->isChoosenShippingMethodPoleconyInSession() ) {
                		update_post_meta($order_id, '_paczkomaty_usluga', 'polecony' );
                	}
                }

                public function checkoutFieldProcess() {
                    global $woocommerce;

                    if ($this->isChoosenShippingMethodInSession() && empty($_POST['paczkomat_id']))
					{
						if (version_compare($woocommerce->version, '2.1.0', '>=')) // WC 2.1
						{
							wc_add_notice( __('Proszę wybrać paczkomat', 'paczkomaty_shipping_method'), 'error' );
						} else {
							$woocommerce->add_error(__('Proszę wybrać paczkomat', 'paczkomaty_shipping_method'));
						}
                    }
                }

                function paczkomatyShippingMethod($methods) {
					$paczkomaty = new WC_Paczkomaty_Shipping_Method();
                    $methods['paczkomaty'] = $paczkomaty;
                    //$methods['paczkomaty'] = 'WC_Paczkomaty_Shipping_Method';
                    //if ( $paczkomaty->polecony_enabled == 'yes' ) {
                    	$paczkomaty_polecony = new WC_Paczkomaty_Polecony_Shipping_Method( $paczkomaty );
                    	$methods['paczkomaty_polecony'] = $paczkomaty_polecony;
                    //}
                    return $methods;
                }

                function init_form_fields() {

                    $this->form_fields = array(
						array(
							'title'     => __( 'Ustawienia główne', 'paczkomaty_shipping_method' ),
							'type'     => 'title',
							'default' => '',
						),
                    	'enabled' => array(
                            'title' => __('Włącz/Wyłącz', 'paczkomaty_shipping_method'),
                            'type' => 'checkbox',
                            'label' => __('Włącz metodę wysyłki', 'paczkomaty_shipping_method'),
                            'default' => 'yes'
                        ),
                    	'email' => array(
                            'title' => __('Login', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                        	'default' => '',
                        ),
                        'password' => array(
                            'title' => __('Hasło', 'paczkomaty_shipping_method'),
                            'type' => 'password',
                        	'default' => '',
                        ),
                        'self_send' => array(
                            'title' => __('Domyślny sposób nadania', 'paczkomaty_shipping_method'),
                            'type' => 'select',
                            'default' => '1',
                            'options' => $this->howToGive,
                        ),
/*
                        'dispatch_point' => array(
                            'title' => __('Punkt odbioru przez kuriera', 'paczkomaty_shipping_method'),
                            'type' => 'select',
                            'default' => '',
                            'options' => $this->dispatch_points_options,
                        ),
*/
                    	'chosen_disable' => array(
                            'title' => __('Rozbudowana lista wyboru', 'paczkomaty_shipping_method'),
                            'type' => 'checkbox',
                            'label' => __('Wyłącz rozbudowaną listę wyboru paczkomatu na stronie zamówienia', 'paczkomaty_shipping_method'),
                        	'default' => '',
                        ),
/*
                        'product_count' => array(
                            'title' => __('Wyłącz Paczkomaty', 'paczkomaty_shipping_method'),
                            'description' => __('Podaj liczbę sztuk produktów, od której metoda wysyłki będzie nieaktywna', 'paczkomaty_shipping_method'),
                            'type' => 'number',
                            'custom_attributes' => array(
                                'step' => '1',
                                'min' => '0'
                            )
                        ),
*/
/*
                    	array(
							'title'     => __( 'Paczkomaty (gabaryt A, B, C,)', 'paczkomaty_shipping_method' ),
							'type'     => 'title',
                    		'default' => '',
						),
                        'title' => array(
                            'title' => __('Tytuł metody', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'default' => __('Paczkomaty', 'paczkomaty_shipping_method'),
                            'desc_tip' => true,
                        ),
*/
/*
                        'cost_per_order' => array(
                            'title' => __('Koszt na zamówienie', 'paczkomaty_shipping_method'),
                            'type' => 'number',
                            'custom_attributes' => array(
                                'step' => 'any',
                                'min' => '0'
                            ),
                            'default' => '',
                            'desc_tip' => true,
                            'placeholder' => '0.00'
                        ),
						'free_shipping_cost' => array(
							'title' => __('Kwota zamówienia, od której wysyłka jest bezpłatna', 'paczkomaty_shipping_method'),
							'type' => 'number',
							'custom_attributes' => array(
									'step' => 'any',
									'min' => '0'
							),
							'default' => '',
							'desc_tip' => __('Produkty wirtualne nie są wliczane do tej kwoty.'),
							'placeholder' => '0.00'
						),
*/
                        'default_package_size' => array(
                            'title' => __('Domyślny rozmiar paczki', 'paczkomaty_shipping_method'),
                            'type' => 'select',
                            'options' => $this->packageSizes,
                        	'default' => '',
                        ),
/*
                    	array(
							'title'     => __( 'E-commerce Polecony', 'paczkomaty_shipping_method' ),
							'type'     => 'title',
                    		'default' => '',
						),
*/
/*
                    	'polecony_enabled' => array(
                            'title' => __('Włącz/Wyłącz', 'paczkomaty_shipping_method'),
                            'type' => 'checkbox',
                            'label' => __('Włącz metodę wysyłki', 'paczkomaty_shipping_method'),
                            'default' => 'yes',
                        ),
*/
/*
                        'polecony_title' => array(
                            'title' => __('Tytuł metody', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'default' => __('Paczkomaty Polecony', 'paczkomaty_shipping_method'),
                            'desc_tip' => true,
                        ),
*/
/*
                        'polecony_cost_per_order' => array(
                            'title' => __('Koszt na zamówienie', 'paczkomaty_shipping_method'),
                            'type' => 'number',
                            'custom_attributes' => array(
                                'step' => 'any',
                                'min' => '0'
                            ),
                            'default' => '',
                            'desc_tip' => true,
                            'placeholder' => '0.00'
                        ),
						'polecony_free_shipping_cost' => array(
							'title' => __('Kwota zamówienia, od której wysyłka jest bezpłatna', 'paczkomaty_shipping_method'),
							'type' => 'number',
							'custom_attributes' => array(
									'step' => 'any',
									'min' => '0'
							),
							'default' => '',
							'desc_tip' => __('Produkty wirtualne nie są wliczane do tej kwoty.'),
							'placeholder' => '0.00'
						),
*/
                    	array(
							'title'     => __( 'Dane nadawcy', 'paczkomaty_shipping_method' ),
							'type'     => 'title',
                    		'default' => '',
                    		'desc_tip' => true,
                    		'description' => __( 'Dane nadawcy wymagane dla usługi E-commerce Polecony', 'paczkomaty_shipping_method' ),
						),
                    	'polecony_sender_name' => array(
                            'title' => __('Imię', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                    		'default' => '',
                        ),
                        'polecony_sender_surName' => array(
                            'title' => __('Nazwisko', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                        	'default' => '',
                        ),
                        'polecony_sender_companyName' => array(
                            'title' => __('Nazwa firmy', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                        	'default' => '',
                        ),
                        'polecony_sender_email' => array(
                            'title' => __('E-mail', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                        	'default' => '',
                        ),
                        'polecony_sender_phoneNum' => array(
                            'title' => __('Telefon', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                        	'default' => '',
                        ),
                        'polecony_sender_street' => array(
                            'title' => __('Ulica', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                        	'default' => '',
                        ),
                        'polecony_sender_street_number' => array(
                            'title' => __('Nr domu i mieszkania', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                        	'default' => '',
                        ),
                    	'polecony_sender_town' => array(
                            'title' => __('Miejscowość', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                    		'default' => '',
                        ),
                        'polecony_sender_zipCode' => array(
                            'title' => __('Kod pocztowy', 'paczkomaty_shipping_method'),
                            'type' => 'text',
                            'desc_tip' => true,
                        	'default' => '',
                        ),
                    );

                    //$this->instance_form_fields = $this->form_fields;

                    //add cost for package size
                    /* foreach ($this->packageSizesKeys as $key) {
                      $this->form_fields['package_cost_' . $key] = array(
                      'title' => __('Opłata za paczkę rozmiar ' . $key, 'paczkomaty_shipping_method'),
                      'type' => 'text',
                      );
                      } */
                }

                function is_available($package) {
                    global $woocommerce;

                    $is_available = true;
                    if ($this->enabled == "no") {
                        $is_available = false;
                    } elseif(!empty($this -> product_count) and $this -> product_count <= $woocommerce->cart->cart_contents_count ){
					    $is_available = false;
                    } elseif (!empty($package['contents'])) {
                        foreach ($package['contents'] as $pack) {
                            $disable = get_post_meta($pack['product_id'], '_paczkomat_disabled', true);
                            if ($disable == 1) {
                                $is_available = false;
                                break;
                            }
                        }
                    }

                    if ( (($woocommerce->customer->shipping_country <> 'PL') || ($woocommerce->customer->get_country() <> 'PL' && empty($woocommerce->customer->shipping_country) ))) {
                        $is_available = false;
                    }

                    return apply_filters('woocommerce_shipping_' . $this->id . '_is_available', $is_available);
                }

                public function calculate_shipping($package = array()) {
/*
					if (!empty($this->free_shipping_cost) && $this->free_shipping_cost <= $package['contents_cost'])
					{
						$rate = array(
							'id' => $this->id,
							'label' => $this->title,
							'cost' => 0,
							'calc_tax' => 'per_order',
						);
					} else {
	                    $rate = array(
	                        'id' => $this->id,
	                        'label' => $this->title,
	                        'cost' => $this->cost_per_order,
	                        'calc_tax' => 'per_order',
	                    );
					}

                    $this->add_rate($rate);
*/
                }

                public function isChoosenShippingMethodInSession() {
                	global $woocommerce;
                	$shippings = $woocommerce->session->get('chosen_shipping_methods');
					$all_shipping_methods = WC()->shipping()->get_shipping_methods();
					if ( empty( $all_shipping_methods ) ) {
						$all_shipping_methods = WC()->shipping()->load_shipping_methods();
					}
					$flexible_shipping = $all_shipping_methods['flexible_shipping'];
					$flexible_shipping_rates = $flexible_shipping->get_all_rates();
					foreach ( $shippings as $id => $shipping ) {
						if ( isset( $flexible_shipping_rates[$shipping] ) ) {
							$shipping_method = $flexible_shipping_rates[$shipping];
							if ( $shipping_method['method_integration'] == 'paczkomaty' ) {
								if ( $shipping_method['paczkomaty_usluga'] == 'paczkomaty' ) {
									return true;
								}
							}
						}
						if ( strpos( $shipping, 'flexible_shipping' ) === 0 && strpos( $shipping, '_paczkomaty_paczkomaty_paczkomaty' ) > 0 ) {
							return true;
						}
					}
					return false;
                }

                public function isChoosenShippingMethodPoleconyInSession() {
                	global $woocommerce;
                	$shippings = $woocommerce->session->get('chosen_shipping_methods');
					$all_shipping_methods = WC()->shipping()->get_shipping_methods();
					if ( empty( $all_shipping_methods ) ) {
						$all_shipping_methods = WC()->shipping()->load_shipping_methods();
					}
					$flexible_shipping = $all_shipping_methods['flexible_shipping'];
					$flexible_shipping_rates = $flexible_shipping->get_all_rates();
					foreach ( $shippings as $id => $shipping ) {
						if ( isset( $flexible_shipping_rates[$shipping] ) ) {
							$shipping_method = $flexible_shipping_rates[$shipping];
							if ( $shipping_method['method_integration'] == 'paczkomaty' ) {
								if ( $shipping_method['paczkomaty_usluga'] == 'polecony' ) {
									return true;
								}
							}
						}
						if ( strpos( $shipping, 'flexible_shipping' ) === 0 && strpos( $shipping, '_paczkomaty_paczkomaty_polecony' ) > 0 ) {
							return true;
						}
					}
					return false;
                }

                public function shippingMethodAfter() {
error_log('shippingMethodAfter');
                    global $woocommerce;

                    if ($this->isChoosenShippingMethodInSession()) {
                        $this->initMachinesList();

                        //create two arrays
                        $machniesNearly = array();
                        if (isset($_POST['postcode']) || isset($_POST['billing_postcode']) || isset($_POST['s_postcode'])) {
                            if(isset($_POST['s_postcode'])){
                                $postcode = $_POST['s_postcode'];
                            }else{
                                $postcode = (isset($_POST['postcode'])) ? $_POST['postcode'] : $_POST['billing_postcode'];
                            }

                            $machniesNearlyData = inpost_find_nearest_machines($postcode);
                            if (!empty($machniesNearlyData)) {
                                foreach ($machniesNearlyData as $machine) {
                                    $machniesNearly[$machine['name']] = $machine['town'] . ", " . $machine['street'] . " " . $machine['buildingnumber'];
                                    unset($this->allMachines[$machine['name']]);
                                }
                            }
                        }
                        $args = array();
                        $args['nearly'] = $machniesNearly;
                        $args['other'] = $this->allMachines;
                        $args['select2'] = ! ($this->chosen_disable == 'yes');
                        echo $this->loadTemplate('shipping-method-after', $args);
                    }
                }

                public function updateOrderReview($post_data) {

                    global $woocommerce;

                    parse_str($post_data, $data);

                    if (isset($data['paczkomat_id'])) {
                    	if (is_array($woocommerce->checkout)) // WC 2.1 patch
                    	{
                        	$woocommerce->checkout['checkout_fields']['shipping']['paczkomat_id'] = $data['paczkomat_id'];
                    	} else { // 2.1
                    		$woocommerce->checkout->checkout_fields['shipping']['paczkomat_id'] = $data['paczkomat_id'];
                    	}
                    } else {
                    	if (is_array($woocommerce->checkout)) // WC 2.1 patch
                    	{
                        	$woocommerce->checkout['checkout_fields']['shipping']['paczkomat_id'] = '';
                    	} else { // 2.1
                    		$woocommerce->checkout->checkout_fields['shipping']['paczkomat_id'] = '';
                    	}
                    }
                }

            }

            class WC_Paczkomaty_Polecony_Shipping_Method extends WC_Shipping_Method {

            	public function __construct( $paczkomaty_shipping_method, $instance_id = 0 ) {

            		$this->id = $paczkomaty_shipping_method->polecony_id;

            		$this->instance_id 	= absint( $instance_id );

            		$this->tax_status = $paczkomaty_shipping_method->tax_status;
            		$this->has_settings = false;
            		$this->enabled = $paczkomaty_shipping_method->enabled;
            		$this->title = $paczkomaty_shipping_method->polecony_title;
            		$this->method_title = $paczkomaty_shipping_method->polecony_title;
            		$this->free_shipping_cost = $paczkomaty_shipping_method->polecony_free_shipping_cost;
            		$this->cost_per_order = $paczkomaty_shipping_method->polecony_cost_per_order;
            		$this->paczkomaty_shipping_method = $paczkomaty_shipping_method;

            		$this->init();

            		$this->supports = array(
            				'settings' => false
            		);

            	}

            	function init() {
            	}

                public function addAdminBox() {
                    global $post_id;
                    global $woocommerce;

                    $postInfo = get_post($post_id);

					if ($postInfo && $postInfo->post_type == 'shop_order') {
                    	$order = new WC_Order($post_id);

                    	if (version_compare($woocommerce->version, '2.1.0', '>=')) // WC 2.1
                    	{

	                        if ($order->has_shipping_method($this->id)) {
	                            add_meta_box('woocommerce-paczkomaty-box', __('Paczkomaty', 'paczkomaty_shipping_method'), array($this, 'adminBox'), 'shop_order', 'side', 'default');
	                        }
                    	} else {
                    		$_shipping_method = get_post_meta($post_id, '_shipping_method', true);
                    		if ($_shipping_method == $this->id) {
                    			add_meta_box('woocommerce-paczkomaty-box', __('Paczkomaty', 'paczkomaty_shipping_method'), array($this, 'adminBox'), 'shop_order', 'side', 'default');
                    		}
                    	}
                    }
                }

                public function adminBox() {
                    global $post_id;
                    $this->adminBoxContent($post_id);
                }

                public function adminBoxContent($post_id, $echo = true) {
                    $args = array();
                    $args['paczkomat_id'] = get_post_meta($post_id, '_paczkomat_id', true);
                    $args['paczkomat_size'] = get_post_meta($post_id, '_paczkomat_size', true);
                    $args['phone'] = get_post_meta($post_id, '_billing_phone', true);
                    $args['paczkomat_packcode'] = get_post_meta($post_id, '_paczkomat_packcode', true);
                    $args['paczkomat_how_to_give'] = get_post_meta($post_id, '_paczkomat_how_to_give', true);
                    $args['post_id'] = $post_id;

                    $order = wc_get_order( $post_id );

                    $args['paczkomat_customer_ref'] = get_post_meta($post_id, '_paczkomat_customer_ref', true);

                    if ( $args['paczkomat_customer_ref'] == '' ) {
                    	$args['paczkomat_customer_ref'] = sprintf( __( 'Zamówienie %s', 'paczkomaty_shipping_method'), $order->get_order_number() );
                    }

                    $args['select2'] = ! ($this->chosen_disable == 'yes');

                    $this->initMachinesList();

                    $content = $this->loadTemplate('admin-box-content', $args);
                    if ($echo != true) {
                        return $content;
                    } else {
                        echo $content;
                    }
                }

                public function calculate_shipping($package = array()) {
/*
            		if ( !empty($this->free_shipping_cost) && $this->free_shipping_cost <= $package['contents_cost'] ) {
            			$rate = array(
            					'id' => $this->id,
            					'label' => $this->title,
            					'cost' => 0,
            					'calc_tax' => 'per_order',
            			);
            		} else {
            			$rate = array(
            					'id' => $this->id,
            					'label' => $this->title,
            					'cost' => $this->cost_per_order,
            					'calc_tax' => 'per_order',
            			);
            		}
            		$this->add_rate($rate);
*/
            	}

            	private function package_weight( $items ) {
					$weight = 0;
					foreach( $items as $item )
						$weight += $item['data']->weight * $item['quantity'];
						return $weight;
				}

            	function is_available($package) {
                    global $woocommerce;

                    $is_available = true;
                    if ($this->enabled == "no") {
                    	$is_available = false;
                    } elseif ( !empty($this -> product_count) and $this -> product_count <= $woocommerce->cart->cart_contents_count ){
					    $is_available = false;
                    } elseif ( !empty($package['contents'] ) ) {
                        foreach ( $package['contents'] as $pack ) {
                            $disable = get_post_meta($pack['product_id'], '_paczkomat_polecony_disabled', true);
                            if ($disable == 1) {
                                $is_available = false;
                                break;
                            }
                        }
                    }

                    if ( (($woocommerce->customer->shipping_country <> 'PL') || ($woocommerce->customer->get_country() <> 'PL' && empty($woocommerce->customer->shipping_country) ))) {
                        $is_available = false;
                    }

                    if ( $is_available )
                    {
                    	$package_weight = wc_get_weight($this->package_weight( $package['contents'] ),'kg');
                    	if ( floatval($package_weight) > 1.0 ) {
                    		$is_available = false;
                    	}
                    }

                    return apply_filters('woocommerce_shipping_' . $this->id . '_is_available', $is_available);
                }

                public function isChoosenShippingMethodInSession() {
                	global $woocommerce;
                	$shippings = $woocommerce->session->get('chosen_shipping_methods');
					$all_shipping_methods = WC()->shipping()->get_shipping_methods();
					if ( empty( $all_shipping_methods ) ) {
						$all_shipping_methods = WC()->shipping()->load_shipping_methods();
					}
					$flexible_shipping = $all_shipping_methods['flexible_shipping'];
					$flexible_shipping_rates = $flexible_shipping->get_all_rates();
					foreach ( $shippings as $id => $shipping ) {
						if ( isset( $flexible_shipping_rates[$shipping] ) ) {
							$shipping_method = $flexible_shipping_rates[$shipping];
							if ( $shipping_method['method_integration'] == 'paczkomaty' ) {
								if ( $shipping_method['paczkomaty_usluga'] == 'paczkomaty' ) {
									return true;
								}
							}
						}
					}
					return false;
                }

            }

            $obj = new WC_Paczkomaty_Shipping_Method();
            $obj->register_hooks();

        }
    }

    add_action('init', 'paczkomatyShippingInit');
//} WAŻNE

function wpdesk_paczkomaty_update_config() {
	$update_config = get_option( 'wpdesk_paczkomaty_update_config', 0 );
	if ( $update_config == 0 ) {
		$woocommerce_paczkomaty_shipping_method_settings = get_option( 'woocommerce_paczkomaty_shipping_method_settings', array() );
		if ( in_array( 'flexible-shipping/flexible-shipping.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			$flexible_shipping_methods = get_option( 'flexible_shipping_methods_0', array() );
			$woocommerce_flexible_shipping_settings = get_option( 'woocommerce_flexible_shipping_settings', array() );
			$id = 0;
			foreach ( $flexible_shipping_methods as $shipping_method ) {
				if ( $id < $shipping_method['id'] ) {
					$id = $shipping_method['id'];
				}
			}
			if ( count( $flexible_shipping_methods ) == 0 ) {
				$woocommerce_flexible_shipping_settings['enabled'] = 'yes';
				$woocommerce_flexible_shipping_settings['title'] = __( 'Flexible Shipping', 'paczkomaty_shipping_method' );
				$woocommerce_flexible_shipping_settings['tax_status'] = $woocommerce_paczkomaty_shipping_method_settings['tax_status'];
				update_option( 'woocommerce_flexible_shipping_settings', $woocommerce_flexible_shipping_settings );
			}
			if ( $woocommerce_paczkomaty_shipping_method_settings['enabled'] == 'yes' ) {
				$id++;
				$shipping_method = array();
				$shipping_method['id'] = $id;
				$shipping_method['method_title'] = $woocommerce_paczkomaty_shipping_method_settings['title'];
				$shipping_method['method_free_shipping'] = $woocommerce_paczkomaty_shipping_method_settings['free_shipping_cost'];
				$shipping_method['method_calculation_method'] = 'sum';
				$shipping_method['method_visibility'] = 'no';
				$shipping_method['method_default'] = 'no';
				$shipping_method['method_enabled'] = 'yes';
				$shipping_method['method_integration'] = 'paczkomaty';
				$shipping_method['paczkomaty_usluga'] = 'paczkomaty';
				$shipping_method['method_rules'] = array();
				$shipping_method['method_rules']['1'] = array();
				$shipping_method['method_rules']['1']['based_on'] = 'none';
				$shipping_method['method_rules']['1']['cost_per_order'] = $woocommerce_paczkomaty_shipping_method_settings['cost_per_order'];
				$shipping_method['method_rules']['1']['stop'] = 0;
				$shipping_method['method_rules']['1']['cancel'] = 0;
				$shipping_method['method_rules']['1']['shipping_class'] = 'all';
				if ( isset( $woocommerce_paczkomaty_shipping_method_settings['product_count'] ) &&  $woocommerce_paczkomaty_shipping_method_settings['product_count'] != '' ) {
					$active_plugins = apply_filters( 'active_plugins', get_option('active_plugins' ) );
					if ( in_array( 'flexible-shipping-pro/flexible-shipping-pro.php', $active_plugins ) ) {
						$shipping_method['method_rules']['2'] = array();
						$shipping_method['method_rules']['2']['based_on'] = 'item';
						$shipping_method['method_rules']['2']['min'] = $woocommerce_paczkomaty_shipping_method_settings['product_count'];
						$shipping_method['method_rules']['2']['cost_per_order'] = 0;
						$shipping_method['method_rules']['2']['stop'] = 0;
						$shipping_method['method_rules']['2']['cancel'] = 1;
						$shipping_method['method_rules']['2']['shipping_class'] = 'all';
					}
				}
				$flexible_shipping_methods[$id] = $shipping_method;
				update_option( 'flexible_shipping_methods_0', $flexible_shipping_methods );
			}
			if ( $woocommerce_paczkomaty_shipping_method_settings['polecony_enabled'] == 'yes' ) {
				$id++;
				$shipping_method = array();
				$shipping_method['id'] = $id;
				$shipping_method['method_title'] = $woocommerce_paczkomaty_shipping_method_settings['polecony_title'];
				$shipping_method['method_free_shipping'] = $woocommerce_paczkomaty_shipping_method_settings['polecony_free_shipping_cost'];
				$shipping_method['method_calculation_method'] = 'sum';
				$shipping_method['method_visibility'] = 'no';
				$shipping_method['method_default'] = 'no';
				$shipping_method['method_enabled'] = 'yes';
				$shipping_method['method_integration'] = 'paczkomaty';
				$shipping_method['paczkomaty_usluga'] = 'polecony';
				$shipping_method['method_rules'] = array();
				$shipping_method['method_rules']['1'] = array();
				$shipping_method['method_rules']['1']['based_on'] = 'none';
				$shipping_method['method_rules']['1']['cost_per_order'] = $woocommerce_paczkomaty_shipping_method_settings['polecony_cost_per_order'];
				$shipping_method['method_rules']['1']['stop'] = 0;
				$shipping_method['method_rules']['1']['cancel'] = 0;
				$shipping_method['method_rules']['1']['shipping_class'] = 'all';
				$flexible_shipping_methods[$id] = $shipping_method;
				update_option( 'flexible_shipping_methods_0', $flexible_shipping_methods );
			}
			update_option( 'wpdesk_paczkomaty_update_config', 1 );
		}
	}
}

add_action( 'admin_init', 'wpdesk_paczkomaty_update_config' );


if ( !in_array( 'flexible-shipping/flexible-shipping.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
	function wpdesk_paczkomaty_flexible_shipping_install( $api, $action, $args ) {
		$download_url = 'http://downloads.wordpress.org/plugin/flexible-shipping.latest-stable.zip';

		if ( 'plugin_information' != $action ||
				false !== $api ||
				! isset( $args->slug ) ||
				'wpdesk-helper' != $args->slug
				) return $api;

				$api = new stdClass();
				$api->name = 'Flexible Shipping';
				$api->version = '1.3';
				$api->download_link = esc_url( $download_url );
				return $api;
	}

	add_filter( 'plugins_api', 'wpdesk_paczkomaty_flexible_shipping_install', 10, 3 );
}

function wpdesk_paczkomaty_notice() {

	$active_plugins = apply_filters( 'active_plugins', get_option('active_plugins' ) );
	if ( in_array( 'flexible-shipping/flexible-shipping.php', $active_plugins ) ) return;

	if ( in_array( 'woo-flexible-shipping/flexible-shipping.php', $active_plugins ) ) {
		$message = sprintf( wp_kses( __( 'Korzystasz ze starej wersji Flexible Shipping. <a href="%s">Proszę przeczytaj o zmianach,</a> które wprowadziliśmy i zaktualizuj do nowej wersji. Dzięki :)', 'paczkomaty_shipping_method' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( 'https://www.wpdesk.pl/blog/nowy-flexible-shipping/' ) );
		echo '<div class="error fade"><p>' . $message . '</p></div>' . "\n";
	}
	else {
		$slug = 'flexible-shipping';
		$install_url = wp_nonce_url( self_admin_url( 'update.php?action=install-plugin&plugin=' . $slug ), 'install-plugin_' . $slug );
		$activate_url = 'plugins.php?action=activate&plugin=' . urlencode( 'flexible-shipping/flexible-shipping.php' ) . '&plugin_status=all&paged=1&s&_wpnonce=' . urlencode( wp_create_nonce( 'activate-plugin_flexible-shipping/flexible-shipping.php' ) );
		$message = sprintf( wp_kses( __( 'Woocommerce Paczkomaty Inpost wymaga wtyczki Flexible Shipping. <a href="%s">Zainstaluj Flexible Shipping →</a>', 'paczkomaty_shipping_method' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( $install_url ) );
		$is_downloaded = false;
		$plugins = array_keys( get_plugins() );
		foreach ( $plugins as $plugin ) {
			if ( strpos( $plugin, 'flexible-shipping/flexible-shipping.php' ) === 0 ) {
				$is_downloaded = true;
				$message = sprintf( wp_kses( __( 'Woocommerce Paczkomaty Inpost wymaga wtyczki Flexible Shipping. <a href="%s">Włącz Flexible Shipping →</a>', 'paczkomaty_shipping_method' ), array(  'a' => array( 'href' => array() ) ) ), esc_url( admin_url( $activate_url ) ) );
			}
		}
		echo '<div class="error fade"><p>' . $message . '</p></div>' . "\n";
	}
}
add_action( 'admin_notices', 'wpdesk_paczkomaty_notice' );
