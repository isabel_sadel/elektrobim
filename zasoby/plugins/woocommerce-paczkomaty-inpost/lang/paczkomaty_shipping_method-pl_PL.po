msgid ""
msgstr ""
"Project-Id-Version: WooCommerce Paczkomaty InPost\n"
"POT-Creation-Date: 2016-01-26 18:47+0100\n"
"PO-Revision-Date: 2016-01-26 18:47+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: pl_PL\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.5\n"
"X-Poedit-Basepath: ..\n"
"X-Poedit-WPHeader: woocommerce-paczkomaty-inpost.php\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;"
"esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;"
"_nx_noop:3c,1,2;__ngettext_noop:1,2\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: *.js\n"

#: inc/wpdesk-functions.php:46
#, php-format
msgid ""
"<a href=\"%s\">Install the WP Desk Helper plugin</a> to activate and get "
"updates for your WP Desk plugins."
msgstr ""
"<a href=\"%s\">Zainstaluj wtyczkę WP Desk Helper</a>, aby aktywować i "
"otrzymywać aktualizacje wtyczek WP Desk."

#: inc/wpdesk-functions.php:52
#, php-format
msgid ""
"<a href=\"%s\">Activate the WP Desk Helper plugin</a> to activate and get "
"updates for your WP Desk plugins."
msgstr ""
"<a href=\"%s\">Włącz wtyczkę WP Desk Helper</a>, aby aktywować i otrzymywać "
"aktualizacje wtyczek WP Desk."

#: inc/wpdesk-functions.php:92
#, php-format
msgid ""
"The %s%s%s License Key has not been activated, so the plugin is inactive! "
"%sClick here%s to activate the license key and the plugin."
msgstr ""
"Klucz licencyjny wtyczki %s%s%s nie został aktywowany, więc wtyczka jest "
"nieaktywna! %sKliknij tutaj%s, aby aktywować klucz licencyjny wtyczki."

#: woocommerce-paczkomaty-inpost-templates/admin-box-content-polecony.php:9
msgid "Punkt nadania"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content-polecony.php:37
msgid "Utwórz przesyłkę"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content-polecony.php:40
#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:89
msgid "Pobierz etykietę"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content-polecony.php:42
#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:91
msgid "Pobierz potwierdzenie nadania paczki"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:10
#: woocommerce-paczkomaty-inpost-templates/order-items-table.php:3
msgid "Wybrany paczkomat"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:34
msgid "Wybrany rozmiar paczki"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:53
#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:108
msgid "Sposób nadania"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:86
msgid "Utwórz paczkę"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:99
msgid "Uzupełnij następujące dane:"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:102
#: woocommerce-paczkomaty-inpost.php:102
msgid "Paczkomat"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:105
msgid "Rozmiar paczki"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:111
msgid "Telefon kupującego"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/admin-box-content.php:119
msgid "Zapisz"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/shipping-method-after.php:2
msgid "Wybierz paczkomat"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/shipping-method-after.php:6
#: woocommerce-paczkomaty-inpost.php:94 woocommerce-paczkomaty-inpost.php:100
#: woocommerce-paczkomaty-inpost.php:124
msgid "wybierz"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/shipping-method-after.php:9
msgid "w pobliżu"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/shipping-method-after.php:15
msgid "pozostałe"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/shipping-method-after.php:35
msgid "Sprawdź gdzie jest Twój nabliższy Paczkomat na"
msgstr ""

#: woocommerce-paczkomaty-inpost-templates/shipping-method-after.php:35
msgid "mapie"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:101
msgid "Oddział"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:107 woocommerce-paczkomaty-inpost.php:516
#: woocommerce-paczkomaty-inpost.php:524 woocommerce-paczkomaty-inpost.php:879
#: woocommerce-paczkomaty-inpost.php:1152
#: woocommerce-paczkomaty-inpost.php:1157
msgid "Paczkomaty"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:108
msgid ""
"Wysyłka przez Paczkomaty InPost. <a href=\"http://www.wpdesk.pl/docs/"
"woocommerce-paczkomaty-docs/\" target=\"_blank\">Instrukcja konfiguracji "
"&rarr;</a>"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:231 woocommerce-paczkomaty-inpost.php:237
msgid "Ustawienia"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:232 woocommerce-paczkomaty-inpost.php:238
msgid "Docs"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:233 woocommerce-paczkomaty-inpost.php:239
msgid "Support"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:272 woocommerce-paczkomaty-inpost.php:293
msgid "Error: "
msgstr ""

#: woocommerce-paczkomaty-inpost.php:279
msgid "Error: Nie udało się wygenerować etykiety"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:282 woocommerce-paczkomaty-inpost.php:303
msgid "Error: Niepoprawny numer paczki"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:300
msgid "Error: Nie udało się wygenerować potwierdzenia"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:389 woocommerce-paczkomaty-inpost.php:475
msgid "Niepoprawne ID zamówienia"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:507
msgid "Paczkomaty Ustawienia"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:508
msgid "Paczkomaty Polecony Ustawienia"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:519 woocommerce-paczkomaty-inpost.php:527
msgid "Polecony Paczkomaty"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:544
msgid ""
"Jeśli ten produkt znajduje się w koszyku wyłącz wysyłkę za pomocą Paczkomatów"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:555
msgid ""
"Jeśli ten produkt znajduje się w koszyku wyłącz wysyłkę za pomocą Paczkomat "
"Polecony"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:804 woocommerce-paczkomaty-inpost.php:806
msgid "Proszę wybrać paczkomat"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:826
msgid "Ustawienia główne"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:830
msgid "Login w systemie Paczkomaty"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:834
msgid "Hasło w systemie Paczkomaty"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:838
msgid "Status podatku"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:842
msgid "Opodatkowany"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:843
msgid "Brak"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:847
msgid "Domyślny sposób nadania"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:853
msgid "Rozbudowana lista wyboru"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:855
msgid "Wyłącz rozbudowaną listę wyboru paczkomatu na stronie zamówienia"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:858
msgid "Wyłącz Paczkomaty"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:859
msgid ""
"Podaj liczbę sztuk produktów, od której metoda wysyłki będzie nieaktywna"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:867
msgid "Paczkomaty (gabaryt A, B, C,)"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:871 woocommerce-paczkomaty-inpost.php:914
msgid "Włącz/Wyłącz"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:873 woocommerce-paczkomaty-inpost.php:916
msgid "Włącz metodę wysyłki"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:877 woocommerce-paczkomaty-inpost.php:920
msgid "Tytuł metody"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:883 woocommerce-paczkomaty-inpost.php:926
msgid "Koszt na zamówienie"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:894 woocommerce-paczkomaty-inpost.php:937
msgid "Kwota zamówienia, od której wysyłka jest bezpłatna"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:901 woocommerce-paczkomaty-inpost.php:944
msgid "Produkty wirtualne nie są wliczane do tej kwoty."
msgstr ""

#: woocommerce-paczkomaty-inpost.php:905
msgid "Domyślna wielokość paczki"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:910
msgid "E-commerce Polecony"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:922
msgid "Paczkomaty Polecony"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:948
msgid "E-commerce Polecony - Dane nadawcy"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:952
msgid "Imię"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:957
msgid "Nazwisko"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:962
msgid "Nazwa firmy"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:967
msgid "E-mail"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:972
msgid "Telefon"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:977
msgid "Ulica"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:982
msgid "Nr domu i mieszkania"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:987
msgid "Miejscowość"
msgstr ""

#: woocommerce-paczkomaty-inpost.php:992
msgid "Kod pocztowy"
msgstr ""

#. Plugin Name of the plugin/theme
msgid "WooCommerce Paczkomaty InPost"
msgstr ""

#. Plugin URI of the plugin/theme
msgid "http://www.wpdesk.pl/sklep/paczkomaty-inpost-woocommerce/"
msgstr ""

#. Description of the plugin/theme
msgid "Wtyczka do WooCommerce. Umożliwia wysyłkę za pomocą Paczkomatów InPost."
msgstr ""

#. Author of the plugin/theme
msgid "WP Desk"
msgstr ""

#. Author URI of the plugin/theme
msgid "http://www.wpdesk.pl/"
msgstr ""
