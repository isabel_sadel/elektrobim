��          <      \       p   i   q   h   �   �   D  �  �  l   �  o   '  �   �                   <a href="%s">Activate the WP Desk Helper plugin</a> to activate and get updates for your WP Desk plugins. <a href="%s">Install the WP Desk Helper plugin</a> to activate and get updates for your WP Desk plugins. The %s%s%s License Key has not been activated, so the plugin is inactive! %sClick here%s to activate the license key and the plugin. Project-Id-Version: WooCommerce Paczkomaty InPost
POT-Creation-Date: 2016-01-26 18:47+0100
PO-Revision-Date: 2016-01-26 18:47+0100
Last-Translator: 
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.8.5
X-Poedit-Basepath: ..
X-Poedit-WPHeader: woocommerce-paczkomaty-inpost.php
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_n:1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;esc_attr__;esc_attr_e;esc_attr_x:1,2c;esc_html__;esc_html_e;esc_html_x:1,2c;_n_noop:1,2;_nx_noop:3c,1,2;__ngettext_noop:1,2
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: *.js
 <a href="%s">Włącz wtyczkę WP Desk Helper</a>, aby aktywować i otrzymywać aktualizacje wtyczek WP Desk. <a href="%s">Zainstaluj wtyczkę WP Desk Helper</a>, aby aktywować i otrzymywać aktualizacje wtyczek WP Desk. Klucz licencyjny wtyczki %s%s%s nie został aktywowany, więc wtyczka jest nieaktywna! %sKliknij tutaj%s, aby aktywować klucz licencyjny wtyczki. 