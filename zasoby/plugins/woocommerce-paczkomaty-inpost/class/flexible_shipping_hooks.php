<?php

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! class_exists( 'WPDesk_Paczkomaty_FS' ) ) {
	class WPDesk_Paczkomaty_FS {

		public function __construct() {

			add_filter( 'flexible_shipping_integration_options', array( $this, 'flexible_shipping_integration_options' ),  10 );
			
			add_filter( 'flexible_shipping_method_settings', array( $this, 'flexible_shipping_method_settings' ), 10, 2 );
			
			add_action( 'flexible_shipping_method_script', array( $this, 'flexible_shipping_method_script' ) );

			add_filter( 'flexible_shipping_process_admin_options', array( $this, 'flexible_shipping_process_admin_options' ), 10, 1 );
			
			add_filter( 'flexible_shipping_method_integration_col', array( $this, 'flexible_shipping_method_integration_col' ), 10, 2 );
			
			add_filter( 'flexible_shipping_method_rate_id', array( $this, 'flexible_shipping_method_rate_id' ), 10, 2 );
			
			add_filter( 'flexible_shipping_add_method', array( $this, 'flexible_shipping_add_method' ), 10, 3 );
			
		}
		
		function flexible_shipping_integration_options( $options ) {
			$options['paczkomaty'] = __( 'Paczkomaty', 'paczkomaty_shipping_method' );
			return $options;
		}
		
		public function flexible_shipping_method_settings( $flexible_shipping_settings, $shipping_method ) {
			$shipping_methods = WC()->shipping->get_shipping_methods();
			if ( $shipping_methods['paczkomaty_shipping_method']->enabled == 'yes' ) {
				$flexible_shipping_settings['method_integration']['options']['paczkomaty'] =  __( 'Paczkomaty', 'paczkomaty_shipping_method' );
			}
			$settings = array(
					'paczkomaty_usluga' => array(
							'title' => __( 'Usługa Paczkomaty', 'paczkomaty_shipping_method' ),
							'type' => 'select',
							'default' => isset( $shipping_method['paczkomaty_usluga'] ) ? $shipping_method['paczkomaty_usluga'] : '',
							'options' => array(
									'paczkomaty'	=> __( 'Paczkomat', 'paczkomaty_shipping_method' ),
									'polecony' 		=> __( 'E-commerce Polecony', 'paczkomaty_shipping_method' ),
							)
					),
			);
			return array_merge( $flexible_shipping_settings, $settings );
		}
		
		public function flexible_shipping_method_script() {
			?>
			<script type="text/javascript">
			jQuery(document).ready(function() {
				function paczkomatyOptions() {
					if ( jQuery('#woocommerce_flexible_shipping_method_integration').val() == 'paczkomaty' ) {
						jQuery('#woocommerce_flexible_shipping_paczkomaty_usluga').closest('tr').css('display','table-row');
					}
					else {
						jQuery('#woocommerce_flexible_shipping_paczkomaty_usluga').closest('tr').css('display','none');
					}
				}
				jQuery('#woocommerce_flexible_shipping_method_integration').change(function() {
					paczkomatyOptions();
				});
				paczkomatyOptions();
			});
			</script>
			<?php
		}
				
		public function flexible_shipping_process_admin_options( $shipping_method )	{
error_log('--woocommerce_flexible_shipping_paczkomaty_usluga');			
			if ( isset( $_POST['woocommerce_flexible_shipping_paczkomaty_usluga'] ) ) {
				$shipping_method['paczkomaty_usluga'] = $_POST['woocommerce_flexible_shipping_paczkomaty_usluga'];
			}
			return $shipping_method;
		}
		
		public function flexible_shipping_method_integration_col( $col, $shipping_method ) {
			$shipping_methods = WC()->shipping->get_shipping_methods();
			if ( $shipping_methods['paczkomaty_shipping_method']->enabled == 'yes' ) {
				if ( isset( $shipping_method['method_integration'] ) && 'paczkomaty' === $shipping_method['method_integration'] ) {				
					ob_start();
					?>
					<td width="1%" class="integration default">
						<?php 
							$tip = __( 'Paczkomaty', 'paczkomaty_shipping_method' );
							if ( isset( $shipping_method['paczkomaty_usluga'] ) && 'polecony' === $shipping_method['paczkomaty_usluga'] ) {
								$tip = __( 'E-commerce Polecony', 'paczkomaty_shipping_method' );
							}
						?>
						<span class="tips" data-tip="<?php echo $tip; ?>">
							<?php echo $shipping_methods['paczkomaty_shipping_method']->method_title; ?>
						</span>
					</td>
					<?php
					$col = ob_get_contents();
					ob_end_clean();
				}
			}
			return $col;
		}

		public function flexible_shipping_method_rate_id( $rate_id, $shipping_method ) {
			if ( isset( $shipping_method['method_integration'] ) && 'paczkomaty' === $shipping_method['method_integration'] ) {
				$rate_id = $rate_id . '_paczkomaty';
				if ( isset( $shipping_method['paczkomaty_usluga'] ) && 'polecony' === $shipping_method['paczkomaty_usluga'] ) {
					$rate_id = $rate_id . '_paczkomaty_polecony';
				}
				else {
					$rate_id = $rate_id . '_paczkomaty_paczkomaty';
				}
			}
			return $rate_id;
		}
		
		public function flexible_shipping_add_method( $add_method, $shipping_method, $package )	{
			if ( isset( $shipping_method['method_integration'] ) && 'paczkomaty' === $shipping_method['method_integration'] 
				&& isset( $shipping_method['paczkomaty_usluga'] ) 
			) {
				if ( $shipping_method['paczkomaty_usluga'] == 'polecony' ) {
					if ( $package['destination']['country'] != 'PL' ) {
						return false;
					}
				}				
				foreach ( $package['contents'] as $item ) {
					if ( $shipping_method['paczkomaty_usluga'] == 'paczkomaty' && get_post_meta( $item['product_id'], '_paczkomat_disabled', true ) == 'yes' )	{
						return false;
					}
					if ( $shipping_method['paczkomaty_usluga'] == 'polecony' && get_post_meta( $item['product_id'], '_paczkomat_polecony_disabled', true ) == 'yes' )	{
						return false;
					}
				}
			}
			return $add_method;
		}
		
		
	}
	
}
