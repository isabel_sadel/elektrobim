<?php
	if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly 
	
if (!class_exists('inspirePlugin'))
{
		

	/**
	 * Base plugin class for Inspire Labs plugins
	 * 
	 * @author Krzysiek
	 *
	 */
    abstract class inspirePlugin
    {  
    	protected $pluginNamespace = "plugin";
    	
    	protected $_pluginPath;
    	protected $_templatePath;
    	
    	protected $_pluginUrl;
    	protected $_defaultViewArgs; // default args given to template
    	
    	public function __construct()
    	{
    		$this->initBaseVariables();
    	}
    	
    	/**
    	 * 
    	 */
    	protected function initBaseVariables()
    	{
    		$reflection = new ReflectionClass($this);
    		
    		// Set Plugin Path
    		$this->_pluginPath = dirname($reflection->getFileName());
    		 
    		// Set Plugin URL
    		$this->_pluginUrl = plugin_dir_url($reflection->getFileName());
    		
    		// 
    		$this->_templatePath = $this->_pluginPath . '/' . str_replace('_', '-', $this->pluginNamespace) . '-templates';
    		
    		$this->_defaultViewArgs = array(
    			'pluginUrl' => $this->getPluginUrl()
    		);
    		
    		if ($reflection->hasMethod( 'action_links' ))
    		{
    			add_filter( 'plugin_action_links_' . plugin_basename( $reflection->getFileName() ), array( $this, 'action_links' ) );
    		}
    	}
    	
        /**
         * 
         * @return string
         */
        public function getPluginUrl()
        {
        	return $this->_pluginUrl;
        }
        
        /**
         * @return string
         */
        public function getTemplatePath()
        {
        	return $this->_templatePath;
        }
        
        /**
         * Renders end returns selected template
         * 
         * @param string $name name of the template
         * @param string $path additional inner path to the template
         * @param array $args args accesible from template
         * @return string
         */
        public function loadTemplate($name, $path = '', $args = array())
        {
        	$args = array_merge($this->_defaultViewArgs, $args);
        	$path = trim($path, '/');
        	
        	if (file_exists($templateName = implode('/', array(get_template_directory(), str_replace('_', '-', $this->pluginNamespace) . '-templates', $path, $name . '.php'))))
        	{
        	} else {
        		$templateName = implode('/', array($this->getTemplatePath(), $path, $name . '.php'));
        	}
			
			ob_start();
        	include($templateName);
        	return ob_get_clean();
        }
        
        /**
         * Gets setting value
         * 
         * @param string $name
         * @param string $default
         * @return Ambigous <mixed, boolean>
         */
        public function getSettingValue($name, $default = null)
        {
        	/*foreach ($this->_settings as $index => $item)
        	 {
        	if ($item['name'] === $name)
        	{
        	$default = $item['value'];
        	}
        	}*/

        	return get_option($this->getNamespace() . '_' . $name, $default);
        }
        
        public function setSettingValue($name, $value)
        {
        	return update_option($this->getNamespace() . '_' . $name, $value);
        }
        
        public function isSettingValue($name)
        {
        	$option = get_option($this->getNamespace() . '_' . $name);
        	return !empty($option);
        }
        
        public function getNamespace()
        {
        	return $this->pluginNamespace;
        }
    }  
}
