<?php if(!empty($args['paczkomat_id'])): ?>
<header>
    <h2><?php _e('Wybrany paczkomat', 'paczkomaty_shipping_method'); ?></h2>
</header>
<dl><?php echo $this->machines[$args['paczkomat_id']]; ?></dl>
<?php endif; ?>