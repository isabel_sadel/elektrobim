<?php

require_once( WC()->plugin_path() . '/includes/wc-template-functions.php' );

$params = array(
    'type' => 'select',
    'options' => $this->adminMachines,
    'class' => array('first'),
    'custom_attributes' => array('style' => 'width:100%'),
    'label' => __('Wybrany paczkomat', 'paczkomaty_shipping_method'),
        );
if (!empty($args['paczkomat_packcode'])){
    $params['custom_attributes']['disabled'] = 'disabled';
}

woocommerce_form_field('paczkomat_id', $params, $args['paczkomat_id']);

if ($args['select2']) : 
?>
    <script type="text/javascript">
        if ( jQuery().select2 ) {
            jQuery("#paczkomat_id").select2();
        }
    </script>
<?php    
endif; 


$params = array(
    'type' => 'select',
    'options' => $this->packageSizes,
    'class' => array('first'),
    'defualt' => $this->default_package_size,
    'label' => __('Wybrany rozmiar paczki', 'paczkomaty_shipping_method'),
        );
        
if (!empty($args['paczkomat_packcode'])){
    $params['custom_attributes']['disabled'] = 'disabled';
}

if(!empty($args['paczkomat_size'])){
    woocommerce_form_field('paczkomat_size', $params, $args['paczkomat_size']);
}
else{
    woocommerce_form_field('paczkomat_size', $params, $this->default_package_size);
}

$params = array(
		'type' => 'text',
		'class' => array('first'),
		'defualt' => $args['paczkomat_customer_ref'],
		'label' => __('Numer referencyjny', 'paczkomaty_shipping_method'),
);
if (!empty($args['paczkomat_packcode'])){
    $params['custom_attributes']['disabled'] = 'disabled';
}
woocommerce_form_field( 'paczkomat_customer_ref', $params, $args['paczkomat_customer_ref'] );

$params = array(
    'type' => 'select',
    'options' => $this->howToGive,
    'class' => array('first'),
    'default' => $this->self_send,
    'label' => __('Sposób nadania', 'paczkomaty_shipping_method'),
        );
        
if (!empty($args['paczkomat_packcode'])){
    $params['custom_attributes']['disabled'] = 'disabled';
}

if(!empty($args['paczkomat_how_to_give'])){
    woocommerce_form_field('paczkomat_how_to_give', $params, $args['paczkomat_how_to_give']);
}
else{
    woocommerce_form_field('paczkomat_how_to_give', $params, $this->self_send);
}
/*
$params = array(
		'type' => 'text',		
		'class' => array('first'),
		'default' => $nr_ref,
		'label' => __('Numer referencyjny', 'paczkomaty_shipping_method'),
);

if (!empty($args['paczkomat_packcode'])){
	$params['custom_attributes']['disabled'] = 'disabled';
}

if(!empty($args['paczkomat_how_to_give'])){
	woocommerce_form_field('paczkomat_how_to_give', $params, $args['paczkomat_how_to_give']);
}
else{
	woocommerce_form_field('paczkomat_how_to_give', $params, $this->self_send);
}
*/

/*
$params = array(
		'type' => 'select',
		'options' => $args['punkty_nadania'],
		'custom_attributes' => array('style' => 'width:100%'),
		'label' => __('Punkt nadania', 'paczkomaty_shipping_method'),
);
woocommerce_form_field('paczkomat_punkt_nadania', $params, $args['punkt_nadania']);
*/

if (!empty($args['paczkomat_packcode'])):
    ?>
    <p><strong>Numer paczki</strong>: <?php echo $args['paczkomat_packcode']; ?></p>
    <?php
endif;

if (!empty($args['paczkomat_id']) && !empty($args['paczkomat_size']) && !empty($args['phone']) && !empty($args['paczkomat_how_to_give'])) {
    ?>
    <?php if (empty($args['paczkomat_packcode'])): ?>
        <a class="button button-primary" href="javascript:void(0)" id="sendPackage"><?php _e('Utwórz paczkę', 'paczkomaty_shipping_method') ?></a>
    <?php else: ?>
        <?php if (!empty($args['paczkomat_packcode'])): ?>
            <a class="button button-primary" href="<?php echo wp_nonce_url(admin_url('?generate_inpost_sticker=true&post=' . $args['post_id']), 'generate_inpost_sticker'); ?>"><?php _e('Pobierz etykietę', 'paczkomaty_shipping_method') ?></a>        
            <?php if ( 1==2 && isset($args['paczkomat_how_to_give']) && ( $args['paczkomat_how_to_give'] == 2 || $args['paczkomat_how_to_give'] == 3 ) ): ?>
                <p><a class="button" href="<?php echo wp_nonce_url(admin_url('?generate_inpost_confirm=true&post=' . $args['post_id']), 'generate_inpost_confirm'); ?>"><?php _e('Pobierz potwierdzenie nadania paczki', 'paczkomaty_shipping_method') ?></a></p>
            <?php endif; ?>
        <?php endif; ?>
    <?php
    endif;
} else {
    ?>
    <div class="errors">
        <p><strong><?php _e('Uzupełnij następujące dane:', 'paczkomaty_shipping_method') ?></strong></p>

		<?php if (empty($args['paczkomat_id'])): ?>
		    <p><?php _e('Paczkomat', 'paczkomaty_shipping_method') ?></p>
		<?php endif; ?>
		<?php if (empty($args['paczkomat_size'])): ?>                                
		    <p><?php _e('Rozmiar paczki', 'paczkomaty_shipping_method') ?></p>
		<?php endif; ?>
		<?php if (empty($args['paczkomat_how_to_give'])): ?>
		    <p><?php _e('Sposób nadania', 'paczkomaty_shipping_method') ?></p>
		<?php endif; ?>
		<?php if (empty($args['phone'])): ?>
		    <p><?php _e('Telefon kupującego', 'paczkomaty_shipping_method') ?></p>
		<?php endif; ?>
    </div>
    <?php
}
?>

<?php if (empty($args['paczkomat_packcode'])): ?>
    <a class="button" href="javascript:void(0)" id="saveOrderBox"><?php _e('Zapisz', 'paczkomaty_shipping_method') ?></a>                        
<?php endif; ?>