<?php

require_once( WC()->plugin_path() . '/includes/wc-template-functions.php' );
/*
$params = array(
		'type' => 'text',
		'class' => array('first'),
		'defualt' => $args['paczkomat_customer_ref'],
		'label' => __('Numer referencyjny', 'paczkomaty_shipping_method'),
);
if (!empty($args['paczkomat_packcode'])){
    $params['custom_attributes']['disabled'] = 'disabled';
}
woocommerce_form_field( 'paczkomat_customer_ref', $params, $args['paczkomat_customer_ref'] );
*/
$params = array(
		'type' => 'select',
		'options' => $args['punkty_nadania'],
		'custom_attributes' => array('style' => 'width:100%'),
		'label' => __('Punkt nadania', 'paczkomaty_shipping_method'),
);
if (!empty($args['paczkomat_packcode'])){
	$params['custom_attributes']['disabled'] = 'disabled';
}
woocommerce_form_field('paczkomat_punkt_nadania', $params, $args['punkt_nadania'] );

if ($args['select2']) :
?>
    <script type="text/javascript">
        if ( jQuery().select2 ) {
            jQuery("#paczkomat_punkt_nadania").select2();
        }
    </script>
<?php    
endif; 


if (!empty($args['paczkomat_packcode'])):
    ?>
    <p><strong>Numer przesyłki</strong>: <?php echo $args['paczkomat_packcode']; ?></p>
    <?php
endif;
?>
<?php if (empty($args['paczkomat_packcode'])): ?>
    <a class="button button-primary" href="javascript:void(0)" id="sendPackagePolecony"><?php _e('Utwórz przesyłkę', 'paczkomaty_shipping_method') ?></a>
<?php else: ?>
    <?php if (!empty($args['paczkomat_packcode'])): ?>
        <a class="button button-primary" href="<?php echo wp_nonce_url(admin_url('?generate_inpost_sticker=true&post=' . $args['post_id']), 'generate_inpost_sticker'); ?>"><?php _e('Pobierz etykietę', 'paczkomaty_shipping_method') ?></a>        
        <?php if (isset($args['paczkomat_how_to_give']) && $args['paczkomat_how_to_give'] == 2): ?>
            <p><a class="button" href="<?php echo wp_nonce_url(admin_url('?generate_inpost_confirm=true&post=' . $args['post_id']), 'generate_inpost_confirm'); ?>"><?php _e('Pobierz potwierdzenie nadania paczki', 'paczkomaty_shipping_method') ?></a></p>
        <?php endif; ?>
    <?php endif; ?>
<?php endif;
