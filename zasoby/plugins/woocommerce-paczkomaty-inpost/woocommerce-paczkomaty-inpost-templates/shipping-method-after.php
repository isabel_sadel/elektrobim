<tr class="shipping paczkomaty-shipping">
    <th><?php _e('Wybierz paczkomat', 'paczkomaty'); ?></th>

    <td>
        <select id="paczkomat_id" name="paczkomat_id">
            <option value="">-- <?php _e('wybierz', 'paczkomaty_shipping_method'); ?> --</option>

            <?php if (count($args['nearly']) > 0): ?>
                <optgroup label="<?php _e('w pobliżu', 'paczkomaty_shipping_method'); ?>">
                    <?php foreach ($args['nearly'] as $key => $value): ?>
                        <option value="<?php echo $key ?>">[<?php echo $key; ?>] <?php echo $value; ?></option>
                    <?php endforeach; ?>
                </optgroup>

                <optgroup label="<?php _e('pozostałe', 'paczkomaty_shipping_method'); ?>">
            <?php endif; ?>

                <?php foreach ($args['other'] as $key => $value): ?>
                    <option value="<?php echo $key ?>">[<?php echo $key; ?>] <?php echo $value; ?></option>
                <?php endforeach; ?>

            <?php if (count($args['nearly']) > 0): ?>
                </optgroup>
            <?php endif; ?>
        </select>

        <?php if ($args['select2']) : ?>
            <script type="text/javascript">
                if ( jQuery().select2 ) {
                    jQuery("#paczkomat_id").select2();
                }
            </script>
        <?php endif; ?>
<?php /*
        <p><?php _e( 'Sprawdź gdzie jest Twój nabliższy Paczkomat na', 'paczkomaty_shipping_method' ); ?> <a href="http://www.paczkomaty.pl/pl/dla-ciebie/znajdz-paczkomat#narrow-content" target="_blank"><?php _e( 'mapie', 'paczkomaty_shipping_method' ); ?></a>.
*/ ?>        
        <?php $geowidget_src = 'https://geowidget.inpost.pl/dropdown.php?dropdown_name=paczkomat_id'; ?>
        <script type="text/javascript" src="<?php echo $geowidget_src; ?>"></script>
        <script type="text/javascript">
            if ( jQuery().select2 ) {
                jQuery( window ).focus(function() {
					jQuery("#paczkomat_id").change();
					setTimeout( function(){ jQuery("#paczkomat_id").change(); }, 500 );
                })
            }
            function openGeowidget() {
                var town = jQuery('#billing_city').val();
                if ( jQuery('#ship-to-different-address-checkbox').is(':checked') ) {
                	town = jQuery('#shipping_city').val();
                }
                town = '';
                var options = {
                   town: town,
                   selected: jQuery('#paczkomat_id').val(),     
                };
            	openMap( options ); 
            }
        </script>
        
        <a href="#" onclick="openGeowidget(); return false;"><?php _e( 'Wybierz paczkomat na mapie', 'paczkomaty_shipping_method' ); ?></a>
        
    </td>
</tr>
