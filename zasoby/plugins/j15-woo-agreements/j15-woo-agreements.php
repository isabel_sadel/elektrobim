<?php
/**
 * Plugin Name:     Zgody WooCommerce
 * Plugin URI:      https://january15th.com
 * Description:     Modyfikuje pola zgód WooCommerce
 * Author:          January 15th Ltd
 * Author URI:      https://january15th.com
 * Text Domain:     j15-woo-agreements
 * Domain Path:     /languages
 * Version:         1.0
 *
 * @package         J15_Woo_Agreements
 * @version         1.0
 */


if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define( 'J15_WOO_PRIV_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'J15_WOO_PRIV_PLUGIN_URL', plugin_dir_url( __FILE__ ) );

add_action( 'wp_enqueue_scripts', 'j15_woo_privacy_scripts_and_styles' );

/**
 * @retun void
 * @since 1.0
 */
function j15_woo_privacy_scripts_and_styles() {

	wp_enqueue_style( 'j15-woo-priv', J15_WOO_PRIV_PLUGIN_URL . 'assets/css/style.css', [], '1.0', 'all' );

}




add_filter( 'woocommerce_get_privacy_policy_text', 'j15_woo_privacy_policy_text_checkbox', 99, 2 );

/**
 * @param $text
 * @param $type
 *
 * @return string
 *
 * @since 1.0
 */

function j15_woo_privacy_policy_text_checkbox( $text, $type ) {

	$new_text = woocommerce_form_field( 'privacy_policy', array(
		'type'               => 'checkbox',
		'return'             => true,
		'class'              => array( 'form-row privacy j15-privacy-field' ),
		'label_class'        => array( 'woocommerce-form__label woocommerce-form__label-for-checkbox checkbox j15-privacy-field__label' ),
		'input_class'        => array( 'woocommerce-form__input woocommerce-form__input-checkbox input-checkbox j15-privacy-field__input' ),
		'custom_attributes'  => array( 'required' => 'true' ),
		'required'           => true,
		'label'              => wc_replace_policy_page_link_placeholders( $text ),
	) );

	echo j15_woo_privacy_add_required_attr( $new_text );

}

add_action( 'woocommerce_checkout_process', 'j15_woo_privacy_not_approved_privacy' );

/**
 * Adds privacy policy field validation in order form
 */
function j15_woo_privacy_not_approved_privacy() {
	if ( ! (int) isset( $_POST['privacy_policy'] ) ) {
		wc_add_notice( __( 'Musisz wyrazić zgodę na <strong>przetwarzanie danych osobowych</strong>.' ), 'error' );
	}
}


/**
 * Adds 'required' parameter which is not passible via $arguments woocommerce_form_field()
 *
 * @param $html
 *
 * @return mixed
 *
 * @since 1.0
 */
function j15_woo_privacy_add_required_attr( $html ) {

	$new_html = str_replace( '<input', '<input required', $html );

	return $new_html;

}

add_shortcode( 'woo_agreement', 'j15_woo_privacy_agreement_shortcode_cb' );

/**
 * A shortcode for displaying WooCommerce agreements anywhere
 *
 * @param $atts
 *
 * @return string
 *
 * @since 1.0
 */
function j15_woo_privacy_agreement_shortcode_cb( $atts ) {

	$atts = shortcode_atts( array(
		'type' => 'checkout'
	), $atts, 'woo_agreement' );

	return j15_woo_privacy_agreement( $atts['type'] );

}


/**
 * @param string $type
 *
 * @return string
 *
 * @since 1.0
 */
function j15_woo_privacy_agreement( $type = 'checkout' ) {

	if( !in_array( $type, ['checkout','registration'] ) ) $type = 'checkout';

	ob_start();

	echo wc_get_privacy_policy_text( $type );

	return ob_get_clean();

}


add_action( 'woocommerce_review_order_before_submit', 'bbloomer_add_checkout_privacy_policy', 9 );



add_filter( 'comment_form_submit_field', 'j15_woo_privacy_comment_field', 99, 1 );

function j15_woo_privacy_comment_field( $html ) {

	return  j15_woo_privacy_agreement('checkout' ). $html;

}

