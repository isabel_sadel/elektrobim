<?php

use WPDesk\PluginBuilder\BuildDirector\LegacyBuildDirector;
use WPDesk\PluginBuilder\Builder\InfoBuilder;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/** @var WPDesk_Plugin_Info $plugin_info */
$builder        = new InfoBuilder( $plugin_info );
$build_director = new LegacyBuildDirector( $builder );
$build_director->build_plugin();

add_action( 'plugins_loaded', 'flexible_shipping_plugins_loaded', 9 );
if ( ! function_exists( 'flexible_shipping_plugins_loaded' ) ) {
	function flexible_shipping_plugins_loaded() {
		if ( ! function_exists( 'should_enable_wpdesk_tracker' ) ) {
			function should_enable_wpdesk_tracker() {
				$tracker_enabled = true;
				if ( ! empty( $_SERVER['SERVER_ADDR'] ) && $_SERVER['SERVER_ADDR'] === '127.0.0.1' ) {
					$tracker_enabled = false;
				}

				return apply_filters( 'wpdesk_tracker_enabled', $tracker_enabled );
			}
		}

		$tracker_factory = new WPDesk_Tracker_Factory();
		$tracker_factory->create_tracker( basename( dirname( __FILE__ ) ) );
	}
}

require_once( __DIR__ . '/inc/functions.php' );
require_once( __DIR__ . '/classes/shipment/functions.php' );
require_once( __DIR__ . '/classes/manifest/functions.php' );

if ( !function_exists( 'wpdesk_is_plugin_active' ) ) {
	function wpdesk_is_plugin_active( $plugin_file ) {
		$active_plugins = (array) get_option( 'active_plugins', array() );

		if ( is_multisite() ) {
			$active_plugins = array_merge( $active_plugins, get_site_option( 'active_sitewide_plugins', array() ) );
		}

		return in_array( $plugin_file, $active_plugins ) || array_key_exists( $plugin_file, $active_plugins );
	}
}
