function flexible_shipping_saas_integration_options(integration) {
	var integration_class = '.' + integration;
	jQuery('.flexible_shipping_method_rules').css('display', 'table-row');
	if (jQuery('#woocommerce_flexible_shipping_method_integration').val() == integration) {
		jQuery(integration_class).closest('tr').css('display', 'table-row');
	}
	else {
		jQuery(integration_class).closest('tr').css('display', 'none');
	}
	flexible_shipping_saas_rates_type_options();
}

function flexible_shipping_saas_rates_type_options() {
	if ( jQuery('#woocommerce_flexible_shipping_method_integration').length ) {
		var integration_class = '.' + jQuery('#woocommerce_flexible_shipping_method_integration').val();
		if ( integration_class !== '.' ) {
			var rates_type_value = jQuery('.fs-rates-type'+integration_class).val();
			jQuery(integration_class).each(function (index) {
				var closest_tr = jQuery(this).closest('tr');
				if ('custom' === rates_type_value) {
					if (!jQuery(this).hasClass('fs-method')) {
						closest_tr.css('display', 'none');
					} else {
						closest_tr.css('display', 'table-row');
					}
				}
				if ('live' === rates_type_value) {
					if (!jQuery(this).hasClass('fs-live')) {
						closest_tr.css('display', 'none');
					} else {
						closest_tr.css('display', 'table-row');
					}
				}
			});
			// force elements show/hide
			jQuery(window).trigger('resize');
			jQuery(integration_class).each(function (index) {
				if ('checkbox' === jQuery(this).attr('type')) {
					jQuery(this).trigger('change');
				}
			});
		}
	}
}

function flexible_shipping_saas_click_action(fs_action, element, event ) {
	let id = jQuery(element).attr('data-id');
	if ( typeof id !== 'undefined' ) {
		event.stopImmediatePropagation();
		event.preventDefault();
		let element_id = fs_id(element);
		fs_ajax( element, element_id, fs_action );
	}
}

function flexible_shipping_saas_checkbox_dependent_field(checkbox) {
	var dependend_field_id = jQuery(checkbox).data('dependent-field-id');
	if (typeof dependend_field_id !== 'undefined' && jQuery(checkbox).is(':visible')) {
		var dependend_field = jQuery('#'+dependend_field_id);
		if (jQuery(checkbox).is(':checked')) {
			dependend_field.closest('tr').css('display','table-row');
		} else {
			dependend_field.closest('tr').css('display','none');
		}
	}
	var dependend_row_class = jQuery(checkbox).data('dependent-row-class');
	if (typeof dependend_row_class !== 'undefined' ) {
		var dependend_row = jQuery('.' + dependend_row_class);
		if ( jQuery(checkbox).is(':visible') ) {
			if (jQuery(checkbox).is(':checked')) {
				dependend_row.css('display', 'table-row');
			} else {
				dependend_row.css('display', 'none');
			}
		} else {
			dependend_row.css('display', 'table-row');
		}
	}
}

function fs_select2() {
	let elements = jQuery( '.fs_select2' );
	if ( elements.length ) {
		if (jQuery.fn.selectWoo) {
			elements.selectWoo();
		} else {
			elements.select2();
		}
	}
}

function flexible_shipping_saas_add_attachment( element, event ) {
	event.stopImmediatePropagation();
	event.preventDefault();

	flexible_shipping_saas_choose_attachment( element, event );
}


function flexible_shipping_saas_send_attachments( element, event ) {
	event.stopImmediatePropagation();
	event.preventDefault();

	var el = jQuery(element);

	var shipment_id = el.data('shipment_id');

	jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').hide();

	var nonce = jQuery('#flexible_shipping_shipment_nonce_' + shipment_id).val();

	jQuery.ajax({
		url: fs_admin.ajax_url,
		type: 'POST',
		data: {
			action: 'flexible_shipping_send_attachments_for_shipment',
			nonce: nonce,
			shipment_id: shipment_id,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response) {
			if (response == '0') {
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html("Invalid response: 0");
			}
			else if (response.status == 'success') {
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible-shipping-shipment-attachments').replaceWith(response.content);
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').hide();
				if ( typeof response.message != 'undefined' ) {
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(response.message);
				}
			}
			else {
				if ( typeof response.content !== 'undefined' ) {
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').removeClass("flexible_shipping_shipment_message_error");
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible-shipping-shipment-attachments').html(response.content);
				}
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(response.message);
			}
		}
		else {
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html("Request failed: invalid method?");
		}
	}).always(function () {
	}).fail(function (jqXHR, textStatus, errorThrown ) {
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(errorThrown);
	})
}

function flexible_shipping_saas_delete_attachment( element, event ) {
	event.stopImmediatePropagation();
	event.preventDefault();

	var el = jQuery(element);

	var shipment_id = el.data('shipment_id');
	var saas_attachment_id = el.data('saas_attachment_id');

	jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').hide();

	var nonce = jQuery('#flexible_shipping_shipment_nonce_' + shipment_id).val();

	jQuery.ajax({
		url: fs_admin.ajax_url,
		type: 'POST',
		data: {
			action: 'flexible_shipping_delete_attachment_for_shipment',
			nonce: nonce,
			shipment_id: shipment_id,
			saas_attachment_id: saas_attachment_id,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response) {
			if (response == '0') {
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html("Invalid response: 0");
			}
			else if (response.status == 'success') {
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible-shipping-shipment-attachments').replaceWith(response.content);
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').hide();
				if ( typeof response.message != 'undefined' ) {
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(response.message);
				}
			}
			else {
				if ( typeof response.content !== 'undefined' ) {
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').removeClass("flexible_shipping_shipment_message_error");
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible-shipping-shipment-attachments').html(response.content);
				}
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(response.message);
			}
		}
		else {
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html("Request failed: invalid method?");
		}
	}).always(function () {
	}).fail(function (jqXHR, textStatus, errorThrown ) {
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(errorThrown);
	})
}


var downloadable_file_frame;
var el_content;
var el_attachment_id;
var el_attachment_filename;
var el_shipment_id;
var el_attachment_type;

function flexible_shipping_saas_choose_attachment( element, event ) {
	event.stopImmediatePropagation();
	event.preventDefault();

	var el = jQuery(element);

	el_content = el.closest('.flexible-shipping-shipment-attachments');
	el_attachment_id = el_content.find('.flexible-shipping-shipment-attachments-attachment-id');
	el_attachment_filename = el_content.find('.flexible-shipping-shipment-attachments-filename');
	el_shipment_id = el.data('shipment_id');
	el_attachment_type = el_content.find('.flexible-shipping-shipment-attachments-type').val();

	// If the media frame already exists, reopen it.
	if ( downloadable_file_frame ) {
		downloadable_file_frame.open();
		return;
	}

	var downloadable_file_states = [
		// Main states.
		new wp.media.controller.Library({
			library:   wp.media.query(),
			multiple:  true,
			title:     el.data('choose'),
			priority:  20,
			filterable: 'uploaded'
		})
	];

	// Create the media frame.
	downloadable_file_frame = wp.media.frames.downloadable_file = wp.media({
		// Set the title of the modal.
		title: el.data('choose'),
		library: {
			type: ''
		},
		button: {
			text: el.data('update')
		},
		multiple: true,
		states: downloadable_file_states
	});

	// When an image is selected, run a callback.
	downloadable_file_frame.on( 'select', function() {
		var attachment_id = '';
		var file_path = '';
		var file_name = '';
		var selection = downloadable_file_frame.state().get( 'selection' );

		selection.map( function( attachment ) {
			attachment = attachment.toJSON();

			if ( attachment.id ) {
				attachment_id = attachment.id;
			}
			if ( attachment.url ) {
				file_path = attachment.url;
			}
			if ( attachment.filename ) {
				file_name = attachment.filename;
			}

			el_attachment_filename.val(file_name).change();
			el_attachment_id.val(attachment_id).change();

			el.attr( 'disabled', true );

			flexible_shipping_saas_add_attachment_to_shipment( el_shipment_id, attachment_id, el_attachment_type );

			el.attr( 'disabled', false );

		});

	});

	// Set post to 0 and set our custom type.
	downloadable_file_frame.on( 'ready', function() {
		downloadable_file_frame.uploader.options.uploader.params = {
			type: 'shipment'
		};
	});

	// Finally, open the modal.
	downloadable_file_frame.open();
}


function flexible_shipping_saas_add_attachment_to_shipment( shipment_id, attachment_id, attachment_type ) {
	jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').hide();

	var nonce = jQuery('#flexible_shipping_shipment_nonce_' + shipment_id).val();

	jQuery.ajax({
		url: fs_admin.ajax_url,
		type: 'POST',
		data: {
			action: 'flexible_shipping_add_attachment_to_shipment',
			nonce: nonce,
			shipment_id: shipment_id,
			attachment_id: attachment_id,
			attachment_type: attachment_type,
		},
		dataType: 'json',
	}).done(function (response) {
		if (response) {
			if (response == '0') {
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html("Invalid response: 0");
			}
			else if (response.status == 'success') {
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible-shipping-shipment-attachments').replaceWith(response.content);
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').hide();
				if ( typeof response.message != 'undefined' ) {
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(response.message);
				}
			}
			else {
				if ( typeof response.content !== 'undefined' ) {
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').removeClass("flexible_shipping_shipment_message_error");
					jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_content').html(response.content);
				}
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
				jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(response.message);
			}
		}
		else {
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
			jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html("Request failed: invalid method?");
		}
	}).always(function () {
	}).fail(function (jqXHR, textStatus, errorThrown ) {
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').addClass("flexible_shipping_shipment_message_error");
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').show();
		jQuery('#flexible_shipping_shipment_' + shipment_id + ' .flexible_shipping_shipment_attachment_message').html(errorThrown);
	})
}


jQuery(document).on('click','.fs-saas-button-create', function( event ) {
	flexible_shipping_saas_click_action( 'send', this, event );
});

jQuery(document).on('click','.fs-saas-button-save', function(e) {
	flexible_shipping_saas_click_action( 'save', this, event );
});

jQuery(document).on('click','.fs-saas-button-cancel-created', function( e ) {
	flexible_shipping_saas_click_action( 'cancel', this, event );
});

jQuery(document).on('change', '.fs-rates-type', function( e ) {
	flexible_shipping_saas_rates_type_options();
});
jQuery(document).on('change', 'input[type=checkbox]', function( e ) {
	flexible_shipping_saas_checkbox_dependent_field(this);
});


jQuery(document).on('click','.fs-saas-button-add-attachment', function( e ) {
	flexible_shipping_saas_add_attachment( this, e );
});
jQuery(document).on('click','.fs-saas-button-delete-attachment', function( e ) {
	flexible_shipping_saas_delete_attachment( this, e );
});
jQuery(document).on('click','.fs-saas-button-send-attachments', function( e ) {
	flexible_shipping_saas_send_attachments( this, e );
});
jQuery(document).on('mouseover','.flexible_shipping_shipment_attachment_message', function( e ) {
//	jQuery(this).hide();
});


jQuery(document).ready(function() {
	fs_select2();
});

