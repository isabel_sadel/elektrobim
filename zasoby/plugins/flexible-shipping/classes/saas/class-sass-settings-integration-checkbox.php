<?php

class WPDesk_Flexible_Shipping_SaaS_Settings_Integration_Checkbox
	implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;

	const OPTION_NAME = 'flexible_shipping_connect_integration_box';

	/**
	 * Connection
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $connection;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Settings_Integration_Checkbox constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection $connection
	 */
	public function __construct( WPDesk_Flexible_Shipping_SaaS_Connection $connection ) {
		$this->connection = $connection;
	}

	public function hooks() {
		add_action( 'wp_ajax_update_fs_connect_integration_setting', [ $this, 'wp_ajax_update_integration_setting' ] );
	}

	/**
	 * Handle AJAX update setting request.
	 */
	public function wp_ajax_update_integration_setting() {
		$state = $_POST['fs_box_state'];
		update_option( self::OPTION_NAME, $state );
		wp_send_json_success( [ 'state' => $state ] );
	}

	/**
	 * Is FS Connect visible
	 *
	 * @return bool
	 */
	public function is_visible() {
		if( $this->connection->is_connected() ) {
			return true;
		}
		return get_option( self::OPTION_NAME, '0' ) === '1';
	}

}