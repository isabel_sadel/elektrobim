<?php
echo sprintf(
    __( 'You never sign up to Flexible Shipping Connect. You may finish the registration process here %1$sSign up for Connect%2$s', 'flexible-shipping' ),
    '<a href="' . $link . '" class="button button-primary">',
    '</a>'
);
?>
