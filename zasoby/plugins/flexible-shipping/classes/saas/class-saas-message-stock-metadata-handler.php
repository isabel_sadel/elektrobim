<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Message_Stock_Metadata_Handler
 */
class WPDesk_Flexible_Shipping_SaaS_Message_Stock_Metadata_Handler implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'woocommerce_before_order_itemmeta', [ $this, 'display_message_stock_in_order_shipping' ], 10, 3 );
	}

	/**
	 * Display message stock in order shipping.
	 *
	 * @param string          $item_id Item Id.
	 * @param WC_Order_Item   $item Item.
	 * @param WC_Product|null $product Produuct.
	 */
	public function display_message_stock_in_order_shipping( $item_id, $item, $product ) {
		if ( $item->is_type( 'shipping' ) ) {
			$fs_saas_data = $item->get_meta( '_fs_saas_data' );
			if ( is_array( $fs_saas_data )
				&& isset( $fs_saas_data['_fs_live_rate_response'] )
				&& isset( $fs_saas_data['_fs_live_rate_response']['messageStack'] )
				&& is_array( $fs_saas_data['_fs_live_rate_response']['messageStack'] )
			) {
				$message = '';
				foreach ( $fs_saas_data['_fs_live_rate_response']['messageStack'] as $message_stock_text ) {
					$message .= $message_stock_text . ' ';
				}
				include 'views/html-order-message-stock.php';
			}
		}
	}

}
