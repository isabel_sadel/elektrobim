<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'WPDesk_Flexible_Shipping_SaaS_User_Registration' ) ) {

	/**
	 * Class WPDesk_Flexible_Shipping_SaaS_User_Registration
	 */
	class WPDesk_Flexible_Shipping_SaaS_User_Registration
		implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant, \WPDesk\PluginBuilder\Plugin\HookableCollection {

		use \WPDesk\PluginBuilder\Plugin\PluginAccess;
		use \WPDesk\PluginBuilder\Plugin\HookableParent;

		const PRIORITY_AFTER_DEFAULT       = 11;
		const PRIORITY_AFTER_AFTER_DEFAULT = 12;

		const PAGE_WOOCOMMERCE_SETTINGS         = 'wc-settings';
		const TAB_SHIPPING                      = 'shipping';
		const SECTION_FLEXIBLE_SHIPPING_CONNECT = 'flexible_shipping_connect';

		const SUBMIT_REGISTER_PARAMETER = 'flexible-shipping-submit-register';

		const SETTING_FLEXIBLE_SHIPPING_USER_EMAIL = 'flexible_shipping_user_email';
		const SETTING_FLEXIBLE_SHIPPING_API_KEY    = 'flexible_shipping_api_key';
		const SETTING_FLEXIBLE_SHIPPING_REGISTERED = 'flexible_shipping_registered';

		const NONCE_REGISTRATION_ACTION = 'fsregister';
		const NONCE_REGISTRATION_NAME   = '_nonce_fsregister';

		const SETTINGS_ERROR_SETTING      = 'flexible-shipping-register';
		const SETTINGS_ERROR_CODE_SUCCESS = 'success';

		const NOTICE_NAME = 'fs_connect_notice';

		const REGISTER_PAGE_SLUG           = 'flexible_shipping_connect_register';
		const FS_CONNECT_FIRST_NOTICE_NAME = 'fs_connect_start';

		/**
		 * Show register form
		 *
		 * @var bool
		 */
		private $show_register_form = true;

		/**
		 * SaaS connection.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Connection
		 */
		private $saas_connection;

		/**
		 * Links.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
		 */
		private $saas_platform_links;

		/**
		 * WPDesk_Flexible_Shipping_Services constructor.
		 *
		 * @param WPDesk_Flexible_Shipping_SaaS_Connection     $saas_connection Connection.
		 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Links.
		 */
		public function __construct(
			WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection,
			WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links
		) {
			$this->saas_connection     = $saas_connection;
			$this->saas_platform_links = $saas_platform_links;

			if ( ! $this->saas_connection->is_connected() && ! $this->is_on_setings_page() ) {
				$this->add_hookable( new WPDesk_Flexible_Shipping_SaaS_Registration_Second_Notice( self::FS_CONNECT_FIRST_NOTICE_NAME ) );
			}
		}

		/**
		 * Is on settings page?
		 *
		 * @return bool
		 */
		public function is_on_setings_page() {
			return ( isset( $_GET['page'] ) && self::PAGE_WOOCOMMERCE_SETTINGS === $_GET['page']
			         && isset( $_GET['tab'] ) && self::TAB_SHIPPING === $_GET['tab']
			         && isset( $_GET['section'] ) && self::SECTION_FLEXIBLE_SHIPPING_CONNECT === $_GET['section']
			);
		}

		/**
		 * Is on registration page?
		 *
		 * @return bool
		 */
		public function is_on_registration_page() {
			return ( isset( $_GET['page'] ) && self::REGISTER_PAGE_SLUG === $_GET['page'] );
		}

		/**
		 * Is on submit page?
		 *
		 * @return bool
		 */
		public function is_on_submit_page() {
			return ( isset( $_GET[ self::SUBMIT_REGISTER_PARAMETER ] ) && '1' === $_GET[ self::SUBMIT_REGISTER_PARAMETER ] );
		}

		/**
		 * Hooks.
		 */
		public function hooks() {
			add_filter( 'removable_query_args', [ $this, 'add_email_parameters_to_removable_query_args' ] );
			add_action( 'admin_init', [ $this, 'register_user_and_add_notice' ] );
			add_action( 'admin_init', [ $this, 'create_registration_notices' ] );
			add_action( 'admin_menu', [ $this, 'add_registration_page' ] );
			$this->hooks_on_hookable_objects();
		}

		/**
		 * Add registration page to WordPress admin area.
		 */
		public function add_registration_page() {
			add_submenu_page(
				null,
				__( 'Flexible Shipping Connect Registration', 'flexible-shipping' ),
				__( 'Flexible Shipping Connect Registration', 'flexible-shipping' ),
				'edit_pages',
				self::REGISTER_PAGE_SLUG,
				[ $this, 'show_registration_page' ]
			);
		}

		/**
		 * Display registration page.
		 */
		public function show_registration_page() {
			$user_email = get_option( 'admin_email' );
			if ( isset( $_REQUEST['user_email'] ) ) {
				$user_email = $_REQUEST['user_email'];
			}
			$action         = admin_url( 'admin.php' );
			$action         = add_query_arg( self::SUBMIT_REGISTER_PARAMETER, '1', $action );
			$url_fs_com     = $this->saas_platform_links->add_utm_general( $this->saas_platform_links->get_base_url() );
			$url_terms      = $this->saas_platform_links->add_utm(
				$this->saas_platform_links->get_terms(),
				'fs-signup-terms',
				'flexible-shipping',
				'link',
				'user-site',
				'terms-of-service'
			);
			$url_privacy    = $this->saas_platform_links->add_utm(
				$this->saas_platform_links->get_privacy(),
				'fs-signup-privacy',
				'flexible-shipping',
				'link',
				'user-site',
				'privacy-policy'
			);
			$saas_connected = $this->saas_connection->is_connected();
			$settings_url   = admin_url( 'admin.php' );
			$settings_url   = add_query_arg( 'page', self::PAGE_WOOCOMMERCE_SETTINGS, $settings_url );
			$settings_url   = add_query_arg( 'tab', self::TAB_SHIPPING, $settings_url );
			$settings_url   = add_query_arg( 'section', self::SECTION_FLEXIBLE_SHIPPING_CONNECT, $settings_url );
			include 'views/html-registration-page.php';
		}

		/**
		 * Show registration notices.
		 */
		public function create_registration_notices() {
			$settings_errors = get_settings_errors( self::SETTINGS_ERROR_SETTING );
			$notices         = array();
			foreach ( $settings_errors as $error ) {
				$notices[] = new \WPDesk\Notice\Notice( $error['message'], $error['type'] );
			}
			return $notices;
		}

		/**
		 * Removable query args.
		 *
		 * @param array $removable_query_args Query args.
		 *
		 * @return array
		 */
		public function add_email_parameters_to_removable_query_args( $removable_query_args ) {
			$removable_query_args[] = 'user_email';
			$removable_query_args[] = '_nonce_fsregister';
			$removable_query_args[] = '_wp_http_referer';
			return $removable_query_args;
		}


		/**
		 * Add platform unavailable notice.
		 */
		private function add_notice_connecton_problem() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = __( 'There are some connection problems. Please try again in few minutes. Thanks!', 'flexible-shipping' );
			add_settings_error( self::SETTINGS_ERROR_SETTING, self::SETTINGS_ERROR_CODE_SUCCESS, $notice_content, $notice_type );
		}

		/**
		 * Add invalid email notice.
		 */
		private function add_notice_invalid_email() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = __( 'Please enter a valid email!', 'flexible-shipping' );
			add_settings_error( self::SETTINGS_ERROR_SETTING, self::SETTINGS_ERROR_CODE_SUCCESS, $notice_content, $notice_type );
		}

		/**
		 * Add notice user registered.
		 *
		 * @param string $user_email User email.
		 */
		private function add_notice_user_registered( $user_email ) {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_SUCCESS;
			$notice_content = sprintf(
				/* Translators: email and url */
				__( '%1$sAccount created, please check your email and click the activation link to connect (%3$s).%2$s If you haven\'t received the email, please %4$scontact us%5$s.', 'flexible-shipping' ),
				'<strong>',
				'</strong>',
				$user_email,
				'<a href=' . $this->saas_platform_links->add_utm(
					$this->saas_platform_links->get_support(),
					'fs-signup-support',
					'flexible-shipping',
					'link',
					'user-site',
					''
				) . ' tagret="_blank">',
				'</a>'
			);
			add_settings_error( self::SETTINGS_ERROR_SETTING, self::SETTINGS_ERROR_CODE_SUCCESS, $notice_content, $notice_type );
		}

		/**
		 * Add notice domain occupied.
		 */
		private function add_notice_domain_occupied() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = sprintf(
				/* Translators: email and url */
				__( 'Your store (%1$s) is already registered with another account! You can register your domain with one account. If you have questions, %2$scontact us%3$s.', 'flexible-shipping' ),
				site_url(),
				'<a target="_blank" href=' . $this->saas_platform_links->add_utm(
					$this->saas_platform_links->get_support(),
					'fs-register-contactus',
					'flexible-shipping',
					'link',
					'user-site',
					'contact-us'
				) . ' tagret="_blank">',
				'</a>'
			);
			add_settings_error( self::SETTINGS_ERROR_SETTING, self::SETTINGS_ERROR_CODE_SUCCESS, $notice_content, $notice_type );
		}

		/**
		 * Add notice domain not allowed.
		 */
		private function add_notice_domain_not_allowed() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = sprintf(
				/* Translators: email and url */
				__( 'Our demo does not support Flexible Shipping Connect registration. Try FS Connect for free. %1$sCheck now →%2$s', 'flexible-shipping' ),
				'<a target="_blank" href=' . $this->saas_platform_links->add_utm(
					$this->saas_platform_links->get_connect(),
					'fs-demo-doesnt-support',
					'flexible-shipping',
					'link',
					'demo',
					'check-now'
				) . ' tagret="_blank">',
				'</a>'
			);
			add_settings_error( self::SETTINGS_ERROR_SETTING, self::SETTINGS_ERROR_CODE_SUCCESS, $notice_content, $notice_type );
		}

		/**
		 * Add notice user already exists.
		 *
		 * @param string $user_email User email.
		 */
		private function add_notice_user_already_exists( $user_email ) {
			$settings_url   = admin_url( 'admin.php' );
			$settings_url   = add_query_arg( 'page', self::PAGE_WOOCOMMERCE_SETTINGS, $settings_url );
			$settings_url   = add_query_arg( 'tab', self::TAB_SHIPPING, $settings_url );
			$settings_url   = add_query_arg( 'section', self::SECTION_FLEXIBLE_SHIPPING_CONNECT, $settings_url );
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_INFO;
			$notice_content = sprintf(
				/* Translators: email and url */
				__( 'Account with this email is already registered! We emailed your Connect Key to %1$s.<br>Enter your key %2$shere%4$s. You can also get your key by logging to %3$syour account%4$s.', 'flexible-shipping' ),
				$user_email,
				'<a href=' . $settings_url . '>',
				'<a href=' . $this->saas_platform_links->get_my_account() . ' target="_blank">',
				'</a>'
			);
			add_settings_error( self::SETTINGS_ERROR_SETTING, self::SETTINGS_ERROR_CODE_SUCCESS, $notice_content, $notice_type );
		}

		/**
		 * Add admin notice.
		 *
		 * @param int $response_code Notice code.
		 */
		private function add_notice_from_response( $response_code ) {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = sprintf(
				/* Translators: notice code */
				__( 'Unknown response code from Flexible Shipping Connect API (%s).', 'flexible-shipping' ),
				$response_code
			);
			add_settings_error( self::SETTINGS_ERROR_SETTING, self::SETTINGS_ERROR_CODE_SUCCESS, $notice_content, $notice_type );
		}

		/**
		 * Registers user trough FS Connect platform
		 *
		 * @param string                                    $email Email.
		 * @param WPDesk_Flexible_Shipping_SaaS_Auth_Params $params Params.
		 *
		 * @return \WPDesk\SaasPlatformClient\Response\Authentication\RegisterResponse
		 */
		private function register_user( $email, WPDesk_Flexible_Shipping_SaaS_Auth_Params $params ) {
			$platform = $this->saas_connection->get_platform();

			return $platform->requestRegister( $email, $params->get_user_domain(), $params->get_user_locale(), admin_url( 'admin.php' ) );
		}

		/**
		 * Register user and show notice.
		 */
		public function register_user_and_add_notice() {
			if ( $this->is_on_submit_page() ) {
				$user_email = '';
				if ( isset( $_REQUEST[ self::NONCE_REGISTRATION_NAME ] )
				     && wp_verify_nonce( $_REQUEST[ self::NONCE_REGISTRATION_NAME ], self::NONCE_REGISTRATION_ACTION )
				     && isset( $_REQUEST['user_email'] )
				) {
					$user_email = $_REQUEST['user_email'];
					$registered = get_option( self::SETTING_FLEXIBLE_SHIPPING_REGISTERED, '' );
					if ( '' !== $user_email && '' === $registered && is_email( $user_email ) ) {
						try {
							$register_response = $this->register_user(
								$user_email,
								new WPDesk_Flexible_Shipping_SaaS_Auth_Params()
							);
							if ( $register_response->isUserRegistered() ) {
								$this->add_notice_user_registered( $user_email );

							} else {
								if ( $register_response->isDomainOccupied() ) {
									$this->add_notice_domain_occupied();
								} elseif ( $register_response->isUserAlreadyExists() ) {
									$this->add_notice_user_already_exists( $user_email );
								} elseif ( $register_response->isBadRequest() ) {
									$this->add_notice_connecton_problem();
								} elseif ( $register_response->isServerFatalError() ) {
									$this->add_notice_connecton_problem();
								} elseif ( $register_response->isDomainNotAllowed() ) {
									$this->add_notice_domain_not_allowed();
								} else {
									$this->add_notice_from_response( $register_response->getResponseCode() );
								}
							}
						} catch ( Exception $e ) {
							$this->add_notice_connecton_problem();
						}
					} else {
						$this->add_notice_invalid_email();
					}
				}
				set_transient( 'settings_errors', get_settings_errors(), 30 );
				$url = admin_url( 'admin.php' );
				$url = add_query_arg( 'page', self::REGISTER_PAGE_SLUG, $url );
				$url = add_query_arg( 'settings-updated', 'true', $url );
				$url = add_query_arg( 'user_email', urlencode( $user_email ), $url );
				wp_safe_redirect( $url );
				exit;
			}
		}

		/**
		 * Show register form - handle hook.
		 */
		public function register_form_notice() {
			if ( $this->show_register_form ) {
				if ( ! $this->saas_connection->is_connected() ) {
					$this->display_registration_notice();
				}
			}
		}

		/**
		 * Get registration notice content.
		 *
		 * @return false|string
		 */
		private function get_registration_notice_content() {
			$html_class_is_dismissible = 'is-dismissible';
			if ( $this->is_on_setings_page() ) {
				$html_class_is_dismissible = '';
			}

			ob_start();
			include 'views/html-flexible-shipping-connect-notice.php';
			$notice_content = ob_get_contents();
			ob_end_clean();

			return $notice_content;
		}

		/**
		 * Display registration form.
		 */
		private function display_registration_notice() {

			$notice_content = $this->get_registration_notice_content();

			if ( ! $this->is_on_setings_page() && ! $this->is_on_registration_page() ) {
				new \WPDesk\Notice\PermanentDismissibleNotice(
					$notice_content,
					self::FS_CONNECT_FIRST_NOTICE_NAME,
					\WPDesk\Notice\Notice::NOTICE_TYPE_INFO,
					10,
					array(
						'class' => 'fs-connect__container',
						'id'    => 'fs_connect_notice',
					)
				);
			}
		}

	}

}
