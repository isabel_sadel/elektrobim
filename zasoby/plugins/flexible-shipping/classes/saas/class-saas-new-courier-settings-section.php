<?php

/**
 * Adds planned integrations section in settings.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_New_Courier_Settings_Section
 */
class WPDesk_Flexible_Shipping_SaaS_New_Courier_Settings_Section {

	/**
	 * Platform links.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
	 */
	private $saas_platform_links;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_New_Courier_Settings_Section constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Platform links.
	 */
	public function __construct( WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links ) {
		$this->saas_platform_links = $saas_platform_links;
	}


	/**
	 * Add fields to settings.
	 *
	 * @param array $settings Settings.
	 *
	 * @return array
	 */
	public function add_fields_to_settings( array $settings ) {
		$url        = $this->saas_platform_links->add_utm(
			$this->saas_platform_links->get_planned(),
			'fs-configuration-flexibleshippingtablerate',
			'flexible-shipping',
			'link',
			'user-site',
			'planned-integrations'
		);
		$settings[] = array(
			'type'    => 'long_row',
			'class'   => 'fs-connect-new-courier',
			'content' => sprintf( // Translators: URL.
				__( 'Do not you see your courier? %1$sCheck planned integrations →%2$s', 'flexible-shipping' ),
				'<a target="_blank" href="' . $url . '">',
				'</a>'
			),
		);
		return $settings;
	}

}
