<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Registration_Second_Notice
 */
class WPDesk_Flexible_Shipping_SaaS_Registration_Second_Notice
	implements \WPDesk\PluginBuilder\Plugin\Hookable {

	const SECOND_NOTICE_NAME        = 'fs_connect_start2';
	const SECOND_NOTICE_OPTION_NAME = 'fs_connect_start2_time';
	const NOTICE_START_TIME_OFFSET  = 604800; // One week.
	//const NOTICE_START_TIME_OFFSET = 60; // One minute.

	/**
	 * First notice name.
	 *
	 * @var string
	 */
	private $first_notice_name;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Registration_Second_Notice constructor.
	 *
	 * @param string $first_notice_name First notice name.
	 */
	public function __construct( $first_notice_name ) {
		$this->first_notice_name = $first_notice_name;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'wpdesk_notice_dismissed_notice', array( $this, 'save_notice_start_date' ) );
		add_action( 'admin_init', array( $this, 'maybe_show_notice' ) );
	}

	/**
	 * Save notice start date.
	 * Fires on notice dismiss.
	 *
	 * @param string $notice_name Dismissed notice name.
	 */
	public function save_notice_start_date( $notice_name ) {
		if ( $this->first_notice_name === $notice_name ) {
			$notice_start_time = current_time( 'timestamp' ) + self::NOTICE_START_TIME_OFFSET;
			update_option( self::SECOND_NOTICE_OPTION_NAME, $notice_start_time );
		}
	}

	/**
	 * Get notice content.
	 *
	 * @return string
	 */
	private function get_notice_content() {
		$link = add_query_arg( 'page', WPDesk_Flexible_Shipping_SaaS_User_Registration::REGISTER_PAGE_SLUG, admin_url( 'admin.php' ) );
		ob_start();
		include 'views/html-flexible-shipping-connect-second-notice.php';
		$notice_content = ob_get_contents();
		ob_end_clean();

		return $notice_content;
	}


	/**
	 * Maybe show notice.
	 */
	public function maybe_show_notice() {
		$notice_start_time = intval( get_option( self::SECOND_NOTICE_OPTION_NAME, '0' ) );
		if ( 0 !== $notice_start_time && $notice_start_time <= current_time( 'timestamp' ) ) {
			$notice_content = $this->get_notice_content();
			new \WPDesk\Notice\PermanentDismissibleNotice(
				$notice_content,
				self::SECOND_NOTICE_NAME,
				\WPDesk\Notice\Notice::NOTICE_TYPE_INFO,
				10,
				array(
					'class' => self::SECOND_NOTICE_NAME,
					'id'    => self::SECOND_NOTICE_NAME,
				)
			);
		}
	}

}

