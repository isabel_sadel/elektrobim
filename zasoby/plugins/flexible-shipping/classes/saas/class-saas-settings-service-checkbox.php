<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Settings_Service_Checkbox
 */
class WPDesk_Flexible_Shipping_SaaS_Settings_Service_Checkbox {

	/**
	 * Integration ID.
	 *
	 * @var string
	 */
	private $integration_id;

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * Required plugin version.
	 *
	 * @var null|string
	 */
	private $required_plugin_version;

	/**
	 * Current plugin version.
	 *
	 * @var string
	 */
	private $current_plugin_version;

	/**
	 * Is checkbox checked?
	 *
	 * @var bool
	 */
	private $is_checked;

	/**
	 * Settings URL.
	 *
	 * @var string
	 */
	private $settings_url;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Settings_Service_Checkbox constructor.
	 *
	 * @param string                                         $integration_id Integration ID.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 * @param string|null                                    $required_plugin_version Required plugin version.
	 * @param string                                         $current_plugin_version Current plugin version.
	 * @param bool                                           $is_checked Checkbox is checked.
	 * @param string                                         $settings_url Settings URL.
	 */
	public function __construct( $integration_id, $shipping_service, $required_plugin_version, $current_plugin_version, $is_checked, $settings_url ) {
		$this->integration_id          = $integration_id;
		$this->shipping_service        = $shipping_service;
		$this->required_plugin_version = $required_plugin_version;
		$this->current_plugin_version  = $current_plugin_version;
		$this->is_checked              = $is_checked;
		$this->settings_url            = $settings_url;
	}

	/**
	 * Add description to checkbox.
	 *
	 * @param array $checkbox_field Checkbox field.
	 *
	 * @return array
	 */
	private function add_description_to_checkbox_for_required_plugin_version( array $checkbox_field ) {
		if ( empty( $this->required_plugin_version ) ) {
			$checkbox_field['description'] = __( 'Invalid requireWordpressPluginVersion capability!', 'flexible-shipping' );
			$checkbox_field['disabled']    = true;
		} elseif ( version_compare( $this->required_plugin_version, $this->current_plugin_version ) > 0 ) {
			$checkbox_field['description'] = sprintf(
				// Translators: plugin version.
				__( 'This integration requires Flexible Shipping Plugin in version %1$s or newer!', 'flexible-shipping' ),
				$this->required_plugin_version
			);
			$checkbox_field['disabled'] = true;
		} else {
			if ( $this->is_checked ) {
				$settings_url = $this->settings_url;
				$link_id      = 'saas-settings-link-' . $this->shipping_service->get_id();
				// Translators: URL.
				$link_text = sprintf( __( 'Go to %s settings</a>', 'flexible-shipping' ), $this->shipping_service->get_name() );

				$checkbox_field['description'] = '<a id="' . esc_attr( $link_id ) . '" href="' . $settings_url . '">' . $link_text . '</a>';
			}
		}
		return $checkbox_field;
	}

	/**
	 * Setup promoted service.
	 *
	 * @param array $checkbox_field
	 *
	 * @return array
	 */
	private function setup_promoted( array $checkbox_field ) {
		$checkbox_field['title'] = sprintf(
			// Translators: span and service name.
			__( '%1$s%2$s (new)%3$s', 'flexible-shipping' ),
			'<span class="fs-new-service">',
			$this->shipping_service->get_name(),
			'</span>'
		);
		return $checkbox_field;
	}

	/**
	 * Prepare checkbox.
	 *
	 * @return array
	 */
	public function prepare_checkbox() {
		$checkbox_field = [
			'type'              => 'checkbox',
			'title'             => $this->shipping_service->get_name(),
			'label'             => __( 'Enable', 'flexible-shipping' ) . ' ' . $this->shipping_service->get_name(),
			'class'             => 'flexible-shipping-saas-shipping-provider',
			'custom_attributes' => array(
				'data-shipping_provider_id'   => esc_attr( $this->shipping_service->get_id() ),
				'data-shipping_provider_name' => esc_attr( $this->shipping_service->get_name() ),
			),
		];

		if ( $this->shipping_service->get_promoted() ) {
			$checkbox_field = $this->setup_promoted( $checkbox_field );
		}

		$checkbox_field = $this->add_description_to_checkbox_for_required_plugin_version( $checkbox_field );

		return $checkbox_field;

	}

}
