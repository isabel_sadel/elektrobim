<?php

/**
 * Handles conditional logic in checkout.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Checkout
 */
class WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Checkout extends WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When {

	/**
	 * Cart shipping rate.
	 *
	 * @var array
	 */
	private $cart_shipping_rate;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Field_Visible_When constructor.
	 *
	 * @param array            $visible_when_conditions Conditions.
	 * @param WC_Shipping_Rate $cart_shipping_rate Cart shipping rate.
	 */
	public function __construct( array $visible_when_conditions, $cart_shipping_rate ) {
		parent::__construct( $visible_when_conditions );
		$this->cart_shipping_rate = $cart_shipping_rate;
	}

	/**
	 * Create for field.
	 *
	 * @param array            $field Field.
	 * @param WC_Shipping_Rate $cart_shipping_rate Cart shipping rate.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Checkout
	 */
	public static function create_for_field( array $field, WC_Shipping_Rate $cart_shipping_rate ) {
		$visible_when_conditions = array();
		if ( isset( $field['visible-when'] ) ) {
			$visible_when_conditions = $field['visible-when'];
		}
		return new self( $visible_when_conditions, $cart_shipping_rate );
	}

	/**
	 * Get service type from shipping rate.
	 *
	 * @return string
	 */
	private function get_service_type_from_cart_shipping_rate() {
		$meta_data    = $this->cart_shipping_rate->get_meta_data();
		$fs_saas_data = array();
		if ( isset( $meta_data['_fs_saas_data'] ) ) {
			$fs_saas_data = $meta_data['_fs_saas_data'];
		}
		$service_type = '';
		if ( isset( $fs_saas_data['_fs_service_type'] ) ) {
			$service_type = $fs_saas_data['_fs_service_type'];
		}
		return $service_type;
	}

	/**
	 * Get service type.
	 *
	 * @return string
	 */
	protected function get_service_type() {
		return $this->get_service_type_from_cart_shipping_rate();
	}

}
