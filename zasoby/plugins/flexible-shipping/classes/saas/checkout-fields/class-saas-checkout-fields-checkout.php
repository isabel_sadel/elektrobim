<?php

/**
 * Displays additional shipping checkout fields on checkout.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Checkout
 */
class WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Checkout implements \WPDesk\PluginBuilder\Plugin\Hookable {

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * Service collection points.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points
	 */
	private $service_collection_points;

	/**
	 * Renderer.
	 *
	 * @var WPDesk\View\Renderer\Renderer;
	 */
	private $renderer;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Checkout constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service                   $shipping_service Shipping service.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points Service collection points.
	 * @param \WPDesk\View\Renderer\Renderer                                   $renderer Renderer.
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service,
		WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Collection_Points $service_collection_points,
		WPDesk\View\Renderer\Renderer $renderer
	) {
		$this->shipping_service          = $shipping_service;
		$this->service_collection_points = $service_collection_points;
		$this->renderer                  = $renderer;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'woocommerce_review_order_after_shipping', array( $this, 'maybe_display_fields_on_checkout_page' ) );
	}

	/**
	 * Get Flexible shipping method from rate meta.
	 *
	 * @param array $shipping_rate_meta Shipping rate meta.
	 * @return array
	 */
	private function get_flexible_shipping_method_from_rate_meta( array $shipping_rate_meta ) {
		$fs_method = array();
		if ( isset( $shipping_rate_meta['_fs_method'] ) ) {
			$fs_method = $shipping_rate_meta['_fs_method'];
		}
		return $fs_method;
	}

	/**
	 * Get method integration from flexible shipping method.
	 *
	 * @param array $fs_method Flexible shipping method.
	 * @return string
	 */
	private function get_method_integration_from_fs_method( array $fs_method ) {
		$method_integration = '';
		if ( isset( $fs_method['method_integration'] ) ) {
			$method_integration = $fs_method['method_integration'];
		}
		return $method_integration;
	}

	/**
	 * Display access point.
	 *
	 * @param WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data Request data.
	 * @param array                                               $field Field.
	 */
	private function display_access_point( WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data, $field ) {
		$access_point = new WPDesk_Flexible_Shipping_SaaS_Checkout_Field_Collection_Points(
			$this->shipping_service,
			$this->service_collection_points,
			$this->renderer,
			$request_data,
			$field
		);
		$access_point->display_collection_point_field_on_checkout();
	}

	/**
	 * Maybe display parcel collection points field for shipping method.
	 *
	 * @param array                                               $field Field.
	 * @param WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data Request data.
	 * @param WC_Shipping_Rate                                    $cart_shipping_rate Shipping method.
	 */
	public function maybe_display_parcel_collection_points_field_for_shipping_method(
		array $field,
		WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data,
		WC_Shipping_Rate $cart_shipping_rate
	) {
		$shipping_rate_meta = $cart_shipping_rate->get_meta_data();
		$fs_method          = $this->get_flexible_shipping_method_from_rate_meta( $shipping_rate_meta );
		if ( $this->shipping_service->get_integration_id() === $this->get_method_integration_from_fs_method( $fs_method ) ) {
			$visible_when = WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Checkout::create_for_field( $field, $cart_shipping_rate );
			if ( $visible_when->is_visible() ) {
				$this->display_access_point( $request_data, $field );
			}
		}
	}

	/**
	 * Maybe display parcel collection points field.
	 *
	 * @param array                                               $field Field.
	 * @param WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data Request data.
	 */
	private function maybe_display_parcel_collection_points_field( array $field, WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data ) {
		$cart_shipping_methods = WC()->cart->calculate_shipping();
		foreach ( $cart_shipping_methods as $cart_shipping_rate ) {
			$this->maybe_display_parcel_collection_points_field_for_shipping_method( $field, $request_data, $cart_shipping_rate );
		}
	}

	/**
	 * Maybe display fields from fieldset.
	 *
	 * @param array                                               $fieldset Fieldset.
	 * @param WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data Request data.
	 */
	private function maybe_display_fields_from_fieldset( array $fieldset, WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data $request_data ) {
		if ( in_array( 'checkout', $fieldset['show-in'], true ) ) {
			foreach ( $fieldset['fields'] as $field ) {
				if ( 'parcel_collection_points' === $field['type'] ) {
					$this->maybe_display_parcel_collection_points_field( $field, $request_data );
				}
			}
		}
	}

	/**
	 * Maybe display fields on checkout page.
	 */
	public function maybe_display_fields_on_checkout_page() {
		$request_data = new WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data( $_REQUEST );
		try {
			$request_fields = $this->shipping_service->get_fields_for_targets( [ $request_data->get_shipping_country() ] );
			foreach ( $request_fields as $fieldset ) {
				$this->maybe_display_fields_from_fieldset( $fieldset, $request_data );
			}
		} catch ( WPDesk_Flexible_Shipping_SaaS_Service_Settings_Not_Found $e ) {
			null;
		}
	}

}
