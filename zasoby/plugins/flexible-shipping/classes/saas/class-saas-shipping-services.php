<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'WPDesk_Flexible_Shipping_SaaS_Shipping_Services' ) ) {

	/**
	 * Class WPDesk_Flexible_Shipping_SaaS_Shipping_Services
	 */
	class WPDesk_Flexible_Shipping_SaaS_Shipping_Services {

		const SHIPPING_SERVICE_FIELD_PREFIX = 'shipping_service_';

		/**
		 * Saas connection.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Connection
		 */
		private $saas_connection;

		/**
		 * Logger.
		 *
		 * @var \Psr\Log\LoggerInterface
		 */
		private $logger;

		/**
		 * Links.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
		 */
		private $saas_platform_links;

		/**
		 * WPDesk_Flexible_Shipping_SaaS_Shipping_Services constructor.
		 *
		 * @param WPDesk_Flexible_Shipping_SaaS_Connection     $saas_connection Saas connection.
		 * @param \Psr\Log\LoggerInterface                     $logger Logger.
		 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Links.
		 */
		public function __construct(
			$saas_connection,
			\Psr\Log\LoggerInterface $logger,
			WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links
		) {
			$this->saas_connection     = $saas_connection;
			$this->logger              = $logger;
			$this->saas_platform_links = $saas_platform_links;
		}

		/**
		 * Get platform.
		 *
		 * @return \WPDesk\SaasPlatformClient\Platform
		 */
		private function get_platform() {
			return $this->saas_connection->get_platform();
		}

		/**
		 * Add shipping service checkbox to settings.
		 *
		 * @param array                                            $settings Settings.
		 * @param \WPDesk\SaasPlatformClient\Model\ShippingService $shipping_service Shipping service.
		 *
		 * @return array
		 */
		private function add_shipping_service_checkbox_to_settings(
			array $settings,
			\WPDesk\SaasPlatformClient\Model\ShippingService $shipping_service
		) {
			$settings[ self::SHIPPING_SERVICE_FIELD_PREFIX . $shipping_service->getId() ] = [
				'class'             => 'flexible-shipping-saas-shipping-service',
				'title'             => $shipping_service->getName(),
				'description'       => $shipping_service->getDescription(),
				'type'              => 'checkbox',
				'custom_attributes' => array(
					'data-shipping_service_id'   => esc_attr( $shipping_service->getId() ),
					'data-shipping_service_name' => esc_attr( $shipping_service->getName() ),
				),
			];

			return $settings;
		}

		/**
		 * Add shipping services checkboxes to settings.
		 *
		 * @param array $settings Settings.
		 *
		 * @return mixed
		 */
		public function add_shipping_services_checkboxes_to_settings( array $settings ) {
			if ( $this->saas_connection->is_connected() ) {
				try {
					$platform          = $this->get_platform();
					$shipping_services = $platform->requestListShippingServices();
					$settings[]        = array(
						'title' => __( 'Services', 'flexible-shipping' ),
						'type'  => 'title',
					);
					foreach ( $shipping_services->getPage() as $shipping_service ) {
						$settings = $this->add_shipping_service_checkbox_to_settings( $settings, $shipping_service );
					}
				} catch ( Exception $e ) {
					$this->logger->debug( "add_shipping_services_checkboxes_to_settings method exception: {$e->getMessage()} code: {$e->getCode()}",
						[
							'exception' => $e
						] );
				}
			}

			return $settings;
		}

		/**
		 * Get shipping services from platform.
		 *
		 * @return array
		 * @throws Exception Exception.
		 */
		public function get_shipping_services_from_platform() {
			$shipping_services_from_platform = array();
			$platform                        = $this->get_platform();
			try {
				$shipping_services = $platform->requestListShippingServices();
				if ( $shipping_services->isBadCredentials() ) {
					throw new WPDesk_Flexible_Shipping_SaaS_Bad_Credentials_Exception( $this->saas_platform_links );
				}
				/**
				 * IDE type hint.
				 *
				 * @var \WPDesk\SaasPlatformClient\Model\ShippingService $shipping_service
				 */
				foreach ( $shipping_services->getPage() as $shipping_service ) {
					$shipping_services_from_platform[] = $shipping_service;
				}
			} catch ( Exception $e ) {
				$this->logger->debug( "get_shipping_services_from_platform method exception: {$e->getMessage()} code: {$e->getCode()}",
					[
						'exception' => $e,
					]
				);
				if ( $e instanceof WPDesk_Flexible_Shipping_SaaS_Bad_Credentials_Exception ) {
					throw $e;
				}
			}

			return $shipping_services_from_platform;
		}

	}

}
