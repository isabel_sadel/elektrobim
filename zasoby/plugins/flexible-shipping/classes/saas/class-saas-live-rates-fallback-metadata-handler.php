<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Live_Rates_Fallback_Handler
 */
class WPDesk_Flexible_Shipping_SaaS_Live_Rates_Fallback_Metadata_Handler implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;

	const MATA_FS_FALLBACK  = '_fs_fallback';
	const META_FS_RATE_TYPE = '_fs_rate_type';

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'woocommerce_order_shipping_method', [ $this, 'add_fallback_to_shipping_method_name' ], 10, 2 );
		add_filter( 'woocommerce_hidden_order_itemmeta', [ $this, 'hide_fallback_meta' ] );
		add_action( 'woocommerce_before_order_itemmeta', [ $this, 'display_fallback_message_in_order_shipping' ], 10, 3 );
	}

	/**
	 * Add fallback to shipping method name.
	 *
	 * @param string            $names Names.
	 * @param WC_Abstract_Order $order Order.
	 *
	 * @return string
	 */
	public function add_fallback_to_shipping_method_name( $names, $order ) {
		$names = array();
		foreach ( $order->get_shipping_methods() as $shipping_method ) {
			$name     = $shipping_method->get_name();
			$fallback = $shipping_method->get_meta( self::MATA_FS_FALLBACK );
			if ( '' !== $fallback ) {
				// Translators: shipping method name.
				$name = sprintf( __( '%1$s (fallback)', 'flexible-shipping' ), $name );
			}
			$names[] = $name;
		}
		return implode( ', ', $names );
	}

	/**
	 * Hide fallback meta.
	 *
	 * @param array $items Items to hide.
	 *
	 * @return array
	 */
	public function hide_fallback_meta( array $items ) {
		$items[] = WPDesk_Flexible_Shipping::META_DEFAULT;
		$items[] = self::MATA_FS_FALLBACK;
		$items[] = self::META_FS_RATE_TYPE;
		return $items;
	}

	/**
	 * Display fallback message in order shipping.
	 *
	 * @param string          $item_id Item Id.
	 * @param WC_Order_Item   $item Item.
	 * @param WC_Product|null $product Produuct.
	 */
	public function display_fallback_message_in_order_shipping( $item_id, $item, $product ) {
		if ( $item->is_type( 'shipping' ) ) {
			$fallback = $item->get_meta( self::MATA_FS_FALLBACK );
			if ( '' !== $fallback ) {
				include 'views/html-order-fallback-message.php';
			}
		}
	}

}
