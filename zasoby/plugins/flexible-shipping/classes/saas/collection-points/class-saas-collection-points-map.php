<?php

/**
 * Loads map with collection points.
 *
 * WPDesk_Flexible_Shipping_SaaS_Collection_Points_Map_Loader
 */
class WPDesk_Flexible_Shipping_SaaS_Collection_Points_Map_Loader implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

	use \WPDesk\PluginBuilder\Plugin\PluginAccess;

	const URL_MAP_PARAMETER = 'flexible-shipping-collection-point-map';
	const SCRIPT_HANDLE     = 'fs_collection_points_map';

	/**
	 * Scripts version.
	 *
	 * @var string
	 */
	private $scripts_version = FLEXIBLE_SHIPPING_VERSION . '.5';

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'init', array( $this, 'maybe_load_map' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
	}

	/**
	 * Admin scripts.
	 */
	public function enqueue_scripts() {
		if ( is_checkout() ) {
			$suffix = defined( 'SCRIPT_DEBUG' ) && SCRIPT_DEBUG ? '' : '.min';
			wp_register_script( self::SCRIPT_HANDLE,
				trailingslashit( $this->plugin->get_plugin_assets_url() ) . 'js/collection-points-map' . $suffix . '.js', array( 'jquery' ),
				$this->scripts_version, true
			);
			
			wp_localize_script( self::SCRIPT_HANDLE, self::SCRIPT_HANDLE, array(
				'ajax_url'                  => admin_url( 'admin-ajax.php' ),
				'collection_points_map_url' => site_url( '?' . self::URL_MAP_PARAMETER ),
				'loading_image'             => $this->plugin->get_plugin_url() .'assets/images/ajax-loading.gif',
				'marker_icon'               => $this->plugin->get_plugin_url() .'assets/images/marker-icon.png',
				'lang'                      => array(
					'no_points'     => __( 'Points can not be found. Check the shipping address.', 'flexible-shipping'),
					'user_location' => __( 'It\'s your location', 'flexible-shipping'),
				),
				'ajax_nonce'                => wp_create_nonce( WPDesk_Flexible_Shipping_SaaS_Collection_Points_Ajax::NONCE_ACTION )
			) );
			wp_enqueue_script( self::SCRIPT_HANDLE );
		}
	}

	/**
	 * Maybe load map.
	 */
	public function maybe_load_map() {
		if ( isset( $_GET[ self::URL_MAP_PARAMETER ] ) ) {
			include 'views/map.php';
			die();
		}
	}

}
