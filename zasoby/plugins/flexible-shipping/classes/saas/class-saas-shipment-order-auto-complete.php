<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Shipment_Order_Auto_Complete
 */
class WPDesk_Flexible_Shipping_SaaS_Shipment_Order_Auto_Complete {

	/**
	 * Integration id.
	 *
	 * @var string
	 */
	private $integration_id;

	/**
	 * Shipping service.
	 *
	 * @var \WPDesk\SaasPlatformClient\Model\ShippingService
	 */
	private $shipping_service;

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Shipment_Settings_Handler constructor.
	 *
	 * @param string                                         $integration_id Integration id.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 * @param \Psr\Log\LoggerInterface                       $logger Logger.
	 */
	public function __construct( $integration_id, $shipping_service, $logger ) {
		$this->integration_id   = $integration_id;
		$this->shipping_service = $shipping_service;
		$this->logger           = $logger;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_action( 'flexible_shipping_shipment_confirmed', array( $this, 'handle_complete_order' ) );
	}

	/**
	 * Handle complete order.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment $shipment Shipment.
	 */
	public function handle_complete_order( WPDesk_Flexible_Shipping_Shipment $shipment ) {
		if ( $this->integration_id === $shipment->get_meta( '_integration', '' ) ) {
			if ( $this->shipping_service->is_shipment_settings_complete_order() ) {
				$order = $shipment->get_order();
				if ( ! $order->has_status( 'completed' ) ) {
					$order->update_status( 'completed', __( 'Order status changed automatically by Flexible Shipping Plugin.', 'flexible-shipping' ) );
				}
			}
		}
	}


}
