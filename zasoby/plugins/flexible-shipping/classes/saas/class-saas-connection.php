<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'WPDesk_Flexible_Shipping_SaaS_Settings_Connection' ) ) {

	/**
	 * Class WPDesk_Flexible_Shipping_SaaS_Settings_Connection
	 */
	class WPDesk_Flexible_Shipping_SaaS_Connection
		implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

		use \WPDesk\PluginBuilder\Plugin\PluginAccess;

		const PRIORITY_BEFORE_DEFAULT = 9;
		const PRIORITY_AFTER_DEFAULT  = 11;

		const PAGE_WOOCOMMERCE_SETTINGS         = 'wc-settings';
		const TAB_SHIPPING                      = 'shipping';
		const SECTION_FLEXIBLE_SHIPPING_CONNECT = 'flexible_shipping_connect';

		const OPTION_SAAS_API_KEY                     = 'fs_saas_api_key';
		const OPTION_FLEXIBLE_SHIPPING_SAAS_CONNECTED = 'fs_saas_connected';

		const CONNECTED    = '1';
		const DISCONNECTED = '0';

		const NONCE_CONNECTION_ACTION = 'fs_connection_nonce';
		const NONCE_CONNECTION_NAME   = '_nonce_fs_connection';

		const SUBMIT_CONNECTION_PARAMETER = 'flexible-shipping-submit-connection';

		const PARAMETER_CONNECT_URL       = 'flexible_shipping_connect_url';
		const PARAMETER_CONNECT_FROM_MAIL = 'connect_from_mail';

		const SETTINGS_ERROR_SETTING = 'flexible-shipping-connect';

		const BUTTON_ID_DISCONNECT = 'flexible-shipping-disconnect';
		const BUTTON_ID_CONNECT    = 'flexible-shipping-connect';

		const LOGGER_SOURCE = 'saas-connection';

		/**
		 * Is processing email link?
		 *
		 * @var bool
		 */
		private $is_processing_email_link = false;

		/**
		 * Saas shipping providers.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Services
		 */
		private $saas_shipping_services;

		/**
		 * Links.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
		 */
		private $saas_platform_links;

		/**
		 * Logger.
		 *
		 * @var \Psr\Log\LoggerInterface
		 */
		private $logger;

		/**
		 * WPDesk_Flexible_Shipping_SaaS_Connection constructor.
		 *
		 * @param \Psr\Log\LoggerInterface                     $logger Logger.
		 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Links.
		 */
		public function __construct(
			\Psr\Log\LoggerInterface $logger = null,
			WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links = null
		) {

			if ( null === $saas_platform_links ) {
				$saas_platform_links = new WPDesk_Flexible_Shipping_SaaS_Platform_Links();
			}
			$this->saas_platform_links = $saas_platform_links;

			if ( null !== $logger ) {
				$this->logger = $logger;
			} else {
				$this->logger = WPDesk_Flexible_Shipping_Logger_Factory::create_logger();
			}
			$this->saas_shipping_services = new WPDesk_Flexible_Shipping_SaaS_Shipping_Services(
				$this,
				$this->logger,
				$saas_platform_links
			);

		}

		/**
		 * Hooks.
		 */
		public function hooks() {
			add_filter( 'removable_query_args', [ $this, 'add_email_parameters_to_removable_query_args' ] );

			add_action( 'update_option_' . self::OPTION_SAAS_API_KEY, [ $this, 'connect_on_api_key_change' ], 10, 3 );

			add_action( 'admin_init', [ $this, 'connect_admin_notices' ], self::PRIORITY_BEFORE_DEFAULT );
			add_action( 'admin_init', [ $this, 'handle_connection_form' ], self::PRIORITY_BEFORE_DEFAULT );
			add_action( 'admin_init', [ $this, 'handle_saas_connect_url' ] );
			add_action( 'admin_init', [ $this, 'check_connection_and_show_notice_on_error' ] );
		}

		/**
		 * Clear SaaS client cache.
		 */
		public function clear_saas_client_cache() {
			$client_cache = new \WPDesk\SaasPlatformClient\Cache\WordpressCache();
			$client_cache->clear();
		}

		/**
		 * Is on settings page?
		 *
		 * @return bool
		 */
		public function is_on_setings_page() {
			return (
				isset( $_GET['page'] )
				&& self::PAGE_WOOCOMMERCE_SETTINGS === $_GET['page']
				&& isset( $_GET['tab'] )
				&& self::TAB_SHIPPING === $_GET['tab']
			);
		}

		/**
		 * Check connection and show notice on error.
		 */
		public function check_connection_and_show_notice_on_error() {
			if ( $this->is_on_setings_page() && $this->is_connected() ) {
				try {
					$platform = $this->get_platform();
					$response = $platform->requestListShippingServices();
					if ( $response->isBadCredentials() ) {
						$this->add_notice_bad_credentials();
					} elseif ( $response->isMaintenance() ) {
						$maintenance_context = new \WPDesk\SaasPlatformClient\Response\Maintenance\MaintenanceResponseContext( $response );
						$this->add_notice_maintenance(
							$maintenance_context->getMaintenanceMessage(),
							$maintenance_context->getMaintenanceTill()
						);
					} elseif ( $response->isServerFatalError() ) {
						$this->add_notice_fatal_error();
					}
				} catch ( Exception $e ) {
					$this->add_notice_fatal_error();
				}
			}
		}

		/**
		 * Add notice for fatal error.
		 *
		 * @param string $platform_message Message from platform.
		 * @param int    $till Time till maintenance is active.
		 */
		public function add_notice_maintenance( $platform_message, $till ) {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = $this->prepare_maintenance_notice( $platform_message, $till );
			new \WPDesk\Notice\Notice( $notice_content, $notice_type );
		}

		/**
		 * Add notice for fatal error.
		 */
		public function add_notice_fatal_error() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = $this->prepare_fatal_error_notice();
			new \WPDesk\Notice\Notice( $notice_content, $notice_type );
		}

		/**
		 * Add notice for bad credentials.
		 */
		public function add_notice_bad_credentials() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = $this->prepare_bad_credentials_notice();
			new \WPDesk\Notice\Notice( $notice_content, $notice_type );
		}

		/**
		 * Removable query args.
		 *
		 * @param array $removable_query_args Query args.
		 *
		 * @return array
		 */
		public function add_email_parameters_to_removable_query_args( $removable_query_args ) {
			$removable_query_args[] = self::PARAMETER_CONNECT_FROM_MAIL;
			$removable_query_args[] = 'connect_key';

			return $removable_query_args;
		}


		/**
		 * Show settings messages.
		 */
		public function connect_admin_notices() {
			$settings_errors = get_settings_errors( self::SETTINGS_ERROR_SETTING );
			foreach ( $settings_errors as $error ) {
				new \WPDesk\Notice\Notice( $error['message'], $error['type'] );
			}
		}

		/**
		 * Get platform.
		 *
		 * @return \WPDesk\SaasPlatformClient\Platform
		 */
		public function get_platform() {
			$platform_factory = new WPDesk_Flexible_Shipping_SaaS_Auth_Platform_Factory( $this->logger );

			return $platform_factory->get_platform();
		}

		/**
		 * Login user.
		 *
		 * @param string                                    $connect_key Key.
		 * @param WPDesk_Flexible_Shipping_SaaS_Auth_Params $params Parameters.
		 *
		 * @return \WPDesk\SaasPlatformClient\Response\Authentication\TokenResponse
		 */
		private function login_user( $connect_key, WPDesk_Flexible_Shipping_SaaS_Auth_Params $params ) {
			return $this->get_platform()->requestToken(
				$connect_key,
				$params->get_user_domain(),
				$params->get_user_locale()
			);
		}

		/**
		 * Add settings error: user authenticated successfully notice.
		 */
		private function add_settings_error_login_success() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_SUCCESS;
			$notice_content = __( 'Your store has been connected to Flexible Shipping Connect. Choose from the available shipping integrations below.',
				'flexible-shipping' );
			add_settings_error( self::SETTINGS_ERROR_SETTING, 'success', $notice_content, $notice_type );
		}

		/**
		 * Add settings error: disconnected notice.
		 */
		private function add_settings_error_disconnected() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_SUCCESS;
			$notice_content = __(	'You are now disconnected from Flexible Shipping Connect. All shipping services have been disabled. To use Flexible Shipping Connect again, please enter a valid Connect Key.',
				'flexible-shipping' );
			add_settings_error( self::SETTINGS_ERROR_SETTING, 'success', $notice_content, $notice_type );
		}

		/**
		 * Add notice already connected. Used for email link.
		 */
		private function add_notice_already_connected() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_INFO;
			$notice_content = __( 'Your store is already connected to Flexible Shipping Connect!', 'flexible-shipping' );
			add_settings_error( self::SETTINGS_ERROR_SETTING, 'success', $notice_content, $notice_type );
		}

		/**
		 * Add settings error: domain occupied.
		 */
		private function add_settings_error_domain_occupied() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = sprintf(
				/* Translators: url */
				__( 'Your store (%1$s) is already registered on another account. You can register your domain only on one account. If you have questions, %2$scontact us%3$s.',
					'flexible-shipping'
				),
				site_url(),
				'<a target="_blank" href="' . $this->saas_platform_links->add_utm(
					$this->saas_platform_links->get_support(),
					'fs-register-contactus',
					'flexible-shipping',
					'link',
					'user-site',
					'contact-us'
				) . '">',
				'</a>'
			);
			add_settings_error( self::SETTINGS_ERROR_SETTING, 'success', $notice_content, $notice_type );
		}

		/**
		 * Add settings error: bad credentials.
		 */
		private function add_settings_error_bad_credentials() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = $this->prepare_bad_credentials_notice();
			add_settings_error( self::SETTINGS_ERROR_SETTING, 'success', $notice_content, $notice_type );
		}

		/**
		 * Add settings error: fatal error.
		 */
		private function add_settings_error_fatal_error() {
			$notice_type    = WPDesk\Notice\Notice::NOTICE_TYPE_ERROR;
			$notice_content = $this->prepare_fatal_error_notice();
			add_settings_error( self::SETTINGS_ERROR_SETTING, 'success', $notice_content, $notice_type );
		}

		/**
		 * Prepare fatal error notice text.
		 *
		 * @param string $platform_message Message from platform.
		 * @param int    $till Time till maintenance is active.
		 * @return string
		 */
		public function prepare_maintenance_notice( $platform_message, $till ) {
			if ( isset( $platform_message ) ) {
				$platform_message = sprintf(
					'%1$s%2$s',
					'<br/><br/>',
					sprintf(
						// Translators: platform message.
						__( 'Message from platform: %1$s%2$s%3$s', 'flexible-shipping' ),
						'<strong>',
						$platform_message,
						'</strong>'
					)
				);
			} else {
				$platform_message = '';
			}
			$notice_content = sprintf(
				/* Translators: url */
				__(
					'FS Connect is undergoing maintenance. To get more information about the platform status check %1$sstatus.flexibleshipping.com &rarr;%2$s%3$s',
					'flexible-shipping'
				),
				'<a href="' . $this->saas_platform_links->add_utm(
					$this->saas_platform_links->get_status(),
					'fs-notice-status',
					'flexible-shipping',
					'link',
					'user-site',
					''
				) . '">',
				'</a>',
				$platform_message
			);
			return $notice_content;
		}

		/**
		 * Prepare fatal error notice text.
		 *
		 * @return string
		 */
		public function prepare_fatal_error_notice() {
			$notice_content  = __(
				'There are some connection problems. Please try again in a few minutes. Thanks!',
				'flexible-shipping'
			);
			$notice_content .= '<br />';
			$notice_content .= sprintf(
				/* Translators: url */
				__( 'To get more information about the platform status check %1$sstatus.flexibleshipping.com &rarr;%2$s',
					'flexible-shipping'
				),
				'<a href="' . $this->saas_platform_links->add_utm(
					$this->saas_platform_links->get_status(),
					'fs-notice-status',
					'flexible-shipping',
					'link',
					'user-site',
					''
				) . '">',
				'</a>'
			);

			return $notice_content;
		}

		/**
		 * Prepare fatal error notice text.
		 *
		 * @return string
		 */
		public function prepare_bad_credentials_notice() {
			$notice_content = sprintf(
				/* Translators: url */
				__( 'The Connect Key that you entered is invalid. Please enter a valid key. %1$sRegister%3$s for a new key or log in to %2$syour account%3$s to get your key.', 'flexible-shipping' ),
				'<a href="' . add_query_arg( 'page', WPDesk_Flexible_Shipping_SaaS_User_Registration::REGISTER_PAGE_SLUG, admin_url( 'admin.php' ) ) . '">',
				'<a href="' . $this->saas_platform_links->add_utm(
					$this->saas_platform_links->get_my_account(),
					'fs-configuration-contactus',
					'flexible-shipping',
					'link',
					'user-site',
					'contact-us'
				) . '">',
				'</a>'
			);

			return $notice_content;
		}

		/**
		 * Is on submit page?
		 *
		 * @return bool
		 */
		public function is_on_submit_page() {
			return ( isset( $_REQUEST[ self::SUBMIT_CONNECTION_PARAMETER ] ) && '1' === $_REQUEST[ self::SUBMIT_CONNECTION_PARAMETER ] );
		}

		/**
		 * Handle connection form.
		 */
		public function handle_connection_form() {
			if ( $this->is_on_submit_page() ) {
				if ( isset( $_REQUEST[ self::NONCE_CONNECTION_NAME ] )
				     && wp_verify_nonce( $_REQUEST[ self::NONCE_CONNECTION_NAME ], self::NONCE_CONNECTION_ACTION )
				     && isset( $_REQUEST['api_key'] )
				) {
					$api_key = sanitize_text_field( $_REQUEST['api_key'] );
					if ( $this->is_connected() ) {
						$api_key = '';
					} else {
						remove_action( 'update_option_' . self::OPTION_SAAS_API_KEY, [
							$this,
							'connect_on_api_key_change',
						], 10, 3 );
						update_option( self::OPTION_SAAS_API_KEY, '' );
						add_action( 'update_option_' . self::OPTION_SAAS_API_KEY, [
							$this,
							'connect_on_api_key_change',
						], 10, 3 );
					}
					update_option( self::OPTION_SAAS_API_KEY, $api_key );
				}
				set_transient( 'settings_errors', get_settings_errors(), 30 );
				$url = admin_url( 'admin.php' );
				$url = add_query_arg( 'page', 'wc-settings', $url );
				$url = add_query_arg( 'tab', 'shipping', $url );
				$url = add_query_arg( 'section', WPDesk_Flexible_Shipping_SaaS_Settings::METHOD_ID, $url );
				$url = add_query_arg( 'settings-updated', 'true', $url );
				wp_safe_redirect( $url );
				exit;
			}
		}

		/**
		 * Update option - do connection when api key changed.
		 *
		 * @param string $old_value Old option value.
		 * @param string $value Current option value.
		 * @param string $option Option name.
		 */
		public function connect_on_api_key_change( $old_value, $value, $option ) {
			$old_key_value = $old_value;
			$new_key_value = $value;
			$connected     = $this->is_connected();
			if ( ( ! $connected || ( $old_key_value !== $new_key_value ) ) && '' !== $new_key_value ) {
				update_option( self::OPTION_FLEXIBLE_SHIPPING_SAAS_CONNECTED, self::DISCONNECTED );
				try {
					$response = $this->login_user(
						$new_key_value,
						new WPDesk_Flexible_Shipping_SaaS_Auth_Params()
					);
					if ( $response->isLoginSuccess() ) {
						update_option( self::OPTION_FLEXIBLE_SHIPPING_SAAS_CONNECTED, self::CONNECTED );
						$this->add_settings_error_login_success();
					} else {
						if ( $response->isBadCredentials() ) {
							$this->add_settings_error_bad_credentials();
						} elseif ( $response->isDomainOccupied() ) {
							$this->add_settings_error_domain_occupied();
						} elseif ( $response->isBadRequest() ) {
							$this->add_settings_error_fatal_error();
						} elseif ( $response->isServerFatalError() ) {
							$this->add_settings_error_fatal_error();
						} else {
							$this->add_settings_error_fatal_error();
						}
					}
				} catch ( Exception $e ) {
					$this->add_settings_error_fatal_error();
				}
			} elseif ( '' === $new_key_value ) {
				update_option( self::OPTION_FLEXIBLE_SHIPPING_SAAS_CONNECTED, self::DISCONNECTED );
				if ( $connected ) {
					$this->add_settings_error_disconnected();
				}
			}
			set_transient( 'settings_errors', get_settings_errors(), 30 );
		}

		/**
		 * Get connected.
		 *
		 * @return string
		 */
		private function get_connected() {
			return get_option( self::OPTION_FLEXIBLE_SHIPPING_SAAS_CONNECTED, '0' );
		}

		/**
		 * Is connected.
		 *
		 * @return bool
		 */
		public function is_connected() {
			return ( $this->get_connected() === self::CONNECTED );
		}

		/**
		 * Handle SaaS connect URL.
		 */
		public function handle_saas_connect_url() {
			if ( ! empty( $_GET[ self::PARAMETER_CONNECT_URL ] ) ) {
				if ( current_user_can( 'manage_woocommerce' ) ) {
					$this->is_processing_email_link = true;
					if ( $this->is_connected() ) {
						$this->add_notice_already_connected();
					} else {
						$api_key = sanitize_text_field( $_GET['connect_key'] );
						/**
						 * Clear option value to trigger connection action in connect_on_api_key_change.
						 */
						update_option( self::OPTION_SAAS_API_KEY, '' );
						update_option( self::OPTION_SAAS_API_KEY, $api_key );
					}
				}
				set_transient( 'settings_errors', get_settings_errors(), 30 );
				$url = admin_url( 'admin.php' );
				$url = add_query_arg( 'page', 'wc-settings', $url );
				$url = add_query_arg( 'tab', 'shipping', $url );
				$url = add_query_arg( 'section', WPDesk_Flexible_Shipping_SaaS_Settings::METHOD_ID, $url );
				$url = add_query_arg( 'settings-updated', 'true', $url );
				wp_safe_redirect( $url );
				exit;
			}
		}


		/**
		 * Get shipping providers from platform.
		 *
		 * @return array|\WPDesk\SaasPlatformClient\Response\ShippingServices\GetShippingServicesListResponse
		 */
		public function get_shipping_services_from_platform() {
			$shipping_services = array();
			if ( $this->is_connected() ) {
				try {
					$platform          = $this->get_platform();
					$shipping_services = $platform->requestListShippingServices();

					return $shipping_services->getPage();
				} catch ( Exception $e ) {
				}
			}

			return $shipping_services;
		}

		public function display_connection_form() {
			$form_action = admin_url( 'admin.php' );
			$form_action = add_query_arg( self::SUBMIT_CONNECTION_PARAMETER, '1', $form_action );
			if ( $this->is_connected() ) {
				$button_id           = self::BUTTON_ID_DISCONNECT;
				$readonly            = 'readonly="readonly"';
				$submit_button_value = __( 'Disconnect', 'flexible-shipping' );
			} else {
				$button_id           = self::BUTTON_ID_CONNECT;
				$readonly            = '';
				$submit_button_value = __( 'Connect', 'flexible-shipping' );
			}
			$api_key        = get_option( self::OPTION_SAAS_API_KEY, '' );
			$saas_connected = $this->is_connected();
			include 'views/html-saas-connection-form.php';
		}

		/**
		 * Get SaaS shipping providers.
		 *
		 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Services
		 */
		public function get_saas_shipping_services() {
			return $this->saas_shipping_services;
		}

		/**
		 * Set SaaS shipping providers.
		 *
		 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Services $saas_shipping_services SaaS shipping providers.
		 */
		public function set_saas_shipping_services( $saas_shipping_services ) {
			$this->saas_shipping_services = $saas_shipping_services;
		}

	}

}
