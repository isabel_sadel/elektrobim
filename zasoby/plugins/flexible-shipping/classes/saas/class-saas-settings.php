<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Settings
 */

class WPDesk_Flexible_Shipping_SaaS_Settings extends WC_Shipping_Method implements WPDesk_Flexible_Shipping_Saas_Connection_Aware {

	const METHOD_ID = 'flexible_shipping_connect';

	const FILTER_FLEXIBLE_SHIPPING_SAAS_SERVICES = 'flexible_shipping_saas_services';

	const WOOCOMMERCE_PAGE_WC_SETTINGS = 'wc-settings';

	const WOOCOMMERCE_SETTINGS_SHIPPING_URL = 'admin.php?page=wc-settings&tab=shipping';

	const WPDESK_HELPER_OPTIONS = 'wpdesk_helper_options';
	/**
	 * Shipping methods manager.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Services_Manager
	 */
	private $shipping_methods_manager;

	/**
	 * Saas connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection = null;

	/**
	 * Links.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
	 */
	private $saas_platform_links;

	/**
	 * Logger settings.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Logger_Settings
	 */
	private $logger_settings;

	/**
	 * Cache settings.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Logger_Settings
	 */
	private $cache_settings;

	/**
	 * New courier settings section.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_New_Courier_Settings_Section
	 */
	private $new_courier_settings_section;

	/**
	 * Settings integration checkbox.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Settings_Integration_Checkbox
	 */
	private $integration_checkbox;

	/**
	 * WPDesk_Flexible_Shipping_Connect constructor.
	 *
	 * @param int                                            $instance_id Instance id.
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection       $saas_connection SaaS connection.
	 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links   $saas_platform_links Links.
	 * @param WPDesk_Flexible_Shipping_SaaS_Services_Manager $shipping_methods_manager Shipping methods manager.
	 * @param WPDesk_Flexible_Shipping_SaaS_Settings_Integration_Checkbox $integration_checkbox Setting integration checkbox.
	 */
	public function __construct(
		$instance_id = 0,
		$saas_connection = null,
		WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links = null,
		$shipping_methods_manager = null,
		WPDesk_Flexible_Shipping_SaaS_Settings_Integration_Checkbox $integration_checkbox

	) {
		parent::__construct( $instance_id );
		$this->id           = self::METHOD_ID;
		$this->enabled      = 'no';
		$this->method_title = __( 'Flexible Shipping', 'flexible-shipping' );

		$this->supports = array(
			'settings',
		);

		$this->shipping_methods_manager = $shipping_methods_manager;
		$this->saas_connection          = $saas_connection;
		$this->integration_checkbox     = $integration_checkbox;

		if ( null === $saas_platform_links ) {
			$saas_platform_links = new WPDesk_Flexible_Shipping_SaaS_Platform_Links();
		}
		$this->saas_platform_links = $saas_platform_links;

		$this->logger_settings = new WPDesk_Flexible_Shipping_SaaS_Logger_Settings( $this );

		$this->cache_settings = new WPDesk_Flexible_Shipping_SaaS_Cache_Settings();

		$this->new_courier_settings_section = new WPDesk_Flexible_Shipping_SaaS_New_Courier_Settings_Section( $this->saas_platform_links );

		$this->init_form_fields();

		add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );
	}

	/**
	 * Set shipping methods manager.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Services_Manager $shipping_methods_manager Shipping methods manager.
	 */
	public function set_shipping_methods_manager( $shipping_methods_manager ) {
		$this->shipping_methods_manager = $shipping_methods_manager;
	}

	/**
	 * Set SaaS Connection.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection $saas_connection SaaS connection.
	 */
	public function set_saas_connection( $saas_connection ) {
		$this->saas_connection = $saas_connection;
	}

	/**
	 * Update all shipping services.
	 *
	 * @param array $shipping_providers Shipping providers.
	 */
	private function update_all_shipping_services( array $shipping_providers ) {
		$this->shipping_methods_manager->update_all_shipping_services( $shipping_providers );
	}

	/**
	 * Update debug mode.
	 */
	private function update_debug_mode() {
		$this->logger_settings->update_option_from_saas_settings( $this );
	}

	/**
	 * Update cache settings.
	 */
	private function update_cache_settings() {
		$this->cache_settings->update_option_from_saas_settings( $this );
	}

	/**
	 * Process admin options.
	 */
	public function process_admin_options() {
		parent::process_admin_options();
		$shipping_service = $this->get_shipping_services_from_platform();
		$this->update_all_shipping_services( $shipping_service );
		$enabled_shipping_services = array();
		/**
		 * IDE type hint.
		 *
		 * @var \WPDesk\SaasPlatformClient\Model\ShippingService $service
		 */
		foreach ( $shipping_service as $service ) {
			$shipping_service = WPDesk_Flexible_Shipping_SaaS_Shipping_Service::create_from_platform_service(
				$this->saas_connection,
				$this->saas_platform_links,
				$service
			);
			if ( 'yes' === $this->get_option( $shipping_service->get_integration_id(), 'no' ) ) {
				$enabled_shipping_services[] = $service->getId();
			}
		}
		$this->update_debug_mode();
		$this->update_cache_settings();
		$this->shipping_methods_manager->update_enabled_shipping_services( $enabled_shipping_services );
		$url = admin_url( self::WOOCOMMERCE_SETTINGS_SHIPPING_URL );
		$url = add_query_arg( 'section', $this->id, $url );
		$url = add_query_arg(
			WPDesk_Flexible_Shipping_SaaS_Services_Manager::PARAMETER_SETTINGS_UPDATED,
			WPDesk_Flexible_Shipping_SaaS_Services_Manager::PARAMETER_SETTINGS_UPDATED_VALUE,
			$url
		);
		wp_safe_redirect( $url );
		exit;
	}

	/**
	 * In settings screen?
	 *
	 * @return bool
	 */
	public function is_in_settings() {
		if ( is_admin() && isset( $_GET['page'] ) && isset( $_GET['section'] ) ) {
			$page    = $_GET['page'];
			$section = $_GET['section'];
			if ( self::WOOCOMMERCE_PAGE_WC_SETTINGS === $page && self::METHOD_ID === $section ) {
				return true;
			}
		}

		return false;
	}

	/**
	 * Get shipping providers from platform.
	 *
	 * @return array
	 * @throws Exception Exception.
	 */
	private function get_shipping_services_from_platform() {
		try {
			$saas_shipping_services          = $this->saas_connection->get_saas_shipping_services();
			$shipping_services_from_platform = $saas_shipping_services->get_shipping_services_from_platform();
		} catch ( WPDesk_Flexible_Shipping_SaaS_Bad_Credentials_Exception $e ) {
			$shipping_services_from_platform = array();
		}

		return $shipping_services_from_platform;
	}

	/**
	 * Maybe update integration checkbox.
	 *
	 * @param string|null $required_plugin_version Required plugin version.
	 * @param string $integration_id Integration ID.
	 */
	private function maybe_update_integration_checkbox( $required_plugin_version, $integration_id ) {
		if ( empty( $required_plugin_version )
		     || version_compare( $required_plugin_version, FLEXIBLE_SHIPPING_VERSION ) > 0
		) {
			$this->update_option( $integration_id, 'no' );
		}
	}

	/**
	 * Add shipping providers checkboxes to settings.
	 *
	 * @param array $settings Settings.
	 * @param array $shipping_services_from_platform Shipping providers from platform.
	 *
	 * @return array
	 */
	private function add_shipping_services_checkboxes_to_settings( array $settings, array $shipping_services_from_platform ) {
		if ( count( $shipping_services_from_platform ) ) {
			$settings[] = array(
				'type'        => 'title',
				'title'       => __( 'Connect Services', 'flexible-shipping' ),
				'description' => __( 'Enable services to automate shipping and print labels with Flexible Shipping Connect.', 'flexible-shipping' ),
			);
			foreach ( $shipping_services_from_platform as $shipping_service_from_platform ) {
				$shipping_service = WPDesk_Flexible_Shipping_SaaS_Shipping_Service::create_from_platform_service(
					$this->saas_connection,
					$this->saas_platform_links,
					$shipping_service_from_platform
				);

				$integration_id = $shipping_service->get_integration_id();

				$required_plugin_version = $shipping_service->get_capability( 'requireWordpressPluginVersion' );
				$this->maybe_update_integration_checkbox( $required_plugin_version, $integration_id );

				$service_checkbox = new WPDesk_Flexible_Shipping_SaaS_Settings_Service_Checkbox(
					$integration_id,
					$shipping_service,
					$required_plugin_version,
					FLEXIBLE_SHIPPING_VERSION,
					'yes' === $this->get_option( $integration_id ),
					admin_url( self::WOOCOMMERCE_SETTINGS_SHIPPING_URL . '&section=' . $integration_id )
				);

				$settings[ $integration_id ] = $service_checkbox->prepare_checkbox();
			}
		}

		return $settings;
	}

	/**
	 * Initialise Settings Form Fields.
	 */
	public function init_form_fields() {
		$this->form_fields = array(
			'flexible_shipping' => array(
				'type' => 'flexible_shipping',
			),
			'saas_connection'   => array(
				'type' => 'saas_connection',
			),
		);

		if ( $this->is_in_settings() ) {
			if ( $this->saas_connection->is_connected() ) {
				$shipping_providers_from_platform = $this->get_shipping_services_from_platform();
				if ( count( $shipping_providers_from_platform ) ) {
					$this->form_fields = $this->add_shipping_services_checkboxes_to_settings( $this->form_fields, $shipping_providers_from_platform );
				}
				$this->form_fields = $this->new_courier_settings_section->add_fields_to_settings( $this->form_fields );
			}
			$this->form_fields[] = array(
				'type'  => 'title',
				'title' => __( 'Advanced settings', 'flexible-shipping' ),
			);
			$this->form_fields   = $this->logger_settings->add_fields_to_settings( $this->form_fields );
			$this->form_fields   = $this->cache_settings->add_fields_to_settings( $this->form_fields );
		}
	}

	/**
	* Generate HTML for FC connect notice
	*
	* @param string $key Key.
	* @param array  $data Data.
	*
	* @return string
	*/
	public function generate_fsconnect_html( $key, $data ) {
		$saas_connected            = $this->saas_connection->is_connected();
		$html_class_is_dismissible = 'is-dismissible';
		ob_start();
		include 'views/html-flexible-shipping-connect-notice.php';
		$notice_content = ob_get_contents();
		ob_end_clean();
		return $notice_content;
	}

	/**
	 * Generate FC connect box
	 *
	 * @param string $key Key.
	 * @param array  $data Data.
	 *
	 * @return string
	 */
	public function generate_flexible_shipping_html( $key, $data ) {
		$html_class_is_dismissible = 'is-dismissible';
		$integration_checkbox      = $this->integration_checkbox->is_visible();
		ob_start();
		include 'views/html-shipping-method-info-description.php';
		$notice_content = ob_get_contents();
		ob_end_clean();
		return $notice_content;
	}

	/**
	 * Generate Flexibble Shipping box.
	 *
	 * @param string $key Key.
	 * @param array  $data Data.
	 *
	 * @return string
	 */
	public function generate_saas_connection_html( $key, $data ) {
		ob_start();
		$saas_connection              = $this->saas_connection;
		$integration_checkbox         = $this->integration_checkbox->is_visible();
		$saas_platform_my_account_url = $this->saas_platform_links->add_utm(
			$this->saas_platform_links->get_my_account(),
			'fs-configuration-myaccount',
			'flexible-shipping',
			'link',
			'user-site',
			'your-account'
		);
		$saas_platform_docs_url       = $this->saas_platform_links->add_utm(
			$this->saas_platform_links->get_docs(),
			'fs-configuration-howtostart',
			'flexible-shipping',
			'link',
			'user-site',
			'check-how-to-start-with-FS-Connect'
		);
		$saas_connected               = $this->saas_connection->is_connected();
		include 'views/html-field-saas-connection.php';

		return ob_get_clean();
	}


	/**
	 * Generate settings HTML.
	 *
	 * @param array $form_fields Form fields.
	 * @param bool  $echo Echo settings.
	 *
	 * @return string
	 * @throws Exception Exception.
	 */
	public function generate_settings_html( $form_fields = array(), $echo = true ) {
		ob_start();
		$this->saas_connection->clear_saas_client_cache();
		$html               = parent::generate_settings_html( $form_fields, $echo );
		$saas_connected     = $this->saas_connection->is_connected();
		$shipping_providers = $this->get_shipping_services_from_platform();
		if ( 0 === count( $shipping_providers ) ) {
			$saas_connected = false;
		} else {
			$this->update_all_shipping_services( $shipping_providers );
		}
		include 'views/html-settings-connect-script.php';
		$html .= ob_get_clean();
		if ( $echo ) {
			echo $html;
		} else {
			return $html;
		}
	}

	/**
	 * Generate Text Input HTML.
	 *
	 * @param string $key Field key.
	 * @param array  $data Field data.
	 *
	 * @since  1.0.0
	 * @return string
	 */
	public function generate_long_row_html( $key, $data ) {
		$field_key = $this->get_field_key( $key );
		$defaults  = array(
			'content' => '',
			'class'   => '',
		);
		$data      = wp_parse_args( $data, $defaults );
		ob_start();
		include 'views/html-settings-long-row.php';

		return ob_get_clean();
	}

}
