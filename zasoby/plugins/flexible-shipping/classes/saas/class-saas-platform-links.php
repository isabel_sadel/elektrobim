<?php

/**
 * Class WPDesk_Flexible_Shipping_SaaS_Platform_Links
 */
class WPDesk_Flexible_Shipping_SaaS_Platform_Links {

	const MY_ACCOUNT   = 'my-account/';
	const SUPPORT      = 'support/';
	const TERMS        = 'terms/';
	const PRIVACY      = 'terms/privacy/';
	const UPGRADE_PLAN = 'my-account/';
	const CONNECT      = 'connect/';
	const PLANNED      = 'product-category/integrations/';

	const DOCS   = 'https://docs.flexibleshipping.com/category/81-getting-started';
	const STATUS = 'https://status.flexibleshipping.com/';

	const UTM_SOURCE   = 'fs-signup';
	const UTM_MEDIUM   = 'banner';
	const UTM_CAMPAIGN = 'fs-signup';
	const UTM_TERM     = '';

	const UTM_CONTENT_GENERAL        = 'general';
	const UTM_CONTENT_PRIVACY_POLICY = 'privacy-policy';
	const UTM_CONTENT_TERMS          = 'terms';


	/**
	 * Base URL.
	 *
	 * @var string
	 */
	private $base_url;

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Platform_Links constructor.
	 *
	 * @param string $base_url Base URL.
	 */
	public function __construct( $base_url = 'https://flexibleshipping.com/' ) {
		$this->base_url = trailingslashit( $base_url );
	}

	/**
	 * Get base URL.
	 *
	 * @return string
	 */
	public function get_base_url() {
		return $this->base_url;
	}

	/**
	 * Get my account URL.
	 *
	 * @return string
	 */
	public function get_my_account() {
		return $this->base_url . self::MY_ACCOUNT;
	}

	/**
	 * Get my account URL.
	 *
	 * @return string
	 */
	public function get_support() {
		return $this->base_url . self::SUPPORT;
	}

	/**
	 * Get terms URL.
	 *
	 * @return string
	 */
	public function get_terms() {
		return $this->base_url . self::TERMS;
	}

	/**
	 * Get privacy URL.
	 *
	 * @return string
	 */
	public function get_privacy() {
		return $this->base_url . self::PRIVACY;
	}

	/**
	 * Get upgrade plan URL.
	 *
	 * @return string
	 */
	public function get_upgrade_plan() {
		return $this->base_url . self::UPGRADE_PLAN;
	}

	/**
	 * Get docs URL.
	 *
	 * @return string
	 */
	public function get_docs() {
		return self::DOCS;
	}

	/**
	 * Get status URL.
	 *
	 * @return string
	 */
	public function get_status() {
		return self::STATUS;
	}

	/**
	 * Get connect URL.
	 *
	 * @return string
	 */
	public function get_connect() {
		return $this->base_url . self::CONNECT;
	}

	/**
	 * Get planned integrations URL.
	 *
	 * @return string
	 */
	public function get_planned() {
		return $this->base_url . self::PLANNED;
	}

	/**
	 * Add UTM to URL.
	 *
	 * @param string $url URL.
	 * @param string $utm_content Parameter utm_content.
	 * @param string $utm_campaign Parameter utm_campaign.
	 * @param string $utm_medium Parameter utm_medium.
	 * @param string $utm_source Parameter utm_source.
	 * @param string $utm_term Parameter utm_term.
	 *
	 * @return string
	 */
	public function add_utm( $url, $utm_content, $utm_campaign = '', $utm_medium = '', $utm_source = '', $utm_term = '' ) {
		$utm_campaign = empty( $utm_campaign ) ? self::UTM_CAMPAIGN : $utm_campaign;
		$utm_medium   = empty( $utm_medium ) ? self::UTM_MEDIUM : $utm_medium;
		$utm_source   = empty( $utm_source ) ? self::UTM_SOURCE : $utm_source;
		$utm_term     = empty( $utm_term ) ? self::UTM_TERM : $utm_term;
		return sprintf(
			'%1$s?utm_source=%2$s&utm_medium=%3$s&utm_campaign=%4$s&utm_content=%5$s&utm_term=%6$s',
			$url,
			$utm_source,
			$utm_medium,
			$utm_campaign,
			$utm_content,
			$utm_term
		);
	}

	/**
	 * Add utm-content general to URL.
	 *
	 * @param string $url URL.
	 *
	 * @return string
	 */
	public function add_utm_general( $url ) {
		return $this->add_utm( $url, self::UTM_CONTENT_GENERAL );
	}

	/**
	 * Add utm-content privacy policy to URL.
	 *
	 * @param string $url URL.
	 *
	 * @return string
	 */
	public function add_utm_privacy_policy( $url ) {
		return $this->add_utm( $url, self::UTM_CONTENT_PRIVACY_POLICY );
	}

	/**
	 * Add utm-content terms to URL.
	 *
	 * @param string $url URL.
	 *
	 * @return string
	 */
	public function add_utm_terms( $url ) {
		return $this->add_utm( $url, self::UTM_CONTENT_TERMS );
	}

}

