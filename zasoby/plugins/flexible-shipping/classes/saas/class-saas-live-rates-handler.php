<?php

/**
 * Handles rate request.
 *
 * Class WPDesk_Flexible_Shipping_SaaS_Live_Rates_Handler
 */
class WPDesk_Flexible_Shipping_SaaS_Live_Rates_Handler {

	const PRIORITY_BEFORE_DEFAULT = 9;

	const RATE_TYPE_LIVE = 'live';
	const FS_RATE_TYPE   = '_fs_rate_type';

	/**
	 * Integration id.
	 *
	 * @var string
	 */
	private $integration_id;

	/**
	 * Shipping service.
	 *
	 * @var \WPDesk\SaasPlatformClient\Model\ShippingService
	 */
	private $shipping_service;

	/**
	 * SaaS connection.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Connection
	 */
	private $saas_connection;

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;

	/**
	 * Fallback error message.
	 *
	 * @var string
	 */
	private $fallback_error_message = '';

	/**
	 * WPDesk_Flexible_Shipping_SaaS_Live_Rates_Handler constructor.
	 *
	 * @param string                                         $integration_id Integration id.
	 * @param WPDesk_Flexible_Shipping_SaaS_Shipping_Service $shipping_service Shipping service.
	 * @param WPDesk_Flexible_Shipping_SaaS_Connection       $saas_connection SaaS Connection.
	 * @param \Psr\Log\LoggerInterface                       $logger Logger.
	 */
	public function __construct( $integration_id, $shipping_service, $saas_connection, \Psr\Log\LoggerInterface $logger = null ) {
		$this->integration_id   = $integration_id;
		$this->shipping_service = $shipping_service;
		$this->saas_connection  = $saas_connection;
		$this->logger           = $logger;
	}

	/**
	 * Hooks.
	 */
	public function hooks() {
		add_filter( 'flexible_shipping_add_method', [ $this, 'handle_saas_live_rates' ], self::PRIORITY_BEFORE_DEFAULT, 4 );
		add_action( 'woocommerce_before_checkout_form', [ $this, 'recalculate_shipping_for_woocomerce_manager' ] );
	}

	/**
	 * Prepare packages.
	 *
	 * @param array  $woocommerce_package Package.
	 * @param string $package_type Package type.
	 * @param string $description Description.
	 * @param float  $weight Weight.
	 * @param int    $height Height.
	 * @param int    $width Width.
	 * @param int    $length Length.
	 *
	 * @return array
	 */
	private function prepare_packages( array $woocommerce_package, $package_type, $description, $weight, $height, $width, $length ) {
		$package = new \WPDesk\SaasPlatformClient\Model\Shipment\Package();
		$package->seTtype( $package_type );
		$package_weight = fs_calculate_package_weight( $woocommerce_package );
		if ( floatval( 0 ) === $package_weight ) {
			$package_weight = $weight;
		}
		$package_weight = intval( wc_get_weight( floatval( $package_weight ), 'g' ) );
		$package->setWeight( $package_weight );

		$package->setDescription( $description );
		$package->dimensions = new \WPDesk\SaasPlatformClient\Model\Shipment\Package\Dimensions();
		$package->dimensions->setHeight( intval( wc_get_dimension( floatval( $height ), 'mm' ) ) );
		$package->dimensions->setWidth( intval( wc_get_dimension( floatval( $width ), 'mm' ) ) );
		$package->dimensions->setLength( intval( wc_get_dimension( floatval( $length ), 'mm' ) ) );

		$packages = array( $package );

		return $packages;
	}

	/**
	 * Prepare ship to address.
	 *
	 * @param array $package Package.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\Address
	 */
	private function prepare_ship_to_address( array $package ) {
		$destination     = $package['destination'];
		$ship_to_address = new \WPDesk\SaasPlatformClient\Model\Shipment\Address();
		$ship_to_address->setAddressLine1( $destination['address'] );
		$ship_to_address->setAddressLine2( $destination['address_2'] );
		$ship_to_address->setPostalCode( str_replace( '-', '', $destination['postcode'] ) );
		$ship_to_address->setCity( $destination['city'] );
		$ship_to_address->setCountryCode( $destination['country'] );
		$ship_to_address->setStateCode( $destination['state'] );

		return $ship_to_address;
	}


	/**
	 * Prepare ship to.
	 *
	 * @param array $package Package.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\Actor
	 */
	private function prepare_ship_to( array $package ) {
		$ship_to = new \WPDesk\SaasPlatformClient\Model\Shipment\Actor();
		$ship_to->setAddress( $this->prepare_ship_to_address( $package ) );
		$ship_to->setName( 'first last' );
		$ship_to->setCompanyName( '' );
		$ship_to->setPhoneNumber( '' );
		$ship_to->setEmail( '' );
		return $ship_to;
	}

	/**
	 * Get string element from array.
	 *
	 * @param array  $array Array.
	 * @param string $key Key.
	 *
	 * @return string
	 */
	private function get_string_element_from_array( array $array, $key ) {
		if ( isset( $array[ $key ] ) ) {
			return $array[ $key ];
		}
		return '';
	}

	/**
	 * Get int element from array.
	 *
	 * @param array  $array Array.
	 * @param string $key Key.
	 *
	 * @return int
	 */
	private function get_int_element_from_array( array $array, $key ) {
		if ( isset( $array[ $key ] ) ) {
			return intval( $array[ $key ] );
		}
		return 0;
	}

	/**
	 * Prepare shipment field from send-as.
	 *
	 * @param array  $field Field.
	 * @param string $send_as Send as field name.
	 * @param mixed  $append_to Append to object.
	 * @param mixed  $field_value Field value.
	 */
	private function prepare_shipment_field_from_send_as( array $field, $send_as, $append_to, $field_value ) {
		$send_as_to_array = explode( '.', $send_as );
		$field_name       = $send_as_to_array[0];
		if ( 1 === count( $send_as_to_array ) ) {
			$append_to[ $field_name ] = $field_value;
		} else {
			unset( $send_as_to_array[0] );
			$append_to_field = $append_to[ $field_name ];
			if ( empty( $append_to_field ) ) {
				$append_to[ $field_name ] = new \WPDesk\SaasPlatformClient\Model\AbstractModel();
			}
			$this->prepare_shipment_field_from_send_as( $field, implode( '.', $send_as_to_array ), $append_to[ $field_name ], $field_value );
		}
	}

	/**
	 * Get shipping method setting for field.
	 *
	 * @param array $field Field.
	 * @param array $shipping_method Shipping method settings.
	 *
	 * @return bool|string
	 */
	private function get_shipping_method_setting_for_field( $field, $shipping_method ) {
		$field_value = '';
		if ( isset( $shipping_method[ $this->integration_id . '_' . $field['id'] ] ) ) {
			$field_value = $shipping_method[ $this->integration_id . '_' . $field['id'] ];
		}
		if ( 'checkbox' === $field['type'] ) {
			$field_value = 'yes' === $field_value;
		}
		return $field_value;
	}

	/**
	 * Get checkout value for field.
	 *
	 * @param array $field Field.
	 *
	 * @return string
	 */
	private function get_checkout_value_for_field( array $field ) {
		$field_name   = $field['id'] . '_' . $this->shipping_service->get_integration_id();
		$request_data = new WPDesk_Flexible_Shipping_Checkout_Ajax_Request_Data( $_REQUEST );
		$field_value  = $request_data->get_data_by_name( $field_name );
		if ( empty( $field_value ) ) {
			$field_value = null;
		}
		return $field_value;
	}

	/**
	 * Prepare rate request additional fields.
	 *
	 * @param \WPDesk\SaasPlatformClient\Model\Rate\RateRequest $rate_request Rate request.
	 * @param array                                             $fields Fields.
	 * @param array                                             $shipping_method Shipping method settings.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Rate\RateRequest
	 */
	private function prepare_rate_request_additional_fields( $rate_request, $fields, $shipping_method ) {
		foreach ( $fields as $fieldset ) {
			foreach ( $fieldset['fields'] as $field ) {
				if ( isset( $field['send-with'] ) && in_array( 'rate', $field['send-with'], true ) ) {
					if ( in_array( 'checkout', $fieldset['show-in'], true ) ) {
						$field_value = $this->get_checkout_value_for_field( $field );
					} else {
						$field_value = $this->get_shipping_method_setting_for_field( $field, $shipping_method );
					}
					if ( isset( $field['send-as'] ) ) {
						$this->prepare_shipment_field_from_send_as( $field, $field['send-as'], $rate_request, $field_value );
					} else {
						$rate_request->{$field['id']} = $field_value;
					}
				}
			}
		}
		return $rate_request;
	}

	/**
	 * Prepare rate request.
	 *
	 * @param array $package Package.
	 * @param array $shipping_method Shipping method.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Rate\RateRequest
	 */
	private function prepare_rate_request( array $package, array $shipping_method ) {
		$rate_request = new \WPDesk\SaasPlatformClient\Model\Rate\RateRequest();
		$rate_request->setServiceType( '' );
		$rate_request->setReferenceNumber( '' );
		$rate_request->setShipTo( $this->prepare_ship_to( $package ) );
		$package_type = $this->get_string_element_from_array( $shipping_method, $this->integration_id . '_package_type' );
		$description  = $this->get_string_element_from_array( $shipping_method, $this->integration_id . '_description' );
		$weight       = floatval( $this->get_string_element_from_array( $shipping_method, $this->integration_id . '_package_weight' ) );
		$height       = $this->get_int_element_from_array( $shipping_method, $this->integration_id . '_height' );
		$width        = $this->get_int_element_from_array( $shipping_method, $this->integration_id . '_width' );
		$length       = $this->get_int_element_from_array( $shipping_method, $this->integration_id . '_length' );
		$rate_request->setPackages( $this->prepare_packages( $package, $package_type, $description, $weight, $height, $width, $length ) );


		$destination_country = $package['destination']['country'];
		$fields              = $this->shipping_service->get_fields_for_targets( array( $destination_country ) );
		$rate_request        = $this->prepare_rate_request_additional_fields( $rate_request, $fields, $shipping_method );

		return $rate_request;
	}

	/**
	 * Get shipping method settings fields.
	 *
	 * @param array $shipping_method Shipping method.
	 *
	 * @return array
	 */
	private function get_shipping_method_settings( array $shipping_method ) {
		return apply_filters( 'flexible_shipping_method_settings', array(), $shipping_method );
	}

	/**
	 * Get custom services enabled for shipping method.
	 *
	 * @param array $shipping_method Shipping method.
	 *
	 * @return bool
	 */
	private function get_custom_services_enabled( array $shipping_method ) {
		$custom_services_enabled = false;
		if ( isset( $shipping_method[ $this->integration_id . '_custom_services' ] )
			&& 'yes' === $shipping_method[ $this->integration_id . '_custom_services' ]
		) {
			$custom_services_enabled = true;
		}
		return $custom_services_enabled;
	}

	/**
	 * Get custom service types from shipping method settings.
	 *
	 * @param array $shipping_method_settings Shipping method settings.
	 *
	 * @return array
	 */
	private function get_custom_service_types_from_settings( array $shipping_method_settings ) {
		if ( isset( $shipping_method_settings[ $this->integration_id . '_service_type_custom_services' ] )
			&& isset( $shipping_method_settings[ $this->integration_id . '_service_type_custom_services' ]['services'] )
		) {
			$custom_services_service_types = $shipping_method_settings[ $this->integration_id . '_service_type_custom_services' ]['services'];
		} else {
			$custom_services_service_types = array();
		}
		return $custom_services_service_types;
	}

	/**
	 * Filter rates.
	 *
	 * @param WPDesk\SaasPlatformClient\Model\Rate\SingleRate[] $rates Rates.
	 * @param array                                             $shipping_method Shipping method.
	 *
	 * @return array
	 */
	private function filter_and_rename_rates( $rates, array $shipping_method ) {
		$shipping_method_settings = $this->get_shipping_method_settings( $shipping_method );

		$custom_services_enabled = $this->get_custom_services_enabled( $shipping_method );

		$custom_services_service_types = $this->get_custom_service_types_from_settings( $shipping_method_settings );

		foreach ( $rates as $rate_id => $rate ) {
			if ( isset( $custom_services_service_types[ $rate->serviceType ] ) ) {
				if ( $custom_services_enabled && 0 === intval( $custom_services_service_types[ $rate->serviceType ]['enabled'] ) ) {
					unset( $rates[ $rate_id ] );
				} else {
					$rates[ $rate_id ]->serviceName = $custom_services_service_types[ $rate->serviceType ]['name'];
				}
			} else {
				unset( $rates[ $rate_id ] );
			}
		}
		return $rates;
	}

	/**
	 * Get rate for given service type.
	 *
	 * @param WPDesk\SaasPlatformClient\Model\Rate\SingleRate[] $rates Rates.
	 * @param string                                            $service_type_id Service type id.
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Rate\SingleRate|null
	 */
	private function get_rate_for_service_type( $rates, $service_type_id ) {
		foreach ( $rates as $rate ) {
			if ( strval( $rate->serviceType ) === strval( $service_type_id ) ) {
				return $rate;
			}
		}
		return null;
	}

	/**
	 * Sort rates.
	 *
	 * @param WPDesk\SaasPlatformClient\Model\Rate\SingleRate[] $rates Rates.
	 * @param array                                             $shipping_method Shipping method.
	 *
	 * @return array
	 */
	private function sort_rates( $rates, array $shipping_method ) {
		$custom_services_enabled = $this->get_custom_services_enabled( $shipping_method );

		if ( $custom_services_enabled ) {
			$sorted_rates                  = array();
			$shipping_method_settings      = $this->get_shipping_method_settings( $shipping_method );
			$custom_services_service_types = $this->get_custom_service_types_from_settings( $shipping_method_settings );
			foreach ( $custom_services_service_types as $service_type_id => $service_type ) {
				$rate = $this->get_rate_for_service_type( $rates, $service_type_id );
				if ( null !== $rate ) {
					$sorted_rates[] = $rate;
				}
			}
			return $sorted_rates;
		}

		return $rates;
	}

	/**
	 * Add rates to checkout.
	 *
	 * @param \WPDesk\SaasPlatformClient\Model\Rate\SingleRate[] $rates Rates.
	 * @param array                                              $shipping_method Shipping method.
	 * @param WPDesk_Flexible_Shipping                           $flexible_shipping Flexible shipping.
	 * @param array                                              $live_rate_response Live rate response.
	 */
	private function add_rates_to_checkout( $rates, $shipping_method, $flexible_shipping, $live_rate_response ) {
		foreach ( $rates as $rate ) {
			$flexible_shipping->add_rate( array(
				'id'        => $flexible_shipping->id . ':' . $flexible_shipping->instance_id . ':' . $rate->serviceType,
				'label'     => $rate->serviceName,
				'cost'      => $rate->totalCharge->getAmount() / 100,
				'meta_data' => array(
					'_fs_method'       => $shipping_method,
					self::FS_RATE_TYPE => self::RATE_TYPE_LIVE,
					'_fs_saas_data' => array(
						WPDesk_Flexible_Shipping::META_DEFAULT => $shipping_method['method_default'],
						'_fs_service_type'                     => $rate->serviceType,
						'_fs_service_name'                     => $rate->serviceName,
						'_fs_live_rate_response'               => $live_rate_response,
					),
				),
			) );
		}
	}

	/**
	 * Is fallback enabled?
	 *
	 * @param array $shipping_method_settings Shipping method settings.
	 *
	 * @return bool
	 */
	private function is_fallback_enabled( array $shipping_method_settings ) {
		return 'yes' === $shipping_method_settings[ $this->integration_id . '_fallback' ];
	}

	/**
	 * Maybe add notice in checkout page for admin.
	 *
	 * @param string $notice_message Notice message.
	 * @param array  $shipping_method_settings Shipping method settings.
	 */
	private function maybe_add_notice_for_admin( $notice_message, $shipping_method_settings ) {
		if ( is_checkout() && current_user_can( 'manage_woocommerce' ) ) {
			$message = sprintf(
				// Translators: notice message.
				__( 'Flexible Shipping Fallback (%1$s): %2$s', 'flexible-shipping' ),
				$shipping_method_settings['method_title'],
				$notice_message
			);
			if ( ! wc_has_notice( $message, 'error' ) ) {
				wc_add_notice( $message, 'error' );
			}
		}
	}

	/**
	 * Maybe show message stock.
	 *
	 * @param array $message_stack Message stack.
	 * @param array $shipping_method_settings Shipping method settings.
	 */
	private function maybe_show_message_stock( array $message_stack, array $shipping_method_settings ) {
		if ( ! is_array( $message_stack ) ) {
			$message_stack = array( $message_stack );
		}
		$message = '';
		foreach ( $message_stack as $message_stack_text ) {
			$message .= $message_stack_text . ' ';
		}
		if ( ! empty( $message ) ) {
			$this->maybe_add_notice_for_admin( $message, $shipping_method_settings );
		}
	}

	/**
	 * Process SaaS live rates.
	 *
	 * @param bool                     $add_method Already processed.
	 * @param array                    $shipping_method_settings Shipping method.
	 * @param array                    $package Package.
	 * @param WPDesk_Flexible_Shipping $flexible_shipping Flexible shipping.
	 *
	 * @return bool
	 */
	private function process_live_rates( $add_method, array $shipping_method_settings, array $package, $flexible_shipping ) {
		$is_fallback = false;
		if ( $this->integration_id === $shipping_method_settings['method_integration'] ) {
			if ( self::RATE_TYPE_LIVE === $shipping_method_settings[ $this->integration_id . '_rates_type' ] ) {
				try {
					$rate_request          = $this->prepare_rate_request( $package, $shipping_method_settings );
					$rate_request_response = $this->saas_connection->get_platform()->requestPostRate( $rate_request, $this->shipping_service->get_id() );
					if ( $rate_request_response->isError() ) {
						$reponse_error_body = $rate_request_response->getResponseErrorBody();
						if ( isset( $reponse_error_body['message'] ) && ! empty( $reponse_error_body['message'] ) ) {
							$this->fallback_error_message = $reponse_error_body['message'];
						} else {
							$this->fallback_error_message = sprintf(
								// Translators: response code.
								__( 'Invalid live rates response! Error code: %1$s', 'flexible-shipping' ),
								$rate_request_response->getResponseCode()
							);
						}
						$this->logger->debug( $this->fallback_error_message );
						$add_method  = $this->is_fallback_enabled( $shipping_method_settings );
						$is_fallback = true;
					} else {
						$rates = $this->filter_and_rename_rates( $rate_request_response->getRate()->rates, $shipping_method_settings );
						$rates = $this->sort_rates( $rates, $shipping_method_settings );

						$rate_request_response_body = $rate_request_response->getResponseBody();
						$this->add_rates_to_checkout( $rates, $shipping_method_settings, $flexible_shipping, $rate_request_response_body );
						$add_method = false;
						$this->maybe_show_message_stock( $rate_request_response->getRate()->messageStack, $shipping_method_settings );
					}
				} catch ( Exception $e ) {
					$this->fallback_error_message = sprintf(
						// Translators: error message and error code.
						__( 'Live rates response exception %1$s; %2$s occurred while sending request', 'flexible-shipping' ),
						$e->getMessage(),
						$e->getCode()
					);
					$this->logger->error( $this->fallback_error_message );
					$add_method  = $this->is_fallback_enabled( $shipping_method_settings );
					$is_fallback = true;
				}
				if ( $is_fallback ) {
					if ( $add_method ) {
						add_filter( 'woocommerce_shipping_method_add_rate', [
							$this,
							'add_fallback_metadata_to_rate',
						], 10, 3 );
					}
					$this->maybe_add_notice_for_admin( $this->fallback_error_message, $shipping_method_settings );
				}
			}
		}
		return $add_method;
	}

	/**
	 * Handle SaaS live rates.
	 *
	 * @param bool                          $add_method Already processed.
	 * @param array                         $shipping_method_settings Shipping method.
	 * @param array                         $package Package.
	 * @param null|WPDesk_Flexible_Shipping $flexible_shipping Flexible shipping.
	 *
	 * @return bool
	 */
	public function handle_saas_live_rates( $add_method, array $shipping_method_settings, array $package, $flexible_shipping = null ) {
		if ( null === $flexible_shipping ) {
			$flexible_shipping = new WPDesk_Flexible_Shipping();
		}
		if ( $flexible_shipping->is_free_shipping( $shipping_method_settings, $flexible_shipping->contents_cost() ) ) {
			$add_method = true;
			add_filter( 'woocommerce_shipping_method_add_rate', [
				$this,
				'add_free_shipping_metadata_to_rate',
			], 10, 3 );
			$this->maybe_add_notice_for_admin( $this->fallback_error_message, $shipping_method_settings );
		} else {
			$add_method = $this->process_live_rates( $add_method, $shipping_method_settings, $package, $flexible_shipping );
		}
		return $add_method;
	}


	/**
	 * Add fallback metadata to shipping rate.
	 *
	 * @param WC_Shipping_Rate   $rate Rate.
	 * @param array              $args Args.
	 * @param WC_Shipping_Method $shipping_method Shipping method.
	 * @return WC_Shipping_Rate
	 */
	public function add_fallback_metadata_to_rate( $rate, array $args, $shipping_method ) {
		remove_filter( 'woocommerce_shipping_method_add_rate', [ $this, 'add_fallback_metadata_to_rate' ], 10, 3 );
		$rate->add_meta_data( '_fs_fallback', $this->fallback_error_message );
		$rate->add_meta_data( self::FS_RATE_TYPE, self::RATE_TYPE_LIVE );
		return $rate;
	}

	/**
	 * Add free shipping metadata to shipping rate.
	 *
	 * @param WC_Shipping_Rate   $rate Rate.
	 * @param array              $args Args.
	 * @param WC_Shipping_Method $shipping_method Shipping method.
	 * @return WC_Shipping_Rate
	 */
	public function add_free_shipping_metadata_to_rate( $rate, array $args, $shipping_method ) {
		remove_filter( 'woocommerce_shipping_method_add_rate', [ $this, 'add_free_shipping_metadata_to_rate' ], 10, 3 );
		$rate->add_meta_data( '_fs_free_shipping', 'Free shipping' );
		$rate->add_meta_data( self::FS_RATE_TYPE, self::RATE_TYPE_LIVE );
		return $rate;
	}

	/**
	 * Recalculate shipping for woocommerce manager.
	 * Used for fallback always visible on checkout.
	 */
	public function recalculate_shipping_for_woocomerce_manager() {
		if ( ! is_ajax() && is_checkout() && current_user_can( 'manage_woocommerce' ) ) {
			WC()->session->set( 'cart_totals', null );
		}
	}

}
