<?php

/**
 * Class WPDesk_Flexible_Shipping_Shipment_Saas_Shipment_Fields
 */
class WPDesk_Flexible_Shipping_Shipment_Request_Fields {

	/**
	 * Shipment.
	 *
	 * @var WPDesk_Flexible_Shipping_Shipment_Saas
	 */
	private $shipment;

	/**
	 * Shipment request.
	 *
	 * @var \WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest Shipment request.
	 */
	private $shipment_request;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Request_Fields constructor.
	 *
	 * @param WPDesk_Flexible_Shipping_Shipment_Saas                    $shipment Shipment.
	 * @param \WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest $shipment_request Shipment request.
	 *
	 * @return WPDesk_Flexible_Shipping_Shipment_Request_Fields
	 */
	public function __construct(
		WPDesk_Flexible_Shipping_Shipment_Saas $shipment,
		\WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest $shipment_request
	) {
		$this->shipment         = $shipment;
		$this->shipment_request = $shipment_request;
		return $this;
	}

	/**
	 * Get field value for platform.
	 *
	 * @param array $field Field.
	 *
	 * @return string|bool
	 */
	private function get_field_value_for_platform( array $field ) {
		$field_value = $this->shipment->get_meta( $field['id'], '' );
		if ( 'checkbox' === $field['type'] ) {
			$field_value = 'yes' === $field_value;
		}
		return $field_value;
	}

	/**
	 * Prepare shipment field from send-as.
	 *
	 * @param array  $field Field.
	 * @param string $send_as Send as field name.
	 * @param mixed  $append_to Append to object.
	 */
	private function prepare_shipment_field_from_send_as( array $field, $send_as, $append_to ) {
		$send_as_to_array = explode( '.', $send_as );
		$field_name       = $send_as_to_array[0];
		if ( 1 === count( $send_as_to_array ) ) {
			$append_to[ $field_name ] = $this->get_field_value_for_platform( $field );
		} else {
			unset( $send_as_to_array[0] );
			$append_to_field = $append_to[ $field_name ];
			if ( empty( $append_to_field ) ) {
				$append_to[ $field_name ] = new \WPDesk\SaasPlatformClient\Model\AbstractModel();
			}
			$this->prepare_shipment_field_from_send_as( $field, implode( '.', $send_as_to_array ), $append_to[ $field_name ] );
		}
	}

	/**
	 * Is visible condition meet.
	 *
	 * @param array $field Field.
	 * @return bool
	 */
	private function is_visible_condition_meet( array $field ) {
		if ( isset( $field['visible-when'] ) ) {
			$visible_when = WPDesk_Flexible_Shipping_SaaS_Shipping_Field_Visible_When_Shipment::create_for_field( $field, $this->shipment );
			return $visible_when->is_visible();
		}
		return true;
	}

	/**
	 * Prepare shipment field.
	 *
	 * @param array $field Field.
	 */
	private function prepare_shipment_field( array $field ) {
		if ( isset( $field['send-with'] )
			&& ( in_array( 'shipment', $field['send-with'], true )
			|| in_array( 'label', $field['send-with'], true ) )
			&& ! in_array( 'package', $field['send-with'], true )
			&& $this->is_visible_condition_meet( $field )
		) {
			if ( isset( $field['send-as'] ) ) {
				$this->prepare_shipment_field_from_send_as( $field, $field['send-as'], $this->shipment_request );
			} else {
				$this->shipment_request->{$field['id']} = $this->get_field_value_for_platform( $field );
			}
		}
	}

	/**
	 * Prepare shipment fields from filedset.
	 *
	 * @param array $fieldset Fieldset.
	 */
	private function prepare_shipment_fields_from_fieldset( array $fieldset ) {
		foreach ( $fieldset['fields'] as $field ) {
			$this->prepare_shipment_field( $field );
		}
	}

	/**
	 * Prepare fields from platform,
	 *
	 * @return \WPDesk\SaasPlatformClient\Model\Shipment\ShipmentRequest
	 */
	public function prepare_shipment_fields() {
		$order  = $this->shipment->get_order();
		$fields = $this->shipment->get_shipping_service()->get_fields_for_targets( [ $order->get_shipping_country() ] );
		foreach ( $fields as $fieldset ) {
			$this->prepare_shipment_fields_from_fieldset( $fieldset );
		}
		return $this->shipment_request;
	}

}
