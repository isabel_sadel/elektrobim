<?php

/**
 * Class WPDesk_Flexible_Shipping_Shipment_Saas_Message_Formatter
 */
class WPDesk_Flexible_Shipping_Shipment_Saas_Message_Formatter {

	/**
	 * Message.
	 *
	 * @var string
	 */
	private $message;

	/**
	 * Integration name.
	 *
	 * @var string
	 */
	private $integration_name;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Saas_Message_Formatter constructor.
	 *
	 * @param string $message Message.
	 * @param string $integration_name Integration name.
	 */
	public function __construct( $message, $integration_name ) {
		$this->message          = $message;
		$this->integration_name = $integration_name;
	}

	/**
	 * Format error message to html.
	 *
	 * @return string
	 */
	public function format_to_html() {
		return sprintf( '%1$s%2$s:%3$s %4$s',
			'<span>',
			// Translators: metabox title.
			sprintf( __( '%1$s notice', 'flexible-shipping' ), $this->integration_name ),
			'</span>',
			$this->message
		);
	}

}
