<?php

/**
 * Flexible Shipping SaaS Shipment.
 *
 * Class WPDesk_Flexible_Shipping_Shipment_Saas
 */
class WPDesk_Flexible_Shipping_Shipment_Saas
	extends WPDesk_Flexible_Shipping_Shipment
	implements WPDesk_Flexible_Shipping_Shipment_Interface {

	const META_SAAS_SHIPMENT_ID = '_saas_shipment_id';

	const META_SAAS_MESSAGE = '_saas_message';

	const META_SAAS_TRACKING_ID = '_saas_tracking_id';

	const META_SAAS_TRACKING_URL = '_saas_tracking_url';

	const META_SAAS_SHIPMENT_COST = '_saas_shipment_cost';

	const FIELD_CALCULATION_METHOD = 'calculation-method';

	const FLOAT_EPSILON = 0.000001;

	const RATE_TYPE = 'rate_type';

	const FALLBACK_MESSAGE = 'fallback_message';

	const FALLBACK = 'fallback';

	const CHECKOUT_SERVICE_TYPE = 'checkout_service_type';
	const CHECKOUT_SERVICE_NAME = 'checkout_service_name';

	/**
	 * Renderer.
	 *
	 * @var WPDesk\View\Renderer\Renderer;
	 */
	private static $renderer;

	/**
	 * Platform links.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Platform_Links
	 */
	private static $saas_platform_links;

	/**
	 * Shipping method.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings
	 */
	private $shipping_method;

	/**
	 * Shipping service.
	 *
	 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	private $shipping_service;

	/**
	 * Logger.
	 *
	 * @var \Psr\Log\LoggerInterface
	 */
	private $logger;

	/**
	 * WPDesk_Flexible_Shipping_Shipment_Saas constructor.
	 *
	 * @param int           $shipment_id Shipment ID.
	 * @param WC_Order|null $order Order.
	 */
	public function __construct( $shipment_id, $order = null ) {
		parent::__construct( $shipment_id, $order );
		$shipping_methods = WC()->shipping()->get_shipping_methods();
		$integration      = $this->get_integration();
		/**
		 * IDE Type hint.
		 *
		 * @var WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings $shipping_method
		 */
		$shipping_method        = $shipping_methods[ $integration ];
		$this->shipping_method  = $shipping_method;
		$this->shipping_service = $shipping_method->get_shipping_service();
	}

	/**
	 * Set renderer.
	 *
	 * @param WPDesk\View\Renderer\Renderer $renderer Renderer.
	 */
	public static function set_renderer( WPDesk\View\Renderer\Renderer $renderer ) {
		self::$renderer = $renderer;
	}

	/**
	 * Set platform links.
	 *
	 * @param WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links Platform links.
	 */
	public static function set_platform_links( WPDesk_Flexible_Shipping_SaaS_Platform_Links $saas_platform_links ) {
		self::$saas_platform_links = $saas_platform_links;
	}

	/**
	 * Save field data from shipping method.
	 *
	 * @param array                  $field Field.
	 * @param array                  $fs_method Flexible Shipping Method settings.
	 * @param WC_Order_Item_Shipping $shipping_method Woocommerce shipping method.
	 * @param array                  $post_data Post data.
	 * @param float                  $package_weight Package weight.
	 */
	private function save_field_data_from_shipping_method( array $field, array $fs_method, $shipping_method, $post_data, $package_weight = 0.0 ) {
		$field_id     = $field['id'];
		$fs_saas_data = $shipping_method->get_meta( '_fs_saas_data' );
		if ( ! is_array( $fs_saas_data ) ) {
			$fs_saas_data = array();
		}
		$meta_field_id = '_fs_' . $field_id;
		$calculated    = false;
		if ( isset( $field[ self::FIELD_CALCULATION_METHOD ] ) ) {
			if ( 'sum_items_weight' === $field[ self::FIELD_CALCULATION_METHOD ] && abs( 0 - floatval( $package_weight ) ) > self::FLOAT_EPSILON ) {
				$this->set_meta( $field_id, $package_weight );
				$calculated = true;
			}
		}
		if ( ! $calculated ) {
			if ( isset( $fs_method[ $this->get_integration() . '_' . $field_id ] ) ) {
				$this->set_meta( $field_id, $fs_method[ $this->get_integration() . '_' . $field_id ] );
			}
			if ( isset( $fs_saas_data[ $meta_field_id ] ) ) {
				$this->set_meta( $field_id, $fs_saas_data[ $meta_field_id ] );
			}
			if ( isset( $post_data[ $field_id . '_' . $this->shipping_service->get_integration_id() ] ) ) {
				$this->set_meta( $field_id, $post_data[ $field_id . '_' . $this->shipping_service->get_integration_id() ] );
			}
		}
	}

	/**
	 * Save fieldset data from shipping method.
	 *
	 * @param array                  $fieldset Fieldset.
	 * @param array                  $fs_method Flexible Shipping method.
	 * @param WC_Order_Item_Shipping $shipping_method Woocommerce shipping method.
	 * @param array                  $post_data Post data.
	 * @param float                  $package_weight Package weight.
	 */
	private function save_fieldset_data_from_shipping_method(
		array $fieldset,
		array $fs_method,
		$shipping_method,
		array $post_data,
		$package_weight
	) {
		foreach ( $fieldset['fields'] as $field ) {
			$this->save_field_data_from_shipping_method( $field, $fs_method, $shipping_method, $post_data, $package_weight );
		}
	}

	/**
	 * Set shop and floe data.
	 *
	 * @param WC_Order_Item_Shipping $shipping_method Shipping method.
	 */
	private function set_shop_and_flow_data( $shipping_method ) {
		$rate_type = $shipping_method->get_meta( '_fs_rate_type' );
		if ( '' === $shipping_method->get_meta( '_fs_rate_type' ) ) {
			$rate_type = 'custom';
		}
		if ( 'live' === $rate_type ) {
			$service_type = $shipping_method->get_meta( '_fs_service_type' );
			if ( '' !== $service_type ) {
				$this->set_meta( self::CHECKOUT_SERVICE_TYPE, $service_type );
			}
			$service_name = $shipping_method->get_meta( '_fs_service_name' );
			if ( '' !== $service_name ) {
				$this->set_meta( self::CHECKOUT_SERVICE_NAME, $service_name );
			}
		}
		$this->set_meta( self::RATE_TYPE, $rate_type );

		$fallback_message = $shipping_method->get_meta( '_fs_fallback' );
		if ( '' !== $fallback_message ) {
			$this->set_meta( self::FALLBACK, '1' );
			$this->set_meta( self::FALLBACK_MESSAGE, $fallback_message );
		} else {
			$this->set_meta( self::FALLBACK, '0' );
			$this->set_meta( self::FALLBACK_MESSAGE, '' );
		}
	}

	/**
	 * Create shipment on checkout.
	 *
	 * @param array $fs_method Flexible Shipping method.
	 * @param array $package   Package.
	 */
	public function checkout( array $fs_method, $package ) {

		$post_data = $_REQUEST;

		$shipping_method = $this->get_meta_shipping_method();

		if ( null !== $shipping_method ) {
			$this->set_shop_and_flow_data( $shipping_method );
		}

		$package_weight = fs_calculate_package_weight( $package );
		if ( abs( 0 - floatval( $package_weight ) ) > self::FLOAT_EPSILON ) {
			$this->set_meta( '_package_weight', $package_weight );
		}

		$order = $this->get_order();

		try {
			$fields = $this->shipping_service->get_fields_for_targets( [ $order->get_shipping_country() ] );
			foreach ( $fields as $fieldset ) {
				if (
					isset( $fieldset['show-in'] ) && in_array( 'metabox', $fieldset['show-in'], true )
					|| isset( $fieldset['show-in'] ) && in_array( 'checkout', $fieldset['show-in'], true )
				) {
					$this->save_fieldset_data_from_shipping_method(
						$fieldset,
						$fs_method,
						$shipping_method,
						$post_data,
						$package_weight
					);
				}
			}
		} catch ( Exception $e ) {
			$this->update_status( 'fs-failed' );
			$this->set_meta(
				self::META_SAAS_MESSAGE,
				sprintf(
					// Translators: error from checkout.
					__( 'There was problems on checkout: %1$s', 'flexible-shipping' ),
					$e->getMessage()
				)
			);
		}

	}

	/**
	 * Get order metabox content.
	 *
	 * @return string
	 */
	public function get_order_metabox_content() {
		$metabox = new WPDesk_Flexible_Shipping_Shipment_Saas_Metabox( $this );
		return $metabox->get_order_metabox_content();
	}

	/**
	 * Display order metabox.
	 */
	public function order_metabox() {
		echo $this->get_order_metabox_content();
	}

	/**
	 * Get order metabox title.
	 *
	 * @return string
	 */
	public function get_order_metabox_title() {
		return $this->get_shipping_service_name();
	}

	/**
	 * Get shipping service name.
	 *
	 * @return string
	 */
	public function get_shipping_service_name() {
		return $this->shipping_service->get_name();
	}

	/**
	 * Get fields for targets.
	 *
	 * @return array
	 */
	public function get_fields_for_targets() {
		$order            = $this->get_order();
		$shipping_country = $order->get_shipping_country();
		if ( empty( $shipping_country ) ) {
			$default_shipping_country = explode( ':', get_option( 'woocommerce_default_country' ) );
			$shipping_country         = $default_shipping_country[0];
		}
		return $this->shipping_service->get_fields_for_targets( [ $shipping_country ] );
	}

	/**
	 * Get platform.
	 *
	 * @return \WPDesk\SaasPlatformClient\Platform
	 */
	public function get_platform() {

		$this->logger = WPDesk_Flexible_Shipping_Logger_Factory::create_logger();

		$platform_factory = new WPDesk_Flexible_Shipping_SaaS_Auth_Platform_Factory( $this->logger );
		$platform         = $platform_factory->get_platform();

		return $platform;
	}

	/**
	 * Send order notes to client?
	 *
	 * @return bool
	 */
	public function order_note_send_to_client() {
		if ( 'yes' === $this->shipping_service->get_option_from_shipping_method( 'email_add_tracking_number' ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Add order note - shipment created.
	 */
	public function add_order_note_shipment_created() {
		$order = $this->get_order();
		$order->add_order_note(
			sprintf(
				// Translators: service name and shipment number.
				__( '%1$s Shipment %2$s was created.', 'flexible-shipping' ),
				$this->shipping_service->get_name(),
				$this->get_tracking_number()
			),
			$this->order_note_send_to_client()
		);
	}

	/**
	 * Add order note - shipment created.
	 *
	 * @param string $tracking_number Tracking number.
	 */
	private function add_order_note_shipment_canceled( $tracking_number ) {
		$order = $this->get_order();
		$order->add_order_note(
			sprintf(
				// Translators: service name.
				__( '%1$s Shipment %2$s was canceled.', 'flexible-shipping' ),
				$this->shipping_service->get_name(),
				$tracking_number
			),
			$this->order_note_send_to_client()
		);
	}

	/**
	 * Cancel shipment on platform.
	 *
	 * @throws WPDesk_Flexible_Shipping_Cancel_Shipment_Exception Cancel Shipment Exception.
	 */
	public function cancel_shipment() {
		$tracking_number = $this->get_tracking_number();
		$platform        = $this->get_platform();
		$response        = $platform->requestPostCancel( $this->get_saas_shipment_id(), $this->shipping_service->get_id() );
		if ( $response->isError() ) {
			$error_body = $response->getResponseErrorBody();
			$message    = __( 'UPS! Something is wrong!', 'flexible-shipping' );

			if ( isset( $error_body['detail'] ) ) {
				$message = $error_body['detail'];
			}
			if ( isset( $error_body['message'] ) ) {
				$message = $error_body['message'];
			}
			throw new WPDesk_Flexible_Shipping_Cancel_Shipment_Exception( $message );
		}

		$this->set_saas_shipment_id( '' );
		$this->delete_meta( self::META_SAAS_MESSAGE );
		$this->delete_meta( self::META_SAAS_TRACKING_URL );
		$this->delete_meta( self::META_SAAS_TRACKING_ID );
		$this->delete_meta( self::META_SAAS_SHIPMENT_COST );
		$this->update_status( 'fs-new' );
		$this->save();

		$this->add_order_note_shipment_canceled( $tracking_number );

		do_action( 'flexible_shipping_shipment_canceled', $this );
	}

	/**
	 * Send shipment to platform.
	 *
	 * @return WPDesk\SaasPlatformClient\Model\Shipment\ShipmentResponse
	 * @throws WPDesk_Flexible_Shipping_Send_Shipment_Exception Send Shipment Exception.
	 */
	public function send_shipment() {
		$send_shipment = new WPDesk_Flexible_Shipping_Shipment_Saas_Send( $this );
		return $send_shipment->send();
	}

	/**
	 * Metabox can be saved?
	 *
	 * @return bool
	 */
	public function matabox_can_be_saved() {
		if ( $this->get_status() === 'fs-new' && $this->get_status() === 'fs-failed' ) {
			return false;
		}
		return true;
	}

	/**
	 * Handle AJAX requests for shipment.
	 *
	 * @param string $action Action.
	 * @param array  $data Data.
	 *
	 * @return array
	 *
	 * @throws WPDesk_Flexible_Shipping_Send_Shipment_Exception Send Shipment Exception.
	 * @throws RuntimeException Runtime Exception.
	 */
	public function ajax_request( $action, $data ) {
		$saas_ajax = new WPDesk_Flexible_Shipping_Shipment_Saas_Ajax( $this, self::$saas_platform_links );
		return $saas_ajax->ajax_request( $action, $data );
	}

	/**
	 * Get label URL.
	 *
	 * @return null|string
	 * Returns URL for label
	 */
	public function get_label_url() {
		if ( in_array( $this->get_status(), array( 'fs-new', 'fs-failed' ), true ) ) {
			return null;
		}
		$label_url = '?flexible_shipping_get_label=' . $this->get_id() . '&nonce=' . wp_create_nonce( 'flexible_shipping_get_label' );
		return site_url( $label_url );
	}

	/**
	 * Get error message.
	 *
	 * @return null|string
	 */
	public function get_error_message() {
		$saas_message = $this->get_meta( self::META_SAAS_MESSAGE );
		if ( ! empty( $saas_message ) ) {
			$message_formatter = new WPDesk_Flexible_Shipping_Shipment_Saas_Message_Formatter(
				$saas_message,
				$this->get_shipping_service_name()
			);
			return $message_formatter->format_to_html();
		}
		return '';
	}

	/**
	 * Get tracking number.
	 *
	 * @return string
	 */
	public function get_tracking_number() {
		return $this->get_meta( self::META_SAAS_TRACKING_ID );
	}

	/**
	 * Set tracking number.
	 *
	 * @param string $saas_tracking_id Tracking ID from platform.
	 */
	public function set_tracking_number( $saas_tracking_id ) {
		$this->set_meta( self::META_SAAS_TRACKING_ID, $saas_tracking_id );
	}

	/**
	 * Get tracking URL.
	 *
	 * @return string
	 */
	public function get_tracking_url() {
		return $this->get_meta( self::META_SAAS_TRACKING_URL );
	}

	/**
	 * Set tracking URL.
	 *
	 * @param string $saas_tracking_url Tracking URL from platform.
	 */
	public function set_tracking_url( $saas_tracking_url ) {
		$this->set_meta( self::META_SAAS_TRACKING_URL, $saas_tracking_url );
	}

	/**
	 * Set shipment cost.
	 *
	 * @param \WPDesk\SaasPlatformClient\Model\Shipment\ClientMoney $shipment_cost Shipment cost.
	 */
	public function set_shipment_cost( $shipment_cost ) {
		$shipment_cost_array = array(
			'amount'   => $shipment_cost->getAmount(),
			'currency' => $shipment_cost->getCurrency(),
		);
		$this->set_meta( self::META_SAAS_SHIPMENT_COST, $shipment_cost_array );
	}

	/**
	 * Get shipment cost as Money.
	 *
	 * @return \Money\Money|null
	 */
	public function get_shipment_cost_as_money() {
		$shipment_cost = $this->get_meta( self::META_SAAS_SHIPMENT_COST );
		if ( ! empty( $shipment_cost ) && is_array( $shipment_cost ) ) {
			return new Money\Money( $shipment_cost['amount'], new \Money\Currency( $shipment_cost['currency'] ) );
		}
		return null;
	}

	/**
	 * Get shipment cost amount.
	 */
	public function get_shipment_cost_amount() {
		$shipment_cost_as_money = $this->get_shipment_cost_as_money();
		$money_formatter        = new WPDesk_Flexible_Shipping_Money_Formatter( $shipment_cost_as_money );
		return $money_formatter->format();
	}

	/**
	 * Get shipment cost currency.
	 */
	public function get_shipment_cost_currency() {
		$shipment_cost_as_money = $this->get_shipment_cost_as_money();
		if ( ! empty( $shipment_cost_as_money ) ) {
			return $shipment_cost_as_money->getCurrency()->getCode();
		}
		return null;
	}

	/**
	 * Is label avaliable?
	 *
	 * @return bool
	 */
	public function label_avaliable() {
		if ( in_array( $this->get_status(), array( 'fs-confirmed', 'fs-manifest' ), true ) ) {
			return true;
		}
		return false;
	}

	/**
	 * Prepare label file name.
	 *
	 * @param string $extension File extension.
	 *
	 * @return string
	 */
	private function prepare_label_file_name( $extension ) {
		return sprintf(
			'label_%1$s_%2$s.%3$s',
			$this->get_shipping_service_name(),
			$this->get_tracking_number(),
			$extension
		);
	}

	/**
	 * Get label from platform.
	 *
	 * @return array
	 *
	 * @throws WPDesk_Flexible_Shipping_Get_Label_Exception Label exception.
	 */
	private function get_label_from_platform() {
		$platform       = $this->get_platform();
		$label_response = $platform->requestPostLabel( intval( $this->get_meta( '_saas_shipment_id' ) ), $this->shipping_service->get_id() );
		if ( $label_response->isError() ) {
			throw new WPDesk_Flexible_Shipping_Get_Label_Exception( $label_response->getResponseCode() . ' - ' . $label_response->getResponseErrorBody()['message'] );
		} else {
			$label      = $label_response->getLabel();
			$label_data = array(
				'label_format' => $label->getLabelContentExt(),
				'content'      => base64_decode( $label->getLabelContent() ),
				'file_name'    => $this->prepare_label_file_name( $label->getLabelContentExt() ),
			);
			return $label_data;
		}
	}

	/**
	 * Get label.
	 *
	 * @return array
	 * @throws WPDesk_Flexible_Shipping_Label_Not_Available_Exception Exception.
	 */
	public function get_label() {
		if ( ! $this->label_avaliable() ) {
			throw new WPDesk_Flexible_Shipping_Label_Not_Available_Exception( __( 'Label is not avaliable for this shipment.', 'flexible-shipping' ) );
		}
		return $this->get_label_from_platform();
	}

	/**
	 * Prepare args tracking data.
	 *
	 * @return array
	 */
	private function prepare_args_tracking_data() {
		return array(
			'shipments' => array(
				array(
					'tracking_url'    => $this->get_tracking_url(),
					'tracking_number' => $this->get_tracking_number(),
				),
			),
		);
	}

	/**
	 * Display checkout fields.
	 *
	 * @param string $folder Template folder.
	 */
	private function maybe_display_checkout_fields( $folder ) {
		$checkout_fields = new WPDesk_Flexible_Shipping_SaaS_Shipping_Checkout_Fields_Shipment(
			$this->shipping_service,
			$this->shipping_service->get_collection_points(),
			self::$renderer,
			$folder
		);
		$checkout_fields->maybe_display_fields_for_shipment( $this );
	}

	/**
	 * Echo after order table.
	 */
	public function get_after_order_table() {
		if ( $this->label_avaliable() ) {
			echo self::$renderer->render( 'myaccount/after_order_table', $this->prepare_args_tracking_data() ); // phpcs:ignore
		}
		$this->maybe_display_checkout_fields( 'myaccount' );
	}

	/**
	 * Echo email after order table.
	 */
	public function get_email_after_order_table() {
		if ( 'yes' === $this->shipping_service->get_option_from_shipping_method( 'email_add_tracking_number' )
			&& $this->label_avaliable()
		) {
			echo self::$renderer->render( 'email/after_order_table', $this->prepare_args_tracking_data() ); // phpcs:ignore
		}
		$this->maybe_display_checkout_fields( 'email' );
	}

	/**
	 * Get SaaS shipment ID (from platform).
	 *
	 * @return array|null|string
	 */
	public function get_saas_shipment_id() {
		return $this->get_meta( self::META_SAAS_SHIPMENT_ID );
	}

	/**
	 * Set SaaS shipment ID.
	 *
	 * @param string $shipment_id Shipment ID.
	 */
	public function set_saas_shipment_id( $shipment_id ) {
		$this->set_meta( self::META_SAAS_SHIPMENT_ID, $shipment_id );
	}

	/**
	 * Get shipping service.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Service
	 */
	public function get_shipping_service() {
		return $this->shipping_service;
	}

	/**
	 * Creates shipment.
	 *
	 * @throws Exception Exception.
	 */
	public function api_create() {
		$this->send_shipment();
	}

	/**
	 * Get shipping method.
	 *
	 * @return WPDesk_Flexible_Shipping_SaaS_Shipping_Service_Settings
	 */
	public function get_shipping_method() {
		return $this->shipping_method;
	}

	/**
	 * Was fallback?
	 *
	 * @return bool
	 */
	public function was_fallback() {
		return '1' === $this->get_meta( self::FALLBACK, '0' );
	}

	/**
	 * Get fallback message.
	 *
	 * @return string
	 */
	public function get_fallback_message() {
		return $this->get_meta( self::FALLBACK_MESSAGE, '' );
	}

	/**
	 * Get rate type.
	 *
	 * @return string
	 */
	public function get_rate_type() {
		return $this->get_meta( self::RATE_TYPE, self::NOT_SET );
	}

	/**
	 * Get checkout service type.
	 *
	 * @return string
	 */
	public function get_checkout_service_type() {
		return $this->get_meta( self::CHECKOUT_SERVICE_TYPE, self::NOT_SET );
	}

	/**
	 * Get checkout service name.
	 *
	 * @return string
	 */
	public function get_checkout_service_name() {
		return $this->get_meta( self::CHECKOUT_SERVICE_NAME, self::NOT_SET );
	}

}
