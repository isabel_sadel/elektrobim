<?php
/**
 * @package Flexible Shipping
 * @var string $type_field_id Type field ID.
 * @var string $attachment_field_id Attachment field ID.
 * @var string $field_label Field label.
 * @var array  $attachments Documents.
 * @var array  $attachment_types Allowed document types.
 * @var string $shipment_id Shipment ID.
 */

$type_value          = '';
$attachment_id_value = '';
?>
<?php $show_send_button = false; ?>
<div class="flexible-shipping-shipment-attachments">
	<hr />
	<h4><?php echo $field_label; ?></h4>
	<div class="flexible-shipping-shipment-attachments-content">
		<table>
			<thead>
				<tr>
					<td><?php _e( 'Type', 'flexible-shipping' ); ?></td>
					<td><?php _e( 'Filename', 'flexible-shipping' ); ?></td>
					<td><?php _e( 'Sent', 'flexible-shipping' ); ?></td>
					<td></td>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $attachments as $attachment ) : ?>
					<?php $show_send_button = true; ?>
					<?php $type_value = $attachment['type']; ?>
					<?php $attachment_id_value = $attachment['attachment_id']; ?>
					<?php $saas_attachment_id_value = $attachment['saas_attachment_id']; ?>
					<?php $attachment_url = ''; ?>
					<?php $filename_value = $attachment['filename']; ?>
					<?php $sent_value = empty( $attachment['sent'] ) ? __( 'No', 'flexible-shipping' ) : __( 'Yes', 'flexible-shipping' ); ?>
					<?php include 'order-metabox-single-attachment.php'; ?>
				<?php endforeach; ?>
			</tbody>
		</table>
		<?php if ( $show_send_button ) : ?>
			<button
				data-shipment_id="<?php echo esc_attr( $shipment_id ); ?>"
				class="fs-saas-button-send-attachments button"
			><?php _e( 'Send', 'flexible-shipping' ); ?>
			</button>
		<?php endif; ?>
	</div><br />
	<?php
	$args = array(
		'label'   => '',
		'id'      => $type_field_id,
		'type'    => 'select',
		'class'   => 'flexible-shipping-shipment-attachments-type',
		'options' => $attachment_types,
		'value'   => $type_value,
	);
	woocommerce_wp_select( $args );

	$args = array(
		'id'    => $attachment_field_id,
		'class' => 'flexible-shipping-shipment-attachments-attachment-id',
		'type'  => 'hidden',
		'value' => $attachment_id_value,
	);
	woocommerce_wp_hidden_input( $args );
	?>
	<button
		data-shipment_id="<?php echo esc_attr( $shipment_id ); ?>"
		class="fs-saas-button-add-attachment button"
	><?php _e( 'Add', 'flexible-shipping' ); ?>
	</button>
	<div class="flexible_shipping_shipment_attachment_message" style="display:none;"></div>
	<hr />
</div>
