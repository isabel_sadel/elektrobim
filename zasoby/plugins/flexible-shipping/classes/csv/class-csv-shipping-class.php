<?php

/**
 * Shipping class in CSV.
 */
class WPDesk_Flexible_Shipping_Csv_Shipping_Class {

	const ENCODED_COMMA = '&#44;';

	/**
	 * Shipping class name.
	 *
	 * @var string
	 */
	private $shipping_class_name;

	/**
	 * WPDesk_Flexible_Shipping_Csv_Shipping_Class constructor.
	 *
	 * @param string $shipping_class_name Shipping class name.
	 */
	public function __construct( $shipping_class_name ) {
		$this->shipping_class_name = $shipping_class_name;
	}

	/**
	 * Create from encoded shipping class name.
	 *
	 * @param string $encoded_shipping_class_name Encoded shipping class name.
	 *
	 * @return WPDesk_Flexible_Shipping_Csv_Shipping_Class
	 */
	public static function create_from_encoded_shipping_class_name( $encoded_shipping_class_name ) {
		return new self( str_replace( self::ENCODED_COMMA, ',', $encoded_shipping_class_name ) );
	}

	/**
	 * Get shipping class name.
	 *
	 * @return string
	 */
	public function get_shipping_class_name() {
		return $this->shipping_class_name;
	}

	/**
	 * Get encoded shipping class name.
	 *
	 * @return string
	 */
	public function get_encoded_shipping_class_name() {
		return str_replace( ',', self::ENCODED_COMMA, $this->shipping_class_name );
	}

}
