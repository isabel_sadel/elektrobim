<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

if ( ! class_exists( 'WPDesk_Flexible_Shipping_Menu' ) ) {

	/**
	 * Class WPDesk_Flexible_Shipping_Menu
	 */
	class WPDesk_Flexible_Shipping_Menu implements \WPDesk\PluginBuilder\Plugin\HookablePluginDependant {

		use \WPDesk\PluginBuilder\Plugin\PluginAccess;

		const MENU_POSITION_AFTER_PRODUCTS = 59;
		const HOOK_PRIORITY_HIGH           = 1;

		/**
		 * Hooks.
		 */
		public function hooks() {
			add_action( 'admin_menu', array( $this, 'admin_menu_action' ), self::HOOK_PRIORITY_HIGH );
		}

		/**
		 * Admin menu action.
		 */
		public function admin_menu_action() {
			//add_menu_page( __( 'Settings', 'flexible-shipping' ), __( 'Flexible Shipping', 'flexible-shipping' ), 'manage_woocommerce', 'flexible-shipping', array( $this, 'settings_menu' ), null, self::MENU_POSITION_AFTER_PRODUCTS );
		}

		/**
		 * Settings menu;
		 */
		public function settings_menu() {
			echo '<div class="wrap"><h2>Settings</h2></div>';
		}


	}

}
