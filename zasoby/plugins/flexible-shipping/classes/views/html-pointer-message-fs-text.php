<?php
/**
 * @var string $shipping_zones_url
 * @var string $shipping_methods_url
 */
echo sprintf(
	__( 'To add first Flexible Shipping method go to %1$sShipping zones%2$s and add Flexible Shipping to a shipping zone. More info about %3$sshipping methods%4$s.', 'flexible-shipping' ),
	'<a href="' . $shipping_zones_url . '">',
	'</a>',
	'<a target="_blank" href="' . $shipping_methods_url . '">',
	'</a>'
);
