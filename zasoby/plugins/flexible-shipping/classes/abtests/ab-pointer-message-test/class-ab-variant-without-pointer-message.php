<?php

use WPDesk\ABTesting\ABVariant;

class WPDesk_Flexible_Shipping_AB_Variant_Without_Pointer_Message implements ABVariant {

	/**
	 * Define a new value for the generated variant.
	 *
	 * @var int
	 */
	const VARIANT_ID = 9;

	/**
	 * Is on?
	 *
	 * @param string $functionality Functionality.
	 *
	 * @return bool
	 */
	public function is_on( $functionality ) {
		return ! WPDesk_Flexible_Shipping_AB_Pointer_Message_Test::FS_WITH_POINTER_MESSAGE_VIDEO === $functionality;
	}

	/**
	 * Get variant ID.
	 *
	 * @return int|string
	 */
	public function get_variant_id() {
		return self::VARIANT_ID;
	}
}
