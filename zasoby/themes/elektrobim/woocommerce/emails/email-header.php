<?php
/**
 * Email Header
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-header.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=<?php bloginfo( 'charset' ); ?>" />
		<title><?php echo get_bloginfo( 'name', 'display' ); ?></title>
	</head>
	<body <?php echo is_rtl() ? 'rightmargin' : 'leftmargin'; ?>="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
		<div id="wrapper" dir="<?php echo is_rtl() ? 'rtl' : 'ltr'; ?>">
			<table border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
				<tr>
					<td align="center" valign="top">
						<div id="template_header_image">

						</div>
						<table border="0" cellpadding="0" cellspacing="0" width="800" id="template_container">
							<tr>
								<td align="center" valign="top" style="padding: 16px 18px;">
									<!-- Header -->
									<table border="0" cellpadding="0" cellspacing="0" width="800">
										<tr>
											<td>
                                                <?php
                                                if ( $img = get_option( 'woocommerce_email_header_image' ) ) {
                                                    echo '<a href="https://www.elektrobim.pl" target="_blank" rel="noreferrer"><img src="' . esc_url( $img ) . '" alt="' . get_bloginfo( 'name', 'display' ) . '" /></a>';
                                                }
                                                ?>
												<!--<h1><?php //echo $email_heading; ?></h1> -->
											</td>
										</tr>
									</table>
									<!-- End Header -->
								</td>
							</tr>
                            <tr style="font-family: Arial, sans-serif; line-height: 1.3em">
                                <td class="top_nav_holder" style="font-family: Arial, sans-serif; line-height: 1.3em; background: #fafafa; border-bottom: 1px solid #f5f5f5" valign="top" align="center">
                                    <table class="top_nav" style="font-family: Arial, sans-serif; line-height: 1.3em; color: #666666" width="auto" cellspacing="0" cellpadding="0" border="0">
                                        <tbody>
                                        <tr style="font-family: Arial, sans-serif; line-height: 1.3em">
                                            <td class="nav-spacer-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 6px">&nbsp; </td>
                                            <td class="nav-text-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 12px"> <a href="https://www.elektrobim.pl" style="color: #666666; text-decoration: none" target="_blank" rel="noreferrer"> Home </a>
                                            </td>
                                            <td class="nav-text-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 12px"> <a href="https://www.elektrobim.pl/moje-konto/" style="color: #666666; text-decoration: none" target="_blank" rel="noreferrer"> Logowanie
                                                </a> </td>
                                            <td class="nav-text-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 12px"> <a href="https://www.elektrobim.pl/sklep/" style="color: #666666; text-decoration: none" target="_blank" rel="noreferrer"> Sklep </a>
                                            </td>
                                            <td class="nav-text-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 12px"> <a href="https://www.elektrobim.pl/pomoc/" style="color: #666666; text-decoration: none" target="_blank" rel="noreferrer"> Pomoc </a>
                                            </td>
                                            <td class="nav-text-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 12px"> <a href="https://www.elektrobim.pl/zwroty/" style="color: #666666; text-decoration: none" target="_blank" rel="noreferrer"> Zwroty </a>
                                            </td>
                                            <td class="nav-text-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 12px"> <a href="https://www.elektrobim.pl/reklamacje/" style="color: #666666; text-decoration: none" target="_blank" rel="noreferrer"> Reklamacje
                                                </a> </td>
                                            <td class="nav-spacer-block" style="font-family: Arial, sans-serif; line-height: 1.3em; height: 38px; font-size: 14px; padding: 8px 6px">&nbsp; </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
							<tr>
								<td align="center" valign="top">
									<!-- Body -->
									<table border="0" cellpadding="0" cellspacing="0" width="600" id="template_body">
										<tr>
											<td valign="top" id="body_content">
												<!-- Content -->
												<table border="0" cellpadding="20" cellspacing="0" width="100%">
													<tr>
														<td valign="top">
															<div id="body_content_inner">
