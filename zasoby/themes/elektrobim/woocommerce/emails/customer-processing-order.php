<?php
/**
 * Customer processing order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-processing-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>
    <div class="top_heading" style="font-family: Arial, sans-serif; font-size: 22px; text-align: left; font-weight: bold">
        <p style="margin: .6em 0">Przyjęliśmy Twoje zamówienie</p>
    </div>
    <p style="margin: .6em 0">Dziękujemy!
        Twoje zamówienie nr <span class="ec_shortcode ec_order"><a href="https://www.elektrobim.pl/moje-konto/view-order/241673/" style="color: #666666; text-decoration: underline" target="_blank" rel="noreferrer"><?php echo $order->get_order_number(); ?></a>
<span class="ec_datetime">(<?php echo wc_format_datetime( $order->get_date_created() ); ?>)</span></span>
zostało przyjęte i zostanie zrealizowane w ciągu 24 godzin (w dni robocze)
<?php
if($order->get_payment_method() == 'bacs') { ?>
    <strong>po zaksięgowaniu wpłaty</strong>
<?php }
?>
.
</p>

<?php
if($order->get_payment_method() == 'payu') { ?>
    <p> W przypadku problemów z płatnością możesz złożyć ponowne zamówienie lub wykonać przelew na poniższe dane:<br/>
        PPHU ELEKTROBIM Konrad Kunka<br/>
        Numer konta: 38 1050 1520 1000 0090 9749 8902<br/>
        Nazwa banku: ING Bank Śląski S.A.<br/>
        Tytuł: prosimy o wpisanie numeru zamówienia
    </p>
<?php } ?>


<p style="margin: .6em 0">Informacje o przebiegu zamówienia będziemy wysyłali na bieżąco drogą mailową.</p>

<p>
    <u>Masz pytania? Skontaktuj się z odpowiednim działem:</u><br>
    <strong>Serwis:</strong> <a class="moz-txt-link-abbreviated" href="mailto:serwis@elektrobim.pl" onclick="return rcmail.command('compose','serwis@elektrobim.pl',this)" rel="noreferrer">serwis@elektrobim.pl</a>
    <br>
    <strong>Księgowość</strong>:
    <a class="moz-txt-link-abbreviated" href="mailto:kontakt@elektrobim.pl" onclick="return rcmail.command('compose','kontakt@elektrobim.pl',this)" rel="noreferrer">kontakt@elektrobim.pl</a>
    <br>
    <strong>Zamówienia hurtowe</strong>:
    <a class="moz-txt-link-abbreviated" href="mailto:elektrobim@elektrobim.pl" onclick="return rcmail.command('compose','elektrobim@elektrobim.pl',this)" rel="noreferrer">elektrobim@elektrobim.pl</a>
    <br>
    <strong>Przesyłki</strong>:
    <a class="moz-txt-link-abbreviated" href="mailto:magazyn@elektrobim.pl" onclick="return rcmail.command('compose','magazyn@elektrobim.pl',this)" rel="noreferrer">magazyn@elektrobim.pl</a>
    <br>
    <strong>Kontakt telefoniczny</strong>:
    <a class="moz-txt-link-abbreviated" href="tel:91 817 14 69" rel="noreferrer" style="text-decoration: none;">91 817 14 69</a>
</p>
<?php
/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */
?>
<?php do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
