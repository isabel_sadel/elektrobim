<?php
/**
 * Customer completed order email
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/customer-completed-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
 * @hooked WC_Emails::email_header() Output the email header
 */
do_action( 'woocommerce_email_header', $email_heading, $email ); ?>

<?php /* translators: %s: Customer first name */ ?>
<div class="top_heading" style="font-family: Arial, sans-serif; font-size: 22px; text-align: left; font-weight: bold">
    <p style="margin: .6em 0">Zamówienie zrealizowane</p>
</div>
<p><?php printf( esc_html__( 'Hi %s,', 'woocommerce' ), esc_html( $order->get_billing_first_name() ) ); ?></p>
<?php /* translators: %s: Site title */ ?>
<p>
    Przesyłka wkrótce zostanie odebrana przez kuriera, a na numer telefonu podany w zamówieniu powinien przyjść SMS z numerem do śledzenia paczki.
</p>
<p><strong>Pamiętaj aby sprawdzić stan przesyłki oraz kompletność zamówienia w obecności kuriera dostarczającego paczkę.</strong></p>


/*
 * @hooked WC_Emails::order_details() Shows the order details table.
 * @hooked WC_Structured_Data::generate_order_data() Generates structured data.
 * @hooked WC_Structured_Data::output_structured_data() Outputs structured data.
 * @since 2.5.0
 */

<p>
    <u>Masz pytania? Skontaktuj się z odpowiednim działem:</u><br>
    <strong>Serwis:</strong> <a class="moz-txt-link-abbreviated" href="mailto:serwis@elektrobim.pl" onclick="return rcmail.command('compose','serwis@elektrobim.pl',this)" rel="noreferrer">serwis@elektrobim.pl</a>
    <br>
    <strong>Księgowość</strong>:
    <a class="moz-txt-link-abbreviated" href="mailto:kontakt@elektrobim.pl" onclick="return rcmail.command('compose','kontakt@elektrobim.pl',this)" rel="noreferrer">kontakt@elektrobim.pl</a>
    <br>
    <strong>Zamówienia hurtowe</strong>:
    <a class="moz-txt-link-abbreviated" href="mailto:elektrobim@elektrobim.pl" onclick="return rcmail.command('compose','elektrobim@elektrobim.pl',this)" rel="noreferrer">elektrobim@elektrobim.pl</a>
    <br>
    <strong>Przesyłki</strong>:
    <a class="moz-txt-link-abbreviated" href="mailto:magazyn@elektrobim.pl" onclick="return rcmail.command('compose','magazyn@elektrobim.pl',this)" rel="noreferrer">magazyn@elektrobim.pl</a>
    <br>
    <strong>Kontakt telefoniczny</strong>:
    <a class="moz-txt-link-abbreviated" href="tel:91 817 14 69" rel="noreferrer" style="text-decoration: none;">91 817 14 69</a>
</p>
<?php do_action( 'woocommerce_email_order_details', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::order_meta() Shows order meta data.
 */
do_action( 'woocommerce_email_order_meta', $order, $sent_to_admin, $plain_text, $email );

/*
 * @hooked WC_Emails::customer_details() Shows customer details
 * @hooked WC_Emails::email_address() Shows email address
 */
do_action( 'woocommerce_email_customer_details', $order, $sent_to_admin, $plain_text, $email );

?>
<p>
<?php esc_html_e( 'Thanks for shopping with us.', 'woocommerce' ); ?>
</p>
<?php

/*
 * @hooked WC_Emails::email_footer() Output the email footer
 */
do_action( 'woocommerce_email_footer', $email );
