<?php
/**
 * Order details table shown in emails.
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/emails/email-order-details.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates/Emails
 * @version 3.3.1
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

$text_align = is_rtl() ? 'right' : 'left';
?>

<h2>
	<?php
	if ( $sent_to_admin ) {
		$before = '<a class="link" href="' . esc_url( $order->get_edit_order_url() ) . '">';
		$after  = '</a>';
	} else {
		$before = '';
		$after  = '';
	} ?>

    <table class="special-title-holder" style="font-family: Arial, sans-serif; line-height: 1.3em; color: #666666" width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody>
        <tr style="font-family: Arial, sans-serif; line-height: 1.3em">
          <td style="font-family: Arial, sans-serif; line-height: 1.3em; font-size: 1px">
              <table style="font-family: Arial, sans-serif; line-height: 1.3em; color: #666666" width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody>
                  <tr style="font-family: Arial, sans-serif; line-height: 1.3em">
                      <td style="font-family: Arial, sans-serif; line-height: 1.3em; font-size: 1px" width="50%">
                          <table style="font-family: Arial, sans-serif; line-height: 1.3em; color: #666666;" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%">
                              <tbody>
                              <tr style="font-family: Arial, sans-serif; line-height: 1.3em; height: 50%;" height="50%">
                                  <td style="font-family: Arial, sans-serif; line-height: 1.3em; font-size: 1px;">&nbsp;</td>
                              </tr>
                              <tr style="font-family: Arial, sans-serif; line-height: 1.3em; height: 50%;" height="50%">
                                  <td style="font-family: Arial, sans-serif; line-height: 1.3em; font-size: 1px; border-top: 1px solid #c6c6c6;" class="gwpa1eb2765_header_content_h2_border">
                                      <br>
                                  </td>
                              </tr>
                              </tbody>
                          </table>
                      </td>
                      <td class="header_content_h2" style="font-family: Arial,sans-serif; line-height: 1.3em; font-size: 14px; font-weight: normal; color: #666666; text-decoration: none; text-transform: uppercase; margin: 0; padding: 0px 5px; white-space: nowrap; padding-right: 6px; padding-left: 6px" width="1%">
                      Szczegóły&nbsp;zamówienia
                      </td>
                      <td style="font-family: Arial, sans-serif; line-height: 1.3em; font-size: 1px" width="50%">
                          <table style="font-family: Arial, sans-serif; line-height: 1.3em; color: #666666" width="100%" height="100%" cellspacing="0" cellpadding="0" border="0">
                              <tbody>
                              <tr style="font-family: Arial, sans-serif; line-height: 1.3em; height: 50%" height="50%">
                                  <td style="font-family: Arial, sans-serif; line-height: 1.3em; font-size: 1px">&nbsp;</td>
                              </tr>
                              <tr style="font-family: Arial, sans-serif; line-height: 1.3em; height: 50%" height="50%">
                                  <td class="header_content_h2_border" style="font-family: Arial, sans-serif; line-height: 1.3em; font-size: 1px; border-top: 1px solid #c6c6c6">
                                      <br>
                                  </td>
                              </tr>
                              </tbody>
                          </table>
                      </td>
                  </tr>
                  </tbody>
              </table>
          </td>
        </tr>
      </tbody>
    </table>
	<?php
        echo wp_kses_post( $before . '<span style="float:left;padding-bottom:30px;"><span class="color_span">' . sprintf( __( 'Order number:', 'woocommerce' ) . '</span> %s</span>' . $after . ' <span style="float:right;padding-bottom:30px;"><span class="color_span">Data zamówienia:</span> <time datetime="%s">%s</time></span><span style="clear:both;"></span>', $order->get_order_number(), $order->get_date_created()->format( 'c' ), wc_format_datetime( $order->get_date_created() ) ) );
	?>
</h2>

<div style="margin-bottom: 40px;">
	<table class="td" cellspacing="0" cellpadding="6" style="width: 800px; font-family: 'Helvetica Neue', Helvetica, Roboto, Arial, sans-serif;" border="1">
		<thead>
			<tr>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Product', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Quantity', 'woocommerce' ); ?></th>
				<th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Price', 'woocommerce' ); ?></th>
                <?php if( $order->get_used_coupons() ) { ?>
                <th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;">Rabat</th>
                <th class="td" scope="col" style="text-align:<?php echo esc_attr( $text_align ); ?>;">Cena po rabacie</th>
                <?php } ?>
			</tr>
		</thead>
		<tbody>
			<?php
			echo wc_get_email_order_items( $order, array( // WPCS: XSS ok.
				'show_sku'      => $sent_to_admin,
				'show_image'    => true,
				'image_size'    => array( 32, 32 ),
				'plain_text'    => $plain_text,
				'sent_to_admin' => $sent_to_admin,
			) );
			?>
		</tbody>
		<tfoot>
			<?php
			$totals = $order->get_order_item_totals();

			if ( $totals ) {
				$i = 0;
				foreach ( $totals as $total ) {
					$i++;
					?>
					<tr>
						<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( $total['label'] ); ?></th>
						<td class="td" <?php if( $order->get_used_coupons() ) { ?> colspan="3" <?php } ?> style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( $total['value'] ); ?></td>
					</tr>
					<?php
				}
			}
			if ( $order->get_customer_note() ) {
				?>
				<tr>
					<th class="td" scope="row" colspan="2" style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php esc_html_e( 'Note:', 'woocommerce' ); ?></th>
					<td class="td" <?php if( $order->get_used_coupons() ) { ?> colspan="3" <?php } ?> style="text-align:<?php echo esc_attr( $text_align ); ?>;"><?php echo wp_kses_post( wptexturize( $order->get_customer_note() ) ); ?></td>
				</tr>
				<?php
			}
			?>
		</tfoot>
	</table>
</div>

<?php do_action( 'woocommerce_email_after_order_table', $order, $sent_to_admin, $plain_text, $email ); ?>
