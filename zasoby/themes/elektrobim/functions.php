<?php

function custom_enqueue_scripts() {
    // stylesheets
    wp_enqueue_style(
        'custom-default',
        get_stylesheet_directory_uri() . '/style.css',
        array(),
        '1.0.1'
    );
    wp_enqueue_script( 'elektrobim_whcookies_js', get_stylesheet_directory_uri() . '/js/whcookies.js', array( 'jquery' ), '1.0.0', true );
}
add_action( 'wp_enqueue_scripts', 'custom_enqueue_scripts', 100 );

function use_parent_theme_stylesheet() {
    // Use the parent theme's stylesheet
    return get_template_directory_uri() . '/style.css';
}
add_filter('stylesheet_uri', 'use_parent_theme_stylesheet');

// Utility function, to display BACS accounts details
function get_bacs_account_details_html( $order_number, $echo = true, $type = 'list' ) {

    ob_start();

    $gateway    = new WC_Gateway_BACS();
    $country    = WC()->countries->get_base_country();
    $locale     = $gateway->get_country_locale();
    $bacs_info  = get_option( 'woocommerce_bacs_accounts');

    // Get sortcode label in the $locale array and use appropriate one
    $sort_code_label = isset( $locale[ $country ]['sortcode']['label'] ) ? $locale[ $country ]['sortcode']['label'] : __( 'Sort code', 'woocommerce' );

    if( $type = 'list' ) :
        ?>
        <div class="woocommerce-bacs-bank-details">
            <p class="wc-bacs-bank-details-heading">Dane do przelewu</p>
            <?php
            $i = -1;
            if ( $bacs_info ) : foreach ( $bacs_info as $account ) :
                $i++;

                $account_name   = esc_attr( wp_unslash( $account['account_name'] ) );
                $bank_name      = esc_attr( wp_unslash( $account['bank_name'] ) );
                $account_number = esc_attr( $account['account_number'] );
                $sort_code      = esc_attr( $account['sort_code'] );
                $iban_code      = esc_attr( $account['iban'] );
                $bic_code       = esc_attr( $account['bic'] );
                ?>
                <ul class="wc-bacs-bank-details order_details bacs_details">
                    <?php if(!empty($account_name)) {?>
                        <li class="">Odbiorca: <strong><?php echo $account_name; ?></strong></li>
                    <?php } ?>
                    <?php if(!empty($bank_name)) {?>
                        <li class="bank_name">Bank: <strong><?php echo $bank_name; ?></strong></li>
                    <?php } ?>
                    <?php if(!empty($account_number)) {?>
                        <li class="account_number">Numer konta: <strong><?php echo $account_number; ?></strong></li>
                    <?php } ?>
                    <?php if(!empty($sort_code)) {?>
                        <li class="sort_code"><?php echo $sort_code_label; ?>: <strong><?php echo $sort_code; ?></strong></li>
                    <?php } ?>
                    <?php if(!empty($iban_code) && !empty($order_number))  {?>
                        <li class="iban">Tytuł: <strong><?php echo $iban_code; ?> <?php echo $order_number; ?></strong></li>
                    <?php } ?>
                    <?php if(!empty($bic_code)) {?>
                        <li class="bic">BIC: <strong><?php echo $bic_code; ?></strong></li>
                    <?php } ?>
                </ul>
            <?php endforeach; endif; ?>
        </div>
    <?php
    else :
        ?>
        <h2><?php _e( 'Account details', 'woocommerce' ); ?>:</h2>
        <table class="widefat wc_input_table" cellspacing="0">
            <thead>
            <tr>
                <th><?php _e( 'Account name', 'woocommerce' ); ?></th>
                <th><?php _e( 'Account number', 'woocommerce' ); ?></th>
                <th><?php _e( 'Bank name', 'woocommerce' ); ?></th>
                <th><?php echo $sort_code_label; ?></th>
                <th><?php _e( 'IBAN', 'woocommerce' ); ?></th>
                <th><?php _e( 'BIC / Swift', 'woocommerce' ); ?></th>
            </tr>
            </thead>
            <tbody class="accounts">
            <?php
            $i = -1;
            if ( $bacs_info ) {
                foreach ( $bacs_info as $account ) {
                    $i++;

                    echo '<tr class="account">
                        <td>' . esc_attr( wp_unslash( $account['account_name'] ) ) . '</td>
                        <td>' . esc_attr( $account['account_number'] ) . '</td>
                        <td>' . esc_attr( wp_unslash( $account['bank_name'] ) ) . '</td>
                        <td>' . esc_attr( $account['sort_code'] ) . '</td>
                        <td>' . esc_attr( $account['iban'] ) . '</td>
                        <td>' . esc_attr( $account['bic'] ) . '</td>
                    </tr>';
                }
            }
            ?>
            </tbody>
        </table>
    <?php
    endif;
    $output = ob_get_clean();

    if ( $echo )
        echo $output;
    else
        return $output;
}