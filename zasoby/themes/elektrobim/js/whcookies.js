﻿/*
 * Skrypt wyświetlający okienko z informacją o wykorzystaniu ciasteczek (cookies)
 * 
 * Więcej informacji: http://webhelp.pl/artykuly/okienko-z-informacja-o-ciasteczkach-cookies/
 * 
 */

function WHCreateCookie(name, value, days) {
    var date = new Date();
    date.setTime(date.getTime() + (days*24*60*60*1000));
    var expires = "; expires=" + date.toGMTString();
	document.cookie = name+"="+value+expires+"; path=/";
}
function WHReadCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for(var i=0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
	}
	return null;
}

window.onload = WHCheckCookies;

function WHCheckCookies() {
    if(WHReadCookie('cookies_accepted') != 'T') {
        var message_container = document.createElement('div');
        message_container.id = 'cookies-message-container';
        var html_code = '<div id="cookies-message" style="padding: 5px 0px; font-size: 14px; line-height: 22px; border-bottom: 1px solid rgb(211, 208, 208); text-align: center; position: fixed; bottom: 0px; background-color: #efefef; width: 100%; z-index: 9999; border-top: 5px solid #0389e5; color: #333;"><p style="margin:auto;width:73%;">' +
            'Nasz Sklep internetowy używa plików cookies do prawidłowego działania strony. Korzystanie ze Sklepu bez zmiany ustawień dla plików cookies oznacza, że będą one zapisywane w pamięci urządzenia. Ustawienia te można zmieniać w przeglądarce internetowej. ' +
            'Więcej informacji udostępniamy w <a href="https://www.elektrobim.pl/polityka-plikow-cookies/" target="_blank" style="color:#0389e5;">Polityce plików cookies</a>' +
            '<a href="javascript:WHCloseCookiesWindow();" id="accept-cookies-checkbox" name="accept-cookies" style="background-color: #0389e5; padding: 5px 10px; color: #FFF; border-radius: 4px; -moz-border-radius: 4px; -webkit-border-radius: 4px; display: inline-block; margin-left: 10px; text-decoration: none; cursor: pointer;">Akceptuję</a></p>' +
            '<a href="javascript:WHCloseCookiesWindow();" class="close-cookie-button" style="position: absolute; text-decoration:none;background-color: #fff;color:#000; border-radius: 100%; width:20px;height:22px;top:5px;right:15px;">X</a></div>';
        message_container.innerHTML = html_code;
        document.body.appendChild(message_container);
    }
}

function WHCloseCookiesWindow() {
    WHCreateCookie('cookies_accepted', 'T', 365);
    document.getElementById('cookies-message-container').removeChild(document.getElementById('cookies-message'));
}
