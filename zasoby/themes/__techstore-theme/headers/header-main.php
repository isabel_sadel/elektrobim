<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge" >
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />

    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

    <?php if (function_exists('wp_site_icon')){ ?>
        <link rel="shortcut icon" href="<?php if ($lt_opt['site_favicon']) {echo esc_attr($lt_opt['site_favicon']);} else {echo get_template_directory_uri().'/favicon.png';} ?>" />
    <?php } ?>

    <!-- Demo Purpose Only. Should be removed in production -->
    <?php
        if (isset($lt_opt['demo_show']) && $lt_opt['demo_show']) {
            wp_enqueue_style('lt-demo-style', get_template_directory_uri() . '/css/demo/config.css', array(), null, 'all');
        }
    ?>
    <!-- Demo Purpose Only. Should be removed in production : END -->

    <?php wp_head(); ?>  
	
</head>

<body <?php body_class(); ?>>

<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<div id="lt-before-load"><div class="lt-relative lt-center"><div class="please-wait type2"></div></div></div>

<!-- For demo -->
<?php
if (isset($lt_opt['demo_show']) && $lt_opt['demo_show']) {
    get_template_part('css/demo/config');
}
?>
<!-- End For demo -->

<div id="wrapper" class="fixNav-enabled">
<?php 
techstore_promotion_recent_post();
techstore_get_header_structure();
?>

<div id="main-content" class="site-main light">
<?php if(function_exists('wc_print_notices')) {
    wc_print_notices();
}