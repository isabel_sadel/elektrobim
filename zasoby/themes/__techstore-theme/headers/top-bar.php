<?php if(!isset($lt_opt['topbar_show']) || $lt_opt['topbar_show']): ?>
    <div id="top-bar" class="top-bar">
        <div class="row">
            <div class="large-12 columns">
                <div class="left-text left">
                    <div class="inner-block">
                        <?php echo isset($lt_opt['topbar_left']) ? do_shortcode($lt_opt['topbar_left']) : '';?>
                    </div>
                </div>
                <div class="right-text right">
                    <div class="topbar-menu-container">
                        <?php techstore_get_menu('topbar-menu', 1); ?>
                    </div>
                    <?php techstore_header_account(); ?>
                </div>
            </div>
        </div>
    </div>
<?php endif; ?>

<?php if( 1== 0 ) : ?>
<div id="under-top-bar" class="under-top-bar">
    <div class="row">
        <div class="large-4 columns">
            <div class="header-search">
                <?php /*techstore_search('full'); */?>
            </div>
        </div>
        <div class="large-5 columns"></div>
        <div class="large-6 columns">
            <ul class="header-icons">
				<li class="first">
				<?php if ( is_user_logged_in() ) { ?>
                    <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Moje Konto','woothemes'); ?>"><span class="icon pe-7s-user"></span>&nbsp; <?php _e('Moje Konto','woothemes'); ?></a>

	<a href="<?php echo wp_logout_url( home_url() ); ?>"><span class="icon pe-7s-plug"></span> Wyloguj się</a>
 <?php }
 else { ?>
	<span class="icon pe-7s-lock"></span>
 	<a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Logowanie / Rejestracja','woothemes'); ?>"><?php _e('Logowanie / Rejestracja','woothemes'); ?></a>
 <?php } ?>
				
				</li>
                <li class="first lt-icon-compare-total"><?php echo do_shortcode(techstore_icon_compare());?></li>
                <li><?php techstore_icon_wishlist(true);?></li>
                <li><?php techstore_mini_cart('full') ?></li>
            </ul>
        </div>
    </div>
</div>

<?php endif; ?>