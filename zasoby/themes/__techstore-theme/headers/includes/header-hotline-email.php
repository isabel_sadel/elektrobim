<?php if((isset($lt_opt['hotline_number']) && $lt_opt['hotline_number']) || (isset($lt_opt['email_surport']) && $lt_opt['email_surport'])):?>
<div class="lt-hotline-email">
    <div class="lt-hotline inline-block"><span class="lt-hotline-email-icon pe-7s-call"></span><span><?php echo $lt_opt['hotline_number'];?></span></div>
    <div class="lt-email inline-block"><a href="mailto:<?php echo esc_attr($lt_opt['email_surport']);?>" title="mailto:<?php echo esc_attr($lt_opt['email_surport']);?>"><span class="lt-hotline-email-icon pe-7s-mail"></span><?php echo $lt_opt['email_surport'];?></a></div>
</div>
<?php endif;
