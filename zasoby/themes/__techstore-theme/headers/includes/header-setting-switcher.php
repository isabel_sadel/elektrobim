<div class="setting-switcher no-bullet inline-block">
    <div class="setting-dropdown">
        <a class="icon pe7-icon pe-7s-config" href="javascript:void(0);" onclick="return false;"></a>
        <div class="nav-dropdown">
            <?php if($language):
                techstore_header_language();
            endif;?>
            <div class="account-link">
                <?php if($language): ?><div class="label-title"></div><?php endif;?>
                <ul class="no-bullet">
                    <?php
                    if (is_user_logged_in()):
                        echo '<li class="menu-item"><a href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '" title="' . esc_html__( 'My Account', 'techstore-theme' ) . '"><span class="pe-7s-user"></span>' . esc_html__('My Account', 'techstore-theme') . '</a></li>';
                        echo '<li class="menu-item"><a class="nav-top-link" href="' . wp_logout_url() . '" title="' . esc_html__( 'Logout', 'techstore-theme' ) . '"><span class="pe-7s-unlock"></span>' . esc_html__('Logout', 'techstore-theme') . '</a></li>';
                    else:
                        echo '<li class="menu-item color"><a href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '" title=""><span class="pe-7s-lock"></span>' . esc_html__('Login or Register','techstore-theme') . '</a></li>';
                    endif;
                    ?>
                    <li><?php echo techstore_tini_wishlist(true);?></li>
                    <li><a href="<?php echo ($woocommerce) ? esc_url($woocommerce->cart->get_cart_url()) : '#';?>" title="<?php esc_html_e('My Cart', 'techstore-theme'); ?>"><span class="pe-7s-shopbag"></span><?php esc_html_e('My Cart','techstore-theme'); ?></a></li>
                    <li><a href="<?php echo ($woocommerce) ? esc_url($woocommerce->cart->get_checkout_url()) : '#';?>" title="<?php esc_html_e('Checkout', 'techstore-theme'); ?>"><span class="pe-7s-check"></span><?php esc_html_e('Checkout', 'techstore-theme'); ?></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>