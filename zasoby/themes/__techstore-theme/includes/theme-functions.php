<?php
// **********************************************************************//
// ! Get logo
// **********************************************************************//
if (!function_exists('techstore_logo')) :

    function techstore_logo($echo = true) {
        global $lt_opt, $wp_query;
        $logo_link = get_post_meta($wp_query->get_queried_object_id(), '_lee_custom_logo', true);
        if ($logo_link == '') {
            $logo_link = isset($lt_opt['site_logo']) ? $lt_opt['site_logo'] : '';
        }
        
        $site_title = esc_attr(get_bloginfo('name', 'display'));
        
        $content = '<div class="logo">';
            $content .= '<a href="' . esc_url(home_url('/')) . '" title="' . $site_title . ' - ' . esc_attr(get_bloginfo('description', 'display')) . '" rel="home">';
                $content .= ($logo_link != '') ? '<img src="' . esc_attr($logo_link) . '" class="header_logo" alt="' . $site_title . '" />' : get_bloginfo('name', 'display');
            $content .= '</a>';
        $content .= '</div>';

        if (!$echo) {
            return $content;
        }

        echo $content;
    }

endif;

// **********************************************************************//
// ! Get header search
// **********************************************************************//
if (!function_exists('techstore_search')) :

    function techstore_search($search_type = 'icon') {
        echo '<div class="lt-search-space inline-block lt_search_' . esc_attr($search_type) . '">';
            $class = '';
            if ($search_type == 'icon'):
                $class = ' hidden-tag lt-over-hide';
                echo 
                    '<a class="search-icon desk-search" href="javascript:void(0);">' .
                        '<span class="circle"></span>' .
                        '<span class="handle"></span>' .
                    '</a>';
            endif;
            
            echo ($search_type == 'full') ? '<span class="icon pe7-icon pe-7s-search"></span>' : '';
            
            echo '<div class="lt-show-search-form' . $class . '">';
                get_search_form();
            echo '</div>';
        echo '</div>';
    }

endif;

// **********************************************************************// 
// ! Get main menu
// **********************************************************************// 
if (!function_exists('techstore_get_main_menu')) :

    function techstore_get_main_menu($menu_location = 'primary', $main_menu_type = '1', $main = true) {
        $id_menu = $main ? ' id="site-navigation" ' : ' ';

        echo '<div class="nav-wrapper inline-block main-menu-type-' . $main_menu_type . '">';
        echo '<ul' . $id_menu . 'class="header-nav">';
        if (has_nav_menu($menu_location)) :
            wp_nav_menu(array(
                'theme_location' => $menu_location,
                'container' => false,
                'items_wrap' => '%3$s',
                'depth' => 5,
                'walker' => new Techstore_NavDropdown()
            ));
        else:
            $allowed_html = array(
                'li' => array(),
                'b' => array()
            );
            echo wp_kses(__('<li>Please Define menu in <b>Apperance > Menus</b></li>', 'techstore-theme'), $allowed_html);
        endif;
        echo '</ul>';
        echo '</div><!-- nav-wrapper -->';
    }

endif;

if (!function_exists('techstore_get_menu')) :

    function techstore_get_menu($menu_location, $depth = 3) {

        if (has_nav_menu($menu_location)) :
            echo '<ul>';
            wp_nav_menu(array(
                'theme_location' => $menu_location,
                'container' => false,
                'items_wrap' => '%3$s',
                'depth' => (int) $depth
            ));
            echo '</ul>';
        endif;
    }

endif;
// **********************************************************************// 
// ! Get Vertical menu
// **********************************************************************// 
if (!function_exists('techstore_get_vertical_menu')) :

    function techstore_get_vertical_menu() {
        global $lt_opt, $wp_query;

        $menu = ($menu_overr = get_post_meta($wp_query->get_queried_object_id(), '_lee_vertical_menu_selected', true)) ? $menu_overr : (isset($lt_opt['vertical_menu_selected']) ? $lt_opt['vertical_menu_selected'] : false);

        if ($menu) {
            $title = ($title_overr = get_post_meta($wp_query->get_queried_object_id(), '_lee_title_ver_menu', true)) ? $title_overr : (isset($lt_opt['title_ver_menu']) ? $lt_opt['title_ver_menu'] : false);

            $vertical_menu_allways_show = get_post_meta($wp_query->get_queried_object_id(), '_lee_vertical_menu_allways_show', true);
            $lt_class_menu_vertical = ($vertical_menu_allways_show) ? ' lt-allways-show' : '';
            ?>
            <div class="vertical-menu lt-vertical-header">
                <div class="title-inner">
                    <h5 class="section-title lt-title-vertical-menu">
                        <span><?php echo $title ? esc_attr($title) : esc_html__('SHOP BY CATEGORY', 'techstore-theme'); ?></span>
                    </h5>
                </div>
                <div class="vertical-menu-container<?php echo esc_attr($lt_class_menu_vertical); ?>">
                    <ul class="vertical-menu-wrapper">
                        <?php
                        wp_nav_menu(array(
                            'menu' => $menu,
                            'container' => false,
                            'items_wrap' => '%3$s',
                            'depth' => 3,
                            'walker' => new Techstore_NavDropdown()
                        ));
                        ?>
                    </ul>
                </div>
            </div>
            <?php
        }
    }

endif;


// **********************************************************************// 
// ! Mobile account menu
// **********************************************************************//
if (!function_exists('techstore_mobile_account')) :

    function techstore_mobile_account() {
        $file = get_stylesheet_directory() . '/includes/mobile-account.php';
        require is_file($file) ? $file : get_template_directory() . '/includes/mobile-account.php';
    }

endif;

// **********************************************************************// 
// ! Header account menu
// **********************************************************************//
if (!function_exists('techstore_header_account')) :

    function techstore_header_account() {
        $file = get_stylesheet_directory() . '/headers/includes/header-account.php';
        require is_file($file) ? $file : get_template_directory() . '/headers/includes/header-account.php';
    }

endif;

// **********************************************************************// 
// ! Header Language Switcher
// **********************************************************************//
if (!function_exists('techstore_header_language')) :

    function techstore_header_language() {
        $file = get_stylesheet_directory() . '/headers/includes/header-language-switcher.php';
        require is_file($file) ? $file : get_template_directory() . '/headers/includes/header-language-switcher.php';
    }

endif;

// **********************************************************************// 
// ! Header Setting Switcher
// **********************************************************************//
if (!function_exists('techstore_header_setting_switcher')) :

    function techstore_header_setting_switcher($language = false) {
        global $woocommerce;
        $file = get_stylesheet_directory() . '/headers/includes/header-setting-switcher.php';
        require is_file($file) ? $file : get_template_directory() . '/headers/includes/header-setting-switcher.php';
    }

endif;


// **********************************************************************// 
// ! Get shop by category menu
// **********************************************************************// 
if (!function_exists('techstore_get_shop_by_category_menu')) :

    function techstore_get_shop_by_category_menu() {
        echo '<div class="nav-wrapper">';
            echo '<ul id="" class="shop-by-category">';
            if (has_nav_menu('shop_by_category')) :
                wp_nav_menu(array(
                    'theme_location' => 'shop_by_category',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'depth' => 3,
                    'walker' => new Techstore_NavDropdown()
                ));
            else:
                $allowed_html = array(
                    'li' => array(),
                    'b' => array()
                );
                echo wp_kses(__('<li>Please Define Shop by Category menu in <b>Apperance > Menus</b></li>', 'techstore-theme'), $allowed_html);
            endif;
            echo '</ul>';
        echo '</div><!-- nav-wrapper -->';
    }

endif;

// **********************************************************************// 
// ! Get shop by Footer menu
// **********************************************************************// 
if (!function_exists('techstore_get_footer_menu')) :

    function techstore_get_footer_menu() {
        echo '<div class="nav-wrapper">';
            echo '<ul class="footer-menu">';
            if (has_nav_menu('footer_menu')) :
                wp_nav_menu(array(
                    'theme_location' => 'footer_menu',
                    'container' => false,
                    'items_wrap' => '%3$s',
                    'depth' => 3,
                    'walker' => new Techstore_NavDropdown()
                ));
            else:
                $allowed_html = array(
                    'li' => array(),
                    'b' => array()
                );
                echo wp_kses(__('<li>Please Define Footer menu in <b>Apperance > Menus</b></li>', 'techstore-theme'), $allowed_html);
            endif;
            echo '</ul>';
        echo '</div><!-- nav-wrapper -->';
    }

endif;

if (!function_exists('techstore_tpl2id')) :

    function techstore_tpl2id($tpl) {
        $pages = get_pages(array(
            'meta_key' => '_wp_page_template',
            'meta_value' => $tpl
        ));

        if (empty($pages)){
            return null;
        }

        foreach ($pages as $page) {
            return $page->ID;
        }
    }

endif;

if (!function_exists('techstore_back_to_page')) :

    function techstore_back_to_page() {
        echo '<a class="back-history" href="javascript: history.go(-1)">' . esc_html__('Return to Previous Page', 'techstore-theme') . '</a>';
    }

endif;

// **********************************************************************// 
// ! Get breadcrumb
// **********************************************************************// 
add_action('lee_get_breadcrumb', 'techstore_get_breadcrumb');
if (!function_exists('techstore_get_breadcrumb')) :

	function techstore_get_breadcrumb($ajax = false) {
		if (!LEE_WOOCOMMERCE_ACTIVED) {
			return;
		}

		global $post, $lt_opt, $wp_query;
		$enable = (isset($lt_opt['breadcrumb_show']) && !$lt_opt['breadcrumb_show']) ? false : true;
		$override = false;

		if (isset($post->ID) && $post->post_type == 'page') {
			$queryObj = $wp_query->get_queried_object_id();
			$show_breadcrumb = get_post_meta($queryObj, '_lee_show_breadcrumb', true);
			$enable = ($show_breadcrumb != 'on') ? false : true;
			$override = true;
		}

		if ($enable === false) {
			return;
		}

		// Theme option
		$bg = (isset($lt_opt['breadcrumb_type']) && $lt_opt['breadcrumb_type'] == 'has-background') ?
			$lt_opt['breadcrumb_bg'] : false;

		$bg_cl = (isset($lt_opt['breadcrumb_bg_color']) && $lt_opt['breadcrumb_bg_color']) ?
			$lt_opt['breadcrumb_bg_color'] : false;

		$h_bg = (isset($lt_opt['breadcrumb_height']) && (int) $lt_opt['breadcrumb_height']) ?
			(int) $lt_opt['breadcrumb_height'] : false;

		$txt_color = (isset($lt_opt['breadcrumb_color']) && $lt_opt['breadcrumb_color']) ?
			$lt_opt['breadcrumb_color'] : false;

		// Override
		if ($override) {
			$type_bg = get_post_meta($queryObj, '_lee_type_breadcrumb', true);
			$bg_override = get_post_meta($queryObj, '_lee_bg_breadcrumb', true);
			$bg_cl_override = get_post_meta($queryObj, '_lee_bg_color_breadcrumb', true);
			$h_override = get_post_meta($queryObj, '_lee_height_breadcrumb', true);
			$color_override = get_post_meta($queryObj, '_lee_color_breadcrumb', true);

			if ($type_bg == '1') {
				$bg = $bg_override ? $bg_override : $bg;
				$bg_cl = $bg_cl_override ? $bg_cl_override : $bg_cl;
				$h_bg = (int) $h_override ? (int) $h_override : $h_bg;
				$txt_color = $color_override ? $color_override : $txt_color;
			}
		}

		// set style by option breadcrumb
		$style_custom = '';
		if ($bg) {
			$style_custom = 'background:url(\'' . esc_url($bg) . '\') center center no-repeat;';
			$style_custom .= ($bg_cl) ? 'background-color:' . $bg_cl . ';' : '';
			$style_custom .= ($h_bg) ? 'height:' . $h_bg . 'px' : 'height:auto';
			$style_custom .= ($txt_color) ? ';color:' . $txt_color : '';
		}

		$defaults = array(
			'delimiter' => '<span class="fa fa-angle-right"></span>',
			'wrap_before' => '<div class="breadcrumb ebim-breadcrumb">',
			'wrap_after' => '</div>',
			'before' => '<span class="ebim-breadcrumb__item">',
			'after' => '</span>',
			'home' => 'Strona główna',
			'ajax' => $ajax
		);

		$args = wp_parse_args($defaults);
		?>

        <div class="bread lt-breadcrumb<?php echo ($bg) ? ' lt-breadcrumb-has-bg' : ''; ?>"<?php echo ($style_custom) ? ' style="' . esc_attr($style_custom) . '"' : ''; ?>>
            <div class="row">
                <div class="large-12 columns">
                    <div class="breadcrumb-row ebim-breadcrumb-row">
						<?php wc_get_template('global/breadcrumb.php', $args); ?>
                    </div>
                </div>
            </div>
        </div>
		<?php
	}

endif;

// **********************************************************************// 
// ! Countdown
// **********************************************************************//
add_action('wp_enqueue_scripts', 'techstore_woocoomerce_countdown');
if(!function_exists('techstore_woocoomerce_countdown')) :

    function techstore_woocoomerce_countdown() {
        wp_enqueue_script('techstore-countdown', get_template_directory_uri() . '/js/countdown.js', array(), false, true);
        wp_localize_script(
            'techstore-countdown', 
            'lee_countdown_l10n', 
            array(
                'days' => 'dni',
                'months' => 'mies',
                'weeks' => 'tyg',
                'years' => 'lat',
                'hours' => 'godz',
                'minutes' => 'min',
                'seconds' => 'sek',
                'day' => 'dni',
                'month' => 'miec',
                'week' => 'tydz',
                'year' => 'lat',
                'hour' => 'godz',
                'minute' => 'min',
                'second' => 'sek'
            )
        );
    }

endif;

// **********************************************************************// 
// ! Add body class
// **********************************************************************//
add_filter('body_class', 'techstore_body_classes');
if(!function_exists('techstore_body_classes')) :

    function techstore_body_classes($classes) {
        global $lt_opt;

        $classes[] = 'antialiased';
        if (is_multi_author()) {
            $classes[] = 'group-blog';
        }

        if ($lt_opt['site_layout'] && $lt_opt['site_layout'] == 'boxed') {
            $classes[] = 'boxed';
        }

        if (isset($lt_opt['promo_popup']) && $lt_opt['promo_popup'] == 1) {
            $classes[] = 'open-popup';
        }

        if (LEE_WOOCOMMERCE_ACTIVED && function_exists('is_product')) {
            if (is_product() && isset($lt_opt['product-zoom']) && $lt_opt['product-zoom']) {
                $classes[] = 'product-zoom';
            }
        }

        return $classes;
    }

endif;

// **********************************************************************// 
// ! Add hr to the widget title
// **********************************************************************//
add_filter('widget_title', 'techstore_widget_title', 10, 3);
if(!function_exists('techstore_widget_title')) :

    function techstore_widget_title($title) {
        return !empty($title) ? $title . '<span class="bery-hr small primary-color"></span>' : '';
    }

endif;

// **********************************************************************// 
// ! Comments
// **********************************************************************//  
if (!function_exists('techstore_comment')) :

    function techstore_comment($comment, $args, $depth) {
        $GLOBALS['comment'] = $comment;
        switch ($comment->comment_type) :
            case 'pingback' :
            case 'trackback' :
                ?>
                <li class="post pingback">
                    <p><?php esc_html_e('Pingback:', 'techstore-theme'); ?> <?php comment_author_link(); ?><?php edit_comment_link(esc_html__('Edit', 'techstore-theme'), '<span class="edit-link">', '<span>'); ?></p>
                <?php
                break;
            default :
                ?>
                <li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
                    <article id="comment-<?php comment_ID(); ?>" class="comment-inner">
                        <div class="row collapse">
                            <div class="large-2 columns">
                                <div class="comment-author">
                                    <?php echo get_avatar($comment, 80); ?>
                                </div>
                            </div>
                            <div class="large-10 columns">
                                <?php printf('<cite class="fn">%s</cite>', get_comment_author_link()); ?>
                                <div class="comment-meta commentmetadata right">
                                    <a href="<?php echo esc_url(get_comment_link($comment->comment_ID)); ?>">
                                        <time datetime="<?php comment_time('c'); ?>">
                                            <?php printf(_x('%1$s at %2$s', '1: date, 2: time', 'techstore-theme'), get_comment_date(), get_comment_time()); ?>
                                        </time>
                                    </a>
                                    <?php edit_comment_link(esc_html__('Edit', 'techstore-theme'), '<span class="edit-link">', '<span>'); ?>
                                </div>
                                <div class="reply">
                                    <?php
                                    comment_reply_link(array_merge($args, array(
                                        'depth' => $depth,
                                        'max_depth' => $args['max_depth'],
                                    )));
                                    ?>
                                </div>
                                <?php if ($comment->comment_approved == '0') : ?>
                                    <em><?php esc_html_e('Your comment is awaiting moderation.', 'techstore-theme'); ?></em>
                                    <br />
                                <?php endif; ?>

                                <div class="comment-content"><?php comment_text(); ?></div>
                            </div>
                        </div>
                    </article>
                <?php
                break;
        endswitch;
    }

endif;

// **********************************************************************// 
// ! Post meta top
// **********************************************************************//  
if (!function_exists('techstore_posted_on')) :

    function techstore_posted_on() {
        $allowed_html = array(
            'span' => array('class' => array()),
            'strong' => array(),
            'a' => array('class' => array(), 'href' => array(), 'title' => array(), 'rel' => array()),
            'time' => array('class' => array(), 'datetime' => array())
        );
        printf(wp_kses(__('<span class="meta-author">by <strong><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></strong>.</span> Posted on <a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a>', 'techstore-theme'), $allowed_html), esc_url(get_permalink()), esc_attr(get_the_time()), esc_attr(get_the_date('c')), esc_html(get_the_date()), esc_url(get_author_posts_url(get_the_author_meta('ID'))), esc_attr(sprintf(esc_html__('View all posts by %s', 'techstore-theme'), get_the_author())), get_the_author()
        );
    }

endif;


// **********************************************************************// 
// ! Promo Popup
// **********************************************************************// 
add_action('after_page_wrapper', 'techstore_promo_popup');
if (!function_exists('techstore_promo_popup')) {

    function techstore_promo_popup() {
        global $lt_opt;
        ?>
        <style type="text/css">
            #lt-popup{
                width: <?php echo isset($lt_opt['pp_width']) ? (int) $lt_opt['pp_width'] : 700; ?>px;
                height: <?php echo isset($lt_opt['pp_height']) ? (int) $lt_opt['pp_height'] : 350; ?>px;
                background-color: <?php echo isset($lt_opt['pp_background_color']) ? $lt_opt['pp_background_color'] : '' ?>;
                background-image: url('<?php echo isset($lt_opt['pp_background_image']) ? $lt_opt['pp_background_image'] : '' ?>');
            }
        </style>
        <div id="lt-popup" class="white-popup-block mfp-hide mfp-with-anim zoom-anim-dialog">
            <?php echo isset($lt_opt['pp_content']) ? do_shortcode($lt_opt['pp_content']) : ''; ?>
            <p class="checkbox-label align-center">
                <input type="checkbox" value="do-not-show" name="showagain" id="showagain" class="showagain" />
                <label for="showagain"><?php esc_html_e("Don't show this popup again", 'techstore-theme'); ?></label>
            </p>
        </div>
        <?php
    }

}

add_filter('wp_nav_menu_objects', 'techstore_add_menu_parent_class');
function techstore_add_menu_parent_class($items) {
    $parents = array();
    foreach ($items as $item) {
        if ($item->menu_item_parent && $item->menu_item_parent > 0) {
            $parents[] = $item->menu_item_parent;
        }
    }

    foreach ($items as $item) {
        if (in_array($item->ID, $parents)) {
            $item->classes[] = 'menu-parent-item';
        }
    }

    return $items;
}

function techstore_ProductShowReviews() {
    if (comments_open()) {
        global $wpdb, $post;

        $count = $wpdb->get_var($wpdb->prepare("
            SELECT COUNT(meta_value) FROM $wpdb->commentmeta
            LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
            WHERE meta_key = %s
            AND comment_post_ID = %s
            AND comment_approved = %s
            AND meta_value > %s", 'rating', $post->ID, '1', '0'
        ));

        $rating = $wpdb->get_var($wpdb->prepare("
            SELECT SUM(meta_value) FROM $wpdb->commentmeta
            LEFT JOIN $wpdb->comments ON $wpdb->commentmeta.comment_id = $wpdb->comments.comment_ID
            WHERE meta_key = %s
            AND comment_post_ID = %s
            AND comment_approved = %s", 'rating', $post->ID, '1'
        ));

        if ($count > 0) {
            $average = number_format($rating / $count, 2);
			$srednia = ceil($average / 5 * 100);
            echo '<div class="star-rating tip-top" data-tip="Średnia: ' . $srednia . '% / Ilość opinii: ' . $count .'"><span style="width:' . ($average * 17) . 'px"><span itemprop="aggregateRating" itemscope itemtype="http://schema.org/AggregateRating" class="rating"><span itemprop="ratingValue">' . $average . '</span><span itemprop="reviewCount" class="hidden">' . $count . '</span></span> ' . esc_html__('out of 5', 'techstore-theme') . '</span></div>';
		}
    }
}

add_action('woocommerce_single_product_summary', 'techstore_ProductShowReviews', 15);
add_action('woocommerce_single_review', 'techstore_ProductShowReviews', 10);

function techstore_get_adjacent_post_product($in_same_cat = false, $excluded_categories = '', $previous = true) {
    global $wpdb;

    if (!$post = get_post()){
        return null;
    }
    
    $current_post_date = $post->post_date;
    $join = '';
    $posts_in_ex_cats_sql = '';
    if ($in_same_cat || !empty($excluded_categories)) {
        $join .= " INNER JOIN $wpdb->term_relationships AS tr ON p.ID = tr.object_id INNER JOIN $wpdb->term_taxonomy tt ON tr.term_taxonomy_id = tt.term_taxonomy_id";

        if ($in_same_cat) {
            if (!is_object_in_taxonomy($post->post_type, 'product_cat')){
                return '';
            }
            $cat_array = wp_get_object_terms($post->ID, 'product_cat', array('fields' => 'ids'));
            if (!$cat_array || is_wp_error($cat_array)) {
                return '';
            }
            $join .= " AND tt.taxonomy='product_cat' AND tt.term_id IN (" . implode(',', $cat_array) . ")";
        }

        $posts_in_ex_cats_sql = "AND tt.taxonomy = 'product_cat'";
        if (!empty($excluded_categories)) {
            if (!is_array($excluded_categories)) {
                if (strpos($excluded_categories, ' and ') !== false) {
                    _deprecated_argument(__FUNCTION__, '3.3', esc_html_e('Use commas instead of and to separate excluded categories.', 'techstore-theme'));
                    $excluded_categories = explode(' and ', $excluded_categories);
                } else {
                    $excluded_categories = explode(',', $excluded_categories);
                }
            }

            $excluded_categories = array_map('intval', $excluded_categories);

            if (!empty($cat_array)) {
                $excluded_categories = array_diff($excluded_categories, $cat_array);
                $posts_in_ex_cats_sql = '';
            }

            if (!empty($excluded_categories)) {
                $posts_in_ex_cats_sql = " AND tt.taxonomy = 'product_cat' AND tt.term_id NOT IN (" . implode($excluded_categories, ',') . ')';
            }
        }
    }

    $adjacent = $previous ? 'previous' : 'next';
    $op = $previous ? '<' : '>';
    $order = $previous ? 'DESC' : 'ASC';

    $join = apply_filters("get_{$adjacent}_post_join", $join, $in_same_cat, $excluded_categories);
    $where = apply_filters("get_{$adjacent}_post_where", $wpdb->prepare("WHERE p.post_date $op %s AND p.post_type = %s AND p.post_status = 'publish' $posts_in_ex_cats_sql", $current_post_date, $post->post_type), $in_same_cat, $excluded_categories);
    $sort = apply_filters("get_{$adjacent}_post_sort", "ORDER BY p.post_date $order LIMIT 1");

    $query = "SELECT p.id FROM $wpdb->posts AS p $join $where $sort";
    $query_key = 'adjacent_post_' . md5($query);
    $result = wp_cache_get($query_key, 'counts');
    if (false !== $result) {
        if ($result){
            $result = get_post($result);
        }
        return $result;
    }

    $result = $wpdb->get_var($wpdb->prepare($query));
    if (null === $result){
        $result = '';
    }
    
    wp_cache_set($query_key, $result, 'counts');

    if ($result){
        $result = get_post($result);
    }

    return $result;
}

// **********************************************************************// 
// ! Blog - Add "Read more" links
// **********************************************************************//
add_action('the_content_more_link', 'techstore_add_morelink_class', 10, 2);
function techstore_add_morelink_class($link, $text) {
    return str_replace('more-link', 'more-link button small', $link);
}
    
// **********************************************************************// 
// ! Language Flags
// **********************************************************************//
add_action('techstore_language_switcher', 'techstore_language_flages', 1);
if (!function_exists('techstore_language_flages')) {

    function techstore_language_flages() {
        $language_output = '<ul>';
        if (function_exists('icl_get_languages')) {
            $languages = icl_get_languages('skip_missing=0&orderby=code');
            if (!empty($languages)) {
                foreach ($languages as $l) {
                    $language_output .= '<li><a href="' . $l['url'] . '"><img src="' . $l['country_flag_url'] . '" height="12" alt="' . $l['language_code'] . '" width="24" /></a></li>';
                }
            }
        } else {
            $language_output .= 
                '<li><a href="#"><img src="' . get_template_directory_uri() . '/images/flag_icons/en_flag.jpg' . '" alt=""></a></li>' .
                '<li><a href="#"><img src="' . get_template_directory_uri() . '/images/flag_icons/gr_flag.jpg' . '" alt=""></a></li>' .
                '<li><a href="#"><img src="' . get_template_directory_uri() . '/images/flag_icons/fr_flag.jpg' . '" alt=""></a></li>';
        }
        $language_output .= '</ul>';

        echo $language_output;
    }

}

add_action('woocommerce_single_product_lightbox_summary', 'woocommerce_template_single_price', 10);
add_action('woocommerce_single_product_lightbox_summary', 'woocommerce_template_single_excerpt', 20);
if (!isset($lt_opt['disable-cart']) || !$lt_opt['disable-cart']) {
    add_action('woocommerce_single_product_lightbox_summary', 'woocommerce_template_single_add_to_cart', 30);
}
add_action('woocommerce_single_product_lightbox_summary', 'woocommerce_template_single_sharing', 40);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 20);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 50);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_title', 5);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 20);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_excerpt', 25);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 35);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_sharing', 35);

add_action('woocommerce_single_product_summary', 'woocommerce_template_single_meta', 1);
add_action('woocommerce_single_product_summary', 'techstore_single_availability', 15);
add_action('woocommerce_single_product_summary', 'techstore_single_hr', 15);

function techstore_single_availability() {
    global $product;
    // Availability
    $availability = $product->get_availability();

    if ($availability['availability']) :
        echo apply_filters('woocommerce_stock_html', '<p class="stock ' . esc_attr($availability['class']) . '">' . wp_kses(__('<span>Availability:</span> ', 'techstore-theme'), array('span' => array())) . esc_html($availability['availability']) . '</p>', $availability['availability']);
    endif;
}

function techstore_single_hr() {
    echo '<hr class="lt-single-hr" />';
}

if (isset($_GET["catalog-mode"])) {
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
    remove_action('woocommerce_simple_add_to_cart', 'woocommerce_simple_add_to_cart', 30);
    remove_action('woocommerce_grouped_add_to_cart', 'woocommerce_grouped_add_to_cart', 30);
    remove_action('woocommerce_variable_add_to_cart', 'woocommerce_variable_add_to_cart', 30);
    remove_action('woocommerce_external_add_to_cart', 'woocommerce_external_add_to_cart', 30);
    remove_action('woocommerce_single_product_lightbox_summary', 'woocommerce_template_single_add_to_cart', 30);

    if (isset($_GET["catalog-mode"])) {
        remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_price', 10);
        remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_price', 10);
        remove_action('woocommerce_single_product_lightbox_summary', 'woocommerce_template_single_price', 10);
    }
    
    add_action('woocommerce_single_product_summary', 'techstore_catalog_mode_product', 30);
    function techstore_catalog_mode_product() {
        global $lt_opt;
        echo '<div class="catalog-product-text">';
        echo do_shortcode($lt_opt['catalog_mode_product']);
        echo '</div>';
    }
    
    add_action('woocommerce_single_product_lightbox_summary', 'techstore_catalog_mode_lightbox', 30);
    function techstore_catalog_mode_lightbox() {
        global $lt_opt;
        echo '<div class="catalog-product-text">';
        echo do_shortcode($lt_opt['catalog_mode_lightbox']);
        echo '</div>';
    }
    
}

add_action('pre_get_posts', 'techstore_pre_get_posts_action');
function techstore_pre_get_posts_action($query) {
    global $lt_opt;
    $action = isset($_GET['action']) ? $_GET['action'] : '';
    if ($action == 'woocommerce_json_search_products') {
        return;
    }
    if (defined('DOING_AJAX') && DOING_AJAX && !empty($query->query_vars['s'])) {
        if (isset($query->query_vars['post_type']))
            $query->query_vars['post_type'] = array($query->query_vars['post_type'], 'post', 'page');
        if (isset($query->query_vars['meta_query']))
            $query->query_vars['meta_query'] = new WP_Meta_Query(array('relation' => 'OR', $query->query_vars['meta_query']));
    }
}

add_filter('posts_results', 'techstore_posts_results_filter', 10, 2);
function techstore_posts_results_filter($posts, $query) {
    if (defined('DOING_AJAX') && DOING_AJAX && !empty($query->query_vars['s'])) {
        foreach ($posts as $key => $post) {
            foreach (array('myaccount', 'edit_address', 'change_password', 'lost_password', 'shop', 'cart', 'checkout', 'pay', 'view_order', 'thanks', 'terms') as $wc_page_type) {
                if ($post->ID == woocommerce_get_page_id($wc_page_type)){
                    unset($posts[$key]);
                }
            }
        }
    }
    return $posts;
}

// This extending class is for solving a problem when "getElementById()" returns NULL
class TechstoreLTDOMDocument extends DOMDocument {

    function getElementById($id) {
        //thanks to: http://www.php.net/manual/en/domdocument.getelementbyid.php#96500
        $xpath = new DOMXPath($this);
        return $xpath->query("//*[@id='$id']")->item(0);
    }

    function output() {
        // thanks to: http://www.php.net/manual/en/domdocument.savehtml.php#85165
        $output = preg_replace('/^<!DOCTYPE.+?>/', '', str_replace(
                array('<html>', '</html>', '<body>', '</body>'), array('', '', '', ''), $this->saveHTML()
            )
        );

        return trim($output);
    }

}

function techstore_pagination($page = '', $range = 2, $page_total = '') {
    global $paged;

    $showitems = ($range * 2) + 1;
    if (empty($paged))
        $paged = 1;

    if ($pages == '') {
        $pages = $page_total;
        if (!$pages)
            $pages = 1;
    }

    if (1 != $pages) {
        //echo "<div class='pagination'>";
        if ($paged > 2 && $paged > $range + 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link(1) . "'>&laquo;</a>";
        if ($paged > 1 && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($paged - 1) . "'>&lsaquo;</a>";

        for ($i = 1; $i <= $pages; $i++) {
            if (1 != $pages && (!($i >= $paged + $range + 1 || $i <= $paged - $range - 1) || $pages <= $showitems ))
                echo ($paged == $i) ? "<span class='current'>" . $i . "</span>" : "<a href='" . get_pagenum_link($i) . "' class='inactive' >" . $i . "</a>";
        }

        if ($paged < $pages && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($paged + 1) . "'>&rsaquo;</a>";
        if ($paged < $pages - 1 && $paged + $range - 1 < $pages && $showitems < $pages)
            echo "<a href='" . get_pagenum_link($pages) . "'>&raquo;</a>";
        //echo "</div>\n";
    }
}

/* ==========================================================================
  ADD VIDEO PLAY BUTTON ON PRODUCT DETAIL PAGE
  ========================================================================== */
if (!function_exists('techstore_product_video_btn_function')) {

    function techstore_product_video_btn_function() {
        global $wc_cpdf;
        if ($wc_cpdf->get_value(get_the_ID(), '_product_video_link')) {
            ?>
            <a class="product-video-popup tip-top" data-tip="<?php esc_html_e('View video', 'techstore-theme'); ?>" href="<?php echo $wc_cpdf->get_value(get_the_ID(), '_product_video_link'); ?>"><span class="fa fa-play"></span><?php esc_html_e('Play Video', 'techstore-theme'); ?></a>
            <?php
            $height = '800';
            $width = '800';
            $iframe_scale = '100%';
            $custom_size = $wc_cpdf->get_value(get_the_ID(), '_product_video_size');
            if ($custom_size) {
                $split = explode("x", $custom_size);
                $height = $split[0];
                $width = $split[1];
                $iframe_scale = ($width / $height * 100) . '%';
            }
            $style = '.has-product-video .mfp-iframe-holder .mfp-content{max-width: ' . $width . 'px;}';
            $style .= '.has-product-video .mfp-iframe-scaler{padding-top: ' . $iframe_scale . '}';
            wp_add_inline_style('product_detail_css_custom', $style);
        }
    }

}
add_action('product_video_btn', 'techstore_product_video_btn_function', 1);

/* ======================================================================= */
/* NEXT - PREV PRODUCTS */
/* ======================================================================= */
add_action('next_prev_product', 'techstore_prev_product');
add_action('next_prev_product', 'techstore_next_product');
/* NEXT / PREV NAV ON PRODUCT PAGES */
function techstore_next_product() {
    $next_post = get_next_post(true, '', 'product_cat');
    if (is_a($next_post, 'WP_Post')) {
        $product_obj = new WC_Product($next_post->ID);
        ?>
        <div class="next-product next-prev-buttons">
            <a href="<?php echo get_the_permalink($next_post->ID); ?>" rel="next" class="icon-next-prev icon-angle-right next" title="<?php echo get_the_title($next_post->ID); ?>"></a>
            <div class="dropdown-wrap">
                <a title="<?php echo get_the_title($next_post->ID); ?>" href="<?php echo get_the_permalink($next_post->ID); ?>">
                    <?php echo get_the_post_thumbnail($next_post->ID, apply_filters('single_product_small_thumbnail_size', 'shop_thumbnail')); ?>
                </a>
                <div>
                    <span class="product-name"><?php echo get_the_title($next_post->ID); ?></span>
                    <span class="price"><?php echo $product_obj->get_price_html(); ?></span>
                </div>
            </div>
        </div>
        <?php
    }
}

function techstore_prev_product() {
    $prev_post = get_previous_post(true, '', 'product_cat');
    if (is_a($prev_post, 'WP_Post')) {
        $product_obj = new WC_Product($prev_post->ID);
        ?>
        <div class="prev-product next-prev-buttons">
            <a href="<?php echo get_the_permalink($prev_post->ID); ?>" rel="prev" class="icon-next-prev icon-angle-left prev" title="<?php echo get_the_title($prev_post->ID); ?>"></a>
            <div class="dropdown-wrap">
                <a title="<?php echo get_the_title($prev_post->ID); ?>" href="<?php echo get_the_permalink($prev_post->ID); ?>">
                    <?php echo get_the_post_thumbnail($prev_post->ID, apply_filters('single_product_small_thumbnail_size', 'shop_thumbnail')); ?>
                </a>
                <div>
                    <span class="product-name"><?php echo get_the_title($prev_post->ID); ?></span>
                    <span class="price"><?php echo $product_obj->get_price_html(); ?></span>
                </div>
            </div>
        </div>
        <?php
    }
}

/* Change category image size */
// unload the original one
remove_action('woocommerce_before_subcategory_title', 'woocommerce_subcategory_thumbnail');
// load mine
add_action('woocommerce_before_subcategory_title', 'techstore_woocommerce_subcategory_thumbnail', 10);
if (!function_exists('techstore_woocommerce_subcategory_thumbnail')) {

    function techstore_woocommerce_subcategory_thumbnail($category) {
        $small_thumbnail_size = apply_filters('single_product_small_thumbnail_size', 'lt-category-thumb');
        $dimensions = wc_get_image_size($small_thumbnail_size);
        $thumbnail_id = get_woocommerce_term_meta($category->term_id, 'thumbnail_id', true);

        if ($thumbnail_id) {
            $image = wp_get_attachment_image_src($thumbnail_id, $small_thumbnail_size);
            $image = $image[0];
        } else {
            $image = wc_placeholder_img_src();
        }

        if ($image) {
            // Prevent esc_url from breaking spaces in urls for image embeds
            // Ref: http://core.trac.wordpress.org/ticket/23605
            $image = str_replace(' ', '%20', $image);

            echo '<img src="' . esc_url($image) . '" alt="' . esc_attr($category->name) . '" width="' . esc_attr($dimensions['width']) . '" height="' . esc_attr($dimensions['height']) . '" />';
        }
    }

}

/* Mobile header */
if (!function_exists('techstore_mobile_header')) {

    function techstore_mobile_header() {
        ?>
        <a href="javascript:void(0);" class="mobile_toggle"><span class="icon-menu"></span></a>
        <a class="icon pe-7s-search mobile-search" href="javascript:void(0);"></a>

        <div class="logo-wrapper">
            <?php techstore_logo(); ?>
        </div>

        <div class="mini-cart">
            <?php techstore_mini_cart('full'); ?>
        </div>
        <?php
    }

}
/* cut string limit */
if (!function_exists('techstore_limit_words')) {

    function techstore_limit_words($string, $word_limit) {
        $words = explode(' ', $string, ($word_limit + 1));
        if (count($words) <= $word_limit) {
            return $string;
        }
        array_pop($words);
        return implode(' ', $words) . ' ...';
    }

}

/* Remove WP Version Param From Any Enqueued Scripts */
// if (!isset($lt_opt['cache_version']) || $lt_opt['cache_version'] == '') {
//     if (!function_exists('techstore_remove_wp_ver_css_js')) {

//         function techstore_remove_wp_ver_css_js($src) {
//             if (strpos($src, 'ver=')) {
//                 $src = esc_url(remove_query_arg('ver', $src));
//             }
//             return $src;
//         }

//     }
//     add_filter('style_loader_src', 'techstore_remove_wp_ver_css_js', 9999);
//     add_filter('script_loader_src', 'techstore_remove_wp_ver_css_js', 9999);
// }

// **********************************************************************// 
// ! Blog post navigation
// **********************************************************************//  
if (!function_exists('techstore_content_nav')) {

    function techstore_content_nav($nav_id) {
        global $wp_query, $post;
        $allowed_html = array(
            'span' => array('class' => array())
        );

        if (is_single()) {
            $previous = ( is_attachment() ) ? get_post($post->post_parent) : get_adjacent_post(false, '', true);
            $next = get_adjacent_post(false, '', false);

            if (!$next && !$previous){
                return;
            }
        }

        if ($wp_query->max_num_pages < 2 && ( is_home() || is_archive() || is_search() )){
            return;
        }

        $nav_class = ( is_single() ) ? 'navigation-post' : 'navigation-paging';
        ?>
        <nav role="navigation" id="<?php echo esc_attr($nav_id); ?>" class="<?php echo esc_attr($nav_class); ?>">
            <?php
            if (is_single()) {
                previous_post_link('<div class="nav-previous left">%link</div>', '<span class="fa fa-caret-left">' . _x('', 'Previous post link', 'techstore-theme') . '</span> %title');
                next_post_link('<div class="nav-next right">%link</div>', '%title <span class="fa fa-caret-right">' . _x('', 'Next post link', 'techstore-theme') . '</span>');
            } elseif ($wp_query->max_num_pages > 1 && (is_home() || is_archive() || is_search())) {
                // navigation links for home, archive, and search pages
                if (get_next_posts_link()) {
                    ?>
                    <div class="nav-previous"><?php next_posts_link(wp_kses(__('Next <span class="fa fa-caret-right"></span>', 'techstore-theme'), $allowed_html)); ?></div>
                <?php
                }
                if (get_previous_posts_link()) {
                    ?>
                    <div class="nav-next"><?php previous_posts_link(wp_kses(__('<span class="fa fa-caret-left"></span> Previous', 'techstore-theme'), $allowed_html)); ?></div>
                <?php
                }
            }
            ?>
        </nav>
        <?php
    }

}

//Add shortcode Top bar Promotion news
function techstore_promotion_recent_post() {
    global $lt_opt;

    if (isset($lt_opt['enable_post_top']) && !$lt_opt['enable_post_top']) {
        return '';
    }

    $content = '';
    $posts = null;
    $_id = rand();
    if (!isset($lt_opt['type_display']) || $lt_opt['type_display'] == 'custom') {
        $content = $lt_opt['content_custom'];
    } elseif (isset($lt_opt['type_display']) && $lt_opt['type_display'] == 'list-posts') {
        if (!isset($lt_opt['category_post']) || !$lt_opt['category_post']) {
            $lt_opt['category_post'] = null;
        }

        if (!isset($lt_opt['number_post']) || !$lt_opt['number_post']) {
            $lt_opt['number_post'] = 4;
        }

        $args = array(
            'post_status' => 'publish',
            'post_type' => 'post',
            'orderby' => 'date',
            'order' => 'DESC',
            'category' => ((int) $lt_opt['category_post'] != 0) ? (int) $lt_opt['category_post'] : null,
            'posts_per_page' => $lt_opt['number_post']
        );

        $posts = get_posts($args);
    }
    
    $file = get_stylesheet_directory() . '/includes/blogs_layout/lt_blogs_carousel.php';
    include is_file($file) ? $file : get_template_directory() . '/includes/blogs_layout/lt_blogs_carousel.php';
}

if (!function_exists('techstore_get_header_sticky')):

    function techstore_get_header_sticky($menu_type = 'primary', $cart_type = 'full') {
        global $lt_opt;
        if (!isset($lt_opt['fixed_nav']) || $lt_opt['fixed_nav']) {
            ?>
            <div class="fixed-header-area hide-for-small hide-for-medium">
                <div class="fixed-header">
                    <div class="row">
                        <div class="large-12 columns header-container"> 
                            <!-- Logo -->
                            <div class="logo-wrapper">
                                <?php techstore_logo(); ?>
                            </div>
                            <!-- Main navigation - Full width style -->
                            <div class="wide-nav">
                                <?php techstore_get_main_menu($menu_type, '1', false); ?>
                            </div>
                            <div class="header-utilities">
                                <?php techstore_mini_cart($cart_type); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
    }

endif;

if (!function_exists('techstore_get_header_structure')):

    function techstore_get_header_structure() {
        global $woocommerce, $woo_options, $lt_opt, $post, $wp_query;

        $hstructure = isset($lt_opt['header-type']) ? $lt_opt['header-type'] : '1';
        if (isset($post->ID)) {
            $custom_header = get_post_meta($wp_query->get_queried_object_id(), '_lee_custom_header', true);
            if (!empty($custom_header)) {
                $hstructure = (int) $custom_header;
            }
        }

        $header_classes = get_post_meta($wp_query->get_queried_object_id(), '_lee_header_transparent', true) ? ' header-transparent' : '';
        $menu_transparent = '';

        if (get_post_meta($wp_query->get_queried_object_id(), '_lee_main_menu_transparent', true)) {
            $menu_transparent = ' lt-menu-transparent';
            $header_classes .= ' lt-has-menu-transparent';
        } else {
            if (isset($lt_opt['main_menu_transparent']) && $lt_opt['main_menu_transparent']) {
                $menu_transparent = ' lt-menu-transparent';
                $header_classes .= ' lt-has-menu-transparent';
            }
        }
        
        $file = get_stylesheet_directory() . '/headers/header-structure-' . ((int) $hstructure) . '.php';
        if(is_file($file)){
            include $file;
        } else {
            $file = get_template_directory() . '/headers/header-structure-' . ((int) $hstructure) . '.php';
            include is_file($file) ? $file : get_template_directory() . '/headers/header-structure-1.php';
        }
    }

endif;

if (!function_exists('techstore_get_block')):

    function techstore_get_block() {
        global $lt_opt;

        return (isset($lt_opt['header-block']) && (int) $lt_opt['header-block'] && ($block = get_post_field('post_content', (int) $lt_opt['header-block']))) ? do_shortcode($block) : '';
    }

endif;