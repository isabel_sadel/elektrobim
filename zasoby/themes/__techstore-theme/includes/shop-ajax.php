<?php
// **********************************************************************// 
// ! Product Quick View
// **********************************************************************// 
add_action('wp_head', 'techstore_register_AjaxUrl', 0, 0);
function techstore_register_AjaxUrl() {
    echo '<script type="text/javascript">var ajaxurl="' . esc_js(admin_url('admin-ajax.php')) . '";</script>';
}

add_action('wp_ajax_jck_quickview', 'techstore_jck_quickview');
add_action('wp_ajax_nopriv_jck_quickview', 'techstore_jck_quickview');
function techstore_jck_quickview() {
    isset($_POST["product"]) or die();
    global $post, $product;
    $prod_id = $_POST["product"];
    $post = get_post($prod_id);
    $product = get_product($prod_id);
    wc_get_template('content-single-product-lightbox.php');
    die();
}

// **********************************************************************//
//	Mini cart - AJAX: Remove product from cart
// **********************************************************************//
function techstore_cart_remove_item() {
    global $woocommerce;
    $array = array('succes' => false);
    if(isset($_POST['item_key']) && ($item_key = $_POST['item_key'])){
        $cart = $woocommerce->cart;
        if ($removed = $cart->remove_cart_item($item_key)){
            $array['sl'] = $cart->get_cart_contents_count();
            $array['pr'] = $cart->get_cart_subtotal();
            $array['succes'] = true;
        }
    }
    
    exit(json_encode($array));
}
add_action('wp_ajax_lt_cart_remove_item' , 'techstore_cart_remove_item');
add_action('wp_ajax_nopriv_lt_cart_remove_item', 'techstore_cart_remove_item');

// **********************************************************************//
//	single add to cart - AJAX
// **********************************************************************//
function techstore_single_add_to_cart() {
    if(!isset($_POST['product_id']) || (int)$_POST['product_id'] <= 0){
        echo json_encode(array('error' => true));
        die();
    }
    
    $error = false;
    $product_id        = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
    $quantity          = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
    $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
    $product_status    = get_post_status($product_id);
    
    $type = (!isset($_POST['product_type']) || !in_array($_POST['product_type'], array('single', 'variations'))) ? 'single' : $_POST['product_type'];
    
    $variation_id = (int)$_POST['variation_id'];
    $variation = isset($_POST['variation']) ? $_POST['variation'] : array();
    
    $product = (new WC_Product_Factory())->get_product((int) $product_id);
    $product_variable = isset($product->product_type) && $product->product_type == 'variable' ? true : false;
    if((!$variation || !$variation_id) && ($type == 'variations' || $product_variable)){
        $error = true;
    }
    
    if (!$error && $passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variation) && 'publish' === $product_status) {
        
        do_action('woocommerce_ajax_added_to_cart', $product_id);
        if (get_option('woocommerce_cart_redirect_after_add') == 'yes') {
            wc_add_to_cart_message($product_id);
        }
        
        // Return fragments
        WC_AJAX::get_refreshed_fragments();
    } else {
        // If there was an error adding to the cart, redirect to the product page to show any errors
        $data = array(
            'error' => true,
            'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id)
        );
        echo json_encode($data);
    }
    die();
}
add_action('wp_ajax_lt_single_add_to_cart' , 'techstore_single_add_to_cart');
add_action('wp_ajax_nopriv_lt_single_add_to_cart', 'techstore_single_add_to_cart');

// **********************************************************************//
//	Load products page - AJAX - filter
// **********************************************************************//
function techstore_products_page() {
    global $lt_opt, $wp_query, $woocommerce, $techstore_wc_query;
    
    if(!$techstore_wc_query || !$woocommerce){
        die();
    }
    
    $json = array();
    $compare_ver = version_compare($woocommerce->version, '2.6.1', ">=");
    $baseUrl = $_GET['baseUrl'];
    $paged = (int)$_GET['paged'];
    $type_taxonomy = isset($_GET['taxonomy']) ? $_GET['taxonomy'] : 'product_cat';
    $term_id = (int) $_GET['catId'];
    
    $args = $techstore_wc_query->get_catalog_ordering_args();
    if(!$args){
        $args['orderby']  = 'menu_order title';
        $args['order']    = 'ASC';
    }
    $args['post_type'] = 'product';
    $args['posts_per_page'] = $lt_opt['products_pr_page'];
    $args['post_status'] = 'publish';
    $args['paged'] = !$paged ? '1' : $paged;
    $args['tax_query'] = array();
    $args['meta_query'] = array();
    
    $visibility = 'catalog';
    $hasResults = true; // Flag in search product
    
    // Filter by Category
    if($term_id > 0){
        $args['tax_query'][] = array(
            'taxonomy'  => $type_taxonomy,
            'field'     => 'id', 
            'terms'     => array($term_id)
        );
    }
    
    if ($_GET['hasSearch'] == 1 && isset($_GET['s'])) {
        // Search product
        $args['post__in'] = $techstore_wc_query->lt_getPostSearch($_GET['s']);
        $visibility = 'search';
        
        $hasResults = empty($args['post__in']) ? false : true;
    }
    
    if ($hasResults) {
        $args['meta_query'][] = array(
            'key' => '_visibility',
            'value' => array('visible', $visibility),
            'compare' => 'IN'
        );

        $_chosen_attributes = array();
        if(!empty($_GET['variations'])){
            foreach ($_GET['variations'] as $v){
                $_chosen_attributes['pa_' . $v['taxonomy']] = array(
                    'terms' => $v['values'],
                    'query_type' => ($v['type'] === 'or') ? 'or' : 'and'
               );
            }
            // Filter by variations
            if(!$compare_ver) {
                if(!isset($args['post__in'])){
                    $args['post__in'] = array();
                }

                $args['post__in'] = $techstore_wc_query->lt_filter_by_variations($_chosen_attributes, $args['post__in']);
            }else{
                foreach ($_GET['variations'] as $v){
                    $args['tax_query'][] = array(
                        'taxonomy' => 'pa_' . $v['taxonomy'],
                        'field'    => 'slug',
                        'terms'    => $v['slug'],
                        'operator' => ($v['type'] === 'or') ? 'IN' : 'AND',
                        'include_children' => false,
                    );
                }
            }
        }

        // Filter by price
        if($_GET['hasPrice'] && ($_GET['min_price'] || $_GET['min_price'])){
            if($compare_ver) {
                $min = isset($_GET['min_price']) ? floatval($_GET['min_price']) : 0;
                $max = isset($_GET['max_price']) ? floatval($_GET['max_price']) : 9999999999;

                $args['meta_query'][] = array(
                    'key'          => '_price',
                    'value'        => array($min, $max),
                    'compare'      => 'BETWEEN',
                    'type'         => 'DECIMAL',
                    'price_filter' => true,
                );
            }else{
                if(!isset($args['post__in'])){
                    $args['post__in'] = array();
                }
                $args['post__in'] = $techstore_wc_query->price_filter($args['post__in']);
            }
        }

        $wp_query = new WP_Query($args);

        // Contruct compare shortcode
        if (class_exists('YITH_Woocompare_Frontend')){
            new YITH_Woocompare_Frontend();
        } elseif (defined('YITH_WOOCOMPARE_DIR') && is_file(YITH_WOOCOMPARE_DIR . 'includes/class.yith-woocompare-frontend.php')) {
            require_once YITH_WOOCOMPARE_DIR . 'includes/class.yith-woocompare-frontend.php';
            new YITH_Woocompare_Frontend();
        }

        $_delay = 0;
        $_delay_item = (isset($lt_opt['delay_overlay']) && (int) $lt_opt['delay_overlay']) ? (int) $lt_opt['delay_overlay'] : 100;
        
        $count = 0;
        
        if($wp_query->post_count){
            ob_start();
            while ($wp_query->have_posts()){
                $wp_query->the_post();
                wc_get_template('content-product.php', array('_delay' => $_delay, 'wrapper' => 'li'));
                $_delay += $_delay_item;
                $count++;
            }
            wp_reset_postdata();
        }else{
            $hasResults = false;
        }
    }
    
    if(!$hasResults){
        ob_start();
        echo '<li class="row"><div class="large-12 columns">';
        wc_get_template('loop/no-products-found.php');
        echo '</div></li>';
    }
    $json['content'] = ob_get_clean();
    
    // Init shortcode
    if (class_exists('WPBMap')) {
        WPBMap::addAllMappedShortcodes();
    }
    
    // Get description term
    $json['description'] = techstore_term_description($term_id, $type_taxonomy);
    
    if($_GET['top'] == '1'){
        $json['cat_header'] = techstore_get_cat_header((int) $_GET['catId']);
        $json['recommend_products'] = techstore_get_recommend_product((int)$_GET['catId']);
    }
    
    if($count <= 1 && $wp_query->max_num_pages <= 1){
        $json['select_order'] = '';
    }else{
        ob_start(); // Refresh Select order
        woocommerce_catalog_ordering();
        $json['select_order'] = ob_get_clean();
    }
    
    ob_start(); // Refresh Pagination
    wc_get_template('loop/pagination.php', array(
        'baseUrl' => $baseUrl,
        'paged' => $paged
    ));
    $json['pagination'] = ob_get_clean();
    
    ob_start(); // Refresh Breadcrumb
    techstore_get_breadcrumb(true);
    $json['breadcrumb'] = ob_get_clean();
    
    // Obj URL
    $json['shop_url']   = wc_get_page_permalink('shop');
    $json['base_url']   = home_url('/');
    $json['friendly']   = preg_match('/\?post_type\=/', $json['shop_url']) ? '0' : '1';
    if(preg_match('/\?page_id\=/', $json['shop_url'])){
        $json['friendly'] = '0';
        $json['shop_url'] = $json['base_url'] . '?post_type=product';
    }
    
    // Refresh Variations
    $attribute_taxonomies = wc_get_attribute_taxonomies();
    $results = array();
    if ($attribute_taxonomies) {
        foreach ($attribute_taxonomies as $tax) {
            if (taxonomy_exists(wc_attribute_taxonomy_name($tax->attribute_name))) {
                $attribute_array[$tax->attribute_name] = $tax->attribute_name;
                $query_type = (isset($_chosen_attributes['pa_' . $tax->attribute_name]['query_type']) && $_chosen_attributes['pa_' . $tax->attribute_name]['query_type'] == 'or') ? 'or' : 'and';
                
                $results = techstore_count_by_variations('pa_'.$tax->attribute_name, $args, $results, $query_type);
            }
        }
    }
    $json['variations'] = $results;
    
    die(json_encode($json));
}
add_action('wp_ajax_lt_products_page' , 'techstore_products_page');
add_action('wp_ajax_nopriv_lt_products_page', 'techstore_products_page');

function techstore_count_by_variations($taxonomy, $args = array(), $result = array(), $query_type = 'and'){
    $get_terms_args = array('hide_empty' => '1');
    $orderby = wc_attribute_orderby($taxonomy);

    switch ($orderby) {
        case 'name' :
            $get_terms_args['orderby']    = 'name';
            $get_terms_args['menu_order'] = false;
            break;
        case 'id' :
            $get_terms_args['orderby']    = 'id';
            $get_terms_args['order']      = 'ASC';
            $get_terms_args['menu_order'] = false;
            break;
        case 'menu_order' :
            $get_terms_args['menu_order'] = 'ASC';
            break;
    }
    unset($args['paged']);
    $args['posts_per_page'] = -1;
    $args['fields'] = 'ids';
    
    $attr = str_replace('pa_', '', $taxonomy);
    $result[$attr] = array();
    
    if (0 < count($terms = get_terms($taxonomy, $get_terms_args))) {
        global $woocommerce;
        $compare_ver = version_compare($woocommerce->version, '2.6.1', ">=");
        $term_counts = $compare_ver ? get_filtered_term_product_counts(wp_list_pluck($terms, 'term_id'), $taxonomy, $query_type, $args, true) : null;
        
        foreach ($terms as $k => $term) {
            if(!$compare_ver){
                $args['tax_query'][1] = array(
                    array(
                        'taxonomy' 	=> $taxonomy,
                        'terms' 	=> $term->term_id,
                        'field' 	=> 'term_id'
                    )
                );
                $result[$attr]['lt_' . $attr . '_'.$term->term_id] = count(get_posts($args));
                unset($args['tax_query'][1]);
            }else{
                $result[$attr]['lt_' . $attr . '_' . $term->term_id] = isset($term_counts[$term->term_id]) ? $term_counts[$term->term_id] : 0;
            }
        }
    }
    
    return $result;
}

function get_filtered_term_product_counts($term_ids, $taxonomy, $query_type, $args, $ajax = false) {
    global $wpdb;
    
    if(!$ajax){
        $meta_query = WC_Query::get_main_meta_query();
        $tax_query  = WC_Query::get_main_tax_query();
    }else{
        $meta_query = isset($args['meta_query']) ? $args['meta_query'] : array();
        $tax_query  = isset($args['tax_query']) ? $args['tax_query'] : array();
    }
    
    if ('or' === $query_type) {
        foreach ($tax_query as $key => $query) {
            if ($taxonomy === $query['taxonomy']) {
                unset($tax_query[$key]);
            }
        }
    }
    
    $meta_query      = new WP_Meta_Query($meta_query);
    $tax_query       = new WP_Tax_Query($tax_query);
    $meta_query_sql  = $meta_query->get_sql('post', $wpdb->posts, 'ID');
    $tax_query_sql   = $tax_query->get_sql($wpdb->posts, 'ID');
    
    // Generate query
    $query           = array();
    $query['select'] = 'SELECT COUNT(DISTINCT ' . $wpdb->posts . '.ID) as term_count, terms.term_id as term_count_id';
    
    $query['from']   = 'FROM ' . $wpdb->posts;
    
    $query['join']   = 
        'INNER JOIN ' . $wpdb->term_relationships . ' AS term_relationships ON ' . $wpdb->posts . '.ID = term_relationships.object_id ' .
        'INNER JOIN ' . $wpdb->term_taxonomy . ' AS term_taxonomy USING(term_taxonomy_id) ' .
        'INNER JOIN ' . $wpdb->terms . ' AS terms USING(term_id) ' .
        $tax_query_sql['join'] . $meta_query_sql['join'];
    
    $query['where']   = 
        'WHERE ' . $wpdb->posts . '.post_type LIKE "product" ' .
        'AND ' . $wpdb->posts . '.post_status LIKE "publish" ' . 
        $tax_query_sql['where'] . $meta_query_sql['where'] . ' ' .
        'AND terms.term_id IN (' . implode(',', array_map('absint', $term_ids)) . ')';

    // For search case
    if(isset($_GET['s']) && $_GET['s']){
        $s = esc_sql(str_replace(array("\r", "\n"), '', stripslashes($_GET['s'])));
        
        $query['where'] .= ' AND (' . $wpdb->posts . '.post_title LIKE "%' . $s . '%" OR ' . $wpdb->posts . '.post_excerpt LIKE "%' . $s . '%" OR ' . $wpdb->posts . '.post_content LIKE "%' . $s . '%")';
    }

    $query['group_by'] = "GROUP BY terms.term_id";
    $queryString = implode(' ', apply_filters('woocommerce_get_filtered_term_product_counts_query', $query));
    $results = $wpdb->get_results($queryString);
    
    return wp_list_pluck($results, 'term_count', 'term_count_id');
}

function techstore_get_pagination_ajax(
    $total = 1,
    $current = 1,
    $type = 'list',
    $prev_text = 'PREV', 
    $next_text = 'NEXT',
    $end_size = 3, 
    $mid_size = 3,
    $prev_next = true,
    $show_all = false
) {
    
    if ($total < 2) {
        return;
    }
    
    if ($end_size < 1) {
        $end_size = 1;
    }
    
    if ($mid_size < 0) {
        $mid_size = 2;
    }
    
    $r = '';
    $page_links = array();
    
    // PREV Button
    if ($prev_next && $current && 1 < $current){
        $page_links[] = '<a class="lt-prev prev page-numbers" data-page="' . ((int)$current - 1) . '" href="javascript:void(0);">' . $prev_text . '</a>';
    }
    
    // PAGE Button
    for ($n = 1; $n <= $total; $n++){
        $page = number_format_i18n($n);
        if ($n == $current){
            $page_links[] = '<a class="lt-current current page-numbers" data-page="' . $page . '" href="javascript:void(0);">' . $page . '</a>';
        } elseif ($show_all || ($n <= $end_size || ($current && $n >= $current - $mid_size && $n <= $current + $mid_size) || $n > $total - $end_size)){
            $page_links[] = '<a class="lt-page page-numbers" data-page="' . $page . '" href="javascript:void(0);">' . $page . "</a>";
        }
    }
    
    // NEXT Button
    if ($prev_next && $current && ($current < $total || -1 == $total)){
        $page_links[] = '<a class="lt-next next page-numbers" data-page="' . ((int)$current + 1)  . '" href="javascript:void(0);">' . $next_text . '</a>';
    }
    
    // DATA Return
    switch ($type) {
        case 'array' :
            return $page_links;

        case 'list' :
            $r .= '<ul class="page-numbers lt-pagination-ajax"><li>';
            $r .= implode('</li><li>', $page_links);
            $r .= '</li></ul>';
            break;

        default :
            $r = implode('', $page_links);
            break;
    }
    
    return $r;
}

// Ajax search
add_action('wp_ajax_nopriv_live_search_products', 'techstore_live_search_products');
add_action('wp_ajax_live_search_products', 'techstore_live_search_products');
function techstore_live_search_products() {
    if (isset($_GET['s']) && trim($_GET['s']) != '') {
        $query_args = array(
            'posts_per_page' => 10,
            'no_found_rows' => true,
            'suppress_filters' => true,
            's' => $_GET['s']
        );

        $search_query = new WP_Query($query_args);
        $search_query->set('post_type', 'product');
        $results = array();

        if ($the_posts = $search_query->get_posts()) {
            foreach ($the_posts as $the_post) {
                $title = get_the_title($the_post->ID);
                if (has_post_thumbnail($the_post->ID)) {
                    $post_thumbnail_ID = get_post_thumbnail_id($the_post->ID);
                    $post_thumbnail_src = wp_get_attachment_image_src($post_thumbnail_ID, 'thumbnail');
                } else {
                    $size = wc_get_image_size('thumbnail');
                    $post_thumbnail_src = array(
                        wc_placeholder_img_src(),
                        esc_attr($size['width']),
                        esc_attr($size['height'])
                    );
                }

                $product = new WC_Product($the_post->ID);
                $results[] = array(
                    'title'     => html_entity_decode($title, ENT_QUOTES, 'UTF-8'),
                    'tokens'    => explode(' ', $title),
                    'url'       => get_permalink($the_post->ID),
                    'image'     => $post_thumbnail_src[0],
                    'price'     => $product->get_price_html()
                );
            }
        }
        wp_reset_postdata();

        die(json_encode($results));
    }
}

add_action('wp_head', 'techstore_search_live_options', 0, 0);
function techstore_search_live_options() {
    global $lt_opt;

    if ($enable = isset($lt_opt['enable_live_search']) ? $lt_opt['enable_live_search'] : true) {
        wp_enqueue_script('lt-typeahead-js', get_template_directory_uri() . '/js/min/typeahead.bundle.min.js', array('jquery'), '', true);
        wp_enqueue_script('lt-handlebars', get_template_directory_uri() . '/js/min/handlebars.min.js', array('lt-typeahead-js'), '', true);
    }

    $search_options = array(
        'live_search_template' => '<div class="item-search"><a href="{{url}}" class="lt-link-item-search" title="{{title}}"><img src="{{image}}" class="lt-item-image-search" height="60" width="60" /><div class="lt-item-title-search"><p>{{title}}</p></div></a></div>',
        'enable_live_search' => $enable
    );

    echo '<script type="text/javascript">var search_options=';
    echo $enable ? json_encode($search_options) : '"0"';
    echo ';</script>';
}