<?php
/**
 * Created by PhpStorm.
 * User: Adam Romanowski January 15th
 * Date: 24.08.2018
 * Time: 11:16
 */

use Carbon_Fields\Container;
use Carbon_Fields\Field;

add_action( 'carbon_fields_register_fields', 'crb_attach_theme_options' );
function crb_attach_theme_options() {

	Container::make( 'theme_options', __( 'Theme Options', 'crb' ) )
	         ->add_fields( array(
		         Field::make( 'text', 'crb_text', 'Text Field' ),
	         ) );


	Container::make( 'post_meta', __('Druga kolumna','elektrobim') )
		->add_fields( array(
			Field::make( 'rich_text', 'ebim_col2_content', 'Treść' )
		) );

}