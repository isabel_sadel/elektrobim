    </div>
    <!-- MAIN FOOTER -->

    <div class="shop-features">
        <?php //echo techstore_get_block(); ?>

        <div class="row">

            <div class="vc_ lt-col large-2 columns"><div class="vc_column-inner "><div class="wpb_wrapper"></div></div></div><div class="vc_ lt-col large-2 columns"><div class="vc_column-inner "><div class="wpb_wrapper">    <div class="service-block style-3 elektro_pointer">
                            <div class="box">
                                <a href="/#promo">
                                    <div class="service-icon buzz_effect pe-7s-gift"></div>
                                    <div class="service-text">
                                        <div class="service-title">Promocje</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div></div></div><div class="vc_ lt-col large-2 columns"><div class="vc_column-inner "><div class="wpb_wrapper">    <div class="service-block style-3 elektro_pointer">
                            <div class="box">
                                <a href="/dostawa">
                                    <div class="service-icon pe-7s-box2"></div>
                                    <div class="service-text">
                                        <div class="service-title">Dostawa DHL</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div></div></div><div class="vc_ lt-col large-2 columns"><div class="vc_column-inner "><div class="wpb_wrapper">    <div class="service-block style-3 elektro_pointer">
                            <div class="box">
                                <a href="/zwroty">
                                    <div class="service-icon rotate_effect pe-7s-refresh"></div>
                                    <div class="service-text">
                                        <div class="service-title">Łatwe Zwroty</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div></div></div><div class="vc_ lt-col large-2 columns"><div class="vc_column-inner "><div class="wpb_wrapper">    <div class="service-block style-3 elektro_pointer">
                            <div class="box">
                                <a href="/pomoc">
                                    <div class="service-icon pe-7s-help2"></div>
                                    <div class="service-text">
                                        <div class="service-title">Wsparcie</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div></div></div><div class="service-border-none vc_ lt-col large-2 columns"><div class="vc_column-inner "><div class="wpb_wrapper">    <div class="service-block style-3 elektro_pointer">
                            <div class="box">
                                <a href="/sklep">
                                    <div class="service-icon pe-7s-credit"></div>
                                    <div class="service-text">
                                        <div class="service-title">PayU</div>
                                    </div>
                                </a>
                            </div>
                        </div>

                    </div></div></div>

        </div>




    </div>

    <footer id="lt-footer" class="footer-wrapper">
        <?php do_action('techstore_footer_layout_style'); ?>
    </footer>
    <!-- END MAIN FOOTER -->
</div>

<a href="javascript:void(0);" id="top-link" class="wow bounceIn"><span class="icon-angle-up"></span></a>
<div class="scroll-to-bullets"></div>

<div class="static-position">
    <div class="black-window hidden-tag"></div>
    <div class="white-window hidden-tag"></div>
    <div class="warpper-mobile-search hidden-tag"><!-- for mobile -->
        <?php get_search_form();?>
    </div>
    <div id="heading-menu-mobile" class="hidden-tag"><i class="fa fa-bars"></i><?php esc_html_e('Navigation','techstore-theme');?></div>
    <div id="mobile-account" class="hidden-tag"><?php include get_template_directory() . '/includes/mobile-account.php';?></div>
    <div id="cart-sidebar" class="hidden-tag">
        <div class="lt-cart-fog hidden-tag"></div>
        <div class="cart-close">
            <h3 class="lt-tit-mycart"><?php echo esc_html__('MY CART', 'techstore-theme');?></h3>
            <a href="javascript:void(0);" title="<?php esc_html_e('Close', 'techstore-theme');?>"><?php esc_html_e('Close','techstore-theme');?></a>
            <hr />
        </div>
        <?php techstore_mini_cart_sidebar(); ?>
    </div>
    <input type="hidden" name="lt_logout_menu" value="<?php echo wp_logout_url(get_home_url()); ?>" />
    <?php if(!isset($lt_opt['cutting_product_name']) || $lt_opt['cutting_product_name'] == '1'):
        $lt_number_cut = (isset($lt_opt['cutting_product_number']) && (int) $lt_opt['cutting_product_number']) ? (int) $lt_opt['cutting_product_number'] : 15; ?>
        <input type="hidden" name="lt_cutting_product_name" value="<?php echo (int) $lt_number_cut;?>" />
    <?php endif; ?>
</div>
<?php wp_footer(); ?>
</body>
</html>