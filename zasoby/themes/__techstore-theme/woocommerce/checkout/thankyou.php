<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woothemes.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.2.0
 */

defined('ABSPATH') or exit;

if ($order) : ?>
<div class="row lt-order-received">
    <div class="large-5 columns lt-order-received-left">
        <div class="lt-warper-order">
            <?php if ($order->has_status('failed')) : ?>
                <p class="woocommerce-thankyou-order-failed"><?php esc_html_e('Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'techstore-theme'); ?></p>

                <p class="woocommerce-thankyou-order-failed-actions">
                    <a href="<?php echo esc_url($order->get_checkout_payment_url()); ?>" class="button pay"><?php esc_html_e('Pay', 'techstore-theme') ?></a>
                    <?php if (is_user_logged_in()) : ?>
                        <a href="<?php echo esc_url(wc_get_page_permalink('myaccount')); ?>" class="button pay"><?php esc_html_e('My Account', 'techstore-theme'); ?></a>
                    <?php endif; ?>
                </p>

            <?php else : ?>

                <p class="woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Dziękujemy. Otrzymaliśmy Twoje zamówienie.', 'techstore-theme'), $order); ?></p>
                <p>Szczegóły zamówienia wysłaliśmy na adres email <font color=green><?php echo $order->billing_email; ?></font><br/>Informacje o przebiegu realizacji zamówienia będziemy wysyłali na bieżąco drogą mailową i SMS.</p>
				<ul class="woocommerce-thankyou-order-details order_details">
                    <li class="order">
                        <?php esc_html_e('Numer zamówienia:', 'techstore-theme'); ?>
                        <strong><?php echo (int) $order->get_order_number(); ?></strong>
                    </li>
                    <li class="date">
                        <?php esc_html_e('Data zamówienia:', 'techstore-theme'); ?>
                        <strong><?php echo date_i18n(get_option('date_format'), strtotime($order->order_date)); ?></strong>
                    </li>
                    <li class="total">
                        <?php esc_html_e('Suma zamówienia:', 'techstore-theme'); ?>
                        <strong><?php echo (int) $order->get_total(); ?> zł</strong>
                    </li>
                    <?php if ($order->payment_method_title) : ?>
                        <li class="method">
                            <?php esc_html_e('Sposób płatności:', 'techstore-theme'); ?>
                            <strong><?php echo $order->payment_method_title; ?></strong>
                        </li>
                    <?php endif; ?>
                </ul>
                <div class="clear"></div>

            <?php endif; ?>
        </div>
    </div>
    <div class="large-7 columns lt-order-received-right">
        <div class="lt-warper-order">
            <?php do_action( 'woocommerce_thankyou_' . $order->get_payment_method(), $order->get_id() ); ?>
                <?php //do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
        </div>
    </div>
</div>



<header>
    <h2><?php _e( 'Customer details', 'woocommerce' ); ?></h2>
</header>
<dl class="customer_details">
<?php
    if ( $order->billing_email ) echo '<dt>' . __( 'Email:', 'woocommerce' ) . '</dt><dd>' . $order->billing_email . '</dd>';
    if ( $order->billing_phone ) echo '<dt>' . __( 'Numer telefonu:', 'woocommerce' ) . '</dt><dd>' . $order->billing_phone . '</dd>';
    // Additional customer details hook
    do_action( 'woocommerce_order_details_after_customer_details', $order );
?>
</dl>

<?php if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) : ?>

<div class="col2-set addresses">

    <div class="col-1">

<?php endif; ?>

        <header class="title">
            <h3><?php _e( 'Dane rozliczeniowe', 'woocommerce' ); ?></h3>
        </header>
        <address>
            <?php
                if ( ! $order->get_formatted_billing_address() ) _e( 'N/A', 'woocommerce' ); else echo $order->get_formatted_billing_address();
            ?>
        </address>

<?php if ( ! wc_ship_to_billing_address_only() && $order->needs_shipping_address() && get_option( 'woocommerce_calc_shipping' ) !== 'no' ) : ?>

    </div><!-- /.col-1 -->

    <div class="col-2">

        <header class="title">
            <h3><?php _e( 'Adres wysyłki', 'woocommerce' ); ?></h3>
        </header>
        <address>
            <?php
                if ( ! $order->get_formatted_shipping_address() ) _e( 'N/A', 'woocommerce' ); else echo $order->get_formatted_shipping_address();
            ?>
        </address>

    </div><!-- /.col-2 -->

</div><!-- /.col2-set -->

<?php endif; ?>


		<!-- Google Code for Elektrobim - Sprzeda&#380; Conversion Page -->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 992212438;
var google_conversion_language = "pl";
var google_conversion_format = "2";
var google_conversion_color = "ffffff";
var google_conversion_label = "_BSECMzluFkQ1uuP2QM";
var google_conversion_value = <?php echo $order->get_total(); ?>;
var google_conversion_currency = "PLN";
var google_remarketing_only = false;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//www.googleadservices.com/pagead/conversion/992212438/?value=<?php echo $order->get_total(); ?>&amp;currency_code=PLN&amp;label=_BSECMzluFkQ1uuP2QM&amp;guid=ON&amp;script=0"/>
</div>
</noscript>
<?php else : ?>
    <p class="woocommerce-thankyou-order-received"><?php echo apply_filters('woocommerce_thankyou_order_received_text', esc_html__('Thank you. Your order has been received.', 'techstore-theme'), null); ?></p>
<?php endif;