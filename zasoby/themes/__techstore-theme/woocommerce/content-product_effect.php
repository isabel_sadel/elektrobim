<a href="<?php the_permalink(); ?>">
    <div class="main-img"><?php echo $product->get_image('shop_catalog'); ?></div>
    <?php
    if ($attachment_ids) :
        $loop = 0;
        foreach ($attachment_ids as $attachment_id) :
            $image_link = wp_get_attachment_url($attachment_id);
            if (!$image_link):
                continue;
            endif;
            $loop++;
            printf('<div class="back-img back">%s</div>', wp_get_attachment_image($attachment_id, 'shop_catalog'));
            if ($loop == 1):
                break;
            endif;
        endforeach;
    else : ?>
        <div class="back-img"><?php echo $product->get_image('shop_catalog'); ?></div>
    <?php endif; ?>
</a>