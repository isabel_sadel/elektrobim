<?php
/**
 * Single Product Share
 *
 * Sharing plugins can hook into here or you can add your own code directly.
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly
?>
<hr class="lt-single-hr" />
<div class="lt-single-share">
    <span class="lt-single-share-text">
        <?php echo esc_html__('Share it: ', 'techstore-theme'); ?>
    </span>
    <?php
    echo (shortcode_exists('share')) ? do_shortcode('[share]') : '';
    do_action('woocommerce_share'); // Sharing plugins can hook into here
    ?>
</div>