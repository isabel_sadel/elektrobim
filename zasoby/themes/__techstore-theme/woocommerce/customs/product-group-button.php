<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
// $_cart_btn = techstore_add_to_cart_btn('small', false, $toltip);
$products_cats = $product->get_categories();
list($fistpart) = explode(',', $products_cats);
$classTip = $toltip ? ' tip-top' : '';
?>
<div class="product-summary">
    <hr class="lt-separator-product-group-btn" />
    <div class="product-interactions">
        <?php echo (!isset($lt_opt['disable-cart']) || !$lt_opt['disable-cart']) ? $_cart_btn : ''; ?>
        <?php if(defined('YITH_WCWL')):?>
            <div class="btn-wishlist<?php echo esc_attr($classTip); ?>" data-prod="<?php echo (int) $product->id; ?>" data-tip="<?php esc_html_e('Wishlist', 'techstore-theme'); ?>" title="<?php esc_html_e('Wishlist', 'techstore-theme'); ?>">
                <div class="btn-link ">
                    <div class="wishlist-icon">
                        <span class="pe-icon pe-7s-like"></span>
                        <span class="hidden-tag lt-icon-text"><?php esc_html_e('Wishlist', 'techstore-theme'); ?></span>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <span class="hidden-tag lt-seperator"></span>
        <div class="quick-view<?php echo esc_attr($classTip); ?>" data-prod="<?php echo (int) $product->id; ?>" data-tip="<?php esc_html_e('Quick View', 'techstore-theme'); ?>" data-head_type="<?php echo esc_attr($head_type);?>" title="            <?php esc_html_e('Quick View', 'techstore-theme'); ?>">
            <div class="btn-link">
                <div class="quick-view-icon">
                    <?php /* span class="pe-icon pe-7s-search"></span */?>
                    <span class="lt-icon-text"><i class="pe-icon pe-7s-look"></i></span>
                </div>
            </div>
        </div>
        <?php if(defined('YITH_WOOCOMPARE')):?>
            <div class="btn-compare<?php echo $classTip;?>" data-prod="<?php echo (int) $product->id; ?>" data-tip="<?php esc_html_e('Compare', 'techstore-theme'); ?>" title="<?php esc_html_e('Compare', 'techstore-theme'); ?>">
                <div class="btn-link">
                    <div class="compare-icon">
                        <span class="pe-icon pe-7s-repeat"></span>
                        <span class="lt-icon-text"><?php esc_html_e('Compare', 'techstore-theme'); ?></span>
                    </div>
                </div>
            </div>
        <?php endif;?>
        <div class="add-to-link">
            <?php echo defined('YITH_WCWL') ? do_shortcode('[yith_wcwl_add_to_wishlist]') : ''; ?>
            <div class="woocommerce-compare-button">
                <?php echo defined('YITH_WOOCOMPARE') ? do_shortcode('[yith_compare_button]') : ''; ?>
            </div>
        </div> 
    </div>
</div>