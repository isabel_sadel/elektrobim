<?php
/**
 *
 * Override this template by copying it to yourtheme/woocommerce/content-widget-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.5.0
 */
global $product, $lt_opt;
$class = ' wow fadeInUp ' . $lt_opt['animated_products'];

if(!isset($delay)){
    global $delay;
    $_delay_item = (isset($lt_opt['delay_overlay']) && (int) $lt_opt['delay_overlay']) ? (int) $lt_opt['delay_overlay'] : 100;
    $delay = !$delay ? 0 : $delay;
    $delay += $_delay_item;
}

$tag_wapper = (isset($wapper) && $wapper == 'div') ? '' : '<li class="li_wapper">';
echo $tag_wapper;
?>

<div class="row item-product-widget lt-item-widget-simple clearfix<?php echo esc_attr($class); ?>" data-wow-duration="1s" data-wow-delay="<?php echo (int) $delay; ?>ms">
    <div class="product-item-content">
        <div class="lt-content-product-widget-custom">
            <div class="large-4 medium-6 small-5 columns images">
                <a href="<?php echo esc_url(get_permalink($product->id)); ?>" title="<?php echo esc_attr($product->get_title()); ?>">
                    <?php echo $product->get_image('shop_thumbnail'); ?>
                    <div class="overlay"></div>
                </a>

            </div>
            <div class="large-8 medium-6 small-7 columns product-meta">
                <div class="product-title separator">
                    <a href="<?php echo esc_url(get_permalink($product->id)); ?>" title="<?php echo esc_attr($product->get_title()); ?>">
                        <?php echo esc_attr($product->get_title()); ?>
                    </a>
                </div>
                <div class="lt-text-cat"><?php echo '<span class="lt-text">' . $product->get_categories(); ?></div>
                <?php wc_get_template('loop/sale-flash.php'); ?>
                <div class="price separator"><?php echo $product->get_price_html(); ?></div>
            </div>
        </div>
        <div class="info">
            <div class="lt-info-group-box">
                <div class="large-12 medium-12 small-12 columns">
                    <div class="lt-btn-group-custom">
                        <?php techstore_product_group_button(false); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $tag_wapper;
