<?php
$disable_drag = (isset($disable_drag) && $disable_drag) ? true : false;
$url = esc_url(get_the_permalink());
?>
<div class="lt-slider owl-carousel lt-thumbs-slide" data-columns="1" data-columns-small="1" data-columns-tablet="1"<?php echo $disable_drag ? ' data-disable-drag="true"' : ''; ?> data-reload="true">
    <?php
    printf('<div class="content-img"><a href="%s">%s</a></div>', $url, $product->get_image('shop_catalog'));
    if ($attachment_ids) :
        foreach ($attachment_ids as $attachment_id) :
            $image_link = wp_get_attachment_url($attachment_id);
            if (!$image_link) :
                continue;
            endif;
            printf('<div class="content-img"><a href="%s">%s</a></div>', $url, wp_get_attachment_image($attachment_id, 'shop_catalog'));
        endforeach;
    endif;
    ?>
</div>
