<?php
/**
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.0.0
 */
if (!defined('ABSPATH'))
    exit; // Exit if accessed directly
global $lt_opt, $wp_query;

$_delay = 0;
$_delay_item = (isset($lt_opt['delay_overlay']) && (int) $lt_opt['delay_overlay']) ? (int) $lt_opt['delay_overlay'] : 100;

$cat = $wp_query->get_queried_object();
$catId = 0;
$lt_type_page = 'product_cat';
$lt_href_page = '';
if (isset($cat->term_id) && isset($cat->taxonomy)) {
    $catId = $cat->term_id;
    $lt_type_page = $cat->taxonomy;
    $lt_href_page = esc_url(get_term_link((int) $cat->term_id, $lt_type_page));
}

$lt_sidebar = (isset($lt_opt['category_sidebar']) && $lt_opt['category_sidebar']) ? $lt_opt['category_sidebar'] : '';
if (isset($_GET['right'])) {
    $lt_sidebar = 'right-sidebar';
}
if (isset($_GET['no-sidebar'])) {
    $lt_sidebar = 'no-sidebar';
}
$typelist = (isset($_COOKIE['gridcookie']) && $_COOKIE['gridcookie'] == 'list') ? true : false;

$hasSidebar = true;
switch ($lt_sidebar):
    case 'right-sidebar':
        $attr = 'large-9 columns left has-sidebar';
        break;

    case 'left-sidebar':
        $attr = 'large-9 columns right has-sidebar';
        break;

    default :
        $hasSidebar = false;
        $attr = 'large-12 columns no-sidebar';
        break;
endswitch;

$lt_recom_pos = isset($lt_opt['recommend_product_position']) ? $lt_opt['recommend_product_position'] : 'top';

get_header('shop');
techstore_get_breadcrumb();
?>

<div class="row category-page">
    <?php do_action('woocommerce_before_main_content'); ?>
    <div class="<?php echo esc_attr($attr); ?>">
        <?php if($lt_recom_pos !== 'bot'){?>
            <span id="position-lt-recommend-product" class="hidden-tag"></span>
            <?php echo techstore_get_recommend_product($catId); ?>
        <?php }?>

        <span id="position-lt-cat-header" class="hidden-tag"></span>
        <?php echo techstore_get_cat_header($catId); ?>

        <div class="row filters-container">
            <div class="large-4 columns">

            </div>
            <div class="large-8 columns">
                <ul class="sort-bar">
                    <?php if ($hasSidebar): ?>
                        <li class="li-toggle-sidebar">
                            <a class="toggle-sidebar" href="javascript:void(0);">
                                <i class="icon-menu"></i> <?php esc_html_e('Kategorie', 'techstore-theme'); ?>
                            </a>
                        </li>
                    <?php endif; ?>
                    <li class="lt-filter-order filter-order"><?php do_action('woocommerce_before_shop_loop'); ?></li>
                </ul>
            </div>
        </div>

        <header>
            <h1>
                <?php
                    if( !is_search() ) echo single_cat_title();
                        else echo "Wyniki wyszukiwania";
                    ?>
            </h1>
        </header>

        <div class=""><?php echo techstore_term_description($catId, $lt_type_page); ?></div>
		<div style="margin-top:30px;"></div>

        <?php woocommerce_product_loop_start(); ?>
        <?php if (have_posts()) : ?>
            <?php woocommerce_product_subcategories(); ?>
            <?php while (have_posts()) : the_post(); ?>
                <!-- Product Item -->
                <?php wc_get_template('content-product.php', array('_delay' => $_delay, 'wrapper' => 'li')); ?>
                <!-- End Product Item -->
                <?php $_delay += $_delay_item; ?>
            <?php endwhile; ?>
        <?php elseif (!woocommerce_product_subcategories(array('before' => woocommerce_product_loop_start(false), 'after' => woocommerce_product_loop_end(false)))) : ?>
            <li class="row"><div class="large-12 columns"><?php wc_get_template('loop/no-products-found.php'); ?></div></li>
        <?php endif; ?>
            
        <?php woocommerce_product_loop_end(); ?>
        
        <div class="row filters-container-down">
            <!-- Pagination -->
            <?php do_action('woocommerce_after_shop_loop'); ?>
            <!-- End Pagination -->
        </div>

        <?php do_action('woocommerce_after_main_content'); ?>
        
        <?php if($lt_recom_pos == 'bot'){?>
            <span id="position-lt-recommend-product" class="hidden-tag"></span>
            <?php echo techstore_get_recommend_product($catId); ?>
        <?php }?>
    </div>

    <?php if ($lt_sidebar == 'right-sidebar') : ?>
        <div class="large-3 right columns col-sidebar">
            <?php if (is_active_sidebar('shop-sidebar')) : dynamic_sidebar('shop-sidebar'); endif; ?>
        </div>
    <?php elseif ($lt_sidebar == 'left-sidebar') : ?>
        <div class="large-3 left columns col-sidebar">
            <?php if (is_active_sidebar('shop-sidebar')) : dynamic_sidebar('shop-sidebar'); endif; ?>
        </div>
    <?php endif; ?>

</div>

<div class="hidden-tag">
    <div class="current-cat hidden-tag">
        <a data-id="<?php echo isset($cat->term_id) ? (int) $cat->term_id : ''; ?>" href="<?php echo esc_url($lt_href_page); ?>" class="lt-filter-by-cat" id="lt-hidden-current-cat" data-taxonomy="<?php echo esc_attr($lt_type_page); ?>"></a>
    </div>
    <p><?php esc_html_e('No products were found matching your selection.', 'techstore-theme'); ?></p>
    <?php if ($s = get_search_query()): ?>
        <input type="hidden" name="lt_hasSearch" id="lt_hasSearch" value="<?php echo esc_attr($s); ?>" />
    <?php endif; ?>
</div>

<?php
get_footer('shop');