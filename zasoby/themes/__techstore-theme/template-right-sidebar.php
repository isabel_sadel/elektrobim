<?php
/*
Template name: 2 col
*/
get_header();
techstore_get_breadcrumb();

if( function_exists('carbon_get_post_meta') ) {
    $col2_content = carbon_get_post_meta( get_the_ID(), 'ebim_col2_content' );
} else {
	$col2_content = "";
}

?>

<div class="page-header">
    <?php if( has_excerpt() ) the_excerpt();?>
</div>

<div class="container-wrap page-left-sidebar">
    <div class="row">

            <div class="page-inner">
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <header class="entry-header">
                        <h1 class="entry-title"><?php the_title(); ?></h1>
                    </header>


                    <div class="large-3 right columns">
		                <?php echo wp_kses_post( wpautop( $col2_content ) ); ?>
                    </div>

                    <div id="content" class="large-8 left columns" role="main">

                        <div class="entry-content">
                            <?php the_content(); ?>
                            <?php
                            wp_link_pages( array(
                                'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'techstore-theme' ),
                                'after'  => '</div>',
                            ) );
                            ?>
                        </div>

                    </div>


                </article>
            </div>

    </div>
</div>

<?php get_footer(); ?>
