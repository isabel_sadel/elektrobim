<?php

use Carbon_Fields\Container;
use Carbon_Fields\Field;

require_once( 'includes/fields.php' );

add_action( 'after_setup_theme', 'crb_load' );
function crb_load() {
	require_once( 'vendor/autoload.php' );
	\Carbon_Fields\Carbon_Fields::boot();
}



/**
 *
 * @package leetheme

 *
 *
 *
/* Check if WooCommerce is active */
define('LEE_WOOCOMMERCE_ACTIVED', (bool) class_exists('WooCommerce'));

/* Check if Lee_framework is active */
defined('LEE_FRAMEWORK_ACTIVED') or define('LEE_FRAMEWORK_ACTIVED', false);

if (!isset($content_width)){
    $content_width = 1000; /* pixels */
}
require_once get_template_directory() . '/admin/index.php';
if(is_admin()){
    /* * ********** Plugin recommendations ********* */
    require_once (get_template_directory() . '/includes/class-tgm-plugin-activation.php');
    add_action('tgmpa_register', 'techstore_register_required_plugins');
}

add_filter( 'woocommerce_product_single_add_to_cart_text', 'woo_custom_cart_button_text' );    // 2.1 +
 
function woo_custom_cart_button_text() {
 
        return __( 'Do koszyka', 'woocommerce' );
 
}

function techstore_register_required_plugins() {
    $plugins = array(
        array(
            'name' => esc_html__( 'WooCommerce', 'techstore-theme' ),
            'slug' => 'woocommerce',
            'source' => esc_url('https://downloads.wordpress.org/plugin/woocommerce.2.6.9.zip'),
            'required' => true,
            'version' => '2.6.9', 
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Lee Framework', 'techstore-theme'),
            'slug' => 'lee_framework',
            'source' => get_template_directory() . '/includes/plugins/lee_framework.zip',
            'required' => true,
            'version' => '1.0',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('WPBakery Visual Composer', 'techstore-theme'),
            'slug' => 'js_composer',
            'source' => get_template_directory() . '/includes/plugins/js_composer.zip',
            'required' => true,
            'version' => '5.0.1',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Taxonomy Metadata', 'techstore-theme'),
            'slug' => 'taxonomy-metadata',
            'source' => esc_url('https://downloads.wordpress.org/plugin/taxonomy-metadata.zip'),
            'required' => true,
            'version' => '0.5',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('YITH WooCommerce Wishlist', 'techstore-theme'),
            'slug' => 'yith-woocommerce-wishlist',
            'source' => esc_url('https://downloads.wordpress.org/plugin/yith-woocommerce-wishlist.2.0.16.zip'),
            'required' => true,
            'version' => '2.0.16',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('YITH WooCommerce Compare', 'techstore-theme'),
            'slug' => 'yith-woocommerce-compare',
            'source' => esc_url('https://downloads.wordpress.org/plugin/yith-woocommerce-compare.2.0.8.zip'),
            'required' => true,
            'version' => '2.0.8',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('Contact Form 7', 'techstore-theme'),
            'slug' => 'contact-form-7',
            'source' => esc_url('https://downloads.wordpress.org/plugin/contact-form-7.zip'),
            'required' => true,
            'version' => '4.6',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        ),
        array(
            'name' => esc_html__('WP Instagram Widget', 'techstore-theme'),
            'slug' => 'wp-instagram-widget',
            'source' => get_template_directory() . '/includes/plugins/wp-instagram-widget.zip',
            'required' => true,
            'version' => '1.9.8',
            'force_activation' => false,
            'force_deactivation' => false,
            'external_url' => '',
        )
    );

    $config = array(
        'domain' => 'techstore-theme', // Text domain - likely want to be the same as your theme.
        'default_path' => '', // Default absolute path to pre-packaged plugins
        'parent_slug' => 'themes.php', // Default parent menu slug
        'menu' => 'install-required-plugins', // Menu slug
        'has_notices' => true, // Show admin notices or not
        'is_automatic' => false, // Automatically activate plugins after installation or not
        'message' => '', // Message to output right before the plugins table
        'strings' => array(
            'page_title' => esc_html__('Install Required Plugins', 'techstore-theme'),
            'menu_title' => esc_html__('Install Plugins', 'techstore-theme'),
            'installing' => esc_html__('Installing Plugin: %s', 'techstore-theme'), // %1$s = plugin name
            'oops' => esc_html__('Something went wrong with the plugin API.', 'techstore-theme'),
            'notice_can_install_required' => _n_noop('This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'techstore-theme'), // %1$s = plugin name(s)
            'notice_can_install_recommended' => _n_noop('This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'techstore-theme'), // %1$s = plugin name(s)
            'notice_cannot_install' => _n_noop('Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'techstore-theme'), // %1$s = plugin name(s)
            'notice_can_activate_required' => _n_noop('The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'techstore-theme'), // %1$s = plugin name(s)
            'notice_can_activate_recommended' => _n_noop('The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'techstore-theme'), // %1$s = plugin name(s)
            'notice_cannot_activate' => _n_noop('Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'techstore-theme'), // %1$s = plugin name(s)
            'notice_ask_to_update' => _n_noop('The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'techstore-theme'), // %1$s = plugin name(s)
            'notice_cannot_update' => _n_noop('Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'techstore-theme'), // %1$s = plugin name(s)
            'install_link' => _n_noop('Begin installing plugin', 'Begin installing plugins', 'techstore-theme'),
            'activate_link' => _n_noop('Activate installed plugin', 'Activate installed plugins', 'techstore-theme'),
            'return' => esc_html__('Return to Required Plugins Installer', 'techstore-theme'),
            'plugin_activated' => esc_html__('Plugin activated successfully.', 'techstore-theme'),
            'complete' => esc_html__('All plugins installed and activated successfully. %s', 'techstore-theme'), // %1$s = dashboard link
            'nag_type' => 'updated' // Determines admin notice type - can only be 'updated' or 'error'
        )
    );

    tgmpa($plugins, $config);
}

if (!function_exists('techstore_setup')) :

    function techstore_setup() {
        load_theme_textdomain('techstore-theme', get_template_directory() . '/languages');
        add_theme_support('woocommerce');
        add_theme_support('automatic-feed-links');

        add_theme_support('post-thumbnails');
        add_theme_support('title-tag');
        add_theme_support('custom-background');
        add_theme_support('custom-header');

        register_nav_menus(array(
            'primary' => esc_html__('Main Menu', 'techstore-theme'),
            'my_account' => esc_html__('My Account', 'techstore-theme'),
            'header_menu' => esc_html__('Header Categories', 'techstore-theme'),
            'footer_menu' => esc_html__('Footer Menu', 'techstore-theme'),
            'topbar-menu' => esc_html__('Topbar Menu Only display level 1 of menu', 'techstore-theme'),
        ));

        require_once get_template_directory() . '/includes/dynamic-css.php';
        require_once get_template_directory() . '/includes/theme-functions.php';
        require_once get_template_directory() . '/includes/woo-functions.php';
        require_once get_template_directory() . '/includes/theme-options.php';
        require_once get_template_directory() . '/includes/shop-ajax.php';
        require_once get_template_directory() . '/includes/lt-mega-menu.php';
    }

endif;
add_action('after_setup_theme', 'techstore_setup');

/**
 * Enqueue scripts and styles
 */
add_action('wp_enqueue_scripts', 'techstore_dequeue_wc_fragments', 100);
function techstore_dequeue_wc_fragments() {
    wp_dequeue_script('wc-cart-fragments');
}

add_action('wp_enqueue_scripts', 'techstore_scripts');
function techstore_scripts() {
    wp_enqueue_style('icons', get_template_directory_uri() . '/css/fonts.css', array(), null, 'all');
    wp_enqueue_style('animate', get_template_directory_uri() . '/css/animate.css', array(), null, 'all');
    wp_enqueue_style('owlcarousel-css', get_template_directory_uri() . '/css/owl.carousel.min.css', array(), null, 'all');
    wp_enqueue_style('style', get_stylesheet_uri(), array(), null, 'all'); // Main Css

    wp_enqueue_script('jquery'); // Library Jquery
    wp_enqueue_script('cookie', get_template_directory_uri() . '/js/min/jquery.cookie.min.js', array(), null, true);
    wp_enqueue_script('modernizer', get_template_directory_uri() . '/js/modernizr.js', array(), null, true);
    wp_enqueue_script('scrollTo', get_template_directory_uri() . '/js/min/jquery.scrollTo.min.js', array(), null, true);
    wp_enqueue_script('JRespond', get_template_directory_uri() . '/js/min/jquery.jRespond.min.js', array(), null, true);
    wp_enqueue_script('hoverIntent', get_template_directory_uri() . '/js/min/jquery.hoverIntent.min.js', array(), null, true);
    wp_enqueue_script('jpanelmenu', get_template_directory_uri() . '/js/min/jquery.jpanelmenu.min.js', array(), null, true);
    wp_enqueue_script('waypoints', get_template_directory_uri() . '/js/min/jquey.waypoints.js', array(), null, true);
    wp_enqueue_script('packer', get_template_directory_uri() . '/js/min/jquery.packer.js', array(), null, true);
    wp_enqueue_script('tipr', get_template_directory_uri() . '/js/min/jquery.tipr.min.js', array(), null, true);
    wp_enqueue_script('variations', get_template_directory_uri() . '/js/min/jquery.variations.min.js', array(), null, true);
    wp_enqueue_script('magnific-popup', get_template_directory_uri() . '/js/min/jquery.magnific-popup.js', array(), null, true);
    wp_enqueue_script('owlcarousel-js', get_template_directory_uri() . '/js/min/owl.carousel.min.js', array(), null, true);
    wp_enqueue_script('slick-slider', get_template_directory_uri() . '/js/min/jquey.slick.min.js', array(), null, true);
    wp_enqueue_script('parallax', get_template_directory_uri() . '/js/min/jquery.stellar.min.js', array(), null, true);
    wp_enqueue_script('countdown', get_template_directory_uri() . '/js/min/countdown.min.js', array(), null, true);
    wp_enqueue_script('easyzoom', get_template_directory_uri() . '/js/min/jquery.easyzoom.min.js', array(), null, true);
    wp_enqueue_script('wow-js', get_template_directory_uri() . '/js/min/wow.min.js', array(), false, true);
    wp_enqueue_script('scrollbar-js', get_template_directory_uri() . '/js/min/jquery.slimscroll.min.js', array(), false, true);
    wp_enqueue_script('theme-js', get_template_directory_uri() . '/js/min/main.min.js', array(), '1.1', true);

    wp_deregister_style('yith-wcwl-font-awesome');
    wp_deregister_style('yith-wcwl-font-awesome-ie7');
    wp_deregister_style('yith-wcwl-main');

    if (!is_admin()) {
        wp_deregister_style('woocommerce-layout');
        wp_deregister_style('woocommerce-smallscreen');
        wp_deregister_style('woocommerce-general');
    }

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}

//* Enqueue script to activate WOW.js
//* Add JavaScript before </body>
function techstore_wow_init() {
    global $lt_opt;
    
    echo '<script type="text/javascript">' . ((!isset($lt_opt['disable_wow']) || !$lt_opt['disable_wow']) ? 'var wow_enable = true; new WOW().init();' : 'var wow_enable = false;') . '</script>';
}

add_action('wp_enqueue_scripts', 'techstore_wow_init_in_footer');
function techstore_wow_init_in_footer() {
    add_action('print_footer_scripts', 'techstore_wow_init');
}

/* UNREGISTRER DEFAULT WOOCOMMERCE HOOKS */
remove_action('woocommerce_single_product_summary', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15);
remove_action('woocommerce_after_single_product_summary', 'woocommerce_output_product_data_tabs', 10);
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_login_form', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_show_messages', 10);
remove_action('woocommerce_before_shop_loop', 'woocommerce_result_count', 20);
remove_action('woocommerce_cart_collaterals', 'woocommerce_cart_totals', 10);
remove_action('woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10);


add_action('techstore_shop_category_count', 'woocommerce_result_count', 20);
add_action('init', 'techstore_post_type_support');

function techstore_post_type_support() {
    add_post_type_support('page', 'excerpt');
}

// Default sidebars
function techstore_widgets_sidebars_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'techstore-theme'),
        'id' => 'sidebar-main',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Shop Sidebar', 'techstore-theme'),
        'id' => 'shop-sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Product Sidebar', 'techstore-theme'),
        'id' => 'product-sidebar',
        'before_widget' => '<aside id="%1$s" class="widget %2$s">',
        'after_widget' => '</aside>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Home Sidebar', 'techstore-theme'),
        'id' => 'home-sidebar',
        'before_widget' => '<aside id="%1$s" class="home-sidebar">',
        'after_widget' => '</aside>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Custom Sidebar', 'techstore-theme'),
        'id' => 'custom-sidebar',
        'before_widget' => '<aside id="%1$s" class="custom-sidebar">',
        'after_widget' => '</aside>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer', 'techstore-theme'),
        'id' => 'sidebar-footer',
        'before_widget' => '<div id="%1$s" class="large-3 columns widget left %2$s">',
        'after_widget' => '</div>',
    ));

    register_sidebar(array(
        'name' => esc_html__('Footer 1', 'techstore-theme'),
        'id' => 'footer-1',
        'before_widget' => '<aside id="%1$s" class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '',
        'after_title' => ''
    ));
    
    register_sidebar(array(
        'name' => esc_html__('Footer 2', 'techstore-theme'),
        'id' => 'footer-2',
        'before_widget' => '<aside id="%1$s" class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '',
        'after_title' => ''
    ));
    
    register_sidebar(array(
        'name' => esc_html__('Footer 3', 'techstore-theme'),
        'id' => 'footer-3',
        'before_widget' => '<aside id="%1$s" class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '',
        'after_title' => ''
    ));
    
    register_sidebar(array(
        'name' => esc_html__('Footer 4', 'techstore-theme'),
        'id' => 'footer-4',
        'before_widget' => '<aside id="%1$s" class="widget">',
        'after_widget' => '</aside>',
        'before_title' => '',
        'after_title' => ''
    ));
}
add_action('widgets_init', 'techstore_widgets_sidebars_init');

// Remove WP Version From Styles	
add_filter( 'style_loader_src', 'sdt_remove_ver_css_js', 9999 );
// Remove WP Version From Scripts
add_filter( 'script_loader_src', 'sdt_remove_ver_css_js', 9999 );

// Function to remove version numbers
function sdt_remove_ver_css_js( $src ) {
	if ( strpos( $src, 'ver=' ) )
		$src = remove_query_arg( 'ver', $src );
	return $src;
}

// Remove WooCommerce Updater
remove_action('admin_notices', 'woothemes_updater_notice');

require_once get_template_directory() . '/includes/google-fonts.php';
require_once get_template_directory() . '/includes/class-wc-product-data-fields.php';
require_once get_template_directory() . '/includes/lt-ext-wc-query.php';

// Includes Woocommerce widgets custom
require_once get_template_directory() . '/woocommerce/widgets/lt-product-categories.php';
require_once get_template_directory() . '/woocommerce/widgets/lt-product-filter-price.php';
require_once get_template_directory() . '/woocommerce/widgets/lt-product-filter-variations.php';
require_once get_template_directory() . '/woocommerce/widgets/lt-tag-cloud.php';
