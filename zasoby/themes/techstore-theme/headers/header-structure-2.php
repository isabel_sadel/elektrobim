<?php techstore_get_header_sticky('primary', 'full'); // Sticky ?>
<div class="header-wrapper header-type-2<?php echo esc_attr($header_classes); ?>">
    <?php //!-- Top bar --?>
    <?php do_action( 'before' ); ?>
    <?php include get_template_directory() . '/headers/top-bar.php';?>
    <?php //!-- End Top bar --?>
    
    <?php //!-- Masthead --?>
    <div class="sticky-wrapper">
        <header id="masthead" class="site-header">
            <div class="row">
                <div class="large-12 columns header-container">
                    <!-- Mobile Menu -->
                    <div class="mobile-menu">
                        <?php techstore_mobile_header(); ?>
                    </div>
                    <!-- Logo -->
                    <div class="logo-wrapper">
                        <?php techstore_logo(); ?>
                    </div>

                    <!-- Group icons -->
                    <div class="header-utilities">

                        <?php //echo techstore_get_block(); ?>

                        <div class="large-5 columns header-utilities__search">
                            <div class="header-search">
                                <?php techstore_search('full'); ?>
                            </div>
                        </div>

                        <div class="large-7 columns header-utilities__ui">


                            <ul class="header-icons header-icons--contact">
                                <li>
                                    <a href="mailto:biuro@elektrobim.pl"><span class="icon pe-7s-mail"></span> biuro@elektrobim.pl</a></li>
                                </li>

                                <li>
                                    <a href="tel:0048501129778"><span class="icon pe-7s-call"></span> 501 129 778</a>
                                </li>
                            </ul>

                            <ul class="header-icons">
                                <li class="first header-icons__login">
                                    <?php if ( is_user_logged_in() ) { ?>
                                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Moje Konto','woothemes'); ?>"><span class="icon pe-7s-user"></span>&nbsp; <?php _e('Moje Konto','woothemes'); ?></a>

                                        <a href="<?php echo wp_logout_url( home_url() ); ?>"><span class="icon pe-7s-plug"></span> Wyloguj się</a>
                                    <?php }
                                    else { ?>
                                        <span class="icon pe-7s-lock"></span>
                                        <a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('Logowanie / Rejestracja','woothemes'); ?>"><?php _e('Logowanie / Rejestracja','woothemes'); ?></a>
                                    <?php } ?>

                                </li>
                                <!--<li class="first lt-icon-compare-total"><?php /*echo do_shortcode(techstore_icon_compare());*/?></li>-->
                                <!--<li><?php /*techstore_icon_wishlist(true);*/?></li>-->
                                <li class="header-icons__cart"><?php techstore_mini_cart('full') ?></li>
                            </ul>
                        </div>

                    </div>

                </div>
            </div>
            <div class="lt-mgr-top-20">
                <div class="large-12">
                    <!-- Main navigation - Full width style -->
                    <div class="wide-nav lt-bg-dark <?php echo $menu_transparent;?>">
                        <div class="row">
                            <div class="large-3 columns" id="lt-menu-vertical-header">
                                <?php techstore_get_vertical_menu(); ?>
                            </div>
                            <div class="large-9 columns">
                                <?php techstore_get_main_menu(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
    </div>
</div>