<?php techstore_get_header_sticky('header_menu', 'full');  // Sticky ?>
<div class="header-wrapper header-type-1<?php echo esc_attr($header_classes);?>">
    <?php //!-- Top bar --?>
    <?php do_action( 'before' ); ?>
    <?php include get_template_directory() . '/headers/top-bar.php';?>
    <?php //!-- End Top bar --?>
    
    <?php //!-- Masthead --?>
    <div class="sticky-wrapper">
        <header id="masthead" class="row site-header">
            <div class="large-12 columns header-container">
                <!-- Mobile Menu -->
                <div class="mobile-menu">
                    <?php techstore_mobile_header(); ?>
                </div>
                <!-- Logo -->
                <div class="logo-wrapper">
                    <?php techstore_logo(); ?>
                </div>
                <!-- Main navigation - Full width style -->
                <div class="wide-nav text-right">
                    <?php techstore_get_main_menu('header_menu'); ?>
                </div>
            </div>
        </header>
    </div>
</div>