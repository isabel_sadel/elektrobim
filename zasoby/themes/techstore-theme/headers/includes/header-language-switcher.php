<div class="language-switcher">
    <div class="label-title"><?php esc_html_e('Language', 'techstore-theme'); ?></div>
    <?php do_action('techstore_language_switcher'); ?>
</div>