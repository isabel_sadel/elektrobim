<?php
/**
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * @package leetheme
 */
if (!isset($lt_opt['blog_layout'])) {
    $lt_opt['blog_layout'] = '';
}

$hasSidebar = true;
$left = true;
/*switch ($lt_opt['blog_layout']):
    case 'left-sidebar':
        $attr = 'large-9 right columns';
        break;
    case 'right-sidebar':
        $left = false;
        $attr = 'large-9 left columns';
        break;
    case 'no-sidebar':
        $hasSidebar = false;
        $left = false;
        $attr = 'large-10 columns large-offset-1';
        break;
    default:
        $left = false;
        $attr = 'large-9 left columns';
        break;
endswitch;*/
$attr = 'large-12 left columns';
$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

get_header();
techstore_get_breadcrumb();
?>

<div class="container-wrap page-<?php echo ($lt_opt['blog_layout']) ? esc_attr($lt_opt['blog_layout']) : 'right-sidebar'; ?>">

    <div class="row">
        <div id="content" class="<?php echo esc_attr($attr); ?>" role="main">
            <?php if (have_posts()) : ?>
                <header class="page-header">
                    <h1 class="page-title page-title--archive">
                        Blog
                    </h1>

                    <div class="taxonomy-description taxonomy-description--blog"><?php echo category_description( 46 ) ?></div>
                </header>

                <div class="page-inner ebim-posts">

                    <div class="row">

                    <?php
                    $i=0;
                    while (have_posts()) :
                        the_post();
	                    $extra_class = '';

	                    $thumb_src = false;

	                    if( has_post_thumbnail() ) {
		                    $thumb_id = get_post_thumbnail_id();
		                    $thumb_src = get_the_post_thumbnail_url( get_the_ID(), 'lt-category-thumb' );
	                    }

	                    if( $thumb_src ) {
		                    $post_bg = 'background-image: url(' . esc_url_raw($thumb_src ) . ');';
	                    } else {
		                    $extra_class = 'no-thumb';
		                    $post_bg = '';
	                    }

	                    if( $i < 2 AND $paged <= 1 ) {
		                    $extra_class .= ' ebim-posts__item--feat';
	                        $size_classes = 'large-6';
	                    } else {
	                        $size_classes = 'medium-6 large-4';
	                    }



                    ?>

                        <div class="small-12 <?php echo $size_classes; ?> left columns">

                        <?php
                        //get_template_part( 'content', get_post_format() );
                        //$part = get_post_format();
                        //$part = trim($part) != '' ? '-' . trim($part) : '';
                        //include get_template_directory() . '/content' . $part . '.php';


                        ?>

                            <article class="ebim-posts__item <?php echo esc_attr($extra_class); ?> " style="<?php echo $post_bg; ?>" >

                                <a href="<?php echo get_permalink(); ?>" title="<?php echo get_the_title(); ?>" class="ebim-posts__item--link">

                                    <h2 class="ebim-posts__item__title"><span><?php echo get_the_title(); ?></span></h2>

                                </a>

                            </article>

                        </div>

                    <?php
                    $i++;
                    endwhile;
                    ?>

                    </div>

                    <div class="large-12 columns navigation-container">
                        <?php techstore_content_nav('nav-below'); ?>
                    </div>
                </div>
            <?php else : ?>
                <?php get_template_part('no-results', 'archive'); ?>
            <?php endif; ?>
        </div>


    </div>
</div>

<?php
get_footer();