<?php
/**
 *
 * Override this template by copying it to yourtheme/woocommerce/content-product.php
 *
 * @author 	WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.6.1
 */
global $product, $lt_opt;

if (!$product->is_visible()):
    return;
endif;

if (isset($is_deals) && $is_deals)
    $time_sale = get_post_meta($product->id, '_sale_price_dates_to', true);
$attachment_ids = $product->get_gallery_attachment_ids();

$stock_status = get_post_meta($product->id, '_stock_status', true) == 'outofstock';
$_wrapper = (isset($wrapper) && $wrapper == 'li') ? 'li' : 'div';
?>

<?php if (isset($lt_opt['animated_products'])) : ?>
    <<?php echo $_wrapper . ' '; ?> class="wow fadeInUp product-item <?php echo esc_attr($lt_opt['animated_products']); ?> grid<?php echo ($stock_status == "1") ? ' out-of-stock' : ''; ?>" data-wow-duration="1s" data-wow-delay="<?php echo esc_attr($_delay); ?>ms">
<?php else: ?>
    <<?php echo $_wrapper . ' '; ?> class="product-item <?php echo esc_attr($lt_opt['animated_products']); ?> grid<?php echo ($stock_status == "1") ? ' out-of-stock' : ''; ?>" data-wow-duration="1s" data-wow-delay="<?php echo esc_attr($_delay); ?>ms">
<?php endif; ?>

<?php //do_action( 'woocommerce_before_shop_loop_item' ); ?>

<div class="inner-wrap<?php echo (isset($is_deals) && $is_deals) ? ' product-deals' : ''; ?>">
    <p class="name">
        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
    </p>
    <div class="product-img<?php echo (isset($lt_opt['product-hover-overlay']) && $lt_opt['product-hover-overlay']) ? ' hover-overlay' : ''; ?>">
        <?php
        if ($lt_opt['animated_products'] == 'slider'):
            include get_template_directory() . '/woocommerce/content-product_thumbSlider.php';
        else:
            include get_template_directory() . '/woocommerce/content-product_effect.php';
        endif;
        ?>

        <?php if ($stock_status == "1"): ?>
            <div class="badge">
                <div class="badge-inner out-of-stock-label">
                    <div class="inner-text"><?php esc_html_e('Sold out', 'techstore-theme'); ?></div>       
                </div>
            </div>
        <?php endif; ?>
        <?php wc_get_template('loop/sale-flash.php'); ?>

        <!-- Product interactions button-->
        <?php do_action('techstore_product_group_button'); ?>
    </div>

    <div class="info">
        <div class="info_main">
            <?php do_action('woocommerce_after_shop_loop_item_title'); ?>
            <div class="product-des">
                <?php echo apply_filters('woocommerce_short_description', $product->post->post_excerpt); ?>
            </div>
        </div>
        <?php if (isset($is_deals) && $is_deals): ?>
            <div class="lt-sc-pdeal-countdown">
                <span class="countdown" data-fomart="dhms" data-countdown="<?php echo esc_attr(date('M j Y H:i:s O', $time_sale)); ?>"></span>
            </div>
            <div class="lt-deal-showmore">
                <a class="button small secondary" title="<?php echo the_title(); ?>" href="<?php echo esc_url(the_permalink()); ?>"><?php esc_html_e('Show more', 'techstore-theme'); ?></a>
            </div>
        <?php endif; ?>
        <!-- Product interactions button-->
        <?php techstore_product_group_button(false); ?>
    </div>
</div>
</<?php echo $_wrapper; ?>>