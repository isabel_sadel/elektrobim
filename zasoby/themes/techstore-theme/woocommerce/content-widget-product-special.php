<?php
/**
 *
 * Override this template by copying it to yourtheme/woocommerce/content-widget-product.php
 *
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.5.0
 */
global $product, $lt_opt;
$class = ' wow fadeInUp ' . $lt_opt['animated_products'];

$tag_wapper = (isset($wapper) && $wapper == 'div') ? '' : '<li class="li_wapper">';
echo $tag_wapper;
?>

<div class="large-6 medium-6 small-12 columns item-product-widget item-product-widget-special clearfix<?php echo esc_attr($class); ?>" data-wow-duration="1s" data-wow-delay="<?php echo (int) $delay; ?>ms">
    <div class="product-item-content">
        <div class="lt-content-product-widget-custom">
            <div class="large-5 medium-6 small-5 columns images">
                <a href="<?php echo esc_url(get_permalink($product->id)); ?>" title="<?php echo esc_attr($product->get_title()); ?>">
<?php echo $product->get_image('shop_catalog'); ?>
                    <div class="overlay"></div>
                </a>

            </div>
            <div class="large-7 medium-6 small-7 columns product-meta">
                <div class="product-title separator">
                    <a href="<?php echo esc_url(get_permalink($product->id)); ?>" title="<?php echo esc_attr($product->get_title()); ?>">
                        <?php echo esc_attr($product->get_title()); ?>
                    </a>
                </div>
                <?php wc_get_template('loop/sale-flash.php'); ?>
                <?php if ($rating_html = $product->get_rating_html()) : ?>
                    <?php echo $rating_html; ?>
                <?php else : ?>
                    <div class="star-rating"></div>
                    <?php endif; ?>
                <div class="price separator lt-special-custom">
                    <?php echo $product->get_price_html(); ?>
                </div>
                <div class="lt-product-item-excerpt">
                    <?php echo apply_filters('woocommerce_short_description', $product->post->post_excerpt); ?>
                </div>

            </div>
        </div>
        <div class="info">
            <div class="lt-info-group-box">
                <div class="large-12 medium-12 small-12 columns">

                    <div class="lt-btn-showmore">
                        <a class="button small" href="<?php echo esc_url(get_permalink($product->id)); ?>"><?php echo esc_html__('Show more', 'techstore-theme'); ?></a>
                    </div>

                    <div class="lt-btn-group-custom">
<?php techstore_product_group_button(false); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
echo $tag_wapper;
