<?php
/**
 * Single Product tabs / and sections
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters('woocommerce_product_tabs', array());
global $lt_opt, $product;

$specifications = (!isset($lt_opt['enable_specifications']) || $lt_opt['enable_specifications'] == '1') ? 
    techstore_get_product_meta_value($product->id, 'lt_specifications') : '';
$specifi_desc = (!isset($lt_opt['merge_specifi_to_desc']) || $lt_opt['merge_specifi_to_desc'] == '1') ? true : false;

if (!empty($tabs)): ?>
    <div class="lt-tabs-content woocommerce-tabs">
        <ul class="lt-tabs">
            <?php foreach ($tabs as $key => $tab) : ?>
                <li class="<?php echo esc_attr($key);?>_tab lt-tab<?php echo $key == 'description' ? ' active first' : '';?>">
                    <a href="javascript:void(0);" data-id="#lt-tab-<?php echo esc_attr($key);?>">
                        <h5><?php echo apply_filters('woocommerce_product_' . $key . '_tab_title', $tab['title'], $key);?></h5>
                        <span class="bery-hr small"></span>
                    </a>
                </li>
                <li class="separator">|</li>
                <?php if($key == 'description' && (trim($specifications) != '' && !$specifi_desc)) : ?>
                    <li class="specifications_tab lt-tab">
                        <a href="javascript:void(0);" data-id="#lt-tab-specifications">
                            <h5><?php echo esc_html__('Specifications', 'techstore-theme'); ?></h5>
                            <span class="bery-hr small"></span>
                        </a>
                    </li>
                    <li class="separator">|</li>
                <?php endif;?>
            <?php endforeach; ?>
            
            <?php if($lt_opt['tab_title']): ?> 
                <li class="additional-tab lt-tab">
                    <a href="javascript:void(0);" data-id="#lt-tab-additional">
                        <h5><?php echo esc_attr($lt_opt['tab_title'])?></h5>
                        <span class="bery-hr small"></span>
                    </a>
                </li>
                <li class="separator">|</li>
            <?php endif; ?>
        </ul>
        <div class="lt-panels">
            <?php foreach ($tabs as $key => $tab) : ?>
                <div class="lt-panel entry-content<?php echo ($key == 'description') ? ' active' : ''; echo ($key == 'description' && $specifi_desc) ? ' no-border' : ''; ?>" id="lt-tab-<?php echo $key; ?>">
                    <?php if($key == 'description' && $specifi_desc):?>
                        <div class="lt-panel-block">
                            <?php call_user_func($tab['callback'], $key, $tab); ?>
                        </div>
                        <?php if(trim($specifications) != ''):?>
                            <div class="lt-panel-block lt-content-specifications">
                                <?php echo $specifications; ?>
                            </div>
                        <?php endif;?>
                    <?php else: 
                        call_user_func($tab['callback'], $key, $tab);
                    endif; ?>
                </div>
                <?php if($key == 'description' && (trim($specifications) != '') && !$specifi_desc):?>
                    <div class="lt-panel entry-content lt-content-specifications" id="lt-tab-specifications">
                        <p><?php echo $specifications;?></p>
                    </div>
                <?php endif; ?>
            <?php endforeach; ?>

            <?php if($lt_opt['tab_title']): ?>
                <div class="lt-panel entry-content" id="lt-tab-additional">
                    <?php echo do_shortcode($lt_opt['tab_content']);?>
                </div>
            <?php endif; ?>
        </div>
    </div>
<?php
endif;