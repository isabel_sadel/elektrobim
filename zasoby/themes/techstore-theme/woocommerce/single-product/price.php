<?php
/**
 * Single Product Price, including microdata for SEO
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.4.9
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

global $product;
?>
<div itemprop="offers" itemscope itemtype="http://schema.org/Offer">
	<p class="price large"><font color="#333333">Cena:</font> <?php echo $product->get_price_html(); ?></p>
	<?php echo ceil($product->get_price() / 1.23); ?> zł netto
	<br/><br/>
	<meta itemprop="priceCurrency" content="<?php echo get_woocommerce_currency(); ?>" />
	<link itemprop="availability" href="http://schema.org/<?php echo $product->is_in_stock() ? 'InStock' : 'OutOfStock'; ?>" />
</div>