<?php
// **********************************************************************//
// ! Is Active WooCommmerce
// **********************************************************************//
if (!function_exists('techstore_has_woocommerce')) {

    function techstore_has_woocommerce() {
        return (bool) class_exists('WooCommerce');
    }

}

// **********************************************************************//
// ! Tiny account
// **********************************************************************//
if (!function_exists('techstore_tiny_account')) {

    function techstore_tiny_account($icon = false) {
        $login_url = '#';
        $register_url = '#';
        $profile_url = '#';
        $logout_url = wp_logout_url(get_permalink());

        if (techstore_has_woocommerce()) { /* Active woocommerce */
            $myaccount_page_id = get_option('woocommerce_myaccount_page_id');
            if ($myaccount_page_id) {
                $login_url = get_permalink($myaccount_page_id);
                $register_url = $login_url;
                $profile_url = $login_url;
            }
        } else {
            $login_url = wp_login_url();
            $register_url = wp_registration_url();
            $profile_url = admin_url('profile.php');
        }

        $redirect_to = (is_ssl() ? 'https://' : 'http://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];

        $result = '';
        if (!is_user_logged_in()) {
            $span = $icon ? '<span class="pe7-icon pe-7s-lock"></span>' : '';
            $result .= '<li class="menu-item color"><a href="' . esc_url($login_url) . '" title="' . esc_html__('Login or Register', 'techstore-theme') . '">' . $span . esc_html__('Login or Register', 'techstore-theme') . '</a></li>';
        } else {
            $span1 = $icon ? '<span class="pe7-icon pe-7s-user"></span>' : '';
            $span2 = $icon ? '<span class="pe7-icon pe-7s-unlock"></span>' : '';
            $result .= 
                '<li class="menu-item"><a href="' . esc_url($profile_url) . '" title="' . esc_html__('My Account', 'techstore-theme') . '">' . $span1 . esc_html__('My Account', 'techstore-theme') . '</a></li>' .
                '<li class="menu-item"><a class="nav-top-link" href="' . esc_url($logout_url) . '" title="' . esc_html__('Logout', 'techstore-theme') . '">' . $span2 . esc_html__('Logout', 'techstore-theme') . '</a></li>';
        }

        return $result;
    }

}

// **********************************************************************//
// ! Mini cart header icon - 1: only show cart icon / 2: show number item, total cart price
// **********************************************************************//
if (!function_exists('techstore_mini_cart')) {

    function techstore_mini_cart($mini_cart_type = 'full', $echo = true) {
        global $woocommerce, $lt_opt;
        if (!$woocommerce || (isset($lt_opt['disable-cart']) && $lt_opt['disable-cart'])) {
            return;
        }
        $items = $price = '';
        if ($mini_cart_type == 'full') {
            $price = 
                '<span class="cart-count">' .
                    '<span class="total-price">' .
                        round($woocommerce->cart->cart_contents_total+$woocommerce->cart->tax_total, 2) . // WAŻNE
                    ' zł</span>' .
                '</span>';
        }
        $hasEmpty = $woocommerce->cart->cart_contents_count == 0 ? ' lt-product-empty' : '';
        $content = 
            '<div class="mini-cart cart-inner mini-cart-type-' . esc_attr($mini_cart_type) . ' inline-block">' .
                '<a href="javascript:void(0);" class="cart-link">' .
                    '<div>' .
                        '<span class="cart-icon icon pe-7s-cart"></span>' .
                            '<span class="products-number' . $hasEmpty . '">' .
                                '<span class="lt-sl">' .
                                    $woocommerce->cart->cart_contents_count .
                                '</span>' .
                                $items .
                            '</span>' .
                        $price .
                    '</div>' .
                '</a>' .
            '</div>';

        if (!$echo) {
            return $content;
        }

        echo $content;
    }

}

// **********************************************************************//
// ! Add to cart dropdown - Refresh mini cart content. Input from header type
// **********************************************************************//
add_filter('woocommerce_add_to_cart_fragments', 'techstore_add_to_cart_refresh');

function techstore_add_to_cart_refresh($fragments) {
    global $lt_opt;
    if (isset($_POST['head_type'])) {
        $lt_opt['header-type'] = $_POST['head_type'];
    }

    switch ($lt_opt['header-type']) {
        default:
            $mini_cart_type = 'full';
            break;
    }

    $fragments['.cart-inner'] = techstore_mini_cart($mini_cart_type, false);
    $fragments['div.widget_shopping_cart_content'] = techstore_mini_cart_sidebar(true);

    return $fragments;
}

// **********************************************************************//
// ! Mini cart sidebar
// **********************************************************************//
if (!function_exists('techstore_mini_cart_sidebar')) {

    function techstore_mini_cart_sidebar($str = false) {
        global $woocommerce, $lt_opt;
        if (!$woocommerce || (isset($lt_opt['disable-cart']) && $lt_opt['disable-cart'])){
            return;
        }
        ob_start();
        $empty = '<p class="empty">' . esc_html__('No products in the cart.', 'techstore-theme') . '</p>';
        
        $file = get_stylesheet_directory() . '/headers/includes/mini-cart-sidebar.php';
        require is_file($file) ? $file : get_template_directory() . '/headers/includes/mini-cart-sidebar.php';
        
        $content = ob_get_clean();
        if ($str) {
            return $content;
        }
        echo '<div class="empty hidden-tag">' . $empty . '</div>';
        echo $content;
    }

}


// **********************************************************************//
// ! Catalog mode - None e-Commerce
// **********************************************************************//
if (isset($lt_opt['disable-cart']) && $lt_opt['disable-cart']) {
    remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
    remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30);
}

// **********************************************************************//
// ! Add to cart button
// **********************************************************************//
function techstore_add_to_cart_btn($type = 'small', $echo = true, $tiptop = true) {
    global $product, $wp_query, $lt_opt;
    $head_type = $lt_opt['header-type'];
    if (isset($product->id)) {
        $custom_header = get_post_meta($wp_query->get_queried_object_id(), '_lee_custom_header', true);
        if (!empty($custom_header)) {
            $head_type = (int) $custom_header;
        }
    }

    $result = '';
    $title = esc_html($product->add_to_cart_text()); //add to cart text;
    switch ($type) {
        case 'large':
            $result .= apply_filters(
                'woocommerce_loop_add_to_cart_link',
                sprintf(
                    '<div class="add-to-cart-btn">' .
                        '<a href="%s" rel="nofollow" data-product_id="%s" class="%s button small product_type_%s add-to-cart-grid" data-head_type="%s" title="%s">' .
                            '<span class="add_to_cart_text">%s</span>' .
                            '<span class="cart-icon-handle"></span>' .
                        '</a>' .
                    '</div>',
                    ($product->product_type != 'simple') ? esc_url($product->add_to_cart_url()) : 'javascript:void(0);', //link
                    esc_attr($product->id), //product id
                    ($product->is_purchasable() && $product->is_in_stock() && $product->product_type == 'simple') ?
                        'ajax_add_to_cart add_to_cart_button' : '', //class name
                    esc_attr($product->product_type), //product type
                    esc_html($head_type), $title, $title
                ),
                $product
            );
            break;
        case 'small':
        default:
            $classTip = $tiptop ? ' tip-top' : '';
            $result .= apply_filters(
                'woocommerce_loop_add_to_cart_link',
                sprintf(
                    '<div class="' . esc_attr($classTip) . '" data-tip="%s">' . // WAŻNE PRZYCISK DOPDAWANIA DO KOSZYKA
                        '<div class="">' .
                            '<button style="margin-right:13px;" data-href="%s" rel="nofollow" data-quantity="1" data-product_id="%s" data-product_sku="%s" class="%s product_type_%s" data-head_type="%s" title="%s">' .
                                '<span class="add_to_cart_text">%s</span>' .
                                '<span class="cart-icon-handle"></span>' .
                            '</button>' .
                        '</div>' .
                    '</div>',
                    $title, //data tip
                    ($product->product_type != 'simple') ? esc_url($product->add_to_cart_url()) : 'javascript:void(0);', //link
                    esc_attr($product->id), //product id
                    esc_attr($product->get_sku()), //product sku
                    ($product->is_purchasable() && $product->is_in_stock() && $product->product_type == 'simple') ?
                        'ajax_add_to_cart add_to_cart_button' : '', //class name
                    esc_attr($product->product_type), //product type
                    esc_html($head_type), $title, $title
                ),
                $product
            );
            break;
    }

    if (!$echo) {
        return $result;
    }
    echo $result;
}

// Product group button
function techstore_product_group_button($toltip = true) {
    global $lt_opt, $product, $wp_query;

    $head_type = $lt_opt['header-type'];
    if (isset($product->id)) {
        $custom_header = get_post_meta($wp_query->get_queried_object_id(), '_lee_custom_header', true);
        if (!empty($custom_header)) {
            $head_type = $custom_header;
        }
    }

    $_cart_btn = techstore_add_to_cart_btn('small', false, $toltip);
    
    $file = get_stylesheet_directory() . '/woocommerce/customs/product-group-button.php';
    include is_file($file) ? $file : get_template_directory() . '/woocommerce/customs/product-group-button.php';
}

add_action('techstore_product_group_button', 'techstore_product_group_button');

// **********************************************************************//
// ! Wishlist link
// **********************************************************************//
function techstore_tini_wishlist($icon = false) {
    if (!techstore_has_woocommerce() || !defined('YITH_WCWL')) {
        return;
    }

    $tini_wishlist = '';
    $wishlist_page_id = get_option('yith_wcwl_wishlist_page_id');
    if (function_exists('icl_object_id')) {
        $wishlist_page_id = icl_object_id($wishlist_page_id, 'page', true);
    }
    $wishlist_page = get_permalink($wishlist_page_id);

    $span = $icon ? '<span class="pe7-icon pe-7s-like"></span>' : '';
    $tini_wishlist .= '<a href="' . esc_url($wishlist_page) . '" title="' . esc_html__('Wishlist', 'techstore-theme') . '">' . $span . esc_html__('Wishlist', 'techstore-theme') . '</a>';

    return $tini_wishlist;
}

// **********************************************************************//
// ! Wishlist link
// **********************************************************************//
if (!function_exists('techstore_icon_wishlist')):

    function techstore_icon_wishlist($echo = false) {
        if (!techstore_has_woocommerce() || !defined('YITH_WCWL')) {
            return;
        }

        $tini_wishlist = '';
        $wishlist_page_id = get_option('yith_wcwl_wishlist_page_id');
        if (function_exists('icl_object_id')) {
            $wishlist_page_id = icl_object_id($wishlist_page_id, 'page', true);
        }
        $wishlist_page = get_permalink($wishlist_page_id);
        $count = yith_wcwl_count_products(); // count wishlist
        $hasEmpty = $count == 0 ? ' lt-product-empty' : '';
        $tini_wishlist .= '<a href="' . esc_url($wishlist_page) . '" title="' . esc_html__('Wishlist', 'techstore-theme') . '"><i class="pe7-icon pe-7s-like"></i><span class="wishlist-number' . $hasEmpty . '"><span class="lt-sl">' . (int) $count . '</span></span></a>';

        if (!$echo) {
            return $tini_wishlist;
        }

        echo $tini_wishlist;
    }

endif;

// **********************************************************************//
// ! Compare link
// **********************************************************************//
if (!function_exists('techstore_icon_compare')):

    function techstore_icon_compare() {
        if (!techstore_has_woocommerce() || !defined('YITH_WOOCOMPARE')) {
            return;
        }

        global $yith_woocompare;
        $count = count($yith_woocompare->obj->products_list);
        $hasEmpty = $count == 0 ? ' lt-product-empty' : '';
        return '<span class="yith-woocompare-widget"><a href="' . add_query_arg(array('iframe' => 'true'), $yith_woocompare->obj->view_table_url()) . '" class="compare"><i class="pe7-icon pe-7s-repeat"></i><span class="compare-number' . $hasEmpty . '"><span class="lt-sl">' . $count . '</span></span></a></span>';
    }

endif;

if (!function_exists('techstore_get_cat_header')):

    function techstore_get_cat_header($catId = null) {
        global $lt_opt;
        if (isset($lt_opt['enable_cat_header']) && $lt_opt['enable_cat_header'] != '1') {
            return '';
        }

        $content = '<div class="cat-header lt-cat-header">';
        $do_content = '';
        if ((int) $catId > 0) {
            $shortcode = get_woocommerce_term_meta($catId, 'cat_header', false);
            $do_content = isset($shortcode[0]) ? do_shortcode($shortcode[0]) : '';
        }

        if (trim($do_content) === '') {
            if (isset($lt_opt['cat_header']) && $lt_opt['cat_header'] != '') {
                $do_content .= do_shortcode($lt_opt['cat_header']);
            }
        }

        if (trim($do_content) === '') {
            return '';
        }

        $content .= $do_content . '</div>';

        return $content;
    }

endif;

if (!function_exists('techstore_get_recommend_product')):

    function techstore_get_recommend_product($catId = null) {
        global $lt_opt, $woocommerce;

        if (!$woocommerce || (isset($lt_opt['enable_recommend_product']) && $lt_opt['enable_recommend_product'] != '1')) {
            return '';
        }

        $columns_number = isset($lt_opt['products_per_row']) ? (int) $lt_opt['products_per_row'] : 3;
        $columns_number = ($lt_opt['category_sidebar'] == "no-sidebar") ? $columns_number + 1 : $columns_number;

        $columns_number_small = 1;
        $columns_number_tablet = ($columns_number < 3) ? $columns_number : 3;

        $number = (isset($lt_opt['recommend_product_limit']) && ((int) $lt_opt['recommend_product_limit'] >= $columns_number)) ? (int) $lt_opt['recommend_product_limit'] : 9;

        switch ($columns_number) {
            case '5':
                $class_column = 'large-block-grid-5 medium-block-grid-3 small-block-grid-2';
                break;
            case '4':
                $class_column = 'large-block-grid-4 medium-block-grid-3 small-block-grid-2';
                break;
            case '3':
                $class_column = 'large-block-grid-3 small-block-grid-2';
                break;
            case '2':
                $class_column = 'large-block-grid-2 small-block-grid-2';
                break;
            default:
                $class_column = 'large-block-grid-1 small-block-grid-1';
                break;
        }

        $catId = (int) $catId ? (int) $catId : null;
        $loop = !function_exists('lt_woocommerce_query') ? // In Lee Framework
            techstore_woocommerce_query_theme('featured_product', $number, $catId) : lt_woocommerce_query('featured_product', $number, $catId);

        $content = '';
        if ($loop->found_posts) {
            ob_start();
            ?>
            <div class="row margin-bottom-50 lt-recommend-product">
                <div class="large-12 columns">
                    <h5 class="lt-title clearfix title-style-2"><?php echo esc_html__('Recommend Products', 'techstore-theme'); ?></h5>
                    <div class="woocommerce">
                        <div class="inner-content">
                            <?php
                            wc_get_template('lee-product-layout/carousel.php', array(
                                'loop' => $loop,
                                'columns_number' => $columns_number,
                                'columns_number_small' => $columns_number_small,
                                'columns_number_tablet' => $columns_number_tablet,
                                'is_deals' => false,
                                'lt_opt' => $lt_opt,
                                'data_margin' => '0'
                            ));
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
            $content = ob_get_clean();
        }

        return $content;
    }

endif;

function techstore_woocommerce_query_theme($type, $post_per_page = -1, $cat = '', $paged = '') {
    global $woocommerce;
    if (!$woocommerce) {
        return array();
    }
    remove_filter('posts_clauses', array($woocommerce->query, 'order_by_popularity_post_clauses'));
    remove_filter('posts_clauses', array($woocommerce->query, 'order_by_rating_post_clauses'));

    if ($paged == '') {
        $paged = ($paged = get_query_var('paged')) ? $paged : 1;
    }
    $args = array(
        'post_type' => 'product',
        'posts_per_page' => $post_per_page,
        'post_status' => 'publish',
        'paged' => $paged
    );
    switch ($type) {
        case 'best_selling':
            $args['meta_key'] = 'total_sales';
            $args['orderby'] = 'meta_value_num';
            $args['ignore_sticky_posts'] = 1;
            $args['meta_query'] = array();
            $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
            $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
            break;
        case 'featured_product':
            $args['ignore_sticky_posts'] = 1;
            $args['meta_query'] = array();
            $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
            $args['meta_query'][] = array(
                'key' => '_featured',
                'value' => 'yes'
            );
            $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
            break;
        case 'top_rate':
            add_filter('posts_clauses', array($woocommerce->query, 'order_by_rating_post_clauses'));
            $args['meta_query'] = array();
            $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
            $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
            break;
        case 'recent_product':
            $args['meta_query'] = array();
            $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
            break;
        case 'on_sale':
            $args['meta_query'] = array();
            $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
            $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
            $args['post__in'] = wc_get_product_ids_on_sale();
            break;
        case 'recent_review':
            $_limit = ($post_per_page == -1) ? 4 : $post_per_page;
            global $wpdb;
            $query = 'SELECT c.comment_post_ID FROM ' . $wpdb->prefix . 'posts p, ' . $wpdb->prefix . 'comments c WHERE p.ID = c.comment_post_ID AND c.comment_approved > 0 AND p.post_type = %s AND p.post_status = %s AND p.comment_count > %s ORDER BY c.comment_date ASC LIMIT 0, ' . (int) $_limit;
            $results = $wpdb->get_results($wpdb->prepare($query, 'product', 'publish', '0', OBJECT));
            $_pids = array();
            foreach ($results as $v) {
                $_pids[] = $v->comment_post_ID;
            }

            $args['meta_query'] = array();
            $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
            $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
            $args['post__in'] = $_pids;
            break;
        case 'deals':
            $args['meta_query'] = array();
            $args['meta_query'][] = $woocommerce->query->stock_status_meta_query();
            $args['meta_query'][] = $woocommerce->query->visibility_meta_query();
            $args['meta_query'][] = array(
                'key' => '_sale_price_dates_to',
                'value' => '0',
                'compare' => '>'
            );
            $args['post__in'] = wc_get_product_ids_on_sale();
            break;
    }

    if (is_numeric($cat) && $cat) {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'id',
                'terms' => array($cat)
            )
        );
    }

    // Find by slug
    elseif ($cat != '') {
        $args['tax_query'] = array(
            array(
                'taxonomy' => 'product_cat',
                'field' => 'slug',
                'terms' => $cat
            )
        );
    }

    return new WP_Query($args);
}

if (!function_exists('techstore_get_product_meta_value')):

    function techstore_get_product_meta_value($post_id, $field_id = null) {
        $meta_value = get_post_meta($post_id, 'wc_productdata_options', true);
        if (isset($meta_value[0]) && $field_id) {
            return isset($meta_value[0][$field_id]) ? $meta_value[0][$field_id] : '';
        }

        return isset($meta_value[0]) ? $meta_value[0] : $meta_value;
    }

endif;

if (!function_exists('techstore_search_by_cat')):

    function techstore_search_by_cat($echo = true) {
        global $lt_opt;
        
        $select = '';
        if(isset($lt_opt['search_by_cat']) && $lt_opt['search_by_cat']){
            $slug = get_query_var('product_cat');
            $lt_catActive = $slug ? $slug : '';
            $lt_terms = get_terms(array(
                'taxonomy' => 'product_cat',
                'hide_empty' => false,
            ));
            
            if($lt_terms) {
                $select .= '<select name="product_cat">';
                foreach ($lt_terms as $v) {
                    $select .= '<option value="' . esc_attr($v->slug) . '"' . (($lt_catActive == $v->slug) ? ' selected' : '') . '>' . esc_attr($v->name) . '</option>';
                }
                $select .= '</select>';
            }
        }
        
        if($echo){
            echo $select;
        } else {
            return $select;
        }
    }

endif;

// Get term description
if(!function_exists('techstore_term_description')) :
    function techstore_term_description($term_id, $type_taxonomy) {
        if(!techstore_has_woocommerce()) {
            return '';
        }
        
        if((int) $term_id < 1) {
            $shop_page = get_post(wc_get_page_id('shop'));
            $desc = $shop_page ? wc_format_content($shop_page->post_content) : '';
        } else {
            $term = get_term($term_id, $type_taxonomy);
            $desc = isset($term->description) ? $term->description : '';
        }
        
        return trim($desc) != '' ? '<div class="page-description">' . do_shortcode($desc) . '</div>' : '';
    }
endif;