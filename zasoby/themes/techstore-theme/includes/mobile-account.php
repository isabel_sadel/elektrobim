<div class="heading-account">
    <i class="fa fa-user"></i>
    <?php echo esc_html__('Account', 'techstore-theme'); ?>
    <hr />
</div>
<div class="content-account">
    <?php
    if (is_user_logged_in()) :
        echo '<a href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '" title="' . esc_html__('My Account', 'techstore-theme') . '"><span class="pe-7s-user"></span> ' . esc_html__('My Account', 'techstore-theme') . '</a>';
        echo '<a class="nav-top-link" href="' . wp_logout_url() . '" title="' . esc_html__('Log Out', 'techstore-theme') . '"><span class="pe-7s-unlock"></span> ' . esc_html__('Logout', 'techstore-theme') . '</a>';
    else :
        echo '<a class="center" href="' . get_permalink(get_option('woocommerce_myaccount_page_id')) . '" title=""><span class="pe-7s-lock"></span> ' . esc_html__('Login or Register', 'techstore-theme') . '</a>';
    endif;
    ?>
</div>