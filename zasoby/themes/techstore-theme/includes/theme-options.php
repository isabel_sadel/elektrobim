<?php

// **********************************************************************// 
// ! Header Type
// **********************************************************************//
add_filter('custom_header_filter', 'techstore_get_header_type', 10);
function techstore_get_header_type() {
    global $lt_opt;
    
    return isset($lt_opt['header-type']) ? $lt_opt['header-type'] : '';
}

add_action('techstore_get_header_theme', 'techstore_get_header_theme');
function techstore_get_header_theme() {
    global $woocommerce, $woo_options, $lt_opt, $post, $wp_query;

    include_once get_template_directory() . '/headers/header-main.php';
}

add_action('techstore_get_footer_theme', 'techstore_get_footer_theme');
function techstore_get_footer_theme() {
    global $lt_opt;

    include_once get_template_directory() . '/footers/footer-main.php';
}

// **********************************************************************// 
// ! Footer Type
// **********************************************************************//
add_action('techstore_footer_layout_style', 'techstore_footer_layout_style_function');
function techstore_footer_layout_style_function() {
    global $lt_opt, $wp_query;
    $pageid = $wp_query->get_queried_object_id();
    $footer_id = get_post_meta($pageid, '_lee_custom_footer', true);

    if ($footer_id == '' || $footer_id == 'default') {
        if (isset($lt_opt['footer-type']) && $lt_opt['footer-type'] != '') {
            $footer_id = $lt_opt['footer-type'];
        }
    }

    if (class_exists('SitePress') && (int) $footer_id) {
        $footer_id = apply_filters('wpml_object_id', $footer_id, 'product', true);
    }

    if ((int) $footer_id) {
        $post_obj = get_post($footer_id);
        if (isset($post_obj->post_content)) {
            echo do_shortcode($post_obj->post_content);
            return;
        }
    }

    get_template_part('templates/footer/default'); // Default footer
}

// **********************************************************************// 
// ! Remove message Woocommerce
// **********************************************************************//
add_action('admin_head', 'techstore_remove_upgrade_nag');
function techstore_remove_upgrade_nag() {
    global $lt_opt;
    echo (isset($lt_opt['plugin_update_notice']) && $lt_opt['plugin_update_notice']) ? 
        '<style type="text/css">' .
            '.woocommerce-message.updated, .plugin-update-tr, .rs-update-notice-wrap {display: none}' .
        '</style>' : '';
}

// **********************************************************************// 
// ! Escape HTML in post and comments
// **********************************************************************// 
// Escape HTML tags in post content
add_filter('the_content', 'techstore_escape_code_fragments');

// Escape HTML tags in comments
add_filter('pre_comment_content', 'techstore_escape_code_fragments');

function techstore_escape_code_fragments($source) {
    $encoded = preg_replace_callback(
        '/<script(.*?)>(.*?)<\/script>/ims', create_function(
            '$matches', '$matches[2] = preg_replace(array("/^[\r|\n]+/i", "/[\r|\n]+$/i"), "", $matches[2]);
            return "<pre" . $matches[1] . ">" . esc_html( $matches[2] ) . "</pre>";'
        ), $source
    );

    return $encoded ? $encoded : $source;
}

// **********************************************************************// 
// ! Remove Plugin update
// **********************************************************************// 
//add_action('admin_menu','techstore_wphidenag');
function techstore_wphidenag() {
    remove_action('admin_notices', 'update_nag', 3);
    remove_filter('update_footer', 'core_update_footer');
}

// **********************************************************************// 
// ! Filter add  property='stylesheet' to the wp enqueue style
// **********************************************************************//
add_filter('style_loader_tag', 'techstore_mycustom_wpenqueue');
function techstore_mycustom_wpenqueue($src) {
    return str_replace("rel='stylesheet'", "rel='stylesheet' property='stylesheet'", $src);
}

// **********************************************************************// 
// ! Add Font Awesome, Font Pe7s, Font Elegant
// **********************************************************************//
add_action('wp_enqueue_scripts', 'techstore_add_font_awesome');
function techstore_add_font_awesome() {
    wp_register_style('techstore-font-awesome-style', get_template_directory_uri() . '/css/font-awesome-4.2.0/css/font-awesome.min.css');
    wp_enqueue_style('techstore-font-awesome-style');
}

add_action('wp_enqueue_scripts', 'techstore_add_font_pe7s');
function techstore_add_font_pe7s() {
    wp_register_style('techstore-font-pe7s-style', get_template_directory_uri() . '/css/pe-icon-7-stroke/css/pe-icon-7-stroke.css');
    wp_register_style('techstore-font-pe7s-helper-style', get_template_directory_uri() . '/css/pe-icon-7-stroke/css/helper.css');
    wp_enqueue_style('techstore-font-pe7s-style');
    wp_enqueue_style('techstore-font-pe7s-helper-style');
}

add_action('wp_enqueue_scripts', 'techstore_add_font_flaticon');
function techstore_add_font_flaticon() {
    wp_register_style('techstore-font-flaticon', get_template_directory_uri() . '/css/font-flaticon/flaticon.css');
    wp_enqueue_style('techstore-font-flaticon');
}

// **********************************************************************// 
// ! Other functions
// **********************************************************************// 
add_filter('attachment_link', 'techstore_enhanced_image_navigation', 10, 2);
function techstore_enhanced_image_navigation($url, $id) {
    if (!is_attachment() && !wp_attachment_is_image($id)){
        return $url;
    }
    
    $image = get_post($id);
    $url .= (!empty($image->post_parent) && $image->post_parent != $id) ? '#main' : '';

    return $url;
}

if (function_exists('get_term_meta')) {

    function techstore_taxonomy_edit_meta_cat_header($term) {
        $t_id = $term->term_id;
        if (!$cat_header = get_term_meta($t_id, 'cat_header')) {
            $cat_header = add_term_meta($t_id, 'cat_header', '');
        }
        ?>
        <tr class="form-field">
            <th scope="row" valign="top">
                <label for="cat_header"><?php esc_html_e('Top Content', 'techstore-theme'); ?></label>
            </th>
            <td>             
                <?php
                $content = isset($cat_header[0]) ? $cat_header[0] : '';
                echo '<textarea id="cat_header" name="cat_header">' . $content . '</textarea>';
                ?>
                <p class="description"><?php esc_html_e('Enter a value for this field. Shortcodes are allowed. This will be displayed at top of the category.', 'techstore-theme'); ?></p>
            </td>
        </tr>
        <?php
    }

    add_action('product_cat_edit_form_fields', 'techstore_taxonomy_edit_meta_cat_header', 10, 2);

    function techstore_save_taxonomy_custom_cat_header($term_id) {
        $term_meta = isset($_POST['cat_header']) ? $_POST['cat_header'] : '';
        update_term_meta($term_id, 'cat_header', $term_meta);
    }

    add_action('edited_product_cat', 'techstore_save_taxonomy_custom_cat_header', 10, 2);
}

if (!is_home()) {

    function techstore_share_meta_head() {
        global $post;
        ?>
        <meta property="og:title" content="<?php the_title(); ?>" />
        <?php if (isset($post->ID)) { ?>
            <?php if (has_post_thumbnail($post->ID)): ?>
                <?php $image = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'single-post-thumbnail'); ?>
                <meta property="og:image" content="<?php echo $image[0]; ?>" />
            <?php endif; ?>
        <?php } ?>
        <meta property="og:url" content="<?php the_permalink(); ?>" />
        <?php
    }

    add_action('wp_head', 'techstore_share_meta_head');
}

function techstore_short_excerpt($limit) {
    $excerpt = explode(' ', get_the_excerpt(), $limit);
    $count = count($excerpt);
    if ($count >= $limit) {
        array_pop($excerpt);
        $excerpt = implode(" ", $excerpt) . '...';
    } else {
        $excerpt = implode(" ", $excerpt);
    }
    $excerpt = preg_replace('`\[[^\]]*\]`', '', $excerpt);
    return $excerpt;
}

function techstore_content($limit) {
    $content = explode(' ', get_the_content(), $limit);
    $count = count($content);
    if ($count >= $limit) {
        array_pop($content);
        $content = implode(" ", $content) . '...';
    } else {
        $content = implode(" ", $content);
    }
    $content = preg_replace('/\[.+\]/', '', $content);
    $content = apply_filters('the_content', $content);
    $content = str_replace(']]>', ']]&gt;', $content);
    return $content;
}

function techstore_hex2rgba($color, $opacity = false) {
    $default = 'rgb(0,0,0)';
    if (empty($color)) {
        return $default;
    }
    if ($color[0] == '#') {
        $color = substr($color, 1);
    }

    if (strlen($color) == 6) {
        $hex = array($color[0] . $color[1], $color[2] . $color[3], $color[4] . $color[5]);
    } elseif (strlen($color) == 3) {
        $hex = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
    } else {
        return $default;
    }

    $rgb = array_map('hexdec', $hex);

    if ($opacity) {
        if (abs($opacity) > 1)
            $opacity = '1.0';
        $output = 'rgba(' . implode(",", $rgb) . ',' . $opacity . ')';
    } else {
        $output = 'rgb(' . implode(",", $rgb) . ')';
    }

    return $output;
}

add_filter('sod_ajax_layered_nav_product_container', 'techstore_bery_product_container');
function techstore_bery_product_container($product_container) {
    return 'ul.products';
}
