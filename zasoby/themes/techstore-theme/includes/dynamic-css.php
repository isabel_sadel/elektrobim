<?php

function techstore_custom_css() {
    global $lt_opt, $wp_query;

    $flag_override_color = get_post_meta($wp_query->get_queried_object_id(), '_lee_pri_color_flag', true);
    if ($flag_override_color) {
        $lt_override_color_primary = get_post_meta($wp_query->get_queried_object_id(), '_lee_pri_color', true);
        $lt_opt['color_primary'] = $lt_override_color_primary ? $lt_override_color_primary : $lt_opt['color_primary'];
    }
    ?>

    <style type="text/css">
    <?php if (isset($lt_opt['type_texts'])) { ?>
            .top-bar-nav a.nav-top-link,
            body,
            p,
            #top-bar,
            /*.cart-inner .nav-dropdown,*/
            .nav-dropdown {
                font-family: <?php echo esc_attr($lt_opt['type_texts']); ?>, helvetica, arial, sans-serif!important;
            }
    <?php } ?>
        
    <?php if (isset($lt_opt['max_height_logo']) && (int) $lt_opt['max_height_logo']) { ?>
            #masthead .header-container .logo-wrapper .logo a img,
            .fixed-header .logo-wrapper .logo a img
            {
                max-height: <?php echo (int) $lt_opt['max_height_logo'] . 'px'; ?>
            }
    <?php } ?>
        
    <?php if (isset($lt_opt['type_nav'])) { ?>
            .cart-count,
            .mini-cart .nav-dropdown-inner,
            .price .amount,
            .megatop > a,
            .header-nav li.root-item > a,
            .lt-tabs .lt-tab a,
            #vertical-menu-wrapper li.root-item > a
            {
                /*font-family: <?php echo esc_attr($lt_opt['type_nav']); ?>,helvetica,arial,sans-serif!important;*/
            }
    <?php } ?>

    <?php if (isset($lt_opt['type_headings'])) { ?>
            .product-title,
            .service-title,
            h1,h2,h3,h4,h5,h6{
                font-family: <?php echo esc_attr($lt_opt['type_headings']); ?>,helvetica,arial,sans-serif!important;
            }
    <?php } ?>

    <?php if (isset($lt_opt['type_alt'])) { ?>
            .alt-font{
                font-family: <?php echo esc_attr($lt_opt['type_alt']); ?>,Georgia,serif!important;
            }
    <?php } ?>
    <?php if (isset($lt_opt['type_banner'])) { ?>
            .banner .banner-content .banner-inner h1,
            .banner .banner-content .banner-inner h2,
            .banner .banner-content .banner-inner h3,
            .banner .banner-content .banner-inner h4,
            .banner .banner-content .banner-inner h5,
            .banner .banner-content .banner-inner h6
            {
                font-family: <?php echo esc_attr($lt_opt['type_banner']); ?>, helvetica,arial,sans-serif!important;
                letter-spacing: 0px;
                font-weight: normal !important;
            }
    <?php } ?>

    <?php if ($lt_opt['site_layout'] == 'boxed') { ?> 
            body.boxed,
            body {
                background-color: <?php echo esc_attr($lt_opt['site_bg_color']); ?>;
                background-image: url("<?php echo esc_url($lt_opt['site_bg_image']); ?>");
                background-attachment: fixed;
            }
    <?php } ?>

        /* COLOR PRIMARY */
    <?php if (isset($lt_opt['color_primary'])) { ?>
            /* COLOR */
            a:hover,
            a:focus,
            .add-to-cart-grid .cart-icon strong,
            .navigation-paging a,
            .navigation-image a,
            #header-outer-wrap .mobile-menu a,
            .logo a,
            li.mini-cart .cart-icon strong,
        		
            #header-outer-wrap .mobile-menu a.mobile-menu a,
            .checkout-group h3,
            .order-review h3,
            #yith-searchsubmit .icon-search,
            .mini-cart-item .cart_list_product_price,
            .remove:hover i,
        		
            .support-icon,
            .entry-meta a,
            #order_review_heading,
            .checkout-group h3,
            .shop_table.cart td.product-name a,
            a.shipping-calculator-button,
            .widget_layered_nav li a:hover,
            .widget_layered_nav_filters li a:hover,
            .product_list_widget .text-info span,
            .left-text i,
            .copyright-footer span,
            #menu-shop-by-category li.active.menu-parent-item .nav-top-link::after,
            .product_list_widget .product-title:hover,
            .item-product-widget .product-meta .product-title a:hover,
        		
        		
            .bread.lt-breadcrumb-has-bg .row .breadcrumb-row a:hover,
            .bread.lt-breadcrumb-has-bg .columns .breadcrumb-row a:hover,
            .group-blogs .blog_info .post-date span,
            .header-type-3 ul.header-nav li a.nav-top-link:hover,
            .widget_layered_nav li:hover a,
            .wpb_wrapper .contact-information li div i,
            .widget_layered_nav_filters li:hover a,
            .remove .icon-close:hover,
            .absolute-footer .left .copyright-footer span,
            .service-block .box .title .icon,
            .contact-information .contact-text strong,
            .nav-wrapper .header-nav li.root-item a:hover,
            .group-blogs .blog_info .read_more a:hover,
            .mini-cart .nav-dropdown .cart_list .mini-cart-item .mini-cart-info .cart_list_product_quantity,
            #top-bar .top-bar-nav li.color a,
            .mini-cart.mini-cart-type-full .cart-link .cart-icon:hover:before,
            .mini-cart-type-simple .cart-link .cart-icon:hover:before,
            .absolute-footer li a:hover,
            .lt-recent-posts li .post-date,
            .lt-recent-posts .read-more a,
            .widget.woocommerce li a:hover,
            .widget.woocommerce li.current-cat a,
            .team-member .member-details h3,
            .shop_table .remove-product .icon-close:hover,
            .product-interactions .btn-link:hover,
            .absolute-footer ul.menu li a:hover,
            .vertical-menu h4.section-title::before,
            .lt-instagram .username-text span,
            .bread .breadcrumb-row h3 a:hover,
            .bread .breadcrumb-row h3 .current,
            .lt-pagination.style-1 .page-number li span.current, .lt-pagination.style-1 .page-number li a.current, .lt-pagination.style-1 .page-number li a.lt-current, .lt-pagination.style-1 .page-number li a:hover,
        		
            #vertical-menu-wrapper li.root-item:hover > a,
            .widget.woocommerce li.cat-item a.lt-active,
            .widget.widget_categories li a.lt-active,
            .widget.widget_archive li a.lt-active,
            .lt-filter-by-cat.lt-active,
            .product-info .stock.in-stock,
            .header-type-6 .lt-bg-dark.lt-menu-transparent .nav-wrapper .header-nav .root-item:hover > a,
            .header-type-2 .lt-bg-dark .lt-vertical-header .vertical-menu-container .vertical-menu-wrapper li.root-item:hover > a,
            .header-type-2 .lt-bg-dark .lt-vertical-header .vertical-menu-container .vertical-menu-wrapper li.root-item:hover > a > i,
            .category-page .sort-bar .select-wrapper select,
            .category-page .sort-bar .select-wrapper select option,
            #lt-footer .lt-contact-footer-custom h5,
            #lt-footer .lt-contact-footer-custom h5 i,
            .group-blogs .lt-blog-info-slider .lt-post-date,
            li.menu-item.lt-megamenu > .nav-dropdown > .div-sub > ul > li.menu-item a:hover,
            .lt-tag-cloud a.lt-active:hover,
            .html-text i {
                color: <?php echo esc_attr($lt_opt['color_primary']) ?>;
            }
            ul.header-nav li.active a.nav-top-link,
            ul li .nav-dropdown > ul > li:hover > a,
            ul li .nav-dropdown > ul > li:hover > a::before,
            ul li .nav-dropdown > ul > li .nav-column-links > ul > li a:hover,
            ul li .nav-dropdown > ul > li .nav-column-links > ul > li:hover > a::before {
                color: <?php echo esc_attr($lt_opt['color_primary']) ?>;
            }
        		
            .blog_shortcode_item .blog_shortcode_text h3 a:hover,
            .main-navigation li.menu-item a:hover,
            .widget-area ul li a:hover,
            h1.entry-title a:hover,
            .comments-area a,
            .progress-bar .bar-meter .bar-number,
            .product-item .info .name a:hover,
            .wishlist_table td.product-name a:hover,
            .product_list_widget .text-info a:hover,
            .product-list .info .name:hover,
            .primary-color,
            .product-info .compare:hover, .product-info .compare:hover:before,
            .product-info .yith-wcwl-add-to-wishlist:hover:before, .product-info .yith-wcwl-add-to-wishlist:hover a, .product-info .yith-wcwl-add-to-wishlist:hover .feedback,
            li.menu-item.lt-megamenu > .nav-dropdown > .div-sub > ul > li.menu-item a:hover:before,
            #lt-footer .widget_tag_cloud .lt-tag-cloud a:hover, #lt-footer .widget_tag_cloud .lt-tag-cloud a.lt-active, #lt-footer .widget_tag_cloud .lt-tag-products-cloud a:hover, #lt-footer .widget_tag_cloud .lt-tag-products-cloud a.lt-active,
            .service-block:hover .box .service-icon,
            .service-block:hover .box .service-text .service-title,
            /*.countdown .countdown-row .countdown-amount,
            .columns.lt-column-custom-4 .lt-sc-p-deal-countdown .countdown-row.countdown-show4 .countdown-section .countdown-amount,*/
            ul.main-navigation li .nav-dropdown > ul > li .nav-column-links > ul > li a:hover {
                color: <?php echo esc_attr($lt_opt['color_primary']) ?> !important;
            }
        		
            /* BACKGROUND */
            .tabbed-content.pos_pills ul.tabs li.active a,
            li.featured-item.style_2:hover a,
            .bery_hotspot,
            .label-new.menu-item a:after,
            .text-box-primary,
            .navigation-paging a:hover,
            .navigation-image a:hover,
            .next-prev-nav .prod-dropdown > a:hover,
            .widget_product_tag_cloud a:hover,
        		
            .lt-tag-cloud a.lt-active,
            .custom-cart-count,
            a.button.trans-button:hover,
            .please-wait i,
            li.mini-cart .cart-icon .cart-count,
            .product-img .product-bg,
            #submit,
            button,
            #submit,
            button,
            input[type="submit"],
            .post-item:hover .post-date,
            .blog_shortcode_item:hover .post-date,
            .group-slider .sliderNav a:hover,
            .support-icon.square-round:hover,
            .entry-header .post-date-wrapper,
            .entry-header .post-date-wrapper:hover,
            .comment-inner .reply a:hover,
            .social-icons .icon.icon_email:hover,
            .widget_collapscat h3,
            ul.header-nav li a.nav-top-link::before,
            .sliderNav a span:hover,
            .shop-by-category h3.section-title,
            ul.header-nav li a.nav-top-link::before,
            .custom-footer-1 .bery-hr,
        		
            .products.list .product-interactions .yith-wcwl-add-button:hover,
            ul.header-nav li a.nav-top-link::before,
            .widget_collapscat h2,
            .shop-by-category h2.widgettitle,
            .rev_slider_wrapper .type-label-2,
            .bery-hr.primary-color,
            .products.list .product-interactions .yith-wcwl-add-button:hover,
            .pagination-centered .page-numbers a:hover, .pagination-centered .page-numbers span.current,
            .cart-wishlist .mini-cart .cart-link .cart-icon .products-number,
            .load-more::before,
        		
            .owl-carousel .owl-nav div:hover,
            .tp-bullets .tp-bullet:hover,
            .tp-bullets .tp-bullet.selected,
            .products-arrow .next-prev-buttons .icon-next-prev:hover,
            .widget_price_filter .ui-slider .ui-slider-handle:after,
            #under-top-bar .header-icons li .products-number .lt-sl, #under-top-bar .header-icons li .wishlist-number .lt-sl, #under-top-bar .header-icons li .compare-number .lt-sl,
            .lt-tabs-content .lt-tabs li.active .bery-hr, .lt-tabs-content .lt-tabs li:hover .bery-hr, .woocommerce-tabs .lt-tabs li.active .bery-hr, .woocommerce-tabs .lt-tabs li:hover .bery-hr,
                        
            .category-page .filter-tabs li.active i, .category-page .filter-tabs li:hover i,
            .lt-title.title-style-2:after,
            .lt-pagination.style-1 .page-number li a.next,
            .collapses.active .collapses-title a:before,
            .title-block .heading-title span:after,
            .product-interactions .btn-link:hover,
            .mini-cart.mini-cart-type-full .cart-link .products-number .lt-sl,
            .header-type-2 .lt-bg-dark 
            {
                background-color: <?php echo esc_attr($lt_opt['color_primary']) ?>
            }
            .button.trans-button.primary,
            button.primary-color,
            #lt-popup .ninja-forms-form-wrap .ninja-forms-form .ninja-forms-all-fields-wrap .field-wrap .button,
            .quick-view-icon .lt-icon-text:hover,
            .newsletter-button-wrap .newsletter-button
            {
                background-color: <?php echo esc_attr($lt_opt['color_primary']) ?> !important
            }
        	
            /* BORDER COLOR */
            .text-bordered-primary,
            .add-to-cart-grid .cart-icon-handle,
            .add-to-cart-grid.please-wait .cart-icon strong,
            .navigation-paging a,
            .navigation-image a,
            .post.sticky,
        		
            .next-prev-nav .prod-dropdown > a:hover,
            .iosSlider .sliderNav a:hover span,
            .woocommerce-checkout form.login,
            li.mini-cart .cart-icon strong,
            li.mini-cart .cart-icon .cart-icon-handle,
            .post-date,
            .main-navigation .nav-dropdown ul,
            .remove:hover i,
            .support-icon.square-round:hover,
            .widget_price_filter .ui-slider .ui-slider-handle,
            h3.section-title span,
            .social-icons .icon.icon_email:hover,
            .button.trans-button.primary,
            .seam_icon .seam,
            .border_outner,	
            .pagination-centered .page-numbers a:hover, .pagination-centered .page-numbers span.current,
            .owl-carousel .owl-nav div:hover,
            .products.list .product-interactions .yith-wcwl-wishlistexistsbrowse a,
            li.menu-item.lt-megamenu > .nav-dropdown > .div-sub > ul > li.menu-item.megatop > hr.hr-lt-megamenu,
            .owl-carousel .owl-dots .owl-dot.active,
            .owl-carousel .owl-dots .owl-dot.active:hover,
            .category-page .filter-tabs li.active i, .category-page .filter-tabs li:hover i,
            .products-arrow .next-prev-buttons .icon-next-prev:hover
            {
                border-color: <?php echo esc_attr($lt_opt['color_primary']) ?>;
            }

            .promo .sliderNav span:hover,
            .quick-view-icon .lt-icon-text:hover,
            .remove .icon-close:hover {
                border-color: <?php echo esc_attr($lt_opt['color_primary']) ?> !important;
            }

            .tabbed-content ul.tabs li a:hover:after,
            .tabbed-content ul.tabs li.active a:after {
                border-top-color: <?php echo esc_attr($lt_opt['color_primary']) ?>;
            }

            .collapsing.categories.list li:hover,
            .please-wait,
            #menu-shop-by-category li.active {
                border-left-color: <?php echo esc_attr($lt_opt['color_primary']) ?> !important;
            }
                
    <?php } ?>
    	
        /* COLOR SECONDARY */
    <?php if (isset($lt_opt['color_secondary'])) { ?>
            a.secondary.trans-button,
            li.menu-sale a {
                color: <?php echo esc_attr($lt_opt['color_secondary']) ?>!important
            }
            .label-sale.menu-item a:after,
            .mini-cart:hover .custom-cart-count,
            .button.secondary,
            .button.checkout,
            #submit.secondary,
            button.secondary,
            .button.secondary,
            input[type="submit"].secondary{
                background-color: <?php echo esc_attr($lt_opt['color_secondary']) ?>
            }
            a.button.secondary,
            .button.secondary {
                border-color: <?php echo esc_attr($lt_opt['color_secondary']) ?>;
            }
            a.secondary.trans-button:hover {
                color: #FFF!important;
                background-color: <?php echo esc_attr($lt_opt['color_secondary']) ?>!important
            }
    <?php } ?>

        /* COLOR SUCCESS */
    <?php if (isset($lt_opt['color_success'])) { ?> 
            .woocommerce-message {
                color: <?php echo esc_attr($lt_opt['color_success']) ?>!important
            }
            .woocommerce-message:before,
            .woocommerce-message:after {
                color: #FFF!important;
                background-color: <?php echo esc_attr($lt_opt['color_success']) ?>!important
            }
            .label-popular.menu-item a:after,
            .tooltip-new.menu-item > a:after {
                background-color: <?php echo esc_attr($lt_opt['color_success']) ?>;
                border-color: <?php echo esc_attr($lt_opt['color_success']) ?>;
            }
            .add-to-cart-grid.please-wait .cart-icon .cart-icon-handle,
            .woocommerce-message {
                border-color: <?php echo esc_attr($lt_opt['color_success']) ?>
            }
            .tooltip-new.menu-item > a:before {
                border-top-color: <?php echo esc_attr($lt_opt['color_success']) ?> !important;
            }
            .out-of-stock-label {
                border-right-color: <?php echo esc_attr($lt_opt['color_success']); ?> !important;
                border-top-color: <?php echo esc_attr($lt_opt['color_success']); ?> !important;
            }
    <?php } ?>
        /* COLOR sale */
    <?php if (isset($lt_opt['color_sale_label'])) : ?>
            .badge .badge-inner{
                background: <?php echo esc_attr($lt_opt['color_sale_label']) ?>
            }
            .badge .badge-inner:before{
                border-color: transparent <?php echo esc_attr($lt_opt['color_sale_label']) ?> transparent transparent;
            }
            .product-gallery .badge .badge-inner:after{
                border-color: <?php echo esc_attr($lt_opt['color_sale_label']) ?> transparent transparent transparent;
            }     
    <?php endif; ?>
        /* COLOR hot */
    <?php if (isset($lt_opt['color_hot_label'])) : ?>
            .badge .badge-inner.hot-label{
                background: <?php echo esc_attr($lt_opt['color_hot_label']) ?>
            }
            .badge .badge-inner.hot-label:before{
                border-color: transparent <?php echo esc_attr($lt_opt['color_hot_label']) ?> transparent transparent;
            }
            .product-gallery .badge .badge-inner.hot-label:after{
                border-color: <?php echo esc_attr($lt_opt['color_hot_label']) ?> transparent transparent transparent;
            }     
    <?php endif; ?>
        /* COLOR Price */
    <?php if (isset($lt_opt['color_price_label'])) : ?>
            .product-price, 
            .price.lt-sc-p-price,
            .price,
            .product-item .info .price,
            .countdown .countdown-row .countdown-amount,
            .columns.lt-column-custom-4 .lt-sc-p-deal-countdown .countdown-row.countdown-show4 .countdown-section .countdown-amount           
            {
                color: <?php echo esc_attr($lt_opt['color_price_label']) ?>
            }
            .amount{
                color: <?php echo esc_attr($lt_opt['color_price_label']) ?> !important;
            }
    <?php endif; ?>    
        /* COLOR BUTTON */
    <?php if (isset($lt_opt['color_button'])) { ?> 
            form.cart .button,
            .cart-inner .button.secondary,
            .checkout-button,
            input#place_order,
            /*input.button,*/
            .btn-viewcart,
            /*.btn-checkout,*/
            input#submit,
            .add-to-cart-grid-style2,
            .add_to_cart,
            button,
            .button
            {
                background-color: <?php echo esc_attr($lt_opt['color_button']) ?>!important;
            }
    <?php } ?>
        
        /* COLOR HOVER */
    <?php if (isset($lt_opt['color_hover'])) { ?>
            form.cart .button:hover,
            a.primary.trans-button:hover,
            .form-submit input:hover,
            #payment .place-order input:hover,
            input#submit:hover,
            .add-to-cart-grid-style2:hover,
            .add-to-cart-grid-style2.added,
            .add-to-cart-grid-style2.loading,
            .product-list .product-img .quick-view.fa-search:hover,
            .footer-type-2 input.button,
            button:hover,
            .button:hover,
            .products.list .product-item .inner-wrap .product-summary .product-interactions .add-to-cart-btn:hover,
            .widget.woocommerce li.lt-li-filter-size.chosen,
            .widget.woocommerce li.lt-li-filter-size.lt-chosen,
            .widget.woocommerce li.lt-li-filter-size:hover,
            .widget.widget_categories li.lt-li-filter-size.chosen,
            .widget.widget_categories li.lt-li-filter-size.lt-chosen,
            .widget.widget_categories li.lt-li-filter-size:hover,
            .widget.widget_archive li.lt-li-filter-size.chosen,
            .widget.widget_archive li.lt-li-filter-size.lt-chosen,
            .widget.widget_archive li.lt-li-filter-size:hover,
            .cart-inner .button.secondary:hover,
            .checkout-button:hover,
            input#place_order:hover,
            /*input.button,*/
            .btn-viewcart:hover,
            /*.btn-checkout,*/
            input#submit:hover,
            .add-to-cart-grid-style2:hover,
            .add_to_cart:hover
            {
                background-color: <?php echo esc_attr($lt_opt['color_hover']) ?>!important;
            }
    <?php } ?>
            
        /* COLOR BORDER BUTTON */
    <?php if (isset($lt_opt['button_border_color'])) { ?>
            #submit, 
            button, 
            .button, 
            input[type="submit"],
            .products.list .product-item .inner-wrap .product-summary .product-interactions > div,
            .product-interactions .btn-link,
            .widget.woocommerce li.lt-li-filter-size a,
            .widget.widget_categories li.lt-li-filter-size a,
            .widget.widget_archive li.lt-li-filter-size a {
                border-color: <?php echo esc_attr($lt_opt['button_border_color']); ?> !important;
            }
    <?php } ?>
            
    <?php if (isset($lt_opt['button_border_color_hover'])) { ?>
            #submit:hover, 
            button:hover, 
            .button:hover, 
            input[type="submit"]:hover,
            .products.list .product-item .inner-wrap .product-summary .product-interactions > div:hover,
            .product-interactions .btn-link:hover,
            .widget.woocommerce li.lt-li-filter-size.chosen a,
            .widget.woocommerce li.lt-li-filter-size.lt-chosen a,
            .widget.woocommerce li.lt-li-filter-size:hover a,
            .widget.widget_categories li.lt-li-filter-size.chosen a,
            .widget.widget_categories li.lt-li-filter-size.lt-chosen a,
            .widget.widget_categories li.lt-li-filter-size:hover a,
            .widget.widget_archive li.lt-li-filter-size.chosen a,
            .widget.widget_archive li.lt-li-filter-size.lt-chosen a,
            .widget.widget_archive li.lt-li-filter-size:hover a{
                border-color: <?php echo esc_attr($lt_opt['button_border_color_hover']); ?> !important;
            }
    <?php } ?>
            
        /* BUTTON TEXT COLOR */
    <?php if (isset($lt_opt['button_text_color'])) { ?>
            #submit, 
            button, 
            .button, 
            input[type="submit"]{
                color: <?php echo esc_attr($lt_opt['button_text_color']); ?> !important;
            }
    <?php } ?>
        
        /* BUTTON TEXT COLOR HOVER */
    <?php if (isset($lt_opt['button_text_color_hover'])) { ?>
            #submit:hover, 
            button:hover, 
            .button:hover, 
            input[type="submit"]:hover {
                color: <?php echo esc_attr($lt_opt['button_text_color_hover']); ?> !important;
            }
    <?php } ?>
        
    <?php if (isset($lt_opt['button_radius'])) { ?>
            #submit, 
            button, 
            .button,
            .products.list .product-item .inner-wrap .info .product-summary .product-interactions .add-to-cart-btn,
            .widget .tagcloud a,
            .widget.woocommerce li.lt-li-filter-color a span,
            .widget.woocommerce li.lt-li-filter-color a span,
            .widget.widget_categories li.lt-li-filter-color a span,
            .widget.widget_categories li.lt-li-filter-color a span,
            .widget.widget_archive li.lt-li-filter-color a span,
            .widget.widget_archive li.lt-li-filter-color a span,
            .products.grid .product-item .inner-wrap.product-deals .info .lt-deal-showmore a.button,
            .products.grid .product-item .inner-wrap.product-deals .info .lt-deal-showmore button,
            .wishlist_table .add_to_cart,
            .yith-wcwl-add-button > a.button.alt,
            input[type="submit"]{
                border-radius: <?php echo (int) $lt_opt['button_radius']; ?>px;
            }
    <?php } ?>

    <?php if (isset($lt_opt['button_border'])) { ?>
            #submit, 
            button, 
            .button,
            input[type="submit"]
            {
                border-width: <?php echo (int) $lt_opt['button_border']; ?>px;
            }
    <?php } ?>
        
    <?php if (isset($lt_opt['input_radius'])) { ?>
            input[type="text"],
            input[type="password"], 
            input[type="date"], 
            input[type="datetime"], 
            input[type="datetime-local"], 
            input[type="month"], 
            input[type="week"], 
            input[type="email"], 
            input[type="number"], 
            input[type="search"], 
            input[type="tel"], 
            input[type="time"], 
            input[type="url"],
            #submit.disabled, #submit[disabled], button.disabled, button[disabled], .button.disabled, .button[disabled], input[type="submit"].disabled, input[type="submit"][disabled],
            .category-page .sort-bar .select-wrapper,
            textarea{
                border-radius: <?php echo (int) $lt_opt['input_radius']; ?>px;
                -webkit-border-radius: <?php echo (int) $lt_opt['input_radius']; ?>px;
                -0-border-radius: <?php echo (int) $lt_opt['input_radius']; ?>px;
            }
    <?php } ?>
            
    <?php if (is_admin_bar_showing()) { ?> 
            .tipr_container_bottom{display: none;position: absolute;margin-top: 13px;z-index: 1000;}
            .tipr_container_top{display: none;position: absolute;margin-top:-70px;z-index: 1000;}
    <?php } ?>
    	
    <?php if (!isset($lt_opt['disable_wow']) || !$lt_opt['disable_wow']) { ?>
            .wow, .lt-panels .lt-panel .owl-item .wow{opacity: 0;}
            .owl-item .wow{opacity: 1}
    <?php } ?>
    </style>

    <?php
}

add_action('wp_head', 'techstore_custom_css', 100);
