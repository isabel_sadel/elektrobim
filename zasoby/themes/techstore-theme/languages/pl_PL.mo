��    e      D  �   l      �     �     �  /   �  �   �  -   �	  D   �	     3
     N
     V
     c
     o
     |
     �
     �
     �
     �
     �
     �
     �
     �
     �
     �
                  
   "  �   -     �     �     �     �     �     �            
   1     <     D  
   I     T  ,   \  )   �     �  &   �  /   �  |   #  %   �     �     �     �  
   �     �       c   %     �     �     �     �     �     �     �  
   �  
   �               #     4     @     Q     e     }     �     �     �  
   �     �     �  	   �     �  g   �  	   [     e     r     y     �  7   �     �     �  
   �     �     �     �  	     	        !      *     K     W  !   c  
   �  �  �     -     H  8   a  �   �  6   �  e   �       
   :     E  
   Z     e     v     �  
   �     �     �     �     �  "   �       	                  2     9     J  �   f          $     7     D     \     i     y     �  
   �     �     �     �     �  8   �  5        L     e  9   �  �   �     J     g     u     �     �     �  
   �  L   �               4     I     a     v     �  	   �     �     �     �     �     �     �     �     �       %   )     O     X  	   m     w     �     �     �  b   �            
   +     6  
   K     V     k     w     }     �     �     �     �     �     �     �     �     �  8        D     ;   e       $   D   b   8      G      )                  Q   %           L   I   ^   *   	      X   2   <       \   A              @   6      ,   7          0   #       3   F   +       N                                J               ?   Y   T   P       /   a   5   c          S   9   "   Z   `   =      W   :      .      _       4   V         R   
   H             !   -      ]           '           d       (                  M       [         O      &          U       >            K   E   C          B   1        + Show more - Show less <span class="fa fa-caret-left"></span> Previous <span class="meta-author">by <strong><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></strong>.</span> Posted on <a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a> <span class="meta-nav">&larr;</span> Previous <strong>%s review</strong> for %s <strong>%s reviews</strong> for %s <strong>1</strong> Comment Account Add a review Add to cart Apply Coupon Average rating By  COMMENTS Cart Totals Checkout Checkout details Checkout your items list Click here to login Close Compare Coupon Default Sorting Edit Enter Coupon GO TO HOME If you have shopped with us before, please enter your details in the boxes below. If you are a new customer please proceed to the Billing &amp; Shipping section. In Stock Items Login Login or Register Logout Lost your password? MY CART Manage your items list. My Account My Cart Name Navigation Newness Next <span class="fa fa-caret-right"></span> Next <span class="meta-nav">&rarr;</span> No products in the cart. No products were added to the wishlist No products were found matching your selection. Note: Shipping and taxes are estimated%s and will be updated during checkout based on your billing and shipping information. Oops! That page can&rsquo;t be found. Order Complete Out of Stock Password Popularity Posted in %1$s Posted in %1$s and tagged %2$s. Posted in %1$s. Bookmark the <a href="%3$s" title="Permalink to %4$s" rel="bookmark">permalink</a>. Price Price: high to low Price: low to high Proceed to Checkout Product Categories Product Name Quantity Quantity:  Quick View Rated %d out of 5 Register Related Products Remember me Remove this item Remove this product Return to Previous Page Returning customer? Review and submit your order Sale Search here Share it:  Shipping Shopping Cart Show more Sidebar Sorry, but the page you are looking for is not found. Please, make sure you have typed the current URL. Sort by:  Stock Status Submit Subtotal Tagged %1$s This product is currently out of stock and unavailable. Total Total: Unit Price Update Cart Username Username or email address View Cart View as:  Wishlist You may be interested in&hellip; Your Rating Your Review Your comment is awaiting approval Your order Project-Id-Version: Techstore Theme v1.0.3
Report-Msgid-Bugs-To: 
POT-Creation-Date: 
PO-Revision-Date: 2018-10-06 15:28+0200
Last-Translator: panadmin987 <radek@elektrobim.pl>
Language-Team: 
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;
X-Generator: Poedit 2.1.1
X-Poedit-SourceCharset: utf-8
X-Poedit-KeywordsList: __;_e;__ngettext:1,2;_n:1,2;__ngettext_noop:1,2;_n_noop:1,2;_c,_nc:4c,1,2;_x:1,2c;_ex:1,2c;_nx:4c,1,2;_nx_noop:4c,1,2
X-Poedit-Basepath: ..
X-Textdomain-Support: yes
Language: pl_PL
X-Poedit-SearchPath-0: .
 + Pokaż więcej kategorii - Pokaż mniej kategorii <span class="fa fa-caret-left"></span> Poprzednia strona <span class="meta-author">dodał <strong><a class="url fn n" href="%5$s" title="%6$s" rel="author">%7$s</a></strong>.</span> dnia <a href="%1$s" title="%2$s" rel="bookmark"><time class="entry-date" datetime="%3$s">%4$s</time></a> <span class="meta-nav">&larr;</span> Poprzednia strona <strong>%s opinii</strong> dla %s <strong>%s opinii</strong> dla %s <strong>%s opinii</strong> dla %s <strong>1</strong> Komentarz Moje Konto Dodaj swoją opinię Do koszyka Zatwierdź kupon Najlepiej oceniane Autor:  KOMENTARZY Forma dostawy Do kasy &rarr; Kasa Sklepu Wprowadź dane w formularzu. Kliknij tutaj, aby się zalogować Zamknij Porównaj Kupon Sortowanie domyślne Edytuj Wpisz kod kuponu WRÓĆ NA STRONĘ GŁÓWNĄ Jeśli było już coś przez Ciebie u nas kupowane, prosimy wpisać poniżej dane logowania. Jeśli jesteś nowym klientem, prosimy przejść do sekcji 'Szczegóły płatności' W magazynie Ilość produktów Zaloguj się Logowanie / Rejestracja Wyloguj się Odzyskaj hasło Koszyk Zarządzaj swoim koszykiem. Moje Konto Koszyk Imię lub pseudonim Menu Sortuj od najnowszych Następna strona <span class="fa fa-caret-right"></span> Następna strona <span class="meta-nav">&rarr;</span> Twój koszyk jest pusty. Brak ulubionych produktów. Nie znaleziono produktów na podstawie Twojego zapytania. Notatka: Dostawa i podatki są szacowane%s i będą uaktualnione w zamówieniu na podstawie Twoich informacji o adresie dostawy i płatności. Ups! Ta strona nie istnieje. Potwierdzenie Brak w magazynie Hasło Sortuj wg. popularności Kategoria: %1$s %1$s, %2$s %1$s, <a href="%3$s" title="Permalink to %4$s" rel="bookmark">Permalink</a>. Suma za produkty Cena: od najwyższej Cena: od najniższej Przejdź do Kasy &rarr; Kategorie produktów Nazwa produktu Ilość: Ilość:  Szybki podgląd Ocena: %d/5 Rejestracja Produkty pokrewne Zapamiętaj mnie Usuń z koszyka Usuń Powrót do poprzedniej strony Masz u nas konto? Potwierdzenie przyjęcia zamówienia. Promocja Szukaj produktów... Wyślij:  Wysyłka Koszyk Zobacz produkt Kategorie produktów Przepraszamy, ale strona której szukasz nie istnieje. Prosimy sprawdzić poprawność adresu URL. Sortowanie:  Stan magazynowy Zatwierdź Wartość produktów Tagi: %1$s Produkt niedostępny Do zapłaty Suma: Cena Zaktualizuj Koszyk Adres email Adres email Pokaż Koszyk Wyświetl jako:  Ulubione Wybierz produkty do kompletu Twoja ocena Twoja opinia Dziękujemy za opinię, wkrótce pojawi się na stronie. Podsumowanie zamówienia 