<?php
/**
 * The template for displaying search forms in leetheme
 *
 * @package     leetheme
 * @author      WooThemes
 * @version     2.5.0
 */
$search_param = array(
    'name'  => 'post_type',
    'value' => 'product'
);

/*$categories = get_categories( array(
    'taxonomy' => 'product_cat',
    'orderby' => 'name'
));*/

$_id = rand();
?>
<div class="search-wrapper lt-ajaxsearchform-container <?php echo esc_attr($_id); ?>_container">
    <form method="get" class="lt-ajaxsearchform" action="<?php echo esc_url(home_url('/')) ?>">
        <div class="search-control-group control-group">
            <label class="sr-only screen-reader-text"><?php esc_html_e('Search here', 'techstore-theme'); ?></label>
            <?php /*select name="cat" id="lt-select-<?php echo esc_attr($_id);?>" class="lt-cats-search">
                <option value="">Category</option>
                <?php if(!empty($categories)):
                    foreach ($categories as $key => $v) :
                        echo '<option value="' . esc_attr($v->term_id) . '">'.$v->name.'</option>';
                    endforeach;
                endif;?>
            </select*/?>
            <input id="lt-input-<?php echo esc_attr($_id);?>" type="text" class="search-field search-input live-search-input" value="<?php echo get_search_query();?>" name="s" placeholder="<?php esc_html_e('Search here', 'techstore-theme'); ?>" />
            <input type="submit" name="page" value="<?php esc_html_e('search', 'techstore-theme'); ?>" style="display: none" />
            <input type="hidden" class="search-param" name="<?php echo esc_attr($search_param['name']); ?>" value="<?php echo esc_attr($search_param['value']); ?>" />
        </div>
    </form>
</div>