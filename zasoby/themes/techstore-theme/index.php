<?php
/**
 * The main template file.
 *
 * @package leetheme
 */

require('category-blog.php');

/*
if (isset($_GET['right-sidebar'])) :
    $lt_opt['blog_layout'] = 'right-sidebar';
elseif(isset($_GET['no-sidebar'])):
    $lt_opt['blog_layout'] = 'no-sidebar';
endif;

if(!isset($lt_opt['blog_layout'])):
    $lt_opt['blog_layout'] = '';
endif;

$hasSidebar = true;
$left = true;
switch ($lt_opt['blog_layout']):
    case 'right-sidebar':
        $left = false;
        $attr = 'large-9 left columns';
        break;
    case 'no-sidebar':
        $hasSidebar = false;
        $left = false;
        $attr = 'large-12 columns';
        break;
    default:
        $attr = 'large-9 right columns';
        break;
endswitch;

get_header();
techstore_get_breadcrumb();
*/?><!--
<div class="container-wrap page-<?php /*echo $lt_opt['blog_layout'] ? esc_attr($lt_opt['blog_layout']) : 'right-sidebar'; */?>">
    
    <?php /*if($hasSidebar):*/?>
        <div class="div-toggle-sidebar center"><a class="toggle-sidebar" href="javascript:void(0);"><i class="icon-menu"></i> <?php /*esc_html_e('Sidebar', 'techstore-theme');*/?></a></div>
    <?php /*endif;*/?>
        
    <div class="row">
        <div id="content" class="<?php /*echo esc_attr($attr);*/?>" role="main">
            <?php /*if( is_home() ) : */?>
            <header class="page-header">
                <h1 class="page-title page-title--archive">
                    Blog
                </h1>
            </header>
            <?php /*endif; */?>
            <div class="page-inner">
                <?php /*if (have_posts()) :
                    while (have_posts()) : the_post();
                        $part = get_post_format();
                        $part = trim($part) != '' ? '-' . trim($part) : '';
                        include get_template_directory() . '/content' . $part . '.php';
                    endwhile;
                else :
                    get_template_part( 'no-results', 'index' );
                endif; */?>

                <div class="large-12 columns navigation-container">
                    <?php /*techstore_content_nav('nav-below'); */?>
                </div>
            </div>
        </div>

        <?php /*if($lt_opt['blog_layout'] == 'left-sidebar' || $lt_opt['blog_layout'] == 'right-sidebar'):*/?>
            <div class="large-3 columns <?php /*echo ($left) ? 'left' : 'right';*/?> col-sidebar">
                <?php /*get_sidebar();*/?>
            </div>
        <?php /*endif;*/?>
    </div>
</div>

--><?php /*get_footer();*/
