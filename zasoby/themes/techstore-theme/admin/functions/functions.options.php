<?php

add_action('init', 'of_options');

if (!function_exists('of_options')) {

    function of_options() {
        $url = ADMIN_DIR . 'assets/images/';
        //Access the WordPress Categories via an Array
        $of_categories = array();
        $of_categories_obj = get_categories('hide_empty=0');
        foreach ($of_categories_obj as $of_cat) {
            $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;
        }
        $categories_tmp = array_unshift($of_categories, "Select a category:");

        //Access the WordPress Pages via an Array
        $of_pages = array();
        $of_pages_obj = get_pages('sort_column=post_parent,menu_order');
        foreach ($of_pages_obj as $of_page) {
            $of_pages[$of_page->ID] = $of_page->post_name;
        }
        $of_pages_tmp = array_unshift($of_pages, "Select a page:");

        //Testing 
        $of_options_select = array("one", "two", "three", "four", "five");
        $of_options_radio = array("one" => "One", "two" => "Two", "three" => "Three", "four" => "Four", "five" => "Five");

        //Sample Homepage blocks for the layout manager (sorter)
        $of_options_homepage_blocks = array
            (
            "disabled" => array(
                "placebo" => "placebo", //REQUIRED!
                "block_one" => "Block One",
                "block_two" => "Block Two",
                "block_three" => "Block Three",
            ),
            "enabled" => array(
                "placebo" => "placebo", //REQUIRED!
                "block_four" => "Block Four",
            ),
        );

        /* ----------------------------------------------------------------------------------- */
        /* TO DO: Add options/functions that use these */
        /* ----------------------------------------------------------------------------------- */

        //More Options
        $uploads_arr = wp_upload_dir();
        $all_uploads_path = $uploads_arr['path'];
        $all_uploads = get_option('of_uploads');
        $other_entries = array("Select a number:", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19");
        $body_repeat = array("no-repeat", "repeat-x", "repeat-y", "repeat");
        $body_pos = array("top left", "top center", "top right", "center left", "center center", "center right", "bottom left", "bottom center", "bottom right");

        $google_fonts = array(
            'arial' => 'Arial',
            'verdana' => 'Verdana, Geneva',
            'trebuchet' => 'Trebuchet',
            'trebuchet ms' => 'Trebuchet MS',
            'georgia' => 'Georgia',
            'times' => 'Times New Roman',
            'tahoma' => 'Tahoma, Geneva',
            'helvetica' => 'Helvetica',
            'Abel' => 'Abel',
            'Abril Fatface' => 'Abril Fatface',
            'Aclonica' => 'Aclonica',
            'Acme' => 'Acme',
            'Actor' => 'Actor',
            'Adamina' => 'Adamina',
            'Advent Pro' => 'Advent Pro',
            'Aguafina Script' => 'Aguafina Script',
            'Aladin' => 'Aladin',
            'Aldrich' => 'Aldrich',
            'Alegreya' => 'Alegreya',
            'Alegreya SC' => 'Alegreya SC',
            'Alex Brush' => 'Alex Brush',
            'Alfa Slab One' => 'Alfa Slab One',
            'Alice' => 'Alice',
            'Alike' => 'Alike',
            'Alike Angular' => 'Alike Angular',
            'Allan' => 'Allan',
            'Allerta' => 'Allerta',
            'Allerta Stencil' => 'Allerta Stencil',
            'Allura' => 'Allura',
            'Almendra' => 'Almendra',
            'Almendra SC' => 'Almendra SC',
            'Amaranth' => 'Amaranth',
            'Amatic SC' => 'Amatic SC',
            'Amethysta' => 'Amethysta',
            'Andada' => 'Andada',
            'Andika' => 'Andika',
            'Angkor' => 'Angkor',
            'Annie Use Your Telescope' => 'Annie Use Your Telescope',
            'Anonymous Pro' => 'Anonymous Pro',
            'Antic' => 'Antic',
            'Antic Didone' => 'Antic Didone',
            'Antic Slab' => 'Antic Slab',
            'Anton' => 'Anton',
            'Arapey' => 'Arapey',
            'Arbutus' => 'Arbutus',
            'Architects Daughter' => 'Architects Daughter',
            'Arimo' => 'Arimo',
            'Arizonia' => 'Arizonia',
            'Armata' => 'Armata',
            'Artifika' => 'Artifika',
            'Arvo' => 'Arvo',
            'Asap' => 'Asap',
            'Asset' => 'Asset',
            'Astloch' => 'Astloch',
            'Asul' => 'Asul',
            'Atomic Age' => 'Atomic Age',
            'Aubrey' => 'Aubrey',
            'Audiowide' => 'Audiowide',
            'Average' => 'Average',
            'Averia Gruesa Libre' => 'Averia Gruesa Libre',
            'Averia Libre' => 'Averia Libre',
            'Averia Sans Libre' => 'Averia Sans Libre',
            'Averia Serif Libre' => 'Averia Serif Libre',
            'Bad Script' => 'Bad Script',
            'Balthazar' => 'Balthazar',
            'Bangers' => 'Bangers',
            'Basic' => 'Basic',
            'Battambang' => 'Battambang',
            'Baumans' => 'Baumans',
            'Bayon' => 'Bayon',
            'Belgrano' => 'Belgrano',
            'Belleza' => 'Belleza',
            'Bentham' => 'Bentham',
            'Berkshire Swash' => 'Berkshire Swash',
            'Bevan' => 'Bevan',
            'Bigshot One' => 'Bigshot One',
            'Bilbo' => 'Bilbo',
            'Bilbo Swash Caps' => 'Bilbo Swash Caps',
            'Bitter' => 'Bitter',
            'Black Ops One' => 'Black Ops One',
            'Bokor' => 'Bokor',
            'Bonbon' => 'Bonbon',
            'Boogaloo' => 'Boogaloo',
            'Bowlby One' => 'Bowlby One',
            'Bowlby One SC' => 'Bowlby One SC',
            'Brawler' => 'Brawler',
            'Bree Serif' => 'Bree Serif',
            'Bubblegum Sans' => 'Bubblegum Sans',
            'Buda' => 'Buda',
            'Buenard' => 'Buenard',
            'Butcherman' => 'Butcherman',
            'Butterfly Kids' => 'Butterfly Kids',
            'Cabin' => 'Cabin',
            'Cabin Condensed' => 'Cabin Condensed',
            'Cabin Sketch' => 'Cabin Sketch',
            'Caesar Dressing' => 'Caesar Dressing',
            'Cagliostro' => 'Cagliostro',
            'Calligraffitti' => 'Calligraffitti',
            'Cambo' => 'Cambo',
            'Candal' => 'Candal',
            'Cantarell' => 'Cantarell',
            'Cantata One' => 'Cantata One',
            'Cardo' => 'Cardo',
            'Carme' => 'Carme',
            'Carter One' => 'Carter One',
            'Caudex' => 'Caudex',
            'Cedarville Cursive' => 'Cedarville Cursive',
            'Ceviche One' => 'Ceviche One',
            'Changa One' => 'Changa One',
            'Chango' => 'Chango',
            'Chau Philomene One' => 'Chau Philomene One',
            'Chelsea Market' => 'Chelsea Market',
            'Chenla' => 'Chenla',
            'Cherry Cream Soda' => 'Cherry Cream Soda',
            'Chewy' => 'Chewy',
            'Chicle' => 'Chicle',
            'Chivo' => 'Chivo',
            'Coda' => 'Coda',
            'Coda Caption' => 'Coda Caption',
            'Codystar' => 'Codystar',
            'Comfortaa' => 'Comfortaa',
            'Coming Soon' => 'Coming Soon',
            'Concert One' => 'Concert One',
            'Condiment' => 'Condiment',
            'Content' => 'Content',
            'Contrail One' => 'Contrail One',
            'Convergence' => 'Convergence',
            'Cookie' => 'Cookie',
            'Copse' => 'Copse',
            'Corben' => 'Corben',
            'Cousine' => 'Cousine',
            'Coustard' => 'Coustard',
            'Covered By Your Grace' => 'Covered By Your Grace',
            'Crafty Girls' => 'Crafty Girls',
            'Creepster' => 'Creepster',
            'Crete Round' => 'Crete Round',
            'Crimson Text' => 'Crimson Text',
            'Crushed' => 'Crushed',
            'Cuprum' => 'Cuprum',
            'Cutive' => 'Cutive',
            'Damion' => 'Damion',
            'Dancing Script' => 'Dancing Script',
            'Dangrek' => 'Dangrek',
            'Dawning of a New Day' => 'Dawning of a New Day',
            'Days One' => 'Days One',
            'Delius' => 'Delius',
            'Delius Swash Caps' => 'Delius Swash Caps',
            'Delius Unicase' => 'Delius Unicase',
            'Della Respira' => 'Della Respira',
            'Devonshire' => 'Devonshire',
            'Didact Gothic' => 'Didact Gothic',
            'Diplomata' => 'Diplomata',
            'Diplomata SC' => 'Diplomata SC',
            'Doppio One' => 'Doppio One',
            'Dorsa' => 'Dorsa',
            'Dosis' => 'Dosis',
            'Dr Sugiyama' => 'Dr Sugiyama',
            'Droid Sans' => 'Droid Sans',
            'Droid Sans Mono' => 'Droid Sans Mono',
            'Droid Serif' => 'Droid Serif',
            'Duru Sans' => 'Duru Sans',
            'Dynalight' => 'Dynalight',
            'EB Garamond' => 'EB Garamond',
            'Eater' => 'Eater',
            'Economica' => 'Economica',
            'Electrolize' => 'Electrolize',
            'Emblema One' => 'Emblema One',
            'Emilys Candy' => 'Emilys Candy',
            'Engagement' => 'Engagement',
            'Enriqueta' => 'Enriqueta',
            'Erica One' => 'Erica One',
            'Esteban' => 'Esteban',
            'Euphoria Script' => 'Euphoria Script',
            'Ewert' => 'Ewert',
            'Exo' => 'Exo',
            'Exo 2' => 'Exo 2',
            'Expletus Sans' => 'Expletus Sans',
            'Fanwood Text' => 'Fanwood Text',
            'Fascinate' => 'Fascinate',
            'Fascinate Inline' => 'Fascinate Inline',
            'Federant' => 'Federant',
            'Federo' => 'Federo',
            'Felipa' => 'Felipa',
            'Fjord One' => 'Fjord One',
            'Flamenco' => 'Flamenco',
            'Flavors' => 'Flavors',
            'Fondamento' => 'Fondamento',
            'Fontdiner Swanky' => 'Fontdiner Swanky',
            'Forum' => 'Forum',
            'Fjalla One' => 'Fjalla One',
            'Francois One' => 'Francois One',
            'Fredericka the Great' => 'Fredericka the Great',
            'Fredoka One' => 'Fredoka One',
            'Freehand' => 'Freehand',
            'Fresca' => 'Fresca',
            'Frijole' => 'Frijole',
            'Fugaz One' => 'Fugaz One',
            'GFS Didot' => 'GFS Didot',
            'GFS Neohellenic' => 'GFS Neohellenic',
            'Galdeano' => 'Galdeano',
            'Gentium Basic' => 'Gentium Basic',
            'Gentium Book Basic' => 'Gentium Book Basic',
            'Geo' => 'Geo',
            'Geostar' => 'Geostar',
            'Geostar Fill' => 'Geostar Fill',
            'Germania One' => 'Germania One',
            'Gilda Display' => 'Gilda Display',
            'Give You Glory' => 'Give You Glory',
            'Glass Antiqua' => 'Glass Antiqua',
            'Glegoo' => 'Glegoo',
            'Gloria Hallelujah' => 'Gloria Hallelujah',
            'Goblin One' => 'Goblin One',
            'Gochi Hand' => 'Gochi Hand',
            'Gorditas' => 'Gorditas',
            'Goudy Bookletter 1911' => 'Goudy Bookletter 1911',
            'Graduate' => 'Graduate',
            'Gravitas One' => 'Gravitas One',
            'Great Vibes' => 'Great Vibes',
            'Gruppo' => 'Gruppo',
            'Gudea' => 'Gudea',
            'Habibi' => 'Habibi',
            'Hammersmith One' => 'Hammersmith One',
            'Handlee' => 'Handlee',
            'Hanuman' => 'Hanuman',
            'Happy Monkey' => 'Happy Monkey',
            'Henny Penny' => 'Henny Penny',
            'Herr Von Muellerhoff' => 'Herr Von Muellerhoff',
            'Holtwood One SC' => 'Holtwood One SC',
            'Homemade Apple' => 'Homemade Apple',
            'Homenaje' => 'Homenaje',
            'IM Fell DW Pica' => 'IM Fell DW Pica',
            'IM Fell DW Pica SC' => 'IM Fell DW Pica SC',
            'IM Fell Double Pica' => 'IM Fell Double Pica',
            'IM Fell Double Pica SC' => 'IM Fell Double Pica SC',
            'IM Fell English' => 'IM Fell English',
            'IM Fell English SC' => 'IM Fell English SC',
            'IM Fell French Canon' => 'IM Fell French Canon',
            'IM Fell French Canon SC' => 'IM Fell French Canon SC',
            'IM Fell Great Primer' => 'IM Fell Great Primer',
            'IM Fell Great Primer SC' => 'IM Fell Great Primer SC',
            'Iceberg' => 'Iceberg',
            'Iceland' => 'Iceland',
            'Imprima' => 'Imprima',
            'Inconsolata' => 'Inconsolata',
            'Inder' => 'Inder',
            'Indie Flower' => 'Indie Flower',
            'Inika' => 'Inika',
            'Irish Grover' => 'Irish Grover',
            'Istok Web' => 'Istok Web',
            'Italiana' => 'Italiana',
            'Italianno' => 'Italianno',
            'Jim Nightshade' => 'Jim Nightshade',
            'Jockey One' => 'Jockey One',
            'Jolly Lodger' => 'Jolly Lodger',
            'Josefin Sans' => 'Josefin Sans',
            'Josefin Slab' => 'Josefin Slab',
            'Judson' => 'Judson',
            'Julee' => 'Julee',
            'Junge' => 'Junge',
            'Jura' => 'Jura',
            'Just Another Hand' => 'Just Another Hand',
            'Just Me Again Down Here' => 'Just Me Again Down Here',
            'Kameron' => 'Kameron',
            'Karla' => 'Karla',
            'Kaushan Script' => 'Kaushan Script',
            'Kelly Slab' => 'Kelly Slab',
            'Kenia' => 'Kenia',
            'Khmer' => 'Khmer',
            'Knewave' => 'Knewave',
            'Kotta One' => 'Kotta One',
            'Koulen' => 'Koulen',
            'Kranky' => 'Kranky',
            'Kreon' => 'Kreon',
            'Kristi' => 'Kristi',
            'Krona One' => 'Krona One',
            'La Belle Aurore' => 'La Belle Aurore',
            'Lancelot' => 'Lancelot',
            'Lato' => 'Lato',
            'League Script' => 'League Script',
            'Leckerli One' => 'Leckerli One',
            'Ledger' => 'Ledger',
            'Lekton' => 'Lekton',
            'Lemon' => 'Lemon',
            'Libre Baskerville' => 'Libre Baskerville',
            'Lilita One' => 'Lilita One',
            'Limelight' => 'Limelight',
            'Linden Hill' => 'Linden Hill',
            'Lobster' => 'Lobster',
            'Lobster Two' => 'Lobster Two',
            'Londrina Outline' => 'Londrina Outline',
            'Londrina Shadow' => 'Londrina Shadow',
            'Londrina Sketch' => 'Londrina Sketch',
            'Londrina Solid' => 'Londrina Solid',
            'Lora' => 'Lora',
            'Love Ya Like A Sister' => 'Love Ya Like A Sister',
            'Loved by the King' => 'Loved by the King',
            'Lovers Quarrel' => 'Lovers Quarrel',
            'Luckiest Guy' => 'Luckiest Guy',
            'Lusitana' => 'Lusitana',
            'Lustria' => 'Lustria',
            'Macondo' => 'Macondo',
            'Macondo Swash Caps' => 'Macondo Swash Caps',
            'Magra' => 'Magra',
            'Maiden Orange' => 'Maiden Orange',
            'Mako' => 'Mako',
            'Marcellus' => 'Marcellus',
            'Marcellus SC' => 'Marcellus SC',
            'Marck Script' => 'Marck Script',
            'Marko One' => 'Marko One',
            'Marmelad' => 'Marmelad',
            'Marvel' => 'Marvel',
            'Mate' => 'Mate',
            'Mate SC' => 'Mate SC',
            'Maven Pro' => 'Maven Pro',
            'Meddon' => 'Meddon',
            'MedievalSharp' => 'MedievalSharp',
            'Medula One' => 'Medula One',
            'Megrim' => 'Megrim',
            'Merienda One' => 'Merienda One',
            'Merriweather' => 'Merriweather',
            'Metal' => 'Metal',
            'Metamorphous' => 'Metamorphous',
            'Metrophobic' => 'Metrophobic',
            'Michroma' => 'Michroma',
            'Miltonian' => 'Miltonian',
            'Miltonian Tattoo' => 'Miltonian Tattoo',
            'Miniver' => 'Miniver',
            'Miss Fajardose' => 'Miss Fajardose',
            'Modern Antiqua' => 'Modern Antiqua',
            'Molengo' => 'Molengo',
            'Monofett' => 'Monofett',
            'Monoton' => 'Monoton',
            'Monsieur La Doulaise' => 'Monsieur La Doulaise',
            'Montaga' => 'Montaga',
            'Montez' => 'Montez',
            'Montserrat' => 'Montserrat',
            'Montserrat Alternates' => 'Montserrat Alternates',
            'Montserrat Subrayada' => 'Montserrat Subrayada',
            'Moul' => 'Moul',
            'Moulpali' => 'Moulpali',
            'Mountains of Christmas' => 'Mountains of Christmas',
            'Mr Bedfort' => 'Mr Bedfort',
            'Mr Dafoe' => 'Mr Dafoe',
            'Mr De Haviland' => 'Mr De Haviland',
            'Mrs Saint Delafield' => 'Mrs Saint Delafield',
            'Mrs Sheppards' => 'Mrs Sheppards',
            'Muli' => 'Muli',
            'Mystery Quest' => 'Mystery Quest',
            'Neucha' => 'Neucha',
            'Neuton' => 'Neuton',
            'News Cycle' => 'News Cycle',
            'Niconne' => 'Niconne',
            'Nixie One' => 'Nixie One',
            'Nobile' => 'Nobile',
            'Nokora' => 'Nokora',
            'Norican' => 'Norican',
            'Nosifer' => 'Nosifer',
            'Nothing You Could Do' => 'Nothing You Could Do',
            'Noticia Text' => 'Noticia Text',
            'Noto Sans' => 'Noto Sans',
            'Nova Cut' => 'Nova Cut',
            'Nova Flat' => 'Nova Flat',
            'Nova Mono' => 'Nova Mono',
            'Nova Oval' => 'Nova Oval',
            'Nova Round' => 'Nova Round',
            'Nova Script' => 'Nova Script',
            'Nova Slim' => 'Nova Slim',
            'Nova Square' => 'Nova Square',
            'Numans' => 'Numans',
            'Nunito' => 'Nunito',
            'Odor Mean Chey' => 'Odor Mean Chey',
            'Old Standard TT' => 'Old Standard TT',
            'Oldenburg' => 'Oldenburg',
            'Oleo Script' => 'Oleo Script',
            'Open Sans' => 'Open Sans',
            'Open Sans Condensed' => 'Open Sans Condensed',
            'Orbitron' => 'Orbitron',
            'Original Surfer' => 'Original Surfer',
            'Oswald' => 'Oswald',
            'Over the Rainbow' => 'Over the Rainbow',
            'Overlock' => 'Overlock',
            'Overlock SC' => 'Overlock SC',
            'Ovo' => 'Ovo',
            'Oxygen' => 'Oxygen',
            'PT Mono' => 'PT Mono',
            'PT Sans' => 'PT Sans',
            'PT Sans Caption' => 'PT Sans Caption',
            'PT Sans Narrow' => 'PT Sans Narrow',
            'PT Serif' => 'PT Serif',
            'PT Serif Caption' => 'PT Serif Caption',
            'Pacifico' => 'Pacifico',
            'Parisienne' => 'Parisienne',
            'Passero One' => 'Passero One',
            'Passion One' => 'Passion One',
            'Patrick Hand' => 'Patrick Hand',
            'Patua One' => 'Patua One',
            'Paytone One' => 'Paytone One',
            'Permanent Marker' => 'Permanent Marker',
            'Petrona' => 'Petrona',
            'Philosopher' => 'Philosopher',
            'Piedra' => 'Piedra',
            'Pinyon Script' => 'Pinyon Script',
            'Plaster' => 'Plaster',
            'Play' => 'Play',
            'Playball' => 'Playball',
            'Playfair Display' => 'Playfair Display',
            'Podkova' => 'Podkova',
            'Poiret One' => 'Poiret One',
            'Poller One' => 'Poller One',
            'Poly' => 'Poly',
            'Pompiere' => 'Pompiere',
            'Pontano Sans' => 'Pontano Sans',
            'Port Lligat Sans' => 'Port Lligat Sans',
            'Port Lligat Slab' => 'Port Lligat Slab',
            'Prata' => 'Prata',
            'Preahvihear' => 'Preahvihear',
            'Press Start 2P' => 'Press Start 2P',
            'Princess Sofia' => 'Princess Sofia',
            'Prociono' => 'Prociono',
            'Prosto One' => 'Prosto One',
            'Puritan' => 'Puritan',
            'Quantico' => 'Quantico',
            'Quattrocento' => 'Quattrocento',
            'Quattrocento Sans' => 'Quattrocento Sans',
            'Questrial' => 'Questrial',
            'Quicksand' => 'Quicksand',
            'Qwigley' => 'Qwigley',
            'Radley' => 'Radley',
            'Raleway' => 'Raleway',
            'Rammetto One' => 'Rammetto One',
            'Rancho' => 'Rancho',
            'Rationale' => 'Rationale',
            'Redressed' => 'Redressed',
            'Reenie Beanie' => 'Reenie Beanie',
            'Revalia' => 'Revalia',
            'Ribeye' => 'Ribeye',
            'Ribeye Marrow' => 'Ribeye Marrow',
            'Righteous' => 'Righteous',
            'Roboto' => 'Roboto',
            'Roboto Sans' => 'Roboto Sans',
            'Rochester' => 'Rochester',
            'Rock Salt' => 'Rock Salt',
            'Rokkitt' => 'Rokkitt',
            'Ropa Sans' => 'Ropa Sans',
            'Rosario' => 'Rosario',
            'Rosarivo' => 'Rosarivo',
            'Rouge Script' => 'Rouge Script',
            'Ruda' => 'Ruda',
            'Ruge Boogie' => 'Ruge Boogie',
            'Ruluko' => 'Ruluko',
            'Rum Raisin' => 'Rum Raisin',
            'Ruslan Display' => 'Ruslan Display',
            'Russo One' => 'Russo One',
            'Ruthie' => 'Ruthie',
            'Sacramento' => 'Sacramento',
            'Sail' => 'Sail',
            'Salsa' => 'Salsa',
            'Sancreek' => 'Sancreek',
            'Sansita One' => 'Sansita One',
            'Sarina' => 'Sarina',
            'Satisfy' => 'Satisfy',
            'Schoolbell' => 'Schoolbell',
            'Seaweed Script' => 'Seaweed Script',
            'Sevillana' => 'Sevillana',
            'Seymour One' => 'Seymour One',
            'Shadows Into Light' => 'Shadows Into Light',
            'Shadows Into Light Two' => 'Shadows Into Light Two',
            'Shanti' => 'Shanti',
            'Share' => 'Share',
            'Shojumaru' => 'Shojumaru',
            'Short Stack' => 'Short Stack',
            'Siemreap' => 'Siemreap',
            'Sigmar One' => 'Sigmar One',
            'Signika' => 'Signika',
            'Signika Negative' => 'Signika Negative',
            'Simonetta' => 'Simonetta',
            'Sirin Stencil' => 'Sirin Stencil',
            'Six Caps' => 'Six Caps',
            'Slackey' => 'Slackey',
            'Smokum' => 'Smokum',
            'Smythe' => 'Smythe',
            'Sniglet' => 'Sniglet',
            'Snippet' => 'Snippet',
            'Sofia' => 'Sofia',
            'Sonsie One' => 'Sonsie One',
            'Sorts Mill Goudy' => 'Sorts Mill Goudy',
            'Special Elite' => 'Special Elite',
            'Spicy Rice' => 'Spicy Rice',
            'Spinnaker' => 'Spinnaker',
            'Spirax' => 'Spirax',
            'Squada One' => 'Squada One',
            'Stardos Stencil' => 'Stardos Stencil',
            'Stint Ultra Condensed' => 'Stint Ultra Condensed',
            'Stint Ultra Expanded' => 'Stint Ultra Expanded',
            'Stoke' => 'Stoke',
            'Sue Ellen Francisco' => 'Sue Ellen Francisco',
            'Sunshiney' => 'Sunshiney',
            'Supermercado One' => 'Supermercado One',
            'Suwannaphum' => 'Suwannaphum',
            'Swanky and Moo Moo' => 'Swanky and Moo Moo',
            'Syncopate' => 'Syncopate',
            'Tangerine' => 'Tangerine',
            'Taprom' => 'Taprom',
            'Telex' => 'Telex',
            'Tenor Sans' => 'Tenor Sans',
            'The Girl Next Door' => 'The Girl Next Door',
            'Tienne' => 'Tienne',
            'Tinos' => 'Tinos',
            'Titan One' => 'Titan One',
            'Titillium Web' => 'Titillium Web',
            'Trade Winds' => 'Trade Winds',
            'Trocchi' => 'Trocchi',
            'Trochut' => 'Trochut',
            'Trykker' => 'Trykker',
            'Tulpen One' => 'Tulpen One',
            'Ubuntu' => 'Ubuntu',
            'Ubuntu Condensed' => 'Ubuntu Condensed',
            'Ubuntu Mono' => 'Ubuntu Mono',
            'Ultra' => 'Ultra',
            'Uncial Antiqua' => 'Uncial Antiqua',
            'UnifrakturCook' => 'UnifrakturCook',
            'UnifrakturMaguntia' => 'UnifrakturMaguntia',
            'Unkempt' => 'Unkempt',
            'Unlock' => 'Unlock',
            'Unna' => 'Unna',
            'VT323' => 'VT323',
            'Varela' => 'Varela',
            'Varela Round' => 'Varela Round',
            'Vast Shadow' => 'Vast Shadow',
            'Vibur' => 'Vibur',
            'Vidaloka' => 'Vidaloka',
            'Viga' => 'Viga',
            'Voces' => 'Voces',
            'Volkhov' => 'Volkhov',
            'Vollkorn' => 'Vollkorn',
            'Voltaire' => 'Voltaire',
            'Waiting for the Sunrise' => 'Waiting for the Sunrise',
            'Wallpoet' => 'Wallpoet',
            'Walter Turncoat' => 'Walter Turncoat',
            'Wellfleet' => 'Wellfleet',
            'Wire One' => 'Wire One',
            'Yanone Kaffeesatz' => 'Yanone Kaffeesatz',
            'Yellowtail' => 'Yellowtail',
            'Yeseva One' => 'Yeseva One',
            'Yesteryear' => 'Yesteryear',
            'Zeyada' => 'Zeyada'
        );

        // Image Alignment radio box
        $of_options_thumb_align = array("alignleft" => "Left", "alignright" => "Right", "aligncenter" => "Center");

        // Image Links to Options
        $of_options_image_link_to = array("image" => "The Image", "post" => "The Post");


        /* ----------------------------------------------------------------------------------- */
        /* The Options Array */
        /* ----------------------------------------------------------------------------------- */

        // Set the Options Array
        global $of_options;
        $of_options = array();

        /* ================== GENERAL HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("General", 'techstore-theme'),
            "target" => 'general',
            "type" => "heading"
        );

        $of_options[] = array(
            "name" => esc_html__("Import Demo Content", 'techstore-theme'),
            "desc" => esc_html__("Click for import. Please ensure our plugins are activated before content is imported.", 'techstore-theme'),
            "id" => "demo_data",
            "std" => "",
            "btntext" => esc_html__("Import Demo Content", 'techstore-theme'),
            "type" => "button",
            "options" => array(
                'classic' => esc_html__("Classic Demo", 'techstore-theme'),
                'agency' => esc_html__("Agency Demo", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Site Layout", 'techstore-theme'),
            "desc" => esc_html__("Selects site layout.", 'techstore-theme'),
            "id" => "site_layout",
            "std" => "wide",
            "type" => "select",
            "options" => array(
                "wide" => esc_html__("Wide", 'techstore-theme'),
                "boxed" => esc_html__("Boxed", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Site Background Color", 'techstore-theme'),
            "id" => "site_bg_color",
            "std" => "#eee",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Site Background Image", 'techstore-theme'),
            "id" => "site_bg_image",
            "std" => get_template_directory_uri() . "/images/bkgd1.jpg",
            "type" => "media"
        );

        $of_options[] = array(
            "name" => esc_html__("Show Demo Panel", 'techstore-theme'),
            "desc" => esc_html__("Show demo panel in homepage for quick select color that you want", 'techstore-theme'),
            "id" => "demo_show",
            "std" => 0,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Disable Transition Loading", 'techstore-theme'),
            "desc" => esc_html__("Disable transition loading for all page", 'techstore-theme'),
            "id" => "disable_wow",
            "std" => 0,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Disable Plugin Update", 'techstore-theme'),
            "desc" => esc_html__("Disable plugin update notice", 'techstore-theme'),
            "id" => "plugin_update_notice",
            "std" => 1,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Cache site version JS & CSS", 'techstore-theme'),
            "desc" => esc_html__("Cache site version", 'techstore-theme'),
            "id" => "cache_version",
            "std" => "",
            "type" => "text"
        );

        /* =================== LOGO AND ICONS HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Logo and icons", 'techstore-theme'),
            "target" => 'logo-icons',
            "type" => "heading"
        );

        $of_options[] = array(
            "name" => esc_html__("Logo", 'techstore-theme'),
            "desc" => esc_html__("Upload logo here.", 'techstore-theme'),
            "id" => "site_logo",
            "std" => get_template_directory_uri() . "/images/logo.png",
            "type" => "media"
        );

        $of_options[] = array(
            "name" => esc_html__("Max height logo", 'techstore-theme'),
            "desc" => esc_html__("Max height logo", 'techstore-theme'),
            "id" => "max_height_logo",
            "std" => "95px",
            "type" => "text"
        );

        $of_options[] = array(
            "name" => esc_html__("Favicon", 'techstore-theme'),
            "desc" => esc_html__("Add your custom Favicon image. 16x16px .ico or .png file required.", 'techstore-theme'),
            "id" => "site_favicon",
            "std" => "",
            "type" => "media"
        );

        $of_options[] = array(
            "name" => esc_html__("Share icons", 'techstore-theme'),
            "desc" => esc_html__("Select icons to be shown on share icons on product page, blog and [share] shortcode", 'techstore-theme'),
            "id" => "social_icons",
            "std" => array(
                "facebook",
                "twitter",
                "email",
                "pinterest",
                "googleplus"
            ),
            "type" => "multicheck",
            "options" => array(
                "facebook" => esc_html__("Facebook", 'techstore-theme'),
                "twitter" => esc_html__("Twitter", 'techstore-theme'),
                "email" => esc_html__("Email", 'techstore-theme'),
                "pinterest" => esc_html__("Pinterest", 'techstore-theme'),
                "googleplus" => esc_html__("Google Plus", 'techstore-theme'),
                "instagram" => esc_html__("Instagram", 'techstore-theme'),
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Instagram URL", 'techstore-theme'),
            "desc" => esc_html__("Input instagram link here.", 'techstore-theme'),
            "id" => "instagram_url",
            "std" => "",
            "type" => "text"
        );

        /* HEADER AND FOOTER HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Header and Footer", 'techstore-theme'),
            "target" => 'header-footer',
            "type" => "heading"
        );

        $of_options[] = array(
            "name" => esc_html__("Header Option", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Header Option", 'techstore-theme') . "</h4>",
            "type" => "info"
        );
        $of_options[] = array(
            "name" => esc_html__("Header type", 'techstore-theme'),
            "desc" => esc_html__("Select header type", 'techstore-theme'),
            "id" => "header-type",
            "std" => "1",
            "type" => "images",
            "options" => array(
                '1' => $url . 'header-1.gif',
                '2' => $url . 'header-2.gif'
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Title vertical menu (For header type 2)", 'techstore-theme'),
            "id" => "title_ver_menu",
            "desc" => esc_html__("Only use for header type 2", 'techstore-theme'),
            "std" => 'SHOP BY CATEGORY',
            "type" => "text"
        );

        $menus = wp_get_nav_menus(array('orderby' => 'name'));
        $option_menu = array('' => 'Select menu');
        if (!empty($menus)) {
            foreach ($menus as $menu_option) {
                $option_menu[$menu_option->term_id] = $menu_option->name;
            }
        }
        $of_options[] = array(
            "name" => esc_html__("Select vertical menu (For header type 2)", 'techstore-theme'),
            "desc" => esc_html__("Only use for header type 2", 'techstore-theme'),
            "id" => "vertical_menu_selected",
            "std" => "",
            "type" => "select",
            "options" => $option_menu
        );

        $block_type = get_posts(array('posts_per_page' => -1, 'post_type' => 'lt_block'));
        $header_blocks = array();
        $header_blocks['default'] = esc_html__('Select the Static Block', 'techstore-theme');
        if (!empty($block_type)) {
            foreach ($block_type as $key => $value) {
                $header_blocks[$value->ID] = $value->post_title;
            }
        }
        $of_options[] = array(
            "name" => esc_html__("Block Header (For header type 2)", 'techstore-theme'),
            "desc" => esc_html__("Only use for header type 2", 'techstore-theme'),
            "id" => "header-block",
            "type" => "select",
            'override_numberic' => true,
            "options" => $header_blocks
        );

        $of_options[] = array(
            "name" => esc_html__("Fixed navigation", 'techstore-theme'),
            "id" => "fixed_nav",
            "desc" => esc_html__("Fixed navigation at top position", 'techstore-theme'),
            "std" => 1,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Show Top Bar", 'techstore-theme'),
            "desc" => esc_html__("Show Top Bar", 'techstore-theme'),
            "id" => "topbar_show",
            "std" => 1,
            "type" => "checkbox"
        );

        /* $of_options[] = array(
            "name" 		=> esc_html__("Enable Switch Languages", 'techstore-theme'),
            "desc" 		=> esc_html__("Enable Switch Languages", 'techstore-theme'),
            "id" 		=> "switch_lang",
            "std" 		=> 1,
            "type" 		=> "checkbox"
        ); */

        $of_options[] = array(
            "name" => esc_html__("Top bar left", 'techstore-theme'),
            "desc" => 'Insert text for left top bar.<br />Default: <b style="color: #333;">' . esc_attr('<span>Contact us: <a style="color: #fedc00;" title="Click to chat" href="#">Click to chat</a> Or <span style="width: 30px; height: 30px; display: inline-block; border: 1px solid #FFF; border-radius: 99px; margin:0 5px;"><i style="color: #fedc00;display: inline-block;width: 16px;height: 16px;line-height: 18px;vertical-align: text-top;text-align: center;" class="icon pe-7s-call"></i></span> +1(668)236-6789</span>') . '</b>',
            "id" => "topbar_left",
            "std" => '<span>Contact us: <a style="color: #fedc00;" title="Click to chat" href="#">Click to chat</a> Or <span style="width: 30px; height: 30px; display: inline-block; border: 1px solid #FFF; border-radius: 99px; margin:0 5px;"><i style="font-size: 140%; color: #fedc00; vertical-align: text-top; margin-left: 5px;" class="icon pe-7s-call"></i></span> +1(668)236-6789</span>',
            "type" => "textarea"
        );

        $of_options[] = array(
            "name" => esc_html__("Footer Option", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Footer Option", 'techstore-theme') . "</h4>",
            "type" => "info"
        );

        $footers_type = get_posts(array(
            'posts_per_page' => -1,
            'post_type' => 'footer'
        ));
        $footers_option = array();
        $footers_option['default'] = esc_html__('Select the Footer type', 'techstore-theme');
        if (!empty($footers_type)) {
            foreach ($footers_type as $key => $value) {
                $footers_option[$value->ID] = $value->post_title;
            }
        }
        $of_options[] = array(
            "name" => esc_html__("Footer type", 'techstore-theme'),
            "desc" => esc_html__("Select footer type", 'techstore-theme'),
            "id" => "footer-type",
            "type" => "select",
            'override_numberic' => true,
            "options" => $footers_option
        );

        /* ================= Option Breadcrumb HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Breadcrumb", 'techstore-theme'),
            "target" => 'breadcumb',
            "type" => "heading"
        );

        $of_options[] = array(
            "name" => esc_html__("Show breadcrumb", 'techstore-theme'),
            "desc" => esc_html__("Show breadcrumb", 'techstore-theme'),
            "id" => "breadcrumb_show",
            "std" => 1,
            "type" => "checkbox",
            'class' => 'lt-breadcrumb-flag-option'
        );

        $of_options[] = array(
            "name" => esc_html__("Breadcrumb type", 'techstore-theme'),
            "desc" => esc_html__("Choose breadcrumb type.", 'techstore-theme'),
            "id" => "breadcrumb_type",
            "std" => "Default",
            "type" => "select",
            "options" => array(
                "default" => esc_html__("Without Background", 'techstore-theme'),
                "has-background" => esc_html__("With Background", 'techstore-theme')
            ),
            'class' => 'hidden-tag lt-breadcrumb-type-option'
        );

        $of_options[] = array(
            "name" => esc_html__("Breadcrumb background image.", 'techstore-theme'),
            "desc" => esc_html__("Breadcrumb background image.", 'techstore-theme'),
            "id" => "breadcrumb_bg",
            "std" => $url . "breadcrumb-bg.jpg",
            "type" => "media",
            'class' => 'hidden-tag lt-breadcrumb-bg-option'
        );

        $of_options[] = array(
            "name" => esc_html__("Breadcrumb background color.", 'techstore-theme'),
            "desc" => esc_html__("Breadcrumb background color.", 'techstore-theme'),
            "id" => "breadcrumb_bg_color",
            "std" => "",
            "type" => "color",
            'class' => 'hidden-tag lt-breadcrumb-color-option'
        );

        $of_options[] = array(
            "name" => esc_html__("Height breadcrumb", 'techstore-theme'),
            "desc" => esc_html__("Height breadcrumb. (px)", 'techstore-theme'),
            "id" => "breadcrumb_height",
            "std" => "150",
            "type" => "text",
            'class' => 'hidden-tag lt-breadcrumb-hieght-option'
        );

        $of_options[] = array(
            "name" => esc_html__("Breadcrumb Text Color", 'techstore-theme'),
            "desc" => esc_html__("Change Breadcrumb Text Color", 'techstore-theme'),
            "id" => "breadcrumb_color",
            "std" => "#fff",
            "type" => "color"
        );
        // End Option Breadcrumb

        /* ===================== TYPE HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Fonts", 'techstore-theme'),
            "target" => 'fonts',
            "type" => "heading"
        );

        $of_options[] = array(
            "name" => esc_html__("Heading fonts (H1, H2)", 'techstore-theme'),
            "desc" => esc_html__("Select heading fonts.", 'techstore-theme'),
            "id" => "type_headings",
            "std" => "Montserrat",
            "type" => "select_google_font",
            "preview" => array(
                "text" => '<strong>LeeTheme</strong><br /><span style="font-size:60%!important">UPPERCASE TEXT</span>',
                "size" => "30px"
            ),
            "options" => $google_fonts
        );

        $of_options[] = array(
            "name" => esc_html__("Text fonts (paragraphs, buttons, sub-navigations)", 'techstore-theme'),
            "desc" => esc_html__("Select heading fonts", 'techstore-theme'),
            "id" => "type_texts",
            "std" => "Lato",
            "type" => "select_google_font",
            "preview" => array(
                "text" => "Lorem ipsum dosectetur adipisicing elit, sed do.Lorem ipsum dolor sit amet, consectetur Nulla fringilla purus at leo dignissim congue. Mauris elementum accumsan leo vel tempor. Sit amet cursus nisl aliquam. Aliquam et elit eu nunc rhoncus viverra quis at felis..", //this is the text from preview box
                "size" => "14px"
            ),
            "options" => $google_fonts
        );

        $of_options[] = array(
            "name" => esc_html__("Main navigation", 'techstore-theme'),
            "desc" => esc_html__("Select navigation fonts", 'techstore-theme'),
            "id" => "type_nav",
            "std" => "Montserrat",
            "type" => "select_google_font",
            "preview" => array(
                "text" => "<span style='font-size:45%'>THIS IS THE TEXT.</span>",
                "size" => "30px"
            ),
            "options" => $google_fonts
        );

        $of_options[] = array(
            "name" => esc_html__("Alterntative font (.alt-font)", 'techstore-theme'),
            "desc" => esc_html__("Select alternative font", 'techstore-theme'),
            "id" => "type_alt",
            "std" => "Dancing Script",
            "type" => "select_google_font",
            "preview" => array(
                "text" => "This is the text.",
                "size" => "30px"
            ),
            "options" => $google_fonts
        );

        $of_options[] = array(
            "name" => esc_html__("Banner font", 'techstore-theme'),
            "desc" => esc_html__("Select banners font", 'techstore-theme'),
            "id" => "type_banner",
            "std" => "Montserrat",
            "type" => "select_google_font",
            "preview" => array(
                "text" => "This is the text.",
                "size" => "30px"
            ),
            "options" => $google_fonts
        );

        $of_options[] = array(
            "name" => esc_html__("Character Sub-sets", 'techstore-theme'),
            "desc" => esc_html__("Choose the character sets you want.", 'techstore-theme'),
            "id" => "type_subset",
            "std" => array("latin"),
            "type" => "multicheck",
            "options" => array(
                "latin" => "Latin",
                "cyrillic-ext" => esc_html__("Cyrillic Extended", 'techstore-theme'),
                "greek-ext" => esc_html__("Greek Extended", 'techstore-theme'),
                "greek" => esc_html__("Greek", 'techstore-theme'),
                "vietnamese" => esc_html__("Vietnamese", 'techstore-theme'),
                "latin-ext" => esc_html__("Latin Extended", 'techstore-theme'),
                "cyrillic" => esc_html__("Cyrillic", 'techstore-theme')
            )
        );

        /* ================== Style and Colors HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Style and Colors", 'techstore-theme'),
            "target" => 'style-color',
            "type" => "heading",
        );

        $of_options[] = array(
            "name" => esc_html__("Style and Colors Global Option", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Style and Colors Global Option", 'techstore-theme') . "</h4>",
            "type" => "info"
        );
        $of_options[] = array(
            "name" => esc_html__("Primary Color", 'techstore-theme'),
            "desc" => esc_html__("Change primary color. Used for primary buttons, link hover, background, etc.", 'techstore-theme'),
            "id" => "color_primary",
            "std" => "#ff3333",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Secondary Color", 'techstore-theme'),
            "desc" => esc_html__("Change secondary color. Used for sale bubble.", 'techstore-theme'),
            "id" => "color_secondary",
            "std" => "#f46e6d",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Success Color", 'techstore-theme'),
            "desc" => esc_html__("Change the success color. Used for global success messages.", 'techstore-theme'),
            "id" => "color_success",
            "std" => "#5cb85c",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Hot label Color", 'techstore-theme'),
            "desc" => esc_html__("Change the hot label color. Used for product hot.", 'techstore-theme'),
            "id" => "color_hot_label",
            "std" => "#6db3f4",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Sale label Color", 'techstore-theme'),
            "desc" => esc_html__("Change the sale label color. Used for product sale.", 'techstore-theme'),
            "id" => "color_sale_label",
            "std" => "#229fff",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Price Color", 'techstore-theme'),
            "desc" => esc_html__("Change the Price color. Used for product.", 'techstore-theme'),
            "id" => "color_price_label",
            "std" => "#ff3333",
            "type" => "color"
        );



        $of_options[] = array(
            "name" => esc_html__("Buttons Style and Color", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Buttons Style and Color", 'techstore-theme') . "</h4>",
            "type" => "info"
        );

        $of_options[] = array(
            "name" => esc_html__("Buttons Background Color", 'techstore-theme'),
            "desc" => esc_html__("Change background color for buttons.", 'techstore-theme'),
            "id" => "color_button",
            "std" => "#fff",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Buttons Background Color Hover", 'techstore-theme'),
            "desc" => esc_html__("Change background color hover for buttons. Default is primary color", 'techstore-theme'),
            "id" => "color_hover",
            "std" => "#ff3333",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Buttons Border Color", 'techstore-theme'),
            "desc" => esc_html__("Change border color for buttons.", 'techstore-theme'),
            "id" => "button_border_color",
            "std" => "#888",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Buttons Border Color Hover", 'techstore-theme'),
            "desc" => esc_html__("Change border color hover for buttons.", 'techstore-theme'),
            "id" => "button_border_color_hover",
            "std" => "#ff3333",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Buttons Text Color", 'techstore-theme'),
            "desc" => esc_html__("Change text color for buttons. Default is primary color", 'techstore-theme'),
            "id" => "button_text_color",
            "std" => "#333",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Buttons Text Color Hover", 'techstore-theme'),
            "desc" => esc_html__("Change text color hover for buttons.", 'techstore-theme'),
            "id" => "button_text_color_hover",
            "std" => "#fff",
            "type" => "color"
        );
        $of_options[] = array(
            "name" => esc_html__("Buttons radius", 'techstore-theme'),
            "desc" => esc_html__("Change Buttons Radius. (px)", 'techstore-theme'),
            "id" => "button_radius",
            "std" => "5",
            "step" => "5",
            "max" => '100',
            "type" => "sliderui"
        );

        $of_options[] = array(
            "name" => esc_html__("Buttons border", 'techstore-theme'),
            "desc" => esc_html__("Change Buttons Border. (px)", 'techstore-theme'),
            "id" => "button_border",
            "std" => "1",
            "step" => "1",
            "max" => '5',
            "type" => "sliderui"
        );

        $of_options[] = array(
            "name" => esc_html__("Inputs radius", 'techstore-theme'),
            "desc" => esc_html__("Change Radius Inputs. (px)", 'techstore-theme'),
            "id" => "input_radius",
            "std" => "5",
            "step" => "5",
            "max" => "100",
            "type" => "sliderui"
        );

        /* ============== Product Page HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Product Page", 'techstore-theme'),
            "target" => 'product-page',
            "type" => "heading",
        );

        $of_options[] = array(
            "name" => esc_html__("Product detail", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Product detail", 'techstore-theme') . "</h4>",
            "type" => "info"
        );

        $of_options[] = array(
            "name" => esc_html__("Product Sidebar", 'techstore-theme'),
            "id" => "product_sidebar",
            "std" => "left_sidebar",
            "type" => "select",
            "options" => array(
                "left_sidebar" => esc_html__("Left Sidebar", 'techstore-theme'),
                "right_sidebar" => esc_html__("Right Sidebar", 'techstore-theme'),
                "no_sidebar" => esc_html__("No sidebar", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Additional Global tab/section title", 'techstore-theme'),
            "id" => "tab_title",
            "std" => "Add Tags",
            "type" => "text"
        );

        $of_options[] = array(
            "name" => esc_html__("Additional Global tab/section content", 'techstore-theme'),
            "id" => "tab_content",
            "std" => "Custom Tab Content here. <br />Tail sed sausage magna quis commodo swine. Aliquip strip steak esse ex in ham hock fugiat in. Labore velit pork belly eiusmod ut shank doner capicola consectetur landjaeger fugiat excepteur short loin. Pork belly laboris mollit in leberkas qui. Pariatur swine aliqua pork chop venison veniam. Venison sed cow short loin bresaola shoulder cupidatat capicola drumstick dolore magna shankle.",
            "type" => "textarea"
        );

        $of_options[] = array(
            "name" => esc_html__("Enable Technical Specifications", 'techstore-theme'),
            "id" => "enable_specifications",
            "desc" => esc_html__("Enable Technical Specifications", 'techstore-theme'),
            "std" => "1",
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Show the Specifications in the Desciption tab", 'techstore-theme'),
            "id" => "merge_specifi_to_desc",
            "desc" => esc_html__("Show the Specifications in the Desciption tab", 'techstore-theme'),
            "std" => "1",
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Category page", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Category page (Shop page)", 'techstore-theme') . "</h4>",
            "type" => "info"
        );
        $of_options[] = array(
            "name" => esc_html__("Category sidebar", 'techstore-theme'),
            "desc" => esc_html__("Select if you want a sidebar on product categories.", 'techstore-theme'),
            "id" => "category_sidebar",
            "std" => "left-sidebar",
            "type" => "select",
            "options" => array(
                "left-sidebar" => esc_html__("Left Sidebar", 'techstore-theme'),
                "right-sidebar" => esc_html__("Right Sidebar", 'techstore-theme'),
                "no-sidebar" => esc_html__("No sidebar", 'techstore-theme'),
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Products per row", 'techstore-theme'),
            "desc" => esc_html__("Change products number display per row for the Shop page", 'techstore-theme'),
            "id" => "products_per_row",
            "std" => "4",
            "type" => "select",
            "options" => array("2" => "2", "3" => "3", "4" => "4", "5" => "5")
        );

        $of_options[] = array(
            "name" => esc_html__("Products per page", 'techstore-theme'),
            "id" => "products_pr_page",
            "desc" => esc_html__("Change products per page.", 'techstore-theme'),
            "std" => "12",
            "type" => "text"
        );

        $of_options[] = array(
            "name" => esc_html__("Pagination page style", 'techstore-theme'),
            "id" => "pagination_style",
            "desc" => esc_html__("Select style for pagination", 'techstore-theme'),
            "std" => 'style-1',
            "type" => "select",
            "options" => array(
                "style-1" => esc_html__("Style 1", 'techstore-theme'),
                "style-2" => esc_html__("Style 2", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Enable cutting product name", 'techstore-theme'),
            "id" => "cutting_product_name",
            "desc" => esc_html__("Cut title product if it is so long.", 'techstore-theme'),
            "std" => "1",
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Title product after cutted", 'techstore-theme'),
            "id" => "cutting_product_number",
            "desc" => esc_html__("Title product after cutted", 'techstore-theme'),
            "std" => "15",
            "type" => "text"
        );

        /* $of_options[] = array( 
          "name"      => esc_html__("Widget variations sidebar", 'techstore-theme'),
          "id"        => "vari_hide_empty",
          "desc"      => esc_html__("Show variation value if exist a minium product. Ex: Show RED color in Filter by Color if exist minimum a product that have RED attribute.", 'techstore-theme'),
          "std"       => "0",
          "type"      => "checkbox"
          ); */

        // recomment_product_limit
        $of_options[] = array(
            "name" => esc_html__("Top content Products page", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Top content Products page", 'techstore-theme') . "</h4>",
            "type" => "info"
        );

        $of_options[] = array(
            "name" => esc_html__("Enable Recommend products", 'techstore-theme'),
            "id" => "enable_recommend_product",
            "desc" => esc_html__("Enable Recommend products", 'techstore-theme'),
            "std" => "1",
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Recommend products limit", 'techstore-theme'),
            "id" => "recommend_product_limit",
            "desc" => esc_html__("Input number limit recomment products in top content.", 'techstore-theme'),
            "std" => "9",
            "type" => "text"
        );
        
        $of_options[] = array(
            "name" => esc_html__("Recommend products position", 'techstore-theme'),
            "id" => "recommend_product_position",
            "std" => "top",
            "type" => "select",
            "options" => array(
                "top" => esc_html__("Top", 'techstore-theme'),
                "bot" => esc_html__("Bottom", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Enable Category top content", 'techstore-theme'),
            "id" => "enable_cat_header",
            "desc" => esc_html__("Enable Category top content", 'techstore-theme'),
            "std" => "1",
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Category top content", 'techstore-theme'),
            "id" => "cat_header",
            "desc" => esc_html__("Input anything shortcode or text here. Recommend to use the Static Blocks to create banner or anything that you want to show the top content in the Shop page", 'techstore-theme'),
            "std" => "",
            "type" => "textarea"
        );

        $of_options[] = array(
            "name" => esc_html__("Products Global option", 'techstore-theme'),
            "std" => "<h4>" . esc_html__("Products Global option", 'techstore-theme') . "</h4>",
            "type" => "info"
        );
        
        $of_options[] = array(
            "name" => esc_html__("Hover product effect", 'techstore-theme'),
            "desc" => esc_html__("Select if you want change hover product image.", 'techstore-theme'),
            "id" => "animated_products",
            "std" => "slider",
            "type" => "select",
            "options" => array(
                "slider" => esc_html__("Slider", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Enable Hover overlay", 'techstore-theme'),
            "id" => "product-hover-overlay",
            "desc" => esc_html__("Enable product hover black overlay on product grid page", 'techstore-theme'),
            "std" => 1,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Delay overlay items", 'techstore-theme'),
            "desc" => esc_html__("(ms) Delay overlay items.", 'techstore-theme'),
            "id" => "delay_overlay",
            "std" => "100",
            "type" => "text"
        );

        $of_options[] = array(
            "name" => esc_html__("Enable Hover product Zoom", 'techstore-theme'),
            "id" => "product-zoom",
            "desc" => esc_html__("Enable product hover zoom on product detail page", 'techstore-theme'),
            "std" => 1,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Disable catalog mode", 'techstore-theme'),
            "id" => "disable-cart",
            "desc" => esc_html__("Disable add to cart button on product category page and single product page.", 'techstore-theme'),
            "std" => "0",
            "type" => "checkbox"
        );

        // Options live search products
        $of_options[] = array(
            "name" => esc_html__("Enable live search products", 'techstore-theme'),
            "desc" => esc_html__("Enable live search ajax", 'techstore-theme'),
            "id" => "enable_live_search",
            "std" => 1,
            "type" => "checkbox"
        );
        // End Options live search products
        
        // Options live search products
        /* $of_options[] = array(
            "name" => esc_html__("Enable search products by Category", 'techstore-theme'),
            "desc" => esc_html__("Enable search products by Category", 'techstore-theme'),
            "id" => "search_by_cat",
            "std" => 0,
            "type" => "checkbox"
        ); */
        // End Options live search products


        /* ============== Blog HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Blog", 'techstore-theme'),
            "target" => 'blog',
            "type" => "heading"
        );

        $of_options[] = array(
            "name" => esc_html__("Blog layout", 'techstore-theme'),
            "desc" => esc_html__("Change blog layout", 'techstore-theme'),
            "id" => "blog_layout",
            "std" => "left-sidebar",
            "type" => "select",
            "options" => array(
                "left-sidebar" => esc_html__("Left sidebar", 'techstore-theme'),
                "right-sidebar" => esc_html__("Right sidebar", 'techstore-theme'),
                "no-sidebar" => esc_html__("No sidebar (Centered)", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Blog style", 'techstore-theme'),
            "desc" => esc_html__("Change blog style", 'techstore-theme'),
            "id" => "blog_type",
            "std" => "blog-standard",
            "type" => "select",
            "options" => array(
                "blog-standard" => esc_html__("Standard", 'techstore-theme'),
                "blog-list" => esc_html__("List style", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("Parallax effect", 'techstore-theme'),
            "id" => "blog_parallax",
            "desc" => "Enable parallax effect on featured images",
            "std" => 1,
            "type" => "checkbox"
        );

        /* =============== Promo Popup HEADING ============== */
        // $of_options[] = array(
        //     "name" 		=> esc_html__("Promo Popup", 'techstore-theme'),
        //     "target" 		=> 'promo-popup',
        //     "type" 		=> "heading"
        // );
        // $of_options[] = array(
        //     "name" 		=> esc_html__("Enable promo popup", 'techstore-theme'),
        //     "desc" 		=> esc_html__("Enable promo popup", 'techstore-theme'),
        //     "id" 		=> "promo_popup",
        //     "std" 		=> 0,
        //     "type" 		=> "checkbox"
        // );
        // $of_options[] = array( 
        //     "name"  => esc_html__("Popup width", 'techstore-theme'),
        //     "id"    => "pp_width",
        //     "std"   => "380",
        //     "type"  => "text"
        // );
        // $of_options[] = array( 
        //     "name"  => esc_html__("Popup height", 'techstore-theme'),
        //     "id"    => "pp_height",
        //     "std"   => "410",
        //     "type"  => "text"
        // );
        // $of_options[] = array(
        //     "name" 		=> esc_html__("Popup content", 'techstore-theme'),
        //     "id" 		=> "pp_content",
        //     "std" 		=> '<h2>NEWSLETTER</h2><span>Subscribe now to get <span class="primary-color">20%</span> off on any products!</span>[ninja_forms id=9]',
        //     "type" 		=> "textarea"
        // );
        // $of_options[] = array(
        //     "name"      => esc_html__("Popup Background Color", 'techstore-theme'),
        //     "desc"      => esc_html__("Insert popup background color.", 'techstore-theme'),
        //     "id"        => "pp_background_color",
        //     "std"       => "#fff",
        //     "type"      => "color"
        // );
        // $of_options[] = array(
        //     "name" 		=> esc_html__("Popup Background", 'techstore-theme'),
        //     "desc" 		=> esc_html__("Insert popup background.", 'techstore-theme'),
        //     "id" 		=> "pp_background_image",
        //     "std" 		=> "",
        //     "type" 		=> "media"
        // );

        /* ============= PORTFOLIO HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Portfolio", 'techstore-theme'),
            "target" => 'portfolio',
            "type" => "heading"
        );
        $of_options[] = array(
            "name" => esc_html__("Recent Projects", 'techstore-theme'),
            "desc" => esc_html__("Recent Projects", 'techstore-theme'),
            "id" => "recent_projects",
            "std" => 1,
            "type" => "checkbox"
        );
        $of_options[] = array(
            "name" => esc_html__("Portfolio Comments", 'techstore-theme'),
            "desc" => esc_html__("Portfolio Comments", 'techstore-theme'),
            "id" => "portfolio_comments",
            "std" => 1,
            "type" => "checkbox"
        );
        $of_options[] = array(
            "name" => esc_html__("Portfolio Count", 'techstore-theme'),
            "desc" => esc_html__("Portfolio Count", 'techstore-theme'),
            "id" => "portfolio_count",
            "std" => 8,
            "type" => "text"
        );
        $of_options[] = array(
            "name" => esc_html__("Project Category", 'techstore-theme'),
            "desc" => esc_html__("Display project category", 'techstore-theme'),
            "id" => "project_byline",
            "std" => 1,
            "type" => "checkbox"
        );
        $of_options[] = array(
            "name" => esc_html__("Project Name", 'techstore-theme'),
            "desc" => esc_html__("Project Name", 'techstore-theme'),
            "id" => "project_name",
            "std" => 1,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Portfolio Columns", 'techstore-theme'),
            "desc" => esc_html__("Portfolio Columns", 'techstore-theme'),
            "id" => "portfolio_columns",
            "std" => "3-cols",
            "type" => "select",
            "options" => array(
                "4-cols" => esc_html__("4 columns", 'techstore-theme'),
                "3-cols" => esc_html__("3 columns", 'techstore-theme'),
                "2-cols" => esc_html__("2 columns", 'techstore-theme')
            )
        );

        $of_options[] = array(
            "name" => esc_html__("portfolio Lightbox", 'techstore-theme'),
            "desc" => esc_html__("portfolio Lightbox", 'techstore-theme'),
            "id" => "portfolio_lightbox",
            "std" => 1,
            "type" => "checkbox"
        );

        /* ================ Promotion news HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Promotion news", 'techstore-theme'),
            "target" => "promotion-news",
            "type" => "heading"
        );

        $of_options[] = array(
            "name" => esc_html__("Enable Top bar Promotion news", 'techstore-theme'),
            "desc" => esc_html__("Checked is Enable", 'techstore-theme'),
            "id" => "enable_post_top",
            "std" => 0,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Type display selected", 'techstore-theme'),
            "desc" => esc_html__('Type display "My content custom" or "List posts"', 'techstore-theme'),
            "id" => "type_display",
            "std" => 'custom',
            "type" => "select",
            "options" => array(
                'custom' => esc_html__('My content custom', 'techstore-theme'),
                'list-posts' => esc_html__('List posts', 'techstore-theme')
            ),
            'class' => 'type_promotion'
        );

        $of_options[] = array(
            "name" => esc_html__("My content custom", 'techstore-theme'),
            "desc" => esc_html__("Content display", 'techstore-theme'),
            "id" => "content_custom",
            "std" => '<a href="javascript:void(0);" title="Title of my content custom"><b>SINGLES DAY</b> Extra 25% Off Sitewide - User Code: <b>ALLFORYOU</b> View Exclusions</a>',
            'type' => 'textarea',
            'class' => 'hidden-tag lt-custom_content'
        );

        $of_options[] = array(
            "name" => esc_html__("Category post", 'techstore-theme'),
            "desc" => esc_html__("Post in category selected", 'techstore-theme'),
            "id" => "category_post",
            "std" => '',
            "type" => "select",
            "options" => techstore_get_cats_array(),
            'class' => 'hidden-tag lt-list_post'
        );

        $of_options[] = array(
            "name" => esc_html__("Limit posts", 'techstore-theme'),
            "desc" => esc_html__("Number posts display", 'techstore-theme'),
            "id" => "number_post",
            "std" => 4,
            "type" => "text",
            'class' => 'hidden-tag lt-list_post'
        );

        $of_options[] = array(
            "name" => esc_html__("Slide display", 'techstore-theme'),
            "desc" => esc_html__("Number posts display in slide", 'techstore-theme'),
            "id" => "number_post_slide",
            "std" => 1,
            "type" => "text",
            'class' => 'hidden-tag lt-list_post'
        );

        $of_options[] = array(
            "name" => esc_html__("Display full width", 'techstore-theme'),
            "desc" => esc_html__("Display full width", 'techstore-theme'),
            "id" => "enable_fullwidth",
            "std" => 1,
            "type" => "checkbox"
        );

        $of_options[] = array(
            "name" => esc_html__("Text promotion color", 'techstore-theme'),
            "desc" => esc_html__("Text promotion color", 'techstore-theme'),
            "id" => "t_promotion_color",
            "std" => "#333",
            "type" => "color"
        );

        $of_options[] = array(
            "name" => esc_html__("Background", 'techstore-theme'),
            "desc" => esc_html__("Background", 'techstore-theme'),
            "id" => "background_area",
            "std" => $url . 'promo_bg.jpg',
            "type" => "media"
        );

        /* =============== Backup Options HEADING ============== */
        $of_options[] = array(
            "name" => esc_html__("Backup and Import", 'techstore-theme'),
            "target" => "backup-import",
            "type" => "heading",
        );

        $of_options[] = array(
            "name" => esc_html__("Backup and Restore Options", 'techstore-theme'),
            "id" => "of_backup",
            "std" => "",
            "type" => "backup",
            "desc" => esc_html__('You can use the two buttons below to backup your current options, and then restore it back at a later time. This is useful if you want to experiment on the options but would like to keep the old settings in case you need it back.', 'techstore-theme'),
        );

        $of_options[] = array(
            "name" => esc_html__("Transfer Theme Options Data", 'techstore-theme'),
            "id" => "of_transfer",
            "std" => "",
            "type" => "transfer",
            "desc" => esc_html__('You can tranfer the saved options data between different installs by copying the text inside the text box. To import data from another install, replace the data in the text box with the one from another install and click "Import Options".', 'techstore-theme'),
        );
    }

//End function: of_options()
}//End chack if function exists: of_options()

function techstore_get_cats_array() {
    $categories = get_categories(array(
        'taxonomy' => 'category',
        'orderby' => 'name'
    ));
    $list = array(
        '' => esc_html__('Select category', 'techstore-theme')
    );

    if (!empty($categories)) {
        foreach ($categories as $v) {
            $list[$v->term_id] = $v->name;
        }
    }

    return $list;
}
