<div class="demo-image-content hide-for-small hide-for-medium"><div class="demo-image-content-detail" data-stt="0"></div></div>
<div class="config hide-for-small hide-for-medium">
    <div class="config-options">
        <div class="ss-content clear-fix">
            <a class="to_buy_now text-center" href="https://themeforest.net/item/techstore-electronics-ajax-woocommerce-theme/18596697?ref=leebrosus">BUY NOW</a>
        </div>
        <!-- <span class="ss-title"><strong>Layout</strong></span>
        <div class="ss-content clearfix">
            <a class="wide-button ss-button active" href="#">Wide</a>
            <a class="boxed-button ss-button" href="#">Boxed</a>
        </div> -->
        
        <div class="ss-demo clearfix">

            <div class="ss-demo-1 lt-items-demo large-12">
                <div class="ss-demo-wrapper">
                    <h3>Homepage 1</h3>
                    <div class="ss-demo-screenshot" data-img="<?php echo get_template_directory_uri().'/css/demo/images/demo_pages/home1.jpg';?>">
                        <a class="hover-overlay link-demo-home-1" data-stt="7" href="/techstore">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="ss-demo-2 lt-items-demo large-12 lt-dm-right">
                <div class="ss-demo-wrapper">
                    <h3>Homepage 2</h3>
                    <div class="ss-demo-screenshot" data-img="<?php echo get_template_directory_uri().'/css/demo/images/demo_pages/home2.jpg';?>">
                        <a class="hover-overlay link-demo-home-2" data-stt="1" href="/techstore/homepage-2">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>
            
            <div class="ss-demo-3 lt-items-demo large-12">
                <div class="ss-demo-wrapper">
                    <h3>Homepage 3</h3>
                    <div class="ss-demo-screenshot" data-img="<?php echo get_template_directory_uri().'/css/demo/images/demo_pages/home3.jpg';?>">
                        <a class="hover-overlay link-demo-home-3" data-stt="5" href="/techstore/homepage-3">
                            <div class="link-opacity"></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>

        <span class="ss-title"><strong>Note:</strong></span>
        <div class="ss-desc">
            This is just a demo. Every color, font, layout etc can completely be customized in a Theme Option
        </div>
        
    </div>
    <a class="show-theme-options" href="javascript:void(0);">
        <center>
            <i class="fa fa-plus"></i><br />
            DEMOS
        </center>
    </a>
		
</div>
<!-- For demo purposes. Can be removed on production : End -->