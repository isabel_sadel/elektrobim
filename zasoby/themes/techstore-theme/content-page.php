<?php
/**
 * @package leetheme
 */
//global $lt_opt;
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_thumbnail() ) { ?>
		<div class="entry-image">
			<?php if(isset($lt_opt['blog_parallax'])) { ?><div class="parallax_img" style="overflow:hidden"><div class="parallax_img_inner" data-velocity="0.15"><?php } ?>
					<?php the_post_thumbnail('lt-blog-detail-thumb'); ?>
					<div class="image-overlay"></div>
					<?php if(isset($lt_opt['blog_parallax'])) { ?></div></div><?php } ?>
		</div>
	<?php } ?>
	<header class="entry-header text-center">
		<h1 class="entry-title"><?php the_title(); ?></h1>
	</header>

	<div class="entry-content">
		<?php the_content(); ?>
		<?php
		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'techstore-theme' ),
			'after'  => '</div>',
		) );
		?>
	</div>

</article>
